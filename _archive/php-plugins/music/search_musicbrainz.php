<?php


/********** HEADERS **********/
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){
	
}else{
	header("content-type: application/json; charset=utf8");
}
//DEFINE OUTPUT ARRAY
$myArray = Array();


include('XML2Array.php');






/********** PROCESS **********/
if(isset($_GET['song'])){
	//URL
	$url = "http://www.musicbrainz.org/ws/2/release?query=".$_GET['song'];
	//Curl query
	$ch = curl_init($url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch,CURLOPT_FAILONERROR,true);
	//curl_setopt($ch,CURLOPT_ERRORBUFFER,true);
	if(curl_exec($ch) === false) {
		$myArray['success'] = 0;
		$myArray['message'] = 'Curl error';
	} else {
		$response = curl_exec($ch);
		$myArray['success'] = 1;
		$myArray['response'] = XML2Array::createArray($response);
	}
	curl_close ($ch);
}else{
	$myArray['success'] = 0;
	$myArray['message'] = 'No song parameter supplied';
}









/********** OUTPUT **********/
/*echo'<pre>';
print_r($myArray['response']);
echo'</pre>';*/
echo json_encode($myArray);




?>