<?php

/*
	(c) KERVE CREATIVE
	
	AREA: ADMIN
	AUTHOR: ALEX BIMPSON
	TYPE: KERVE XML LOCALE IMPORT
	FOR: 
	VERSION: v1.0
	UPDATED: 2012-10-15
*/


/******************** PARSE XML ********************/
$locale_xml = new DOMDocument;
if ( !$locale_xml->load( APP_ROOT . '/locale/' . LOCALE . '.xml' ) ) {
	exit( 'Error parsing XML' );
}
$locale_xpath = new DOMXpath( $locale_xml );


/******************** SNIPPET FUNCTIONS ********************/
function locale_snippet ( $key ) {
	global $locale_xpath;
	$snippet = $locale_xpath->query( '//snippet[@key="' . $key . '"]' );
	if ( $snippet->length > 0 ) {
		return preg_replace( '/(^\s+)|(\s+$)/us', '', $snippet->item(0)->nodeValue );
	}
	else {
		return locale_term( 'no_snippet_found' );
	}
}

function locale_term ( $key ) {
	global $locale_xpath;
	$snippet = $locale_xpath->query( '//term[@key="' . $key . '"]' );
	if ( $snippet->length > 0 ) {
		return $snippet->item(0)->nodeValue;
	}
	else {
		return locale_term( 'no_term_found' );
	}
}

function locale_setting ( $key ) {
	global $locale_xpath;
	$snippet = $locale_xpath->query( '//setting[@key="' . $key . '"]' );
	if ( $snippet->length > 0 ) {
		return $snippet->item(0)->nodeValue;
	}
	else {
		return locale_term( 'no_setting_found' );
	}
}

?>