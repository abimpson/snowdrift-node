<?php

/*
	(C) KERVE
	
	AUTHOR:		ALEX BIMPSON
	NAME:		PHP FUNCTIONS
	VERSION:	1.0
	UPDATED:	2013-10-29
*/


/******************** DOMAIN PROTOCOL ********************/
define( 'DOMAIN_PROTOCOL', (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://");


?>