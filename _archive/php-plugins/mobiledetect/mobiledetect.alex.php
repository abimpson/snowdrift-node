<?php




/****************************************************************************************/
/**************************************** CONFIG ****************************************/
/****************************************************************************************/


/********** INITIALISE **********/

//user agent
$DEVICE = array();
$DEVICE['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
//mobile - http://mobiledetect.net
if (!class_exists('Mobile_Detect')) {
	include('Mobile_Detect.php');
}
$DETECT_MOBILE = new Mobile_Detect();
//desktop - https://github.com/garetjax/phpbrowscap
date_default_timezone_set("Europe/London");


/********** VARS **********/
//type
$DEVICE['type'] = 'Unknown';
//touchscreen
$DEVICE['touchscreen'] = 'Unknown';
//os
$DEVICE['os'] = 'Unknown';
$DEVICE['os_version'] = 'Unknown';
//browser
$DEVICE['browser'] = 'Unknown';
$DEVICE['browser_version'] = 'Unknown';
//device
$DEVICE['mobile_device'] = 'Unknown';


/********** DEVICE DETECT **********/
if($DETECT_MOBILE->isTablet()){
    $DEVICE['type'] = 'Tablet';
}else if($DETECT_MOBILE->isMobile()){
    $DEVICE['type'] = 'Mobile';
}else{
	$DEVICE['type'] = 'Desktop';
}


/********** TOUCHSCREEN DETECT **********/
if($DEVICE['type'] == 'Mobile' || $DEVICE['type'] == 'Tablet'){
	$DEVICE['touchscreen'] = true;
	/*echo '<script type="text/javascript"> var TOUCHSCREEN = true; </script>';*/
}else{
	$DEVICE['touchscreen'] = false;
	/*echo '<script type="text/javascript"> var TOUCHSCREEN = false; </script>';*/
}





/****************************************************************************************/
/**************************************** MOBILE ****************************************/
/****************************************************************************************/


if($DEVICE['type'] == 'Mobile' || $DEVICE['type'] == 'Tablet'){
	
	
	/********** MOBILE BROWSER DETECT **********/
	if( $DETECT_MOBILE->isBlazer() ){
		$DEVICE['browser'] = 'Blazer';
	}else if( $DETECT_MOBILE->isBolt() ){
		$DEVICE['browser'] = 'Bolt';
	}else if( $DETECT_MOBILE->isChrome() ){
		$DEVICE['browser'] = 'Chrome';
	}else if( $DETECT_MOBILE->isDiigoBrowser() ){
		$DEVICE['browser'] = 'Diigo';
	}else if( $DETECT_MOBILE->isDolfin() ){
		$DEVICE['browser'] = 'Dolfin';
	}else if( $DETECT_MOBILE->isFirefox() ){
		$DEVICE['browser'] = 'Firefox';
	}else if( $DETECT_MOBILE->isIE() ){
		$DEVICE['browser'] = 'IE';
	}else if( $DETECT_MOBILE->isMercury() ){
		$DEVICE['browser'] = 'Mercury';
	}else if( $DETECT_MOBILE->isOpera() ){
		$DEVICE['browser'] = 'Opera';
	}else if( $DETECT_MOBILE->isPuffin() ){
		$DEVICE['browser'] = 'Puffin';
	}else if( $DETECT_MOBILE->isSafari() ){
		$DEVICE['browser'] = 'Safari';
	}else if( $DETECT_MOBILE->isSkyfire() ){
		$DEVICE['browser'] = 'Skyfire';
	}else if( $DETECT_MOBILE->isTeaShark() ){
		$DEVICE['browser'] = 'Teashark';
	}else if( $DETECT_MOBILE->isTizen() ){
		$DEVICE['browser'] = 'Tizen';
	}else if( $DETECT_MOBILE->isUCBrowser() ){
		$DEVICE['browser'] = 'UCBrowser';
	}
	
	
	/********** MOBILE OS DETECT **********/
	if( $DETECT_MOBILE->isAndroidOS() ){
		$DEVICE['os'] = 'Android';
	}else if( $DETECT_MOBILE->isbadaOS() ){
		$DEVICE['os'] = 'BadaOS';
	}else if( $DETECT_MOBILE->isBlackBerryOS() ){
		$DEVICE['os'] = 'Blackberry';
		if($DETECT_MOBILE->version(BlackBerry)){
			$DEVICE['os_version'] = $DETECT_MOBILE->version(BlackBerry);
		}
	}else if( $DETECT_MOBILE->isBREWOS() ){
		$DEVICE['os'] = 'BREWOS';
	}else if( $DETECT_MOBILE->isiOS() ){
		$DEVICE['os'] = 'iOS';
		if($DETECT_MOBILE->version('iOS')){
			$DEVICE['os_version'] = $DETECT_MOBILE->version('iOS');
		}
	}else if( $DETECT_MOBILE->isJavaOS() ){
		$DEVICE['os'] = 'JavaOS';
	}else if( $DETECT_MOBILE->isMaemoOS() ){
		$DEVICE['os'] = 'MaemoOS';
	}else if( $DETECT_MOBILE->isMeeGoOS() ){
		$DEVICE['os'] = 'MeeGoOS';
	}else if( $DETECT_MOBILE->isPalmOS() ){
		$DEVICE['os'] = 'PalmOS';
	}else if( $DETECT_MOBILE->isSymbianOS() ){
		$DEVICE['os'] = 'Symbian';
	}else if( $DETECT_MOBILE->iswebOS() ){
		$DEVICE['os'] = 'WebOS';
	}else if( $DETECT_MOBILE->isWindowsMobileOS() ){
		$DEVICE['os'] = 'WindowsMobile';
	}else if( $DETECT_MOBILE->isWindowsPhoneOS() ){
		$DEVICE['os'] = 'WindowsPhone';
	}
	
	
	/********** MOBILE DEVICE DETECT **********/
	if($DETECT_MOBILE->isiPhone()){
		$DEVICE['mobile_device'] = 'iPhone';
	}else if($DETECT_MOBILE->isiPad()){
		$DEVICE['mobile_device'] = 'iPad';
	}
	
	
}





/****************************************************************************************/
/**************************************** OUTPUT ****************************************/
/****************************************************************************************/


/********* OUTPUT JS OBJECT **********/
$DEVICE_JS = '<script type="text/javascript">
		var DEVICE = {
			user_agent: "'.$DEVICE['user_agent'].'",
			type: "'.$DEVICE['type'].'",
			touchscreen: '.($DEVICE['touchscreen']==true?'true':'false').',
			mobile_device: "'.$DEVICE['mobile_device'].'",
			os: "'.$DEVICE['os'].'",
			os_version: "'.$DEVICE['os_version'].'",
			browser: "'.$DEVICE['browser'].'",
			browser_version: "'.$DEVICE['browser_version'].'"
		};
	</script>';





?>