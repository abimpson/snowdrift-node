<?php




/****************************************************************************************/
/**************************************** CONFIG ****************************************/
/****************************************************************************************/


/********** INITIALISE **********/

//user agent
$DEVICE = array();
$DEVICE['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
//mobile - http://mobiledetect.net
if (!class_exists('Mobile_Detect')) {
	include('Mobile_Detect.php');
}
$DETECT_MOBILE = new Mobile_Detect();
//desktop - https://github.com/garetjax/phpbrowscap
date_default_timezone_set("gmt"); 
include('Browscap.php');
use phpbrowscap\Browscap;
//$browser = get_browser(null, true);


/********** VARS **********/
//type
$DEVICE['type'] = 'Unknown';
//touchscreen
$DEVICE['touchscreen'] = 'Unknown';
//os
$DEVICE['os'] = 'Unknown';
$DEVICE['os_version'] = 'Unknown';
//browser
$DEVICE['browser'] = 'Unknown';
$DEVICE['browser_version'] = 'Unknown';
//device
$DEVICE['mobile_device'] = 'Unknown';
//webkit
$DEVICE['webkit'] = 'Unknown';
$DEVICE['webkit_version'] = 'Unknown';


/********** DEVICE DETECT **********/
if($DETECT_MOBILE->isTablet()){
    $DEVICE['type'] = 'Tablet';
}else if($DETECT_MOBILE->isMobile()){
    $DEVICE['type'] = 'Mobile';
}else{
	$DEVICE['type'] = 'Desktop';
}


/********** TOUCHSCREEN DETECT **********/
if($DEVICE['type'] == 'Mobile' || $DEVICE['type'] == 'Tablet'){
	$DEVICE['touchscreen'] = true;
	/*echo '<script type="text/javascript"> var TOUCHSCREEN = true; </script>';*/
}else{
	$DEVICE['touchscreen'] = false;
	/*echo '<script type="text/javascript"> var TOUCHSCREEN = false; </script>';*/
}





/****************************************************************************************/
/**************************************** MOBILE ****************************************/
/****************************************************************************************/


if($DEVICE['type'] == 'Mobile' || $DEVICE['type'] == 'Tablet'){
	
	
	/********** MOBILE BROWSER DETECT **********/
	if( $DETECT_MOBILE->isBlazer() ){
		$DEVICE['browser'] = 'Blazer';
	}else if( $DETECT_MOBILE->isBolt() ){
		$DEVICE['browser'] = 'Bolt';
	}else if( $DETECT_MOBILE->isChrome() ){
		$DEVICE['browser'] = 'Chrome';
	}else if( $DETECT_MOBILE->isDiigoBrowser() ){
		$DEVICE['browser'] = 'Diigo';
	}else if( $DETECT_MOBILE->isDolfin() ){
		$DEVICE['browser'] = 'Dolfin';
	}else if( $DETECT_MOBILE->isFirefox() ){
		$DEVICE['browser'] = 'Firefox';
	}else if( $DETECT_MOBILE->isIE() ){
		$DEVICE['browser'] = 'IE';
	}else if( $DETECT_MOBILE->isMercury() ){
		$DEVICE['browser'] = 'Mercury';
	}else if( $DETECT_MOBILE->isOpera() ){
		$DEVICE['browser'] = 'Opera';
	}else if( $DETECT_MOBILE->isPuffin() ){
		$DEVICE['browser'] = 'Puffin';
	}else if( $DETECT_MOBILE->isSafari() ){
		$DEVICE['browser'] = 'Safari';
	}else if( $DETECT_MOBILE->isSkyfire() ){
		$DEVICE['browser'] = 'Skyfire';
	}else if( $DETECT_MOBILE->isTeaShark() ){
		$DEVICE['browser'] = 'Teashark';
	}else if( $DETECT_MOBILE->isTizen() ){
		$DEVICE['browser'] = 'Tizen';
	}else if( $DETECT_MOBILE->isUCBrowser() ){
		$DEVICE['browser'] = 'UCBrowser';
	}
	
	
	/********** MOBILE OS DETECT **********/
	if( $DETECT_MOBILE->isAndroidOS() ){
		$DEVICE['os'] = 'Android';
	}else if( $DETECT_MOBILE->isbadaOS() ){
		$DEVICE['os'] = 'BadaOS';
	}else if( $DETECT_MOBILE->isBlackBerryOS() ){
		$DEVICE['os'] = 'Blackberry';
		if($DETECT_MOBILE->version(BlackBerry)){
			$DEVICE['os_version'] = $DETECT_MOBILE->version(BlackBerry);
		}
	}else if( $DETECT_MOBILE->isBREWOS() ){
		$DEVICE['os'] = 'BREWOS';
	}else if( $DETECT_MOBILE->isiOS() ){
		$DEVICE['os'] = 'iOS';
		if($DETECT_MOBILE->version('iOS')){
			$DEVICE['os_version'] = $DETECT_MOBILE->version('iOS');
		}
	}else if( $DETECT_MOBILE->isJavaOS() ){
		$DEVICE['os'] = 'JavaOS';
	}else if( $DETECT_MOBILE->isMaemoOS() ){
		$DEVICE['os'] = 'MaemoOS';
	}else if( $DETECT_MOBILE->isMeeGoOS() ){
		$DEVICE['os'] = 'MeeGoOS';
	}else if( $DETECT_MOBILE->isPalmOS() ){
		$DEVICE['os'] = 'PalmOS';
	}else if( $DETECT_MOBILE->isSymbianOS() ){
		$DEVICE['os'] = 'Symbian';
	}else if( $DETECT_MOBILE->iswebOS() ){
		$DEVICE['os'] = 'WebOS';
	}else if( $DETECT_MOBILE->isWindowsMobileOS() ){
		$DEVICE['os'] = 'WindowsMobile';
	}else if( $DETECT_MOBILE->isWindowsPhoneOS() ){
		$DEVICE['os'] = 'WindowsPhone';
	}
	
	
	/********** MOBILE DEVICE DETECT **********/
	if($DETECT_MOBILE->isiPhone()){
		$DEVICE['mobile_device'] = 'iPhone';
	}else if($DETECT_MOBILE->isiPad()){
		$DEVICE['mobile_device'] = 'iPad';
	}
	
	
	/********** MISC **********/
	/*
	version(Version)	
	version(Opera)
	version(Opera Mobi)
	version(Safari)
	*/
	
	
}





/****************************************************************************************/
/*************************************** DESKTOP ****************************************/
/****************************************************************************************/


if($DEVICE['type'] == 'Desktop'){
	
	
	/********** DESKTOP **********/
	$DETECT_BROWSCAP = new Browscap('cache');
	$DETECT_BROWSCAP = $DETECT_BROWSCAP->getBrowser();
	//echo '<pre>';print_r($DETECT_BROWSCAP);echo '</pre>';
	
	
	/********** DESKTOP BROWSER DETECT **********/
	$DEVICE['browser'] = $DETECT_BROWSCAP->Browser;
	$DEVICE['browser_version'] = $DETECT_BROWSCAP->Version;

	
	/********** DESKTOP OS DETECT **********/
	if(strstr($DEVICE['user_agent'],'Win')){
		$DEVICE['os'] = 'Windows';
		if(preg_match('/Win16/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '3.11';
		}else if(preg_match('/(Windows 95)|(Win95)|(Windows_95)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '95';
		}else if(preg_match('/(Windows 98)|(Win98)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '98';
		}else if(preg_match('/Windows ME/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'ME';
		}else if(preg_match('/(Windows NT 4.0)|(WinNT4.0)|(WinNT)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'NT 4.0';
		}else if(preg_match('/(Windows NT 5.0)|(Windows 2000)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '2000';
		}else if(preg_match('/(Windows NT 5.1)|(Windows XP)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'XP';
		}else if(preg_match('/(Windows NT 5.2)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'Server 2003';
		}else if(preg_match('/(Windows NT 6.0)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'Vista';
		}else if(preg_match('/(Windows NT 7.0)|(Windows NT 6.1)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '7';
		}else if(preg_match('/(Windows NT 6.2)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = '8';
		}
	}else if(strstr($DEVICE['user_agent'],'Mac')){ //(Mac_PowerPC)|(Macintosh)
		$DEVICE['os'] = 'Mac';
		if(preg_match('/(OS X 10_0)|(OS X 10.0)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.0';
		}else if(preg_match('/(OS X 10_1)|(OS X 10.1)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.1';
		}else if(preg_match('/(OS X 10_2)|(OS X 10.2)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.2';
		}else if(preg_match('/(OS X 10_3)|(OS X 10.3)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.3';
		}else if(preg_match('/(OS X 10_4)|(OS X 10.4)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.4';
		}else if(preg_match('/(OS X 10_5)|(OS X 10.5)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.5';
		}else if(preg_match('/(OS X 10_6)|(OS X 10.6)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.6';
		}else if(preg_match('/(OS X 10_7)|(OS X 10.7)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.7';
		}else if(preg_match('/(OS X 10_8)|(OS X 10.8)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.8';
		}else if(preg_match('/(OS X 10_9)|(OS X 10.9)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.9';
		}else if(preg_match('/(OS X 10_10)|(OS X 10.10)/i', $DEVICE['user_agent'])){
			$DEVICE['os_version'] = 'OS X 10.10';
		}
	}else if(strstr($DEVICE['user_agent'],'Linux')){ //(Linux)|(X11)
		$DEVICE['os'] = 'Linux';
	}else if(strstr($DEVICE['user_agent'],'Unix')){
		$DEVICE['os'] = 'Unix';
	}else if(preg_match('/OpenBSD/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'Open BSD';
	}else if(preg_match('/SunOS/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'Sun OS';
	}else if(preg_match('/QNX/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'QNX';
	}else if(preg_match('/BeOS/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'BeOS';
	}else if(preg_match('/OS\/2/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'OS/2';
	}else if(preg_match('/(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)/2/i', $DEVICE['user_agent'])){
		$DEVICE['os'] = 'Search Bot';
	}
	
	
}





/****************************************************************************************/
/**************************************** OTHER *****************************************/
/****************************************************************************************/


/********* WEBKIT DETECT **********/
if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'webkit') !== false){
    $DEVICE['webkit'] = true;
	if($DETECT_MOBILE->version('Webkit')){
		$DEVICE['webkit_version'] = $DETECT_MOBILE->version('Webkit');
	}
}else{
	$DEVICE['webkit'] = false;
}





/****************************************************************************************/
/**************************************** OUTPUT ****************************************/
/****************************************************************************************/


/********* OUTPUT JS OBJECT **********/
echo '<script type="text/javascript">
	var DEVICE = {
		user_agent: "'.$DEVICE['user_agent'].'",
		type: "'.$DEVICE['type'].'",
		touchscreen: '.($DEVICE['touchscreen']==true?'true':'false').',
		mobile_device: "'.$DEVICE['mobile_device'].'",
		os: "'.$DEVICE['os'].'",
		os_version: "'.$DEVICE['os_version'].'",
		browser: "'.$DEVICE['browser'].'",
		browser_version: "'.$DEVICE['browser_version'].'",
		webkit: '.($DEVICE['webkit']==true?'true':'false').',
		webkit_version: "'.$DEVICE['webkit_version'].'"
	};
</script>';





?>