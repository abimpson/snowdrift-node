<?php
//ERRORS
error_reporting(E_ALL); //E_ALL, 0
ini_set('display_errors', 1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Mobile Detect</title>
	
	<!-- META -->
    <meta name="viewport" content="width=600">
	
	<!-- MOBILE DETECTION -->
	<?php include_once('devicedetect.alex.php'); ?>
    <script>
		console.log(DEVICE);
	</script>
	
	<!-- CSS -->
	<style>
		body{
			padding:20px;
		}
		h1{
			margin:0;
		}
		h2{
			margin:20px 0 0;
		}
		p{
			margin:0;
		}
		p+h1{
			margin:40px 0 0;
		}
	</style>
	
</head>
<body>
    
    
    <h1>DEVICE DETECTION</h1>
    
    <h2>User Agent</h2>
    <p><?php echo $DEVICE['user_agent']; ?></p>
    
    <h2>Mobile / Tablet / Desktop</h2>
    <p><?php echo $DEVICE['type'] ?></p>
    
    <h2>Touchscreen</h2>
    <p><?php echo ($DEVICE['touchscreen']==true?'Yes':'No'); ?></p>
    
    <h2>OS</h2>
    <p><?php echo $DEVICE['os']; ?>, <?php echo $DEVICE['os_version']; ?></p>
    
    <h2>Browser</h2>
    <p><?php echo $DEVICE['browser']; ?>, <?php echo $DEVICE['browser_version']; ?></p>
    
    <h2>Webkit</h2>
    <p><?php echo ($DEVICE['webkit']==true?'Yes':'No'); ?>, <?php echo $DEVICE['webkit_version']; ?></p>
    
    
    <h1>OBJECT OUTPUT</h1>
	<pre>
    	<?php print_r($DEVICE); ?>
    </pre>


</body>
</html>