

/*
	(C) KERVE
	
	AUTHOR:		ALEX BIMPSON
	NAME:		MUSIC JS APP FRAMEWORK
	VERSION:	1.0
	UPDATED:	2013-11-5
*/


/********************************************************************************************************************************/
/******************************************************* MUSIC - FRAMEWORK ******************************************************/
/********************************************************************************************************************************/

var MUSIC = new Object({
	
	
	/******************** CONFIG ********************/
	path_to_includes: '../includes/music/',
	lastfm_api_key: '84f61c34ace3a030f5eee1752eeb3c04',
	
	
	/******************** VARIABLES ********************/
	init: null,
	
	
	/******************** SLUGIFY ********************/
	slugify: function(text){
		return text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'+');
	},
	
	
	/******************** SPOTIFY - SONG SEARCH ********************/
	search_spotify: function(song,callback){
		var var_url = 'http://ws.spotify.com/search/1/track.json?q=' + MUSIC.slugify(song) + '&page=1';
		$.ajax({
			method: "get",
			url: var_url,
			dataType: "json",
			async: true,
			crossDomain: true,
			timeout: 20000,
			success:	function(response){
				MUSIC.return_success('Spotify',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('Spotify',xhr,status,error,callback);
			}
		});
	},


	/******************** ITUNES - SONG SEARCH ********************/
	search_itunes: function(song,callback){
		$.ajax({
			type:		'post',
			url:		'https://itunes.apple.com/search',
			data:		{
							term: 		song,
							country:	'GB',
							media:		'music', //movie, podcast, music, musicVideo, audiobook, shortFilm, tvShow, software, ebook, all
							entity:		'song',
							limit:		10,
							'sort':		'popular'
						},
			dataType:	'jsonp',
			async:		true,
			timeout:	20000,
			success:	function(response){
				MUSIC.return_success('iTunes',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('iTunes',xhr,status,error,callback);
			}
		});
	},


	/******************** LASTFM - TRACK SEARCH ********************/
	search_lastfm: function(song,callback){
		$.ajax({
			type:		'get',
			url:		'http://ws.audioscrobbler.com/2.0/',
			data:		{
							method:		'track.search',
							track:		song,
							api_key:	MUSIC.lastfm_api_key,
							format:		'json'
						},
			dataType:	'json',
			async:		true,
			timeout:	20000,
			success:	function(response){
				MUSIC.return_success('LastFM',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('LastFM',xhr,status,error,callback);
			}
		});
	},
	
	
	/******************** DEEZER - SEARCH ********************/
	search_deezer: function(song,callback){
		$.ajax({
			type:		'get',
			url:		'http://api.deezer.com/2.0/search',
			data:		{
							q:			song,
							output:		'jsonp'
						},
			dataType:	'jsonp',
			async:		true,
			timeout:	20000,
			success:	function(response){
				MUSIC.return_success('Deezer',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('Deezer',xhr,status,error,callback);
			}
		});
	},
	
	
	/******************** RDIO - SEARCH ********************/
	search_rdio: function(song,callback){
		$.ajax({
			type:		'post',
			url:		MUSIC.path_to_includes+'search_rdio.php',
			data:		{
							query:	song
						},
			dataType:	'json',
			async:		true,
			timeout:	20000,
			success:	function(response){
				MUSIC.return_success('Rdio',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('Rdio',xhr,status,error,callback);
			}
		});
	},


	/******************** GROOVESHARK - SEARCH ********************/
	search_grooveshark: function(song,callback){
		$.ajax({
			type:		'get',
			url:		MUSIC.path_to_includes+'search_tinysong.php',
			data:		{
							song:		MUSIC.slugify(song)
						},
			dataType:	'json',
			async: true,
			crossDomain: true,
			timeout: 20000,
			success:	function(response){
				MUSIC.return_success('Grooveshark',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('Grooveshark',xhr,status,error,callback);
			}
		});
	},
	
	
	/******************** MUSICBRAINZ - SEARCH ********************/
	search_musicbrainz: function(song,callback){
		$.ajax({
			type:		'get',
			url:		MUSIC.path_to_includes+'search_musicbrainz.php',
			data:		{
							song:		MUSIC.slugify(song)
						},
			dataType:	'json',
			async: true,
			crossDomain: true,
			timeout: 20000,
			success:	function(response){
				MUSIC.return_success('MusicBrainz',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('MusicBrainz',xhr,status,error,callback);
			}
		});
	},
	
	
	/******************** DECIBEL - SEARCH ********************/
	search_decibel: function(song,callback){
		$.ajax({
			type:		'get',
			url:		MUSIC.path_to_includes+'search_decibel.php',
			data:		{
							song:		MUSIC.slugify(song)
						},
			dataType:	'json',
			async: true,
			crossDomain: true,
			timeout: 20000,
			success:	function(response){
				MUSIC.return_success('Decibel',response,callback);
			},
			error:		function(xhr,status,error){
				MUSIC.return_error('Decibel',xhr,status,error,callback);
			}
		});
	},
	
	
	/******************** RETURN RESULTS ********************/
	return_success: function(service,response2,callback){
		var response = {
			status: 'ok',
			service: service,
			response: response2
		};
		if(typeof(callback)=="function"){
			callback(response);
		};
	},
	return_error: function(service,xhr,status,error,callback){
		var response = {
			status: 'error',
			service: service,
			response:{
				error: error,
				status: status,
				xhr: xhr
			}
		};
		if(typeof(callback)=="function"){
			callback(response);
		};
	}
	
	
});





/********************************************************************************************************************************/
/******************************************************* MUSIC - INITIALISE *****************************************************/
/********************************************************************************************************************************/


/******************** EXTEND CONFIG ********************/
$(document).ready(function(){
	if(typeof MUSIC_CONFIG !== 'undefined' && MUSIC_CONFIG != null){
		$.extend(MUSIC, MUSIC_CONFIG);
	};
});






