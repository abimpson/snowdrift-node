

/*
	(C) KERVE
	
	AUTHOR:		ALEX BIMPSON
	NAME:		TWITTER JS APP FRAMEWORK
	VERSION:	1.2
	UPDATED:	2014-10-25
*/


/********************************************************************************************************************************/
/****************************************************** TWITTER - FRAMEWORK *****************************************************/
/********************************************************************************************************************************/

var $twitter = new Object({
	
	
	/******************** CONFIG ********************/
	path_to_api: '../php-plugins/twitter',
	
	
	/******************** VARIABLES ********************/
	user: null,
	
	
	/******************** AUTHORISE ********************/
	authenticate: function(){
		if(window.location.protocol == 'https:'){
			var secure = true;
		}else{
			var secure = false;
		}
		window.open($twitter.path_to_api+'/twitter.auth.php?secure='+secure);
	},
	
	
	/******************** AUTHORISATION CALLBACK ********************/
	authenticate_callback: function(response){
		//console.log(response);
		if(response.success == true){
			$twitter.user = {
				username: response.username
			};
			$twitter.twit_click_auth();
		}else{
			$twitter.twit_cancelled();
		};
	},
	
	
	/******************** AUTH CALLBACKS ********************/
	twit_click_pre: function(){},
	twit_click_auth: function(){},
	twit_cancelled: function(){},
	
	
	/******************** FUNCTIONS - GET ********************/
	//MENTIONS TIMELINE
	get_user_timeline: function(){
		app.ajax(
			$twitter.path_to_api+'/twitter.functions.php',
			{
				query: 'get_user_timeline',
				user_id: '65347826'
			},
			$twitter.get_user_timeline_callback,
			'GET'
		);
	},
	get_user_timeline_callback: function(response){
		console.log(response);
	},
	//MENTIONS TIMELINE
	get_mentions_timeline: function(){
		app.ajax(
			$twitter.path_to_api+'/twitter.functions.php',
			{
				query: 'get_mentions_timeline'
			},
			$twitter.get_mentions_timeline_callback,
			'GET'
		);
	},
	get_mentions_timeline_callback: function(response){
		console.log(response);
	},
	
	
	/******************** TWEET DIALOG ********************/
	share: function(url,text){
		if(typeof(url) == "string" && typeof(text) == "string"){
			window.open('https://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text),'_blank');
		}else if(typeof(url) == "string"){
			window.open('https://twitter.com/share?url='+encodeURIComponent(url),'_blank');
		}else if(typeof(text) == "string"){
			window.open('https://twitter.com/share?text='+encodeURIComponent(text),'_blank');
		}else{
			window.open('https://twitter.com/share','_blank');
		};
	}
	
	
});





/********************************************************************************************************************************/
/***************************************************** TWITTER - INITIALISE SDK *************************************************/
/********************************************************************************************************************************/


/******************** LOGIN BUTTON ********************/
$(function(){
	$('.twitter_login').click(function(){
		$twitter.twit_click_pre();
		$twitter.authenticate();
		return false;
	});
});


/******************** EXTEND ********************/
$(document).ready(function(){
	//EXTEND CONFIG
	if(typeof $twitter_config !== 'undefined' && $twitter_config != null){
		$.extend($twitter, $twitter_config);
	};
	if(typeof $twitter_functions !== 'undefined' && $twitter_functions != null){
		$.extend($twitter, $twitter_functions);
	};
});



