


var $filter = {
	
	
	/******************** CHECK ********************/
	check: function(string){
		//REMOVE SYMBOLS AND SPLIT INTO WORDS
		var string2 = string.replace(/[^A-Za-z ]/g,'');
		var words = string2.split(' ');
		//FOR EACH WORD CHECK IF ITS IN SWEAR LIST
		var found = false;
		for(var i in words){
			if(jQuery.inArray(words[i].toLowerCase(), filter.list) > -1){
				found = true;
				break;
			};
			for(var j in filter.list){
				var mysearch = words[i].split(filter.list[j]);
				if (mysearch != words[i]){
					found = true;
					break;
				};
			};
		};
		//RETURN
		if(found == true){
			//console.log(string);
			return true;
		}else{
			return false;
		};
	},
	
	
	/******************** REMOVE EXACT WORDS ********************/
	remove: function(string){
		//REPLACE SWEAR WORDS WITH STARS
		for(var i in filter.list){
			var regex = new RegExp(filter.list[i],"gi");
			var replaceLength = filter.list[i].length;
			var replaceString = '';
			for(j=0;j<replaceLength;j++){
				replaceString = replaceString+'*';
			};
			string = string.replace(regex,replaceString);
		};
		//RETURN
		return string;
	},
	
	
	/******************** EXPLICIT WORDS ********************/
	list: new Array(
		'anal',
		'anus',
		'arse',
		'ass',
		'asshole',
		'ballsack',
		'balls',
		'bastard',
		'bitch',
		'biatch',
		'bloody',
		'blowjob',
		'blow job',
		'bollocks',
		'bollok',
		'boner',
		'boob',
		'bugger',
		'bum',
		'butt',
		'buttplug',
		'clitoris',
		'cock',
		'coon',
		'crap',
		'cunt',
		'damn',
		'dick',
		'dildo',
		'dyke',
		'fag',
		'feck',
		'fellate',
		'fellatio',
		'felching',
		'fuck',
		'f u c k',
		'fudgepacker',
		'fudge packer',
		'flange',
		'Goddamn',
		'God damn',
		'hell',
		'homo',
		'jerk',
		'jizz',
		'knobend',
		'knob end',
		'labia',
		'lmao',
		'lmfao',
		'muff',
		'nigger',
		'nigga',
		'omg',
		'penis',
		'piss',
		'poop',
		'prick',
		'pube',
		'pussy',
		'queer',
		'scrotum',
		'sex',
		'shit',
		's hit',
		'sh1t',
		'slut',
		'smegma',
		'spunk',
		'tit',
		'tosser',
		'turd',
		'twat',
		'vagina',
		'wank',
		'whore',
		'wtf'
	)
	
	
};





