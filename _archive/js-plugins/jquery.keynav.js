// JavaScript Document


$(document).ready(function () {
    //MAP KEYS
	var keys = {
		left: false,
		right: false,
		up: false,
		down: false
	};
    var keymap = {
		37: "left",
		38: "up",
		39: "right",
		40: "down"
	};
    var keytimer = null;
    //ASSIGN KEY FUNCTIONS
    function updatekeys(){
		if(keys.left){
			swipeLeft();
		};
		if(keys.right){
			swipeRight();
		};
    };
	//HANDLE KEY PRESS
	var delay = 330;
    function keycheck(){
    	if(keys.left || keys.right || keys.up || keys.down){
	    	if (keytimer === null){
	    		delay = 330;
	    		var doTimer = function(){
	    			updatekeys();
	    			keytimer = setTimeout(doTimer, delay);
	    			delay = 60;
	    		};
	    		doTimer();
	    	}
    	}else{
    		clearTimeout(keytimer);
    		keytimer = null;
    	};
    };
    //LISTEN FOR KEY PRESS
	if(window.addEventListener) {
		//Modern Browsers
		window.addEventListener('keydown', function(e){
			keys[keymap[e.keyCode]] = true;
			keycheck();
		});
		window.addEventListener('keyup', function(e){
			keys[keymap[e.keyCode]] = false;
			keycheck();
		});
	}else if(window.attachEvent) {
		//IE and Opera
		window.attachEvent('onkeydown', function(e){
			keys[keymap[e.keyCode]] = true;
			keycheck();
		});
		window.attachEvent('onkeyup', function(e){
			keys[keymap[e.keyCode]] = false;
			keycheck();
		});
	}else if(document.addEventListener){
		//FireFox
		document.addEventListener('keydown', function(e){
			keys[keymap[e.keyCode]] = true;
			keycheck();
		});
		document.addEventListener('keyup', function(e){
			keys[keymap[e.keyCode]] = false;
			keycheck();
		});
	};
});