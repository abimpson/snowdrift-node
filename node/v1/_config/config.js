


/******************** REQUIRES ********************/
const flags = require('node-flag');
const _utils = require('./../lib/utils');


/******************** CONFIG ********************/
var config = {
	env: process.env.NODE_ENV || 'development',
	root: '',
	cdn: '',
	flags:{
		open: (flags.isset('browser')?flags.get('browser'):false)
	},
	server:{
		ip: _utils.get_ip(),
		port: process.env.PORT || 4000
	},
	socket:{
		port: 4200
	},
	mongo: {
    url: 'mongodb://localhost:27017/dev'
	},
}


/******************** EXPORTS ********************/
module.exports = config;







