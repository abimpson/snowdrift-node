


/******************** REQUIRES ********************/
const colors = require('colors');
const cluster = require('cluster');
// const open = require('open');
const _config = require('./_config/config');
// const _mailgun = require('./lib/mailgun');


/******************** STARTUP ********************/
async function startup(){
	console.log(('***************** STARTUP - '+(_config.env.toUpperCase())+' *****************').bgGreen);
	// send email alert
	// _mailgun.restart();
}


/******************** CLUSTER ********************/
if(_config.env !== 'production'){
	startup();
}
if(cluster.isMaster && _config.env === 'production'){
	const cpuCount = process.env.WEB_CONCURRENCY || require('os').cpus().length;
	startup();
	for (let i = 0; i < cpuCount; i += 1) {
		cluster.fork();
	}
}else{
	const server = require('./lib/server').create();
	const socket = require('./lib/socket.js').create('s1',server);
}


/******************** CLUSTER - EXIT ********************/
cluster.on('exit', worker => {
	console.log('Worker '+worker.id+' died, restarting...'.bgRed);
	return cluster.fork();
});


/******************** OPEN BROWSER WINDOW ********************/
// if(_config.flags.open){
// 	open('http://'+_config.server.ip+':'+_config.server.port);
// }






