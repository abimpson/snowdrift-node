


/******************** REQUIRES ********************/
const bodyParser = require('body-parser');
const colors = require('colors');
const cookieParser = require('cookie-parser');
const fs = require('fs');
const http = require('http');
const path = require('path');
const express = require('express');
const session = require('express-session');
const mongostore = require('connect-mongo')(session);
const _api = require('./api');
const _config = require('./../_config/config');
const _mongo = require('./mongo');


/******************** CREATE SERVER ********************/
function create(){
  // create express app
  var app = express();
  // bodyparser - enable reading of post vars
  app.use(bodyParser.json()); // support json encoded bodies
  app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  // cookieparser - enable use of browser cookies
  app.use(cookieParser());
  // session - setup
  app.use(session({
    key: 'user_sid',
    secret: 'FR33PR0P3RB33R',
    store: new mongostore({ url:_config.mongo.url }),
    resave: true,
    saveUninitialized: false,
    // name: environmentVars.cookie.name,
    // maxAge: environmentVars.cookie.expiry,
    // domain: environmentVars.cookie.domain,
    // httpOnly: true,    
    proxy: true,        
    secureProxy: 'auto', //(app.get('env') === 'production'?true:false),
    cookie: {
      expires: 1000 * 60 * 60 * 24,
      secure: 'auto', //(app.get('env') === 'production'?true:false)
    }
  }));
  // clear cookies - for example on server restart
  // app.use((req, res, next) => {
  // 	if (req.cookies.user_sid && !req.session.user) {
  // 		res.clearCookie('user_sid');        
  // 	}
  // 	next();
  // });
  // set public folder
  app.use(express.static('public'));
  // set view engine
  app.set('view engine', 'ejs');
  app.set('views','views');
  // routes
  routes(app);
  // 404
  app.use(function (req, res, next) {
    res.status(404).send("404 Error");
  });
  // error handler
  app.use(function (err, req, res, next) {
    res.status(404).send('Unknown Error');
  });
  // create server
  var server = http.createServer(app);
  // listen
  server.listen(_config.server.port, _config.server.ip, function(){});
  // log
  console.log(('SERVER: http://'+_config.server.ip+':'+_config.server.port).bgGreen);
  // return
  return server;
}


/******************** ROUTES ********************/
function routes(app){
  
  // www redirect
  if(_config.env==='production'){
    app.get('*', function(req, res, next) {
      if (req.headers.host.slice(0, 3) != 'www') {
        console.log(('route: www redirect').bgCyan);
        res.redirect('https://www.' + req.headers.host + req.url, 301);
        // }else if (!req.secure) {
      }else if (req.headers && req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto']=='http') {
        console.log(('route: https redirect').bgCyan);
        res.redirect('https://' + req.headers.host + req.url);
      } else {
        next();
      }
    });
  }
  
  // route: main
  app.get('/', function(req, res) {
    console.log(('route: reveal').bgCyan);
    res.render('index');
  });
  
  // route: api
  app.all([
    '/api/:request'
  ], function(req, res) {
    console.log(('route: api').bgCyan);
    var method = req.params.request;
    var query = (req.method== 'POST'?req.body:req.query);
    var callback = function(json){
      res.setHeader('Content-Type','application/json');
      res.setHeader('Access-Control-Allow-Origin','*')
      res.send(JSON.stringify(json));
    }
    _api.request(method,query,req,callback);
  });
  
  // route: catch all
  app.get('/:path', function(req, res, next) {
    console.log(('route: ejs - '+req.params.path).bgCyan);
    try{
      res.render(req.params.path,{});
    }catch(e){
      next();
    }
  });
  
}


/******************** EXPORTS ********************/
module.exports = {
  create: create
}









