


(function(){
  
  
  /******************** REQUIRES ********************/
  const colors = require('colors');
  const dgram = require('dgram');
  const server = dgram.createSocket('udp4');
  const _config = require('./../_config/config');
  
  
  /******************** SOCKET ********************/
  var socket = null;
  function get_socket(){
    if(socket==null){
      var _socket = require('./socket.js');
      socket = _socket.get('s1');
    }
    return socket;
  }
  
  
  /******************** UDP - SETUP SERVER ********************/
  
  server.on('listening', () => {
    const address = server.address();
    console.log(`UDP:    ${address.address}:${address.port}`.cyan);
  });
  
  server.on('error', (err) => {
    console.log(`udp - error:\n${err.stack}`.red);
    server.close();
  });
  
  server.on('message', (msg, rinfo) => {
    console.log(`udp - received: ${msg} from ${rinfo.address}:${rinfo.port}`.yellow);
    var s = get_socket();
    if(s!==null){
      s.emit('udp_response',msg.toString('utf8'));
    }
  });
  
  server.bind(_config.udp.port);
  
  
  /******************** UDP - SEND MESSAGE ********************/
  function send(ip,port,msg){
    // msg = Buffer.from(msg);
    console.log(('udp - sending: '+msg+' to '+ip+':'+port).magenta)
    msg = new Buffer(msg);
    server.send(msg, port, ip, (err) => {
      if(err){
        console.log((error).red);
      }
    });
  }
  
  
  /******************** EXPORTS ********************/
  module.exports = {
    send: send
  };
  
  
}());


// prompt.start();
// function listen(){
// 	prompt.get(['message'], function (err, result) {
// 	  var message = result.message;
// 	});
// }
// listen();




