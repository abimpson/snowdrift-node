


/******************** REQUIRES ********************/
const colors = require('colors');
const cryptojs = require('crypto-js');
const mongoose = require('mongoose');
const request = require('request');
const _config = require('./../_config/config');
const _utils = require('./utils.js');


/******************** CONNECT ********************/
mongoose.connect(_config.mongo.url).then(res => {
	console.log(('MONGOOSE:  connected').bgGreen);
}).catch(err => {
	console.log(('MONGOOSE:  error connecting').bgRed);
});


/******************** SCHEMAS ********************/
// ADMINS
const admin_schema = mongoose.Schema({
	hash: String,
	salt: String
},{
	timestamps: { createdAt: 'created', updatedAt: 'updated' }
});
// USERS
const user_schema = new mongoose.Schema({
	email: { type: String, default: null },
	mobile: { type: String, default: null },
	code_string: { type: String, default: null },
	code: { type: mongoose.Schema.Types.ObjectId, default: null, ref: 'Code' },
},{
	collection: 'users',
	timestamps: { createdAt: 'created', updatedAt: 'updated' }
});
// CODES
const code_schema = new mongoose.Schema({
	code: { type: String, default: null },
	created: { type: Date, default: new Date() },
	cdn_path: { type: String, default: null },
	used: { type: Boolean, default: false },
	used_date: { type: Date, default: null },
	user_id: { type: mongoose.Schema.Types.ObjectId, default: null, ref: 'User' },
	redeemed: { type: Boolean, default: false },
	redeemed_date: { type: Date, default: null },
	redeemed_pub: { type: mongoose.Schema.Types.ObjectId, default: null, ref: 'Pub' },
},{
	collection: 'codes'
});
// PUBS
const pub_schema = mongoose.Schema({
	// pub details
	name: { type: String, default: null },
	address: { type: String, default: null },
	postcode: { type: String, default: null },
	lat: { type: Number, default: null },
	long: { type: Number, default: null },
	twitter: { type: String, default: null },
	facebook: { type: String, default: null },
	web: { type: String, default: null },
	web_full: { type: String, default: null },
	// contact
	contact: { type: String, default: null },
	phone: { type: String, default: null },
	email: { type: String, default: null },
	mbc: { type: String, default: null },
	// stock
	kegs1: { type: Number, default: 0 },
	kegs2: { type: Number, default: 0 },
	stock: { type: Number, default: 0 },
	redeemed: { type: Number, default: 0 },
	// misc
	hash: { type: String, default: '95e00e6abe7a1ff20c202addcd812c77f5772942f1c161e95803871600210330' },
	salt: { type: String, default: '3d2816314007e75a' },
	live: { type: Boolean, default: false },
},{
	timestamps: { createdAt: 'created', updatedAt: 'updated' }
});
// BROADCASTS
const broadcast_schema = new mongoose.Schema({
	user: { type: mongoose.Schema.Types.ObjectId, default: null, ref: 'User' },
	message_id: { type: Number, default: null },
	time: { type: Date, default: null },
	delay: { type: Number, default: 0 },
	pending: { type: Boolean, default: false },
});
// STATS
const stats_schema = new mongoose.Schema({
	vouchers: Number,
	vouchers_sms: Number,
	vouchers_email: Number,
	pints: Number,
	pints2: Number,
	time: Number
},{
	collection: 'stats',
	timestamps: { createdAt: 'created', updatedAt: 'updated' }
});
// BUILD SCHEMAS OBJECT
const schemas = {
	admins: mongoose.model('Admin', admin_schema),
	users: mongoose.model('User', user_schema),
	codes: mongoose.model('Code', code_schema),
	pubs: mongoose.model('Pub', pub_schema),
	broadcasts: mongoose.model('Broadcast', broadcast_schema),
	stats: mongoose.model('Stats', stats_schema),
};


/******************** PARSE RESULT ********************/
function parse_result (res){
	var result = res.toObject();
	if(result.hash) delete result.hash;
	if(result.salt) delete result.salt;
	// if(result.created) delete result.created;
	// if(result.updated) delete result.updated;
	return result;
}


/******************** LOGIN ********************/
async function login(schema,id,password){
	console.log(('login - '+schema).bgMagenta);
	schema = schema.toString();
	id = id.toString();
	password = password.toString();
	try{
		var result;
		await schemas[schema].findById(id).then(res => {
			var match = false;
			if(res){
				var pass = cryptojs.SHA256(password).toString(cryptojs.enc.Hex);
				var salt = res.salt;
				var hash = res.hash;
				var newhash = cryptojs.SHA256(pass+salt).toString(cryptojs.enc.Hex);
				match = (hash==newhash?true:false);
			}
			if(match){
				result = parse_result(res);
			}else{
				throw new Error('login0');
			}
		}).catch(err => {
			throw new Error('login1');
		});
		return result;
	}catch(e){
		throw new Error('login2');
	}
}


/******************** SET PASSWORD ********************/
function set_password(schema,id,password){
	var pass = cryptojs.SHA256(password).toString(cryptojs.enc.Hex);
	var salt = cryptojs.lib.WordArray.random(8).toString(cryptojs.enc.Hex);
	var hash = cryptojs.SHA256(pass+salt).toString(cryptojs.enc.Hex);
	schemas[schema].findByIdAndUpdate(id, {
		hash:hash,
		salt:salt
	}).then(res => {
		console.log('Password set');
	}).catch(err => {
		console.log('Error setting password'.red);
	});
}
// set_password('admins','5a93da8a28e80af82dd18ce9','M34nt1m3');


/******************** GET COLLECTION ********************/
async function get_collection(schema,query){
	// vars
	if(query && query._id){
		query._id = mongoose.Types.ObjectId(query._id.toString());
	}
	// query
	return schemas[schema].find(query).then(res => {
		result = res;//.toObject();
		for(var i in result){
			result[i] = parse_result(result[i]);
		}
		return result;
	}).catch(err => {
		throw 'get_collection.0';
	});
}


/******************** GET DOCUMENT BY ID ********************/
async function get_document(schema,id){
	return schemas[schema].findById(id).then(res => {
		result = parse_result(res);
		return res;
	}).catch(err => {
		throw 'get_document.0';
	});
}


/******************** SIGNUP - ADD USER & ASSIGN CODE ********************/
async function signup(query){
	// vars + validation
	var q = {};
	if(typeof(query.mobile)!=='undefined'){
		q.mobile = _utils.cleanPhone(query.mobile.toString());
		if(!_utils.isValidPhone(q.mobile) || q.mobile.length!=11 || q.mobile[0]!='0' || q.mobile[1]!='7') throw 'signup.1';
	}else if(typeof(query.email)!=='undefined'){
		q.email = query.email.toString();
		if(!_utils.isValidEmail(q.email)) throw 'signup.2';
	}else{
		throw 'signup.0';
	}
	// check user isn't already signed up
	return schemas.users.findOne(q)
	// insert new user
	.then(res => {
		if(res) throw 'signup.3';
		var new_user = new schemas.users(q);
		return new_user.save()
	})
	// assign a code
	.then(res => {
		var result = parse_result(res);
		return schemas.codes.findOneAndUpdate({
			used: false,
			user_id: null
		},{
			used: true,
			used_date: Date.now(),
			user_id: mongoose.Types.ObjectId(result._id)
		},{
			new: true
		})
	})
	// add code to user
	.then(res => {
		var result = parse_result(res);
		return schemas.users.findOneAndUpdate(q,{
			code_string: result.code,
			code: mongoose.Types.ObjectId(result._id),
		},{
			new: true
		});
	})
	// send broadcast
	.then(res => {
		if(res && res._id){
			broadcast(res._id,1);
		}
		return res;
	});
}


/******************** BROADCAST ********************/
function broadcast(user,msg,time){
	// vars
	var vars = {
		user: mongoose.Types.ObjectId(user),
		message_id: msg,
		time: (time?time:Date.now())
	}
	// add to schema
	var new_broadcast = new schemas.broadcasts(vars);
	// insert
	return new_broadcast.save();
}
// broadcast('5ac4d393820049a09c9bec54',1).then(res => {
// 	console.log(res);
// }).catch(err => {
// 	console.log(err);
// })


/******************** REDEEM ********************/
function redeem(query){
	// vars
	code = query.code.toString();
	pub = query.pub.toString();
	// redeem
	return schemas.codes.findOneAndUpdate({
		code: code,
		used: true,
		redeemed: {
			$ne: true
		}
	},{
		redeemed: true,
		redeemed_date: Date.now(),
		redeemed_pub: mongoose.Types.ObjectId(pub)
	})
	// update pub redeemed
	.then(res => {
		if(!res) throw 'redeem.1';
		return schemas.pubs.findByIdAndUpdate(pub, {$inc : {'redeemed' : 1}});
	})
	// get latest data
	.then(res => {
		return schemas.codes.findOne({
			code: code,
			used: true,
			redeemed: true
		})
		.populate('user_id')
		.populate('redeemed_pub');
	})
	// send broadcast
	.then(res => {
		if(res && res.user_id && res.user_id._id){
			if(res.user_id.mobile!=null && res.user_id.mobile!=''){
				broadcast(res.user_id._id,2);
			}
			broadcast(res.user_id._id,3,(Date.now()+(60*10*1000)));
		}
		return res;
	});
}


/******************** ADD PUB ********************/
async function add_pub(query){
	// try{
		var result;
		// vars
		if(typeof(query.password)!=='undefined'){
			var pass = cryptojs.SHA256(query.password.toString()).toString(cryptojs.enc.Hex);
			var salt = cryptojs.lib.WordArray.random(8).toString(cryptojs.enc.Hex);
			var hash = cryptojs.SHA256(pass+salt).toString(cryptojs.enc.Hex);
		}
		var vars = {
			name: (typeof(query.name)!=='undefined'?query.name.toString():''),
			address: (typeof(query.address)!=='undefined'?query.address.toString():''),
			postcode: (typeof(query.postcode)!=='undefined'?query.postcode.toString():''),
			twitter: (typeof(query.twitter)!=='undefined'?query.twitter.toString():''),
			facebook: (typeof(query.facebook)!=='undefined'?query.facebook.toString():''),
			web: (typeof(query.web)!=='undefined'?query.web.toString():''),
			web_full: (typeof(query.web_full)!=='undefined'?query.web_full.toString():''),
			lat: (typeof(query.lat)!=='undefined' && !isNaN(parseFloat(query.lat))?parseFloat(query.lat):''),
			long: (typeof(query.long)!=='undefined' && !isNaN(parseFloat(query.long))?parseFloat(query.long):''),
			stock: (typeof(query.stock)!=='undefined' && !isNaN(parseInt(query.stock))?parseInt(query.stock):0),
			redeemed: 0,
			hash: (typeof(hash)!=='undefined'?hash:''),
			salt: (typeof(salt)!=='undefined'?salt:''),
			live: (typeof(query.live)!=='undefined'?(query.live==true || query.live=='true'?true:false):false)
		};
		console.log(vars);
		// add to schema
		var new_pub = new schemas.pubs(vars);
		// insert
		await new_pub.save().then(res => {
			result = true;
		}).catch(err => {
			throw new Error('add_pub0');
		});
		// return
		if(result){
			return result;
		}else{
			throw new Error('add_pub1');
		}
	// }catch(e){
	// 	throw new Error('add_pub2');
	// }
}


/******************** UPDATE PUB ********************/
async function update_pub(query){
	// try{
		var result;
		// vars
		var vars = {};
		if(typeof(query.name)!=='undefined') vars.name = query.name.toString();
		if(typeof(query.address)!=='undefined') vars.address = query.address.toString();
		if(typeof(query.postcode)!=='undefined') vars.postcode = query.postcode.toString();
		if(typeof(query.twitter)!=='undefined') vars.twitter = query.twitter.toString();
		if(typeof(query.facebook)!=='undefined') vars.facebook = query.facebook.toString();
		if(typeof(query.web)!=='undefined') vars.web = query.web.toString();
		if(typeof(query.web_full)!=='undefined') vars.web_full = query.web_full.toString();
		if(typeof(query.lat)!=='undefined' && !isNaN(parseFloat(query.lat))) vars.lat = parseFloat(query.lat);
		if(typeof(query.long)!=='undefined' && !isNaN(parseFloat(query.long))) vars.long = parseFloat(query.long);
		if(typeof(query.stock)!=='undefined' && !isNaN(parseInt(query.stock))) vars.stock = parseInt(query.stock);
		if(typeof(query.live)!=='undefined') vars.live = (query.live==true || query.live=='true'?true:false);
		if(typeof(query.password)!=='undefined' && query.password!=''){
			var pass = cryptojs.SHA256(query.password.toString()).toString(cryptojs.enc.Hex);
			var salt = cryptojs.lib.WordArray.random(8).toString(cryptojs.enc.Hex);
			var hash = cryptojs.SHA256(pass+salt).toString(cryptojs.enc.Hex);
			vars.salt = salt;
			vars.hash = hash;
		}
		// update
		await schemas.pubs.findByIdAndUpdate(query._id.toString(),vars).then(res => {
			result = true;
		}).catch(err => {
			console.log(err);
			throw new Error('query0');
		});
		// return
		if(result){
			return result;
		}else{
			throw new Error('query1');
		}
	// }catch(e){
	// 	throw new Error('query2');
	// }
}


/******************** UPDATE STATS ********************/
function update_stats(){
	console.log('Stats updating...'.magenta);
	var t1 = new Date();
	var promises = [
		schemas.users.find(),
		schemas.codes.find({
			redeemed: true
		}),
		schemas.codes.find({
			redeemed: true,
			user_id:null
		}),
	];
	return Promise.all(promises.map(p=>p.catch(e=>e))).then(function(res) {
		// results
		var users = res[0];
		var pints = res[1];
		var pints2 = res[2];
		// messenger
		var sms = 0;
		var email = 0;
		for(var i in users){
			if(users[i].mobile && users[i].mobile!=null && users[i].mobile!=''){
				sms++;
			}else{
				email++;
			}
		}
		// time
		var t2 = new Date();
		var dif = t2.getTime() - t1.getTime();
		console.log(('Stats updated in ' + dif + 'ms').magenta);
		// update
		return schemas.stats.findByIdAndUpdate('5ab8f09fb58199192f4b4766',{
			vouchers: sms + email,
			vouchers_sms: sms,
			vouchers_email: email,
			pints: (pints && pints.length?pints.length:0),
			pints2: (pints2 && pints2.length?pints2.length:0),
			time: dif
		},{
			new: true
		}).then(res => {
			// console.log(res);
			return true;
		}).catch(err => {
			console.log(err);
			throw 'update_stats.0';
		});
	}).catch(err => {
		console.log(err);
		throw 'update_stats.1';
	});
}


/******************** GET STATS ********************/
function get_stats(){
	var force_update = false;
	if(force_update){
		return update_stats().then(res => {
			return get_stats2();
		});
	}else{
		return get_collection('stats').then(res => {
			var t1 = new Date().getTime();
			var t2 = new Date(res[0].updated).getTime();
			var dif = (t1 - t2)/1000;
			if(dif <= 60){
				return get_stats2();
			}else{
				return update_stats().then(res => {
					return get_stats2();
				});
			}
		});
	}
}
function get_stats2(){
	var promises = [
		get_collection('pubs'),
		get_collection('stats'),
	];
	return Promise.all(promises.map(p=>p.catch(e=>e)));
}


/******************** EXPORT CODES ********************/
// function export_codes(total){
// 	var codes = [];
// 	schemas.codes.find({
// 		used: false
// 	}).sort({'$natural': -1}).limit(total).then(res => {
// 		for(var i in res){
// 			codes.push(res[i].code);
// 		}
// 		console.log(codes);
// 		schemas.codes.updateMany({
// 	    code: { $in: codes },
// 			used: false
// 		},{
// 			used: true
// 		}).then(res => {
// 			save_exported_codes();
// 		});
// 	})
// }
// function save_exported_codes(){
// 	var codes = [];
// 	schemas.codes.find({
// 		used: true,
// 		user_id: null,
// 		redeemed: {
// 			$ne: true
// 		}
// 	}).then(res => {
// 		console.log(res.length);
// 		for(var i in res){
// 			codes.push({
// 				code: res[i].code
// 			})
// 		}
// 		const Json2csvParser = require('json2csv').Parser;
// 		const json2csvParser = new Json2csvParser();
// 		const csv = json2csvParser.parse(codes);
// 		_utils.writeFile('../bin/codes.csv',csv,function(){
// 			console.log('Saved');
// 		});
// 	})
// }
// export_codes(10000);
// save_exported_codes();


/******************** EXPORTS ********************/
module.exports = {
	// auth
	login: login,
	// general
	get_collection: get_collection,
	get_document: get_document,
	// specific
	signup: signup,
	broadcast: broadcast,
	redeem: redeem,
	add_pub: add_pub,
	update_pub: update_pub,
	update_stats: update_stats,
	get_stats: get_stats,
	// get_drinkers: get_drinkers,
}








/******************** MONGOOSE EXAMPLES ********************/
setTimeout(function(){
	
	// IMPORT PUBS
	// schemas.pubs.remove({}).then(res => {
	// 	_utils.readFile('../bin/fpb2.csv', function (err, text) {
	// 
	// 		const csv = require('csvtojson')
	// 		csv({noheader:true})
	// 		.fromString(text)
	// 		.on('csv',(csvRow)=>{
	// 			/*
	// 			'Name',
	// 		  'Address 1 ',
	// 		  'Postcode',
	// 		  'Contact',
	// 		  'Contact Number',
	// 		  'MBC Contact',
	// 		  'Email',
	// 		  'Twitter',
	// 		  'Facebook',
	// 		  'Pretty\' Web Address',
	// 		  'Full Web Address',
	// 		  'KEGS required',
	// 		  'EIG Kegs (Josh)' ]
	// 			*/
	// 			// console.log({
	// 			// 	 'Name': csvRow[0],
	// 			//   'Address:': csvRow[1],
	// 			//   'Postcode': csvRow[2],
	// 			//   'Contact': csvRow[3],
	// 			//   'Contact Number': csvRow[4],
	// 			//   'MBC Contact': csvRow[5],
	// 			//   'Email': csvRow[6],
	// 			//   'Twitter': csvRow[7],
	// 			//   'Facebook': csvRow[8],
	// 			//   'Pretty\' Web Address': csvRow[9],
	// 			//   'Full Web Address': csvRow[10],
	// 			//   'Kegs 1': csvRow[11],
	// 			//   'Kegs 2': csvRow[12]
	// 			// });
	// 
	// 			var k1 = parseInt(csvRow[11]);
	// 			var k2 = parseInt(csvRow[12]);
	// 			var kegs = (!isNaN(k1)?k1:0) + (!isNaN(k2)?k2:0);
	// 			var vars = {
	// 				// main
	// 				name: csvRow[0],
	// 				address: csvRow[1],
	// 				postcode: csvRow[2],
	// 				twitter: csvRow[7],
	// 				facebook: csvRow[8],
	// 				web: csvRow[9],
	// 				web_full: csvRow[10],
	// 				// stock
	// 				kegs1: (!isNaN(k1)?k1:0),
	// 				kegs2: (!isNaN(k2)?k2:0),
	// 				stock: (kegs * 124),
	// 				// contact
	// 				contact: csvRow[3],
	// 				phone: (csvRow[4] && csvRow[4][0]!=0 && csvRow[4].length<11?'0'+csvRow[4]:csvRow[4]),
	// 				email: csvRow[6],
	// 				mbc: csvRow[5],
	// 			}
	// 			var new_pub = new schemas.pubs(vars);
	// 			new_pub.save().then(res => {
	// 			}).catch(err => {
	// 				console.log('error');
	// 				console.log(err);
	// 			});
	// 		}).on('done',()=>{
	// 		  console.log('done');
	// 		});
	// 	});
	// });
	
	
	// UPDATE KEGS
	// get_collection('pubs').then(res => {
	// 	for(var i in res){
	// 		var id = res[i]._id;
	// 		var k = res[i].kegs1 + res[i].kegs2;
	// 		k = k * 88;
	// 		schemas.pubs.findByIdAndUpdate(id,{
	// 			stock: k
	// 		}).then(res => {
	// 			console.log('updated');
	// 		});
	// 		// console.log(id,k);
	// 	}
	// });
	
	
	// // RESET PUB PASSWORDS
	// get_collection('pubs').then(res => {
	// 	for(var i in res){
	// 		var id = res[i]._id;
	// 		set_password('pubs',id,'givemebeer26');
	// 	}
	// });
	
	
	// // RENAME COLLECTION
	// let db = mongoose.connection.db;
  // return db.collection('pubs').rename('_old_pubs');
	
	
	// // DELETE USERS
	// schemas.users.remove({}).then(res => {});
	
	
	// // RESET ASSIGNED CODES
	// schemas.codes.updateMany({
	// 	used: true
	// },{
	// 	used: false,
	// 	used_date: null,
	// 	user_id: null
	// }).then(res => {});
	
	
	// // RESET REDEEMED PUBS
	// schemas.pubs.updateMany({},{
	// 	redeemed: 0
	// }).then(res => {});
	
	
	// // RESET REDEEMED CODES
	// schemas.codes.updateMany({
	// 	redeemed: true
	// },{
	// 	redeemed: false,
	// 	redeemed_date: null,
	// 	redeemed_pub: null
	// }).then(res => {});
	
	
	// // DUPLICATE DOCS
	// var pubs = [];
	// schemas.pubs.find({}).then(res => {
	// 	console.log(res);
	// 	for(var i in res){
	// 		var res2 = parse_result(res[i]);
	// 		pubs.push(res2._id);
	// 	}
	// }).then(res => {
	// 	for(var i in pubs){
	// 		schemas.pubs.findById(pubs[i]).exec(
	// 			function(err, res) {
	// 				res = parse_result(res);
	// 				delete res._id;
	// 				var new_pub = new schemas.pubs(res);
	// 				new_pub.save().then(res => {
	// 					console.log('added');
	// 				}).catch(err => {
	// 					console.log('error');
	// 				});
	// 			}
	// 		);
	// 	}
	// });
	
	
	// GENERATE ADMIN
	// var myAdmin = new schemas.admins({
	// 	_id: new mongoose.Types.ObjectId()
	// });
	// myAdmin.save().then(res => {
	// 	console.log('Admin successfully saved.');
	// }).catch(err => {
	// 	throw err;
	// });
	
	
	// GENERATE STATS
	// var myStats = new schemas.stats();
	// myStats.save().then(res => {
	// 	console.log('Admin successfully saved.');
	// }).catch(err => {
	// 	throw err;
	// });
	
	
	// UPDATE CODE FIELDS
	// function update_fields(){
	// 	console.log('starting'.magenta);
	// 	schemas.codes.update({
	// 		_id: mongoose.Types.ObjectId('5aa297e13abd54722261be1f')
	// 	}, {
	// 		redeemed: false,
	// 		redeemed_date: null,
	// 		redeemed_pub: null
	// 	}, {multi: true}).then(res => {
	// 		console.log('finished'.magenta);
	// 		console.log(res);
	// 	}).catch(err => {
	// 		console.log('finished'.magenta);
	// 		console.log(err);
	// 	});
	// }
	// update_fields();
	
	
	// // GET ALL DOCUMENTS
	// Admin.find().then(res => {
	// 	console.log(res);
	// }).catch(err => {
	// 	console.log(err);
	// });
	// 
	// 
	// 
	// 
	// var myUser = new User({
	// 	_id: new mongoose.Types.ObjectId(),
	// 	firstName: 'Alex',
	// 	lastName: 'Bimpson',
	// });
	// 
	// myUser.save().then(res => {
	// 	console.log('User successfully saved.');
	// }).catch(err => {
	// 	throw err;
	// });
	// 
	// 
	// 
	// 
	// User.findByIdAndUpdate('5a9d509cf92e0d45d0b2f06f', {
	// 	lastName: 'newName'
	// }, 
	// function(err, author) {
	// 	if (err) throw err;
	// 
	// 	console.log(author);
	// });
	// 
	// var devSchema = mongoose.Schema({
	// 	name: String,
	// 	address: String,
	// 	postcode: String,
	// 	phone: String,
	// 	field1: String,
	// 	field2: String,
	// 	field3: String,
	// 	field4: String,
	// 	field5: String,
	// 	live: Boolean
	// },{
	// 	collection: 'dev'
	// });
	// var Dev = mongoose.model('Dev', devSchema);
	// 
	// var myUser = new Dev({
	// 	name: 'Firstname Lastname',
	// 	address: '18 Queen Square, Bath',
	// 	postcode: 'BA1 2HN',
	// 	phone: '01234 567890',
	// 	field1: 'Lorem ipsum dolor',
	// 	field2: 'Lorem ipsum dolor',
	// 	field3: 'Lorem ipsum dolor',
	// 	field4: 'Lorem ipsum dolor',
	// 	field5: 'Lorem ipsum dolor',
	// 	live: Math.random() >= 0.5
	// });
	// myUser.save().then(res => {
	// 	console.log('User successfully saved.');
	// }).catch(err => {
	// 	throw err;
	// });
	// 
	// console.log('starting');
	// var t1 = new Date();
	// Dev.find({
	// 	live: true
	// }).then(res => {
	// 	var t2 = new Date();
	// 	var dif = t2.getTime() - t1.getTime();
	// 	console.log(res.length + ' results found in ' + dif + 'ms');
	// }).catch(err => {
	// 	console.log('find error'.red);
	// });
	// 
	// Dev.findById('5a9d2ae306119631f7d9ac94').then(res => {
	// 	console.log(res);
	// }).catch(err => {
	// 	console.log('find error'.red);
	// });
	// 
	// Dev.find({
	// 	live: true
	// }).then(res => {
	// 	console.log(res);
	// }).catch(err => {
	// 	console.log('find error'.red);
	// });
	
	
	
	// GENERATE CODES
	// for(var i = 0;i<1000;i++){
	// 	var myCode = new schemas.codes({
	// 		code: 'A'+(1000+i),
	// 		cdn_path: 'A'+(1000+i)+'.jpg'
	// 	});
	// 	myCode.save().then(res => {
	// 		console.log('done');
	// 	});
	// }
	
	
	// POPULATE TEST
	// schemas.codes.findOne({
	// 	code: 'A1000'
	// }).populate('user_id').then(res => {
	// 	console.log(res);
	// });
	
	
	// POPULATE STATS
	// var s = new schemas.stats({"vouchers":1,"vouchers_messenger":2,"vouchers_email":3,"pints":4,"time":0});
	// s.save();
	
	
	// REMOVE DATA
	// schemas.users.updateMany({
	// 	mobile: {
	// 		$ne: null
	// 	}
	// },{
	// 	mobile: 'mobile'
	// }).then(res => {
	// 	console.log('done');
	// });
	
	
},500);










