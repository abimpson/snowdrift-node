


/******************** REQUIRES ********************/
const colors = require('colors');
const io = require('socket.io');
const _api = require('./api');
const _config = require('./../_config/config');


/******************** DATA ********************/
var sockets = {};


/******************** CREATE SOCKET ********************/
function create(id,server){
  if(typeof(sockets[id])==='undefined'){
    // new socket
    sockets[id] = new io(server); //new io(_config.socket.port,server);
    var socket = sockets[id];
    // connection
    socket.sockets.on('connection',function(s){
      // send init
      s.emit('init',{
        'status': 'ok'
      });
      // standard calls
      s.on('ping',function(data){
        s.emit('ping',{});
      });
      s.on('close',function(data){
        socket.sockets.emit('close',{});
        socket.server.close();
      });
      s.on('join',function(room){
        s.join(room);
        s.emit('join',{
          room:room
        });
      });
      s.on('leave',function(room){
        s.leave(room);
      });
      s.on('test',function(data){
        s.emit('test',{
          data: data
        });
      });
      s.on('api',function(data){
        var method = data.method;
        var query = data.query;
        var callback = function(json){
          s.emit('response',{
            data: json
          });
        }
        _api.request(method,query,callback);
      });
    });
    // log
    console.log(('SOCKET: http://'+_config.server.ip+':'+_config.server.port+' ('+id+')').bgGreen);
    // return
    return socket;
  }else{
    return sockets[id];
  }
}


/******************** GET SOCKET ********************/
function get(id){
  if(typeof(sockets[id])==='undefined'){
    return null;
  }else{
    return sockets[id];
  }
}


/******************** EXPORTS ********************/
module.exports = {
  create: create,
  get: get
}


