


/******************** REQUIRES ********************/
const _config = require('./../_config/config');
const colors = require('colors');
const fs = require('fs');
const os = require('os');


/******************** DATA ********************/
var data = {
  ip: null
};


/******************** GET IP ADDRESS ********************/
exports.get_ip = function(){
  if(data.ip==null){
    var interfaces = os.networkInterfaces();
    var addresses = [];
    for (var k in interfaces) {
      for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
          addresses.push(address.address);
        }
      }
    }
    if(addresses.length>0){
      if(addresses.indexOf('192.168.0.200')>=0){
        data.ip = '192.168.0.200';
      }else{
        data.ip = addresses[0];
      }
    }
  }
  return data.ip;
};


/******************** READ FILE ********************/
// exports.readFile = function(path,callback) {
// 	try {
// 		var filename = require.resolve(path);
// 		fs.readFile(filename, 'utf8', callback);
// 	} catch (e) {
// 		callback(e);
// 	}
// }


/******************** WRITE FILE ********************/
// exports.writeFile = function(path,data,callback) {
// 	try {
// 		var filename = require.resolve(path);
// 		fs.writeFile(filename, data, 'utf8', callback);
// 	} catch (e) {
// 		callback(e);
// 	}
// }


/******************** PARSE EMAIL ********************/
// exports.email = {
// 	parse: {
// 		text: function(text,vars){
// 			return exports.replaceLabels(text,vars);
// 		},
// 		html: function(html,vars,src) {
// 			html = exports.replaceLabels(html,vars);
// 			html = html.replace(/src="/g,'src="'+src);
// 			console.log(src);
// 			return html;
// 		}
// 	}
// }


/******************** REPLACE LABELS ********************/
// exports.replaceLabels = function(html,vars) {
// 	for(var i in vars){
// 		var pattern = '{{'+i+'}}';
// 		var regex = new RegExp(pattern, "g");
// 		html = html.replace(regex,vars[i]);
// 	}
// 	return html;
// }


/******************** VALIDATE EMAIL ********************/
// exports.isValidEmail = function(e) {
// 	if(e.trim()!=''){
// 		var regExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// 		return regExp.test(e);
// 	}else{
// 		return false;
// 	};
// };


/******************** VALIDATE PHONE ********************/
// exports.isValidNumber = function(number){
// 	if(number.trim()!=''){
// 		var regExp = /^\d+$/;
// 		return regExp.test(number);
// 	}else{
// 		return false;
// 	};
// };
// exports.cleanPhone = function(phone){
// 	return phone.trim().replace(/\+/g,'').replace(/\-/g,'').replace(/\(/g,'').replace(/\)/g,'').replace(/ /g,'');
// };
// exports.isValidPhone = function(phone){
// 	if(phone.trim()!=''){
// 		var cleaned = phone.replace(/\+/g,'').replace(/\-/g,'').replace(/\(/g,'').replace(/\)/g,'').replace(/ /g,'');
// 		if(exports.isValidNumber(cleaned)){
// 			return true;
// 		}else{
// 			return false;
// 		};
// 	}else{
// 		return false;
// 	};
// };


/******************** GET DATE ********************/
// exports.get_date = function(){
// 	var today = new Date();
// 	var dd = today.getDate();
// 	var mm = today.getMonth()+1;
// 	var yyyy = today.getFullYear();
// 	return yyyy+'-'+mm+'-'+dd;
// };




