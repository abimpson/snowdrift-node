


/******************** REQUIRES ********************/
const _config = require('./../_config/config');
const _utils = require('./utils');
const colors = require('colors');
const nodemailer = require('nodemailer/lib/mail-composer');
const mailgun = require('mailgun-js')({
	apiKey: _config.mailgun.api_key,
	domain: _config.mailgun.domain
});


/******************** SERVER RESTART ********************/
function restart(){
	if(_config.env==='production'){
		var time = new Date().toLocaleString();
		var data = {
			from: 'Free Proper Beer <mail@freeproperbeer.com>',
			to: 'alex@kerve.co.uk',
			subject: 'Free Proper Beer - server restarted at '+time,
			text: 'Free Proper Beer - server restarted at '+time
		};
		mailgun.messages().send(data, (err, res) => {
			console.log(res);
		});
	}
}


/******************** SCHEDULE ********************/
function schedule(){
	if(_config.env==='production'){
		var time = new Date().toLocaleString();
		var data = {
			from: 'Free Proper Beer <mail@freeproperbeer.com>',
			to: 'alex@kerve.co.uk',
			subject: 'Free Proper Beer - tasks executed at '+time,
			text: 'Free Proper Beer - tasks executed at '+time
		};
		mailgun.messages().send(data, (err, res) => {
			console.log(res);
		});
	}
}


/******************** SIGNUP ********************/
function signup(opts){
	if(_config.mailgun.enabled){
		_utils.readFile('../public/email/signup/plaintext.txt', function (err, text) {
			_utils.readFile('../public/email/signup/index.html', function (err, html) {
				console.log('sending signup email'.magenta);
				// options
				var options = {
					from: 'Free Proper Beer <mail@freeproperbeer.com>',
					to: opts.to,
					subject: 'Your Free Proper Beer Code',
					text: _utils.replaceLabels(text,{
						code: opts.code
					}),
					html: _utils.replaceLabels(html,{
						code: opts.code,
						img: opts.img,
						path: _config.root+'/email/signup/'
					})
				};
				// create mail
				var mail = new nodemailer(options);
				// send mail
				mail.compile().build((err, message) => {
					var mailData = {
						to: opts.to,
						message: message.toString('ascii')
					};
					mailgun.messages().sendMime(mailData, (err, res) => {
						console.log(res);
					});
				});
			});
		});
	}
}
// signup({
// 	to: 'alex@kerve.co.uk',
//  code: 12345
// });


/******************** BATCH EMAIL ********************/
function batch(){
	if(_config.mailgun.enabled){
		_utils.readFile('../public/email/signup/plaintext.txt', function (err, text) {
			_utils.readFile('../public/email/signup/index.html', function (err, html) {
				
				// recipients
				var v = 1;
				var recipients = ['alex@kerve.co.uk', 'abimpson@gmail.com', 'alex@bimpson.com'];
				var recipientVars = {
					'alex@kerve.co.uk': {
						id: 1,
						email: 'alex@kerve.co.uk',
						subject: 'Subject A '+v
					},
					'abimpson@gmail.com': {
						id: 2,
						email: 'abimpson@gmail.com',
						subject: 'Subject B '+v
					},
					'alex@bimpson.com': {
						id: 3,
						email: 'alex@bimpson.com',
						subject: 'Subject C '+v
					}
				};
				
				// var data = {
				// 	from: 'Free Proper Beer <mail@freeproperbeer.com>',
				// 	to: recipients,
				// 	subject: 'Your Free Proper Beer Code - %recipient.subject%',
				// 	text: 'Your Free Proper Beer Code - %recipient.subject%',
				// 	'recipient-variables': recipientVars,
				// };
				// mailgun.messages().send(data, (err, res) => {
				// 	console.log(res);
				// });
				
				// options
				var options = {
					from: 'Free Proper Beer <mail@freeproperbeer.com>',
					to: recipients,
					// to: '%recipient.to%',
				
					'recipient-variables': recipientVars,
					// 'recipient-variables': JSON.stringify(rvars),
				
					subject: 'Your Free Proper Beer Code - %recipient.subject%',
					text: _utils.replaceLabels(text,{
						code: 12345
					}),
					html: _utils.replaceLabels(html,{
						code: 12345,
						path: _config.root+'/email/signup/'
					}),
					headers: {
						'X-Mailgun-Recipient-Variables': JSON.stringify(recipientVars)
					}
				};
				// create mail
				var mail = new nodemailer(options);
				// send mail
				mail.compile().build((err, message) => {
					var mailData = {
						to: recipients, //options.to,
						message: message.toString('ascii'),
						'recipient-variables': recipientVars
					};
					mailgun.messages().sendMime(mailData, (err, res) => {
						console.log(res);
					});
				});
				
			});
		});
	}
}
// batch();


/******************** EXPORTS ********************/
module.exports = {
	restart: restart,
	schedule: schedule,
	signup: signup
}







