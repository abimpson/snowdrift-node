


/******************** REQUIRES ********************/
var electron = require('electron');
var path = require('path');
var url = require('url');
var os = require('os');
var common = require('./lib/common.js');
var myApp = require('./app.js');


/******************** CONFIG ********************/
var config = {
	name: 'Huawei',
	open_dev_tools: false,
	open_links_in_browser: true
}


/******************** APP ********************/
var app = electron.app; // Module to control application life.
var BrowserWindow = electron.BrowserWindow; // Module to create native browser window.
var Menu = electron.Menu;
var MenuItem = electron.MenuItem;

// SET LOCAL STORAGE PATH
app.setPath('userData',app.getPath('appData') + '/' + config.name);


/******************** DATA ********************/
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow;

//electron.webFrame.setZoomLevelLimits(1, 1);

//app.commandLine.appendSwitch('--enable-viewport-meta', 'true');

/*var webFrame = electron.webFrame;
webFrame.setVisualZoomLevelLimits(1, 1);
webFrame.setLayoutZoomLevelLimits(0, 0);*/


/******************** CREATE WINDOW ********************/
function createWindow () {
	
	// CREATE BROWSER WINDOW
	mainWindow = new BrowserWindow({
		kiosk: false, //true,
		width:1024, //1024,
		height:870,
		// frame: false,
		// titleBarStyle: 'hiddenInset', // hidden, hiddenInset
		// webPreferences: {
		// 	nodeIntegration: false
		// 	webSecurity: false
		// }
	});
	
	// LOAD INITIAL CONTENT
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'public/index.html'), //common.get_ip()+':3000',
		protocol: 'file:', // file, http, https
		slashes: true
	}));
	
	// DEFINE MENUS
	var menuTemplate = [
		{
			label: config.name,
			submenu: [
				{
					label: 'Close Window',
					accelerator: 'Cmd+W',
					click: function(){
						app.quit();
					}
				},
				{
					label: 'Quit',
					accelerator: 'Cmd+Q',
					click: function(){
						app.quit();
					}
				}
			]
		},
		{
			label: "Edit",
			submenu: [
				{ label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
				{ label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
				{ type: "separator" },
				{ label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
				{ label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
				{ label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
				{ label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
			]
		},
		{
			label: 'Developer',
			submenu: [
				{
					role: 'reload'
				},
				{
					role: 'forcereload'
				},
				{
					role: 'toggledevtools'
				}
			]
		}
	];
	
	// APPLY MENU
	var menu = Menu.buildFromTemplate(menuTemplate);
	Menu.setApplicationMenu(menu);
	
	// OPEN DEV TOOLS
	if(config.open_dev_tools){
		mainWindow.webContents.openDevTools();
	}
	
	// OPEN LINKS IN NEW WINDOW
	if(config.open_links_in_browser){
		console.log('new-window');
		mainWindow.webContents.on('new-window', function(event, url){
			event.preventDefault();
			var shell = electron.shell;
			shell.openExternal(url);
		});
	}
	
	// QUIT ON WINDOW CLOSE
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null;
		app.quit();
	});
	
	// SEND SESSION DETAILS TO BROWSER WINDOW
	mainWindow.webContents.on('did-finish-load', function() {
		sendIP();
		setTimeout(function(){
			sendIP();
		},1000);
	});
	function sendIP(){
		try{ mainWindow.webContents.send('message',{
			ip: common.get_ip(),
			host: os.hostname()
		}); }catch(e){};
	}
	
}


/******************** APP STATES ********************/
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow();
	}
});




