<?php


/********** HEADERS **********/
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){
	
}else{
	header("content-type: application/json; charset=utf8");
}
//DEFINE OUTPUT ARRAY
$myArray = Array();


include('XML2Array.php');






/********** PROCESS **********/
if(isset($_GET['song'])){
	$applicationID = '1940a4b6';
	$applicationKey = '3e1475251543805d81820af9c3072459';
	//URL
	$url = "http://api.decibel.net/v1/Tracks/?trackTitle=".$_GET['song'];
	//CURL QUERY
	$ch = curl_init($url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$headers = array(
		'DecibelAppID: ' . $applicationID,
		'DecibelAppKey: ' . $applicationKey,
		'DecibelTimestamp: ' . date('Ymd H:i:s', time())
	);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	//RESPONSE
	if(curl_exec($ch) === false) {
		$myArray['success'] = 0;
		$myArray['message'] = 'Curl error';
	} else {
		$response = curl_exec($ch);
		$myArray['success'] = 1;
		$myArray['response'] = XML2Array::createArray($response);
	}
	curl_close ($ch);
}else{
	$myArray['success'] = 0;
	$myArray['message'] = 'No song parameter supplied';
}










/********** OUTPUT **********/
//echo $response;
echo json_encode($myArray);




?>