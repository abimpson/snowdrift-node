/*

	AUTHOR:		Alex Bimpson
	NAME:		Prrple Scroll Trigger
	VERSION:	1.1
	UPDATED:	2014-05-07

*/


/************************************************************************************************************/
/****************************************** PRRPLE - SCROLL TRIGGER *****************************************/
/************************************************************************************************************/


var prrpleTrigger = {
	
	
	/******************** CONFIG ********************/
	config:{
		facebook_canvas_detect_interval: 200
	},
	
	
	/******************** DATA ********************/
	data:{
		init: false,
		window_size: null,
		win_w: null,
		win_h: null,
		scrolled_y: null,
		visible_y: null
	},
	
	
	/******************** WINDOW SIZE ********************/
	window_size: function(override){
		clearTimeout(prrpleTrigger.data.window_size);
		prrpleTrigger.data.window_size = setTimeout(prrpleTrigger.do_window_size,100);
	},
	do_window_size: function(){
		prrpleTrigger.data.win_w = $(window).width();
		prrpleTrigger.data.win_h = $(window).height();
	},
	
	
	/******************** DETECT SCROLLING ********************/
	scroll_detect: function(){
		if(window.self === window.top){
			prrpleTrigger.data.scrolled_y = $(window).scrollTop();
			prrpleTrigger.data.visible_y = prrpleTrigger.data.scrolled_y + prrpleTrigger.data.win_h;
			prrpleTrigger.scroll();
		}
	},
	
	
	/******************** DETECT FACEBOOK CANVAS SCROLLING ********************/
	facebook: function(){
		//TODO - detect canvas
		if(window.self != window.top){
			window.setInterval(function(){
				FB.Canvas.getPageInfo(function(response){
					prrpleTrigger.data.scrolled_y = response.scrollTop;
					prrpleTrigger.data.visible_y = prrpleTrigger.data.scrolled_y + response.clientHeight - response.offsetTop;
					prrpleTrigger.scroll();
				});
			},prrpleTrigger.config.facebook_canvas_detect_interval);
		};
	},
	
	
	/******************** SCROLL ********************/
	scroll: function(){
		//FUNCTIONS ONCE
		for(var i in prrpleTrigger.triggers){
			if(!prrpleTrigger.triggers[i].done){
				if(prrpleTrigger.data.visible_y >= prrpleTrigger.triggers[i].min_x){
					prrpleTrigger.triggers[i].func();
					prrpleTrigger.triggers[i].done = true;
				}
			}
		}
	},
	
	
	/******************** TRIGGERS ********************/
	triggers:[],
	triggers_clear: function(){
		prrpleTrigger.triggers = [];
	},
	
	
	/******************** ADD TRIGGERS ********************/
	add: function(min_x,func){
		prrpleTrigger.triggers.push({
			min_x:min_x,
			func:func
		});
		prrpleTrigger.scroll_detect();
	}
	
	
}





/************************************************************************************************************/
/************************************************ INITIALISE ************************************************/
/************************************************************************************************************/


$(window).resize(function(){
	prrpleTrigger.window_size();
	prrpleTrigger.scroll_detect();
});
$(window).load(function(){
	prrpleTrigger.do_window_size();
	prrpleTrigger.facebook();
});
$(document).ready(function(){
	prrpleTrigger.do_window_size();
	$(window).bind('scroll',function(e){
		prrpleTrigger.scroll_detect();
	});
});




