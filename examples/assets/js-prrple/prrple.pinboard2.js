// JavaScript Document
/*
	(c) ALEX BIMPSON
	
	AUTHOR: ALEX BIMPSON
	TITLE: PINBOARD LAYOUT
	VERSION: v1.0
	UPDATED: 2012-10-02
*/

(function($){
	$.fn.pinBoard = function(options){
		
		
		/********** CONFIG *********/
		
		var DEFAULTS = {
			col_width:200,
			v_spacing:15,
			h_spacing:15
		};
		if(options){
			$.extend(DEFAULTS, options);
		}
		
		var PINBOARD = $(this);
		
		var NO_OF_COLS = 0;
		var COL_ARRAY = new Array();
		
		
		
		/********** INIT *********/
		
		$(document).ready(function(){
			//SET COL WIDTHS
			$('.pin_item').each(function(){
				$(this).css('width',DEFAULTS.col_width);
			});
			//POSITION COLS
			adjustLayout();
		});
		
		$(window).resize(function(){
			adjustLayout();
		});
		
		
		
		/********** CREATE LAYOUT *********/
		
		function adjustLayout(){
			//CLEAR PINBOARD WIDTH
			PINBOARD.removeAttr('style');
			
			//CALCULATE NO OF COLS
			var pinboard_width = PINBOARD.width();
			var noOfCols = (pinboard_width/DEFAULTS.col_width).toString();
			var noOfCols2 = noOfCols.split('.');
			var noOfCols3 = parseInt(noOfCols2[0]);
			var totalWidth = (noOfCols3 * DEFAULTS.col_width) + (noOfCols3 * DEFAULTS.h_spacing) - DEFAULTS.h_spacing;
			if(totalWidth > pinboard_width){
				NO_OF_COLS = noOfCols3-1;
			}else{
				NO_OF_COLS = noOfCols3;
			}
			
			//CREATE COLUMN ARRAY
			COL_ARRAY = new Array();
			for(i=0;i<NO_OF_COLS;i++){
				COL_ARRAY[i]=0;
			}
			
			//POSITION EACH pin_item
			$('.pin_item').each(function(){
				//GET SHORTEST COL
				var height = $(this).height();
				var minCol = 0;
				var minHeight = 0;
				for(var i in COL_ARRAY){
					if(i == 0 || COL_ARRAY[i] < minHeight){
						minHeight = COL_ARRAY[i];
						minCol = i;
					}
				}
				//UPDATE ARRAY
				COL_ARRAY[minCol] = COL_ARRAY[minCol] + DEFAULTS.v_spacing + height;
				//CALCULATE NEW POSITION
				var x = (minCol * DEFAULTS.col_width) + ((minCol-1)*DEFAULTS.h_spacing) + DEFAULTS.h_spacing;
				var y = minHeight;
				$(this).css({
					'left':x,
					'top':y
				});
				//CALCULATE PINBOARD DIMENSIONS
				var maxCol = 0;
				var maxHeight = 0;
				for(var i in COL_ARRAY){
					if(i == 0 || COL_ARRAY[i] > maxHeight){
						maxHeight = COL_ARRAY[i];
					}
				}
				//UPDATE PINBOARD DIMENSIONS
				PINBOARD.css({
					'height':maxHeight-DEFAULTS.v_spacing,
					'width':(NO_OF_COLS * DEFAULTS.col_width) + (NO_OF_COLS * DEFAULTS.h_spacing) - DEFAULTS.h_spacing
				});
			});
		}
		
	}
})(jQuery);