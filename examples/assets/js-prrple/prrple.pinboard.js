// JavaScript Document



/*
	(C) ALEX BIMPSON
	
	AUTHOR:		ALEX BIMPSON
	NAME:		PINBOARD
	VERSION:	1.0
	UPDATED:	2012-10-02
*/


/*
	TODO
	- detect if window dimensions have changed (width)
	- don't reposition if already positioned
	- perfect delays
*/



var PINBOARD_ADJUSTING = false;
(function($){
	$.fn.pinBoard = function(options){
		
		/********** CONFIG *********/
		var DEFAULTS = {
			pin_item:		'pin_item',
			col_width:		250,	//width of columns
			v_spacing:		15,		//spacing between items
			h_spacing:		15,		//spacing between items
			side_spaces:	false	//include spacing on outer edges
		};
		if(options){
			$.extend(DEFAULTS, options);
		}
		var PINBOARD = $(this);
		var PINBOARD_NO_COLS = 0;
		var PINBOARD_ARRAY = new Array();
		var PINBOARD_DELAY = 500;
		
		/********** INIT *********/
		$(document).ready(function(){
			//SET COL WIDTHS
			$('.' + DEFAULTS.pin_item).each(function(){
				$(this).css('width',DEFAULTS.col_width);
			});
			//POSITION COLS
			adjustLayout();
		});
		
		/********** DETECT RESIZES *********/
		$(window).resize(function(){
			adjustLayout();
		});
		
		/********** CREATE LAYOUT *********/
		function adjustLayout(loop){
			if(PINBOARD_ADJUSTING==true){
				if(!loop){loop=0;}
				if(loop<2){
					loop++;
					setTimeout(function(){
						adjustLayout(loop);
					},PINBOARD_DELAY+100);
				}
			}else{
				//TIMERS
				PINBOARD_ADJUSTING = true;
				setTimeout(function(){
					PINBOARD_ADJUSTING = false;
				},PINBOARD_DELAY);
				
				//CALCULATE NO OF COLS
				var maxWidth = PINBOARD.parent().width();
				var noOfCols = (maxWidth/DEFAULTS.col_width).toString();
				var noOfCols2 = noOfCols.split('.');
				var noOfCols3 = parseInt(noOfCols2[0]);
				var totalWidth = (noOfCols3 * DEFAULTS.col_width) + (noOfCols3 * DEFAULTS.h_spacing) + (DEFAULTS.side_spaces==true?+DEFAULTS.h_spacing:-DEFAULTS.h_spacing);
				if(totalWidth > maxWidth){
					PINBOARD_NO_COLS = noOfCols3-1;
				}else{
					PINBOARD_NO_COLS = noOfCols3;
				}
				
				//CREATE COLUMN ARRAY
				PINBOARD_ARRAY = new Array();
				for(i=0;i<PINBOARD_NO_COLS;i++){
					PINBOARD_ARRAY[i]=0;
				}
				
				//POSITION EACH pin_item
				$('.' + DEFAULTS.pin_item).each(function(){
					//GET SHORTEST COL
					var height = $(this).height();
					var minCol = 0;
					var minHeight = 0;
					for(var i in PINBOARD_ARRAY){
						if(i == 0 || PINBOARD_ARRAY[i] < minHeight){
							minHeight = PINBOARD_ARRAY[i];
							minCol = i;
						}
					}
					//UPDATE ARRAY
					PINBOARD_ARRAY[minCol] = PINBOARD_ARRAY[minCol] + DEFAULTS.v_spacing + height;
					//CALCULATE NEW POSITION
					var x = (minCol * DEFAULTS.col_width) + ((minCol-1)*DEFAULTS.h_spacing) + DEFAULTS.h_spacing;
					var y = minHeight;
					$(this).css({
						'left':x,
						'top':y
					});
					//LABEL COL NO
					for(k=0;k<100;k++){
						$(this).removeClass('pin_column_'+k);
					}
					var colNo = 'pin_column_'+minCol;
					$(this).addClass(colNo);
				});
				
				//CALCULATE PINBOARD DIMENSIONS
				var maxCol = 0;
				var maxHeight = 0;
				for(var i in PINBOARD_ARRAY){
					if(i == 0 || PINBOARD_ARRAY[i] > maxHeight){
						maxHeight = PINBOARD_ARRAY[i];
					}
				}
				
				//UPDATE PINBOARD DIMENSIONS
				var actual_PINBOARD_NO_COLS = $('.' + DEFAULTS.pin_item).length;
				if(PINBOARD_NO_COLS > actual_PINBOARD_NO_COLS){
					PINBOARD_NO_COLS = actual_PINBOARD_NO_COLS;
				}
				var newWidth = (PINBOARD_NO_COLS * DEFAULTS.col_width) + (PINBOARD_NO_COLS * DEFAULTS.h_spacing) - DEFAULTS.h_spacing;
				PINBOARD.css({
					'height':maxHeight-DEFAULTS.v_spacing,
					'width':newWidth
				});
			}
		}
	}
})(jQuery);