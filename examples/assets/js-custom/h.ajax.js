

var ap = ap || {};
ap.ajax = function(options){
	
	
	//OPTIONS
	var config = {
		url: '',				//url to fetch
		data: {},				//parameters to pass
		dataType: 'json',		//format of data beign fetched (json,xml etc)
		method: 'POST',			//ajax method (POST or GET)
		callback: function(){},	//callback function after AJAX
		callback_vars: false,	//additional vars to pass to the callback function
		retry: 0,				//stores how many times the query has been retried (don't edit this)
		timeout: 20000,			//ajax function timeout limit
		limit: 1,				//how many times to retry after a timeout
		error: false,			//is this an error log?
		crossdomain: true		//cross domain?
	};
	$.extend(config, options);
	
	//PREVENT CACHING
	$.ajaxSetup({cache:false});
	
	//RETRY
	config.retry++;
	
	//AJAX CALL
	$.ajax({
		type:			config.method,
		url:			config.url,
		data:			config.data,
		dataType:		config.dataType,
		async:			true,
		timeout:		config.timeout,
		crossDomain:	config.crossdomain,
		//SUCCESS
		success: function(response) {
			config.callback(response,config.callback_vars);
			/*if(response.error && response.error.length>0){
				try{
					ap.errors.response_error(response);
					ap.errors.log_error('AJAX Response Error');
				}catch(e){};
			};*/
		},
		//ERROR
		error: function(xhr, status, error){
			//alert(xhr.responseText);
			if(config.error==false){
				if(config.retry < config.limit){
					ap.ajax(config)
				}else{
					if(status == 'timeout'){
						try{
							ap.errors.timeout_error(config.url);
							ap.errors.log_error({
								message: 'AJAX Timeout Error',
								url: config.url
							});
						}catch(e){};
					}else{
						try{
							ap.errors.ajax_error(config.url);
							ap.errors.log_error({
								message: 'AJAX Response Error',
								url: config.url
							});
						}catch(e){};
					};
				};
			};
		}
	});
	
	
};