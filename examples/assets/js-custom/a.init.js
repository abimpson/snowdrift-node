


var ap = ap || {};
ap.init = {
	
	
	/******************** DOCUMENT READY ********************/
	ready: function(){
		ap.m.app.ready = true;
		//START CHECKS
		if(ap.m.device.ie7==false){
			try{ap.init.checks.start();}catch(e){};
		};
	},
	
	
	/******************** DOCUMENT LOADED ********************/
	loaded: function(){
		ap.m.app.loaded = true;
	},
	
	
	/******************** LAUNCH APP ********************/
	launched: false,
	launch_timeout: null,
	launch: function(){
		if(typeof(ap.auth)!=='undefined'){
			if(ap.m.app.loaded==true && ap.preload.done>=ap.preload.total){
				if(
					FACEBOOK.got_status==true //got login status from facebook
					&& ap.init.checks.status=='finished' //init checks finished
					&& (ap.auth.checks.status=='finished' || ap.auth.checks.status=='no_user') //auth checks finished
				){
					ap.init.launch2();
				};
			}else{
				clearTimeout(ap.init.launch_timeout);
				ap.init.launch_timeout = setTimeout(function(){
					ap.init.launch();
				},50);
			};
		}else{
			ap.init.launch2();
		};
	},
	launch2: function(){
		console.log('%c*** LAUNCH ***','color:#0097c9;');
		ap.all.hide_loader();
	},
	
	
	
	
	
	/****************************************************************************************/
	/**************************************** CHECKS ****************************************/
	/****************************************************************************************/
	
	
	/******************** CHECKS ********************/
	checks: {
		current: 0,
		status: null, //null, started, finished
		recurring_func: null,
		//START CHECKS
		start: function(){
			if(!ap.m.device.ie8){
				if(ap.init.checks.status == null){
					ap.init.checks.status = 'started';
					ap.init.checks.continuex();
				};
			};
		},
		//CONTINUE CHECKS
		continuex: function(){
			ap.init.checks.current++;
			ap.init.checks.goto(ap.init.checks.current);
		},
		//GO TO CHECK
		goto: function(check){
			ap.init['check'+check]();
		},
		//RECHECK
		recheck: function(check){
			ap.init.checks.current = check;
			ap.init.checks.goto_check(ap.init.checks.current);
		},
	},
	
	
	/******************** RECURRING CHECKS ********************/
	/*recurring: function(){
		clearInterval(ap.checks.recurring_func);
		ap.checks.recurring_func = setInterval(function(){
			
		},30000);
	},*/
	
	
	/******************** INIT - CHECK 1 - INVITATION ********************/
	check1: function(){
		console.log('--- INIT - CHECK 1 - INVITATION ---');
		console.log(ap.m.app_data);
		/*if(ap.m.app_data.set==true){
			//ap.init.checks.continuex();
			ap.ajax({
				url: ap.config.api_root+'user.getDetails',
				data: {
					identity: ap.m.app_data.data.id
				},
				callback: ap.init.check1_callback,
				method: 'GET'
			});
		}else{
			ap.init.checks.continuex();
		};*/
		ap.init.checks.continuex();
	},
	check1_callback: function(response){
		console.log(response);
		/*if(response.success==true){
			ap.m.invitation_update(response.data.userObj);
		};*/
		ap.init.checks.continuex();
	},
	
	
	/******************** INIT - CHECK 2 - FINISHED ********************/
	check2: function(){
		console.log('--- INIT - CHECK 2 - FINISHED ---');
		ap.init.checks.status = 'finished';
		ap.init.launch();
	}
	
	
};

