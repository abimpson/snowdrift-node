

var ap = ap || {};
ap.share = {
	
	
	/******************** SHARE - FB - TIMELINE ********************/
	fb_timeline: function(to){
		console.log(ap.config.fb_link+(ap.m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+ap.m.user.identity+'"}'):''));
		FB.ui({
			method: 		'feed',
			to:				(typeof(to)!=='undefined'?to:''),
			link: 			ap.config.fb_link+(ap.m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+ap.m.user.identity+'"}'):''),
			picture: 		ap.config.fb_picture,
			name: 			ap.config.fb_title,
			caption: 		ap.config.fb_caption,
			description: 	ap.config.fb_description
		},function(response){
			ap.analytics.google('Share - Facebook Timeline');
			//console.log(response);
			/*if(response!=null){
				//console.log('SUCCESS');
			};*/
			//ap.shared.googleAnalytics((ap.data.logged_in==true?'Auth':'Unauth')+' - Share - Fb - Timeline');
		});
	},
	
	
	/******************** SHARE - FB - MESSAGE ********************/
	fb_message: function(){
		FB.ui({
			method:			'send',
			//to:			userId,
			link:			ap.config.fb_link+(ap.m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+ap.m.user.identity+'"}'):''),
			picture:		ap.config.fb_picture,
			name:			ap.config.fb_title,
			description:	ap.config.fb_description
		},function(response){
			//console.log(response);
			//ap.shared.googleAnalytics((ap.data.logged_in==true?'Auth':'Unauth')+' - Share - Fb - Message');
		});
	},
	
	
	/******************** SHARE - FB - APP REQUEST ********************/
	fb_request: function(){
		FB.ui({
			method:			'apprequests',
			message:		ap.config.fb_description,
			data:			encodeURIComponent('{"id":"'+ap.m.user.identity+'"}')
		},function(response){
			console.log(response);
		});
	},
	
	
	/******************** SHARE - FB - POST TO WALL (AUTOMATIC) ********************/
	fb_post: function(){
		FB.api(
			"/me/feed",
			"POST",
			{
				"message": "This is a test post",
				"link": "http://www.prrple.com"
			},
			function (response) {
				console.log(response);
			}
		);
	},
	
	
	/******************** SHARE - FB - PHOTO ********************/
	fb_photo: function(){
		FACEBOOK.upload_photo(
			'http://3.bp.blogspot.com/-6m8Efby6bRw/Tphtmo3zR3I/AAAAAAAAASY/QVcJtUoYUDE/s400/sea-wallpaper+5.jpg',
			'This is a test post',
			ap.all.callback
		);
	},
	
	
	/******************** SHARE - FB - ACTION ********************/
	fb_action: function(){
		FACEBOOK.add_perms('publish_actions',ap.share.fb_action2);
	},
	fb_action2: function(){
		FB.api('/me/'+FACEBOOK.namespace+':create', 'post', 
			{
				soundtrack:actionUrl
			},
			function(response) {
				//console.log(response);
			}
		);
	},
	
	
	/******************** SHARE - FB - NOTIFICATION ********************/
	fb_notification: function(){
		FACEBOOK.send_notification(
			FACEBOOK.user.id,
			APP_ACCESS_TOKEN,
			'http://www.kerve.com',
			'This is a test notification',
			ap.all.callback
		);
	},
	
	
	/******************** SHARE - TWEET ********************/
	tweet: function(){
		TWITTER.share(ap.config.fb_link+(ap.m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+ap.m.user.identity+'"}'):''),FACEBOOK.user.first_name + ' ' + ap.config.twit_copy);
		//ap.shared.googleAnalytics((ap.data.logged_in==true?'Auth':'Unauth')+' - Share - Twitter');
	}


};

