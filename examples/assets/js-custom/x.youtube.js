

/*
	(C) KERVE
	
	AUTHOR:		ALEX BIMPSON
	NAME:		YOUTUBE JS APP FRAMEWORK
	VERSION:	1.0
	UPDATED:	2013-11-5
*/


/********************************************************************************************************************************/
/****************************************************** YOUTUBE - FRAMEWORK *****************************************************/
/********************************************************************************************************************************/

var YOUTUBE = new Object({
	
	
	/******************** CONFIG ********************/
	api_key: 'AIzaSyAGmIOzAwsnI-QUInxt7XtqB5Q38iCIf9s',
	
	
	/******************** VARIABLES ********************/
	init: false,
	
	
	/******************** SEARCH YOUTUBE ********************/
	search_youtube: function(query,callback){
		$.ajax({
			type:		'get',
			url:		'https://www.googleapis.com/youtube/v3/search',
			data:		{
							key:				YOUTUBE.api_key,
							q:					query,
							part:				'snippet',
							maxResults: 		5,
							order:				'relevance',
							safeSearch:			'moderate',
							type:				'video',
							videoEmbeddable:	true
						},
			dataType:	'jsonp',
			async:		true,
			timeout:	20000,
			//SUCCESS
			success:	function(response2){
				var response = {
					status: 'ok',
					response: response2
				};
				if(typeof(callback)=="function"){
					callback(response);
				};
			},
			//ERROR
			error:		function(xhr, status, error){
				var response = {
					status: 'error',
					response:{
						error: error,
						status: status,
						xhr: xhr
					}
				};
				if(typeof(callback)=="function"){
					callback(response);
				};
			}
		});
	}
	
	
	
	
	
	
	
	
	
	/********** YOUTUBE - INIT **********/
	/*var PREVIEW_YOUTUBE;
	var PREVIEW_YOUTUBE_READY = false;
	var PREVIEW_YOUTUBE_PLAYLIST = new Array();
	var PREVIEW_YOUTUBE_CURRENT = 0;
	var PREVIEW_YOUTUBE_COUNT = PREVIEW_YOUTUBE_PLAYLIST.length;
	function previewUpdate(){
		PREVIEW_YOUTUBE_CURRENT = 0;
		PREVIEW_YOUTUBE_COUNT = PREVIEW_YOUTUBE_PLAYLIST.length;
		if(PREVIEW_YOUTUBE_PLAYLIST.length>0){
			if(PREVIEW_YOUTUBE_PLAYLIST.length>1){
				$('#preview_player_controls').show();
			}else{
				$('#preview_player_controls').hide();
			}
			$('#preview_player_wrap').html('<div id="preview_player"></div>');
			$('#preview_video').show();
			previewVideo();
			//previewCueVideo(PREVIEW_YOUTUBE_PLAYLIST[0]);
		}else{
			$('#preview_video').hide();
		}
	}
	function onYouTubeIframeAPIReady(){
		YOUTUBE_API_READY = true;
	}
	YOUTUBE_API_READY = true;
	//PREVIEW_YOUTUBE_READY = true;
	function previewVideo(){
		if(YOUTUBE_API_READY == true){
			PREVIEW_YOUTUBE = new YT.Player('preview_player', {
				height: '252',
				width: '415',
				playerVars: {
					autoplay: 0,
					showinfo: 0,
					rel: 0,
					modestbranding: 1,
					color: 'white',
					autohide: 1,
					egm: 0,
					hd: 1,
					showsearch: 0,
					iv_load_policy: 3,
					wmode: 'transparent'
					//controls: 0
				},
				iv_load_policy: 3,
				videoId: PREVIEW_YOUTUBE_PLAYLIST[0],
				events: {
					'onReady': previewPlayerReady,
					'onStateChange': previewPlayerStateChange
				}
			});
		}else{
			setTimeout(function(){
				previewVideo();
			},200);
		}
	}
	function previewPlayerReady(event){
		PREVIEW_YOUTUBE_READY = true;
	}
	
	
	/********** YOUTUBE - STATE CHANGE **********/
	/*function previewPlayerStateChange(state){
		//unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
		if(state.data == 0){
			previewVideoRight(true);
		}
	}
	
	
	/********** YOUTUBE - CUE VIDEO **********/
	/*function previewCueVideo(youtubeId){
		if(PREVIEW_YOUTUBE_READY == true){
			PREVIEW_YOUTUBE.cueVideoById(youtubeId, 0, 'hd720');
			$('.playlist_row_title').removeClass('playing');
			$('#playlist_row_'+youtubeId+' .playlist_row_title').addClass('playing');
		}else{
			setTimeout(function(){
				previewCueVideo(youtubeId);
			},200);
		}
	}
	
	
	/********** YOUTUBE - LOAD VIDEO **********/
	/*function previewLoadVideo(youtubeId){
		PREVIEW_YOUTUBE.loadVideoById(youtubeId, 0, 'hd720');
		$('.playlist_row_title').removeClass('playing');
		$('#playlist_row_'+youtubeId+' .playlist_row_title').addClass('playing');
	}
	
	
	/********** YOUTUBE - SKIP **********/
	/*function previewVideoLeft(){
		if(PREVIEW_YOUTUBE_CURRENT <= 0){
			PREVIEW_YOUTUBE_CURRENT = PREVIEW_YOUTUBE_COUNT-1;
		}else{
			PREVIEW_YOUTUBE_CURRENT--;
		}
		previewCueVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
	}
	function previewVideoRight(play){
		if(PREVIEW_YOUTUBE_CURRENT >= PREVIEW_YOUTUBE_COUNT-1){
			PREVIEW_YOUTUBE_CURRENT = 0;
		}else{
			PREVIEW_YOUTUBE_CURRENT++;
		}
		if(play && play==true){
			previewLoadVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
		}else{
			previewCueVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
		}
	}*/
	
	
	
	
	
	
	
	
});





/********************************************************************************************************************************/
/****************************************************** YOUTUBE - INITIALISE ****************************************************/
/********************************************************************************************************************************/


/******************** EXTEND CONFIG ********************/
$(document).ready(function(){
	if(typeof YOUTUBE_CONFIG !== 'undefined' && YOUTUBE_CONFIG != null){
		$.extend(YOUTUBE, YOUTUBE_CONFIG);
	};
});








