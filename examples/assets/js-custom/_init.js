


/************************************************************************************************************/
/************************************************ INITIALISE ************************************************/
/************************************************************************************************************/


/******************** CUSTOM ERROR HANDLER ********************/
/*window.onerror = function(msg, url, linenumber){
	//console.log('%c### ERROR ###','color:#F00;');
	console.log('%cError: '+msg+'\nFile:  '+url.substring(url.lastIndexOf('/')+1)+'\nLine:  '+linenumber,'color:#c34000;');
	return true;
};*/


/******************** CONSOLE LOG FIX ********************/
if(!console){var console={log:function(){}}};


/******************** DOM STATES ETC ********************/
$(document).ready(function() {
	console.log('%c****************************** LAUNCH ******************************','color:#0097c9;');
	ap.modernizr();
	if(typeof(ap.docsize)!=='undefined'&&typeof(ap.docsize.run)!=='undefined'){ap.docsize.run(true)};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true)};
	if(typeof(ap.scroll)!=='undefined'&&typeof(ap.scroll.run)!=='undefined'){ap.scroll.run(true)};
	ap.ready();
});
$(window).load(function(){
	ap.loaded();
	if(typeof(ap.preload)!=='undefined'&&typeof(ap.preload.init)!=='undefined'){ap.preload.init()};
	if(typeof(ap.docsize)!=='undefined'&&typeof(ap.docsize.run)!=='undefined'){ap.docsize.run(true)};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true)};
	if(typeof(ap.scroll)!=='undefined'&&typeof(ap.scroll.run)!=='undefined'){ap.scroll.run(true)};
	if(!window.FB){
		//ap.errors.generic_error('Sorry, Facebook\'s app server is currently unavailable.<br />Please try again shortly.');
	};
});
$(window).resize(function(){
	if(typeof(ap.docsize)!=='undefined'&&typeof(ap.docsize.run)!=='undefined'){ap.docsize.run(true)};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true)};
	if(typeof(ap.scroll)!=='undefined'&&typeof(ap.scroll.run)!=='undefined'){ap.scroll.run(true)};
});
$(window).scroll(function(){
	//try{ap.scroll.run()}catch(e){};
});
$(window).blur(function(){
	
});





/************************************************************************************************************/
/********************************************** APP INITIALISE **********************************************/
/************************************************************************************************************/


/******************** DEFINE APP ********************/
var ap = ap || {};


/******************** MODERNIZR ********************/
ap.modernizr = function(){
	console.log('%c*** MODERNIZR ***','color:#098c0c;');
	try{console.log(Modernizr);}catch(e){};
	if($('#retina').is(':visible')){
		ap.m.device.retina = true;
	};
};


/******************** READY ********************/
ap.ready = function() {
	console.log('%c*** READY ***','color:#098c0c;');
	try{console.timeStamp('READY');}catch(e){};
	if(typeof(ap.m)!=='undefined'&&typeof(ap.m.device_update)!=='undefined'){ap.m.device_update()};
	if(typeof(ap.m)!=='undefined'&&typeof(ap.m.url_data_update)!=='undefined'){ap.m.url_data_update()};
	if(typeof(ap.m)!=='undefined'&&typeof(ap.m.app_data_update)!=='undefined'){ap.m.app_data_update()};
	if(typeof(ap.m)!=='undefined'&&typeof(ap.m.app_requests_update)!=='undefined'){ap.m.app_requests_update()};
	if(typeof(ap.errors)!=='undefined'&&typeof(ap.errors.init)!=='undefined'){ap.errors.init()};
	if(typeof(ap.nav)!=='undefined'&&typeof(ap.nav.init)!=='undefined'){ap.nav.init()};
	if(typeof(ap.hijax)!=='undefined'&&typeof(ap.hijax.init)!=='undefined'){ap.hijax.init()};
	if(typeof(ap.inputs)!=='undefined'&&typeof(ap.inputs.init)!=='undefined'){ap.inputs.init()};
	if(typeof(ap.pages)!=='undefined'&&typeof(ap.pages.init)!=='undefined'){ap.pages.init()};
	if(typeof(ap.popups)!=='undefined'&&typeof(ap.popups.init)!=='undefined'){ap.popups.init()};
	if(typeof(ap.watchers)!=='undefined'&&typeof(ap.watchers.init)!=='undefined'){ap.watchers.init()};
	if(typeof(ap.all)!=='undefined'&&typeof(ap.all.init)!=='undefined'){ap.all.init()};
	if(typeof(ap.all)!=='undefined'&&typeof(ap.all.canvas)!=='undefined'){ap.all.canvas()};
	if(typeof(ap.init)!=='undefined'&&typeof(ap.init.ready)!=='undefined'){ap.init.ready()};
	if(typeof(ap.auth)!=='undefined'&&typeof(ap.auth.ready)!=='undefined'){ap.auth.ready()};
};


/******************** LOADED ********************/
ap.loaded = function() {
	console.log('%c*** LOADED ***','color:#098c0c;');
	try{console.timeStamp('LOADED');}catch(e){};
	if(typeof(ap.auth)!=='undefined'&&typeof(ap.init.loaded)!=='undefined'){ap.init.loaded()};
	if(typeof(ap.auth)!=='undefined'&&typeof(ap.auth.loaded)!=='undefined'){ap.auth.loaded()};
};






