

/*
	
	AUTHOR: 	Alex Bimpson @ Kerve
	NAME: 		Standard Functions
	WEBSITE:	http://www.kerve.com
	LICENSE:	Distributed under the MIT License
	VERSION:	1.32
	UPDATED:	2014-11-30
	
*/


/****************************************************************************************/
/**************************************** CONFIG ****************************************/
/****************************************************************************************/


$.support.cors = true; //force enable cross-site scripting
jQuery.fx.interval = 22;
var CONFIG_LOG = true; //enable console logs





/****************************************************************************************/
/*********************************** BROWSER DETECTION **********************************/
/****************************************************************************************/


/********** BROWSER DETECTION **********/
var browserDetect = {
	init: function(){
		this.browser = this.searchString(this.dataBrowser) || "Other";
		this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
		this.OS = this.searchString(this.dataOS) || "Unknown";
	},
	searchString: function(data){
		for(var i=0 ; i < data.length ; i++){
			var dataString = data[i].string;
			this.versionSearchString = data[i].subString;
			if(dataString.indexOf(data[i].subString) != -1){
				return data[i].identity;
			};
		};
	},
	searchVersion: function (dataString){
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ string: navigator.userAgent, subString: "Chrome",  identity: "Chrome" },
		{ string: navigator.userAgent, subString: "MSIE",    identity: "IE" },
		{ string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
		{ string: navigator.userAgent, subString: "Safari",  identity: "Safari" },
		{ string: navigator.userAgent, subString: "Opera",   identity: "Opera" },
		{ string: navigator.userAgent, subString: "OmniWeb", versionSearch: "OmniWeb/", identity: "OmniWeb" },
		{ string: navigator.vendor, subString: "iCab", identity: "iCab" },
		{ string: navigator.vendor, subString: "KDE", identity: "Konqueror" },
		{ string: navigator.vendor, subString: "Camino", identity: "Camino" },
		// for newer Netscapes (6+)
		{ string: navigator.userAgent, subString: "Netscape", identity: "Netscape" },
		{ string: navigator.userAgent, subString: "MSIE", identity: "Explorer", versionSearch: "MSIE" },
		{ string: navigator.userAgent, subString: "Gecko", identity: "Mozilla", versionSearch: "rv" },
		// for older Netscapes (4-)
		{ string: navigator.userAgent, subString: "Mozilla", identity: "Netscape", versionSearch: "Mozilla" }
	],
	dataOS : [
		{ string: navigator.platform, subString: "Win", identity: "Windows" },
		{ string: navigator.platform, subString: "Mac", identity: "Mac" },
		{ string: navigator.userAgent, subString: "iPhone", identity: "iPhone/iPod" },
		{ string: navigator.platform, subString: "Linux", identity: "Linux" }
	]
};
browserDetect.init();
var yourBrowser = browserDetect.browser;
var yourBrowserVersion = browserDetect.version;
var yourOS = browserDetect.OS;





/****************************************************************************************/
/************************************ CONSOLE LOGGING ***********************************/
/****************************************************************************************/


/********** DISABLE CONSOLE LOGS - FOR BROWSERS THAT DON'T SUPPORT IT **********/
if(!window.console || (typeof(CONFIG_LOG)!=='undefined' && CONFIG_LOG == false) || (typeof(ap)!=='undefined' && typeof(ap.config)!=='undefined' && typeof(ap.config.console_log)!=='undefined' && ap.config.console_log==false)){// || (yourBrowser == 'IE' && yourBrowserVersion < 9)) {
	(function() {
		var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
		window.console = {};
		for (var i = 0; i < names.length; ++i) {
			window.console[names[i]] = function() {};
		};
	}());
};


/********** CONSOLE LOG **********/
window.log = function(string) {
	parseConsoleLog(string,'log');
};
window.error = function(string) {
	parseConsoleLog(string,'error');
};
parseConsoleLog = function(string,name) {
	if(CONFIG_LOG == true){
		if((name == null) || (name == '') || (name == 'undefined')) {
			var name = 'log';
		};			
		if(yourBrowser == 'IE'){
			//IF IE - OUTPUT OBJECT AS STRING
			if(($.isPlainObject(string) == true) || ((typeof(string) === "object"))) {
				console[name](JSON.stringify(string));
			}else{
				console[name](string);
			};
		}else{
			//NORMAL CONSOLE LOG
			console[name](string);
		};
	};
};





/****************************************************************************************/
/************************************ CACHE ELEMENTS ************************************/
/****************************************************************************************/


var $cache = {};
function $el(el,force){
	if(force==true || typeof($cache[el])==='undefined'){
		var el2 = $(el);
		if(el2.length<1){
			return el2;
		}else{
			$cache[el] = el2;
			return $cache[el];
		};
	}else{
		return $cache[el];
	};
};





/*************************************************************************************/
/********************************** AUTO FUNCTIONS ***********************************/
/*************************************************************************************/


/********** TEXT AREA MAX LENGTH **********/
/*$(document).ready(function() { 
	var txts = document.getElementsByTagName('textarea');
	for(var i = 0, l = txts.length; i < l; i++) {
		if(/^[0-9]+$/.test(txts[i].getAttribute("maxlength"))) { 
			var func = function() { 
				var len = parseInt(this.getAttribute("maxlength"), 10);
				if(this.value.length > len) { 
					alert('Maximum length exceeded: ' + len); 
					this.value = this.value.substr(0, len); 
					return false; 
				}; 
			};
			txts[i].onkeyup = func;
			txts[i].onblur = func;
		}; 
	}; 
});*/





/*************************************************************************************/
/***************************** MISCELLANEOUS FUNCTIONS *******************************/
/*************************************************************************************/


/********** PREVENT PAGE ELASTIC SCROLLING ON IOS **********/
function BlockMove(event) {
	event.preventDefault() ;
};


/********** CHECK DOMAIN PROTOCOL **********/
function isSecure(){
	return window.location.protocol == 'https:';
};


/********** COPY TO CLIPBOARD **********/
function copyToClipboard(string) {
	//IE
	if (window.clipboardData && clipboardData.setData) {
		clipboardData.setData('Text', string);
	};
};


/********** FIT IMAGE TO PARENT **********/
function fitImageToParent(img){
	var parent = img.parent();
	if(parent && typeof(parent)!='undefined'){
		img.removeAttr('style').css({
			height:'100%',
			width:'auto'
		});
		var img_w = img.width();
		var img_h = img.height();
		var parent_w = parent.width();
		var parent_h = parent.height();
		if(img_w < parent_w){
			img.css({
				width:'100%',
				height:'auto'
			});
			var img_t = (parent_h-img_h)/2;
			img.css({
				top:'-'+img_t+'px'
			});
		}else{
			var img_l = (parent_w-img_w)/2;
			img.css({
				left:img_l+'px'
			});
		};
	};
};





/****************************************************************************************/
/*************************************** ENCRYPTION *************************************/
/****************************************************************************************/


/******************** BASIC ENCRYPTION ********************/
//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/string/rot13 [v1.0]
String.prototype.rot13 = function() {
	return this.replace(/[a-zA-Z]/g, function(c){
		return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
	});
};





/****************************************************************************************/
/*************************************** URL STUFF **************************************/
/****************************************************************************************/


/********** GET URL DIRECTORY **********/
function getURLDirectory(){
	var u = window.location.pathname.substring(1);
	return u;
}


/********** GET PARAMETER VALUE FROM URL **********/
function getURLParameter(param){
	var u = window.location.search.substring(1);
	var uVars = u.split('&');
	for(var i = 0; i < uVars.length; i++){
		var parameterName = uVars[i].split('=');
		if (parameterName[0] == param){
			return parameterName[1];
		};
		if(i == uVars.length-1){
			return 'null';
		};
	};
};


/********** SLUGIFY STRING **********/
// lower case, no spaces
function slugify(Text){
	return Text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'+');
};
function deslugify(Text){
	return Text.replace(/\++/g,' ');
};





/****************************************************************************************/
/**************************************** NUMBERS ***************************************/
/****************************************************************************************/


/********** GET NUMBER SUFFIX **********/
function getNumberSuffix(n){
	n = n.toString();
	var lastDigit = n.substr(n.length-1);
	if(lastDigit == 0) return 'th';
	if(lastDigit == 1) return 'st';
	if(lastDigit == 2) return 'nd';
	if(lastDigit == 3) return 'rd';
	if(lastDigit == 4) return 'th';
	if(lastDigit == 5) return 'th';
	if(lastDigit == 6) return 'th';
	if(lastDigit == 7) return 'th';
	if(lastDigit == 8) return 'th';
	if(lastDigit == 9) return 'th';
};


/********** CHECK IF VAR IS A NUMBER OR NOT **********/
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
};


/********** ODD AND EVEN CHECKS **********/
function isEven(n){
	return isNumber(n) && (n % 2 == 0);
};
function isOdd(n){
	return isNumber(n) && (n % 2 == 1);
};


/********** GET RANDOM NUMBER **********/
function randomNumber(minNo,maxNo){
	var difference = maxNo-minNo+1;
	var randomNo = Math.floor(Math.random()*difference);
	return randomNo+minNo;
};


/********** ROUNDING - TO X DECIMAL PLACES **********/
function round(number,dp){
	if(dp && dp!='' && dp!='undefined'){
		var multiplier = '1';
		for(i=0;i<dp;i++){
			multiplier = multiplier+'0';
		}
		return Math.round(number * parseInt(multiplier)) / parseInt(multiplier);
	}else{
		return Math.round(number);
	}
};
function roundUp(number,dp){
	if(dp && dp!='' && dp!='undefined'){
		var multiplier = '1';
		for(i=0;i<dp;i++){
			multiplier = multiplier+'0';
		}
		return Math.ceil(number * parseInt(multiplier)) / parseInt(multiplier);
	}else{
		return Math.ceil(number);
	}
};
function roundDown(number,dp){
	if(dp && dp!='' && dp!='undefined'){
		var multiplier = '1';
		for(i=0;i<dp;i++){
			multiplier = multiplier+'0';
		}
		return Math.floor(number * parseInt(multiplier)) / parseInt(multiplier);
	}else{
		return Math.floor(number);
	}
};


/********** NUMBER TO WORDS **********/
function numberToWords(s){
	// Convert numbers to words
	// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
	// permission to use this Javascript on your web page is granted
	// provided that all of the code (including this copyright notice) is
	// used exactly as shown (you can change the numbering system if you wish)
	
	//VARS
	var th = ['','thousand','million', 'billion','trillion'];// American Numbering System
	// var th = ['','thousand','million', 'milliard','billion'];// uncomment this line for English Number System
	var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
	var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
	var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
	//CALCULATE
	s = s.toString();
	s = s.replace(/[\, ]/g,'');
	if (s != parseFloat(s)) return 'not a number';
	var x = s.indexOf('.');
	if (x == -1) x = s.length;
	if (x > 15) return 'too big';
	var n = s.split('');
	var str = '';
	var sk = 0;
	for (var i=0; i < x; i++) {
		if ((x-i)%3==2) {
			if (n[i] == '1') {
				str += tn[Number(n[i+1])] + ' ';
				i++;
				sk=1;
			} else if (n[i]!=0) {
				str += tw[n[i]-2] + ' ';
				sk=1;
			};
		} else if (n[i]!=0) {
			str += dg[n[i]] +' ';
			if ((x-i)%3==0) str += 'hundred ';
			sk=1;
		};
		if ((x-i)%3==1) {
			if (sk) str += th[(x-i-1)/3] + ' ';
			sk=0;
		};
	};
	if (x != s.length) {
		var y = s.length;
		str += 'point ';
		for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';
	};
	return str.replace(/\s+/g,' ');
};


/********** DEGREES / RADIANS CONVERSION **********/
var deg2rad = Math.PI/180;
var rad2deg = 180/Math.PI;
function toDegrees(n){
	return n * rad2deg;
};
function toRadians(n){
	return n * deg2rad;
};





/****************************************************************************************/
/**************************************** STRINGS ***************************************/
/****************************************************************************************/


/********** CAPITALISE ALL WORDS IN STRING **********/
String.prototype.capitalize = function() {
	return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};


/********** CAPITALISE WORD **********/
function capitaliseWord(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
}


/********** WHITESPACE **********/
function removeWhitespace(text){
	return $.trim(text);
};
function checkWhitespace(text){
	if(removeWhitespace(text) == ''){
		return true
	}else{
		return false;
	};
};


/********** TRIM A STRING TO A SPECIFIC NUMBER OF WORDS **********/
function trimWords(string, numWords, dots) {
	var countString = string.split(/\s+/).length;
	var expString = string.split(/\s+/,numWords);
	var newString = expString.join(" ");
	if(dots && dots == true){
		if(countString > numWords){
			newString += '...';
		};
	};
	return newString;
};





/****************************************************************************************/
/*********************************** OBJECTS AND ARRAYS *********************************/
/****************************************************************************************/


/********** RETURN THE LENGTH OF AN OBJECT **********/
function objectLength(object){
	var length = 0;
	for(var i in object){
		length = parseInt(length)+1;
	};
	return length;
};


/********** SORT OBJECT - BY KEY - e.g. object: { keyToSortBy: val } **********/
function sortObject(obj,order){
	var key, tempArray = [], i, tempObj = {};
	// build the tempArray
	for ( key in obj ) {
		tempArray.push(key);
	};
	// sort the tempArray
	tempArray.sort(
		function(a, b) {
			return a.toLowerCase().localeCompare( b.toLowerCase() );
		}
	);
	// create new object
	if(order === 'desc'){
		for( i = tempArray.length - 1; i >= 0; i-- ){
			tempObj[ tempArray[i] ] = obj[ tempArray[i] ];
		};
	}else{
		for( i = 0; i < tempArray.length; i++ ){
			tempObj[ tempArray[i] ] = obj[ tempArray[i] ];
		};
	};
	return tempObj;
};


/********** SORT OBJECT - BY VAL - e.g. object:{ key: valToSortBy } **********/
function sortObjectByVal(obj,val,order){
	
};


/********** SORT OBJECT - BY VAL 2 - e.g. object:{ key: { key: valToSortBy } } **********/
function sortObjectByVal2(obj,val,order){
	var tempArray = [];
	var tempObj = {};
	// build the tempArray
	for (var x in obj) {
		tempArray.push({ 'key': x, 'val': obj[x][val] });
	}
	// sort the tempArray
	tempArray.sort(function (a, b) {
		var as = a['val'],
			bs = b['val'];
		return as == bs ? 0 : (as > bs ? 1 : -1);
	});
	// create new object
	if(order === 'desc'){
		for( i = tempArray.length - 1; i >= 0; i-- ){
			tempObj[tempArray[i]['key']] = obj[tempArray[i]['key']];
		};
	}else{
		for( i = 0; i < tempArray.length; i++ ){
			tempObj[tempArray[i]['key']] = obj[tempArray[i]['key']];
			console.log(tempArray[i]['key']+' - '+tempArray[i]['val']);
		};
	};
	return tempObj;
};


/********** ARRAY - MAKE UNIQUE **********/
function unique_array(array){
	return $.grep(array, function(el, index) {
		return index == $.inArray(el, array);
	});
}


/********** ARRAY - DELETE ITEM **********/
function removeFromArray(array,x){
	//var index = array.indexOf(x);
	var index = $.inArray(x, array);
	console.log(index);
	if(index > -1) {
		array.splice(index, 1);
	};
}


/********** ARRAY - REMOVE KEY **********/
function removeKey(arrayName,key){
	var x;
	var tmpArray = new Array();
	for(x in arrayName){
		if(x!=key){
			tmpArray[x] = arrayName[x];
		};
	};
	return tmpArray;
};


/********** ARRAY - SHUFFLE **********/
function shuffleArray(array){
	/*for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;*/
	var currentIndex = array.length, temporaryValue, randomIndex ;
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
};





/****************************************************************************************/
/************************************** VALIDATION **************************************/
/****************************************************************************************/


/********** VALIDATE STRING LENGTH **********/
function isValidLength(val,length){
	if(val.length<length || checkWhitespace(val)==true){
		return false;
	}else{
		return true;
	};
};
function isValidLengthRange(val,minl,maxl){
	if(val.length<minl || val.length>maxl || checkWhitespace(val)==true){
		return false;
	}else{
		return true;
	};
};


/********** VALIDATE NUMBER, NO OTHER CHARS **********/
function isValidNumber(number){
	if($.trim(number)!=''){
		var regExp = /^\d+$/;
		return regExp.test(number);
	}else{
		return false;
	};
};


/********** VALIDATE NAME **********/
function isValidName(name){
	if(name.length<2 || checkWhitespace(name)==true){
		return false;
	}else{
		return true;
	};
};


/********** VALIDATE PASSWORD **********/
function isValidPassword(password){
	if(password.length<6 || checkWhitespace(password)==true){
		return false;
	}else{
		return true;
	};
};


/********** VALIDATE POSTCODE **********/
function isValidPostcode(postcode){
	if($.trim(postcode)!=''){
		var regExp = /^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$/i;
		return regExp.test(postcode);
	}else{
		return false;
	};
};


/********** VALIDATE PHONE **********/
function isValidPhone(phone){
	if($.trim(phone)!=''){
		var cleaned = phone.replace(/\+/g,'').replace(/\-/g,'').replace(/\(/g,'').replace(/\)/g,'').replace(/ /g,'');
		if(isValidNumber(cleaned)){
			return true;
		}else{
			return false;
		};
		/*new Regex(
			"(\\+44)?\r\n(\\s+)?\r\n(\\(?)\r\n(?<area_code>(\\d{1,5}|\\d{4}\\s+"+
			"?\\d{1,2}))(\\)?)\r\n(\\s+|-)?\r\n(?<tel_no>\r\n(\\d{1,4}\r\n(\\s+|-"+
			")?\\d{1,4}\r\n|(\\d{6})\r\n))",
		RegexOptions.IgnoreCase
		| RegexOptions.Singleline
		| RegexOptions.ExplicitCapture
		| RegexOptions.CultureInvariant
		| RegexOptions.IgnorePatternWhitespace
		| RegexOptions.Compiled
		);*/
	}else{
		return false;
	};
};


/********** VALIDATE EMAIL ADDRESS **********/
function isValidEmailAddress(e) {
	if($.trim(e)!=''){
		var regExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return regExp.test(e);
	}else{
		return false;
	};
};


/********** VALIDATE URL **********/
function isValidUrl(url){
	if($.trim(url)!=''){
		var regExp = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
		return regExp.test(url);
	}else{
		return false;
	};
};


/********** VALIDATE YOUTUBE URL **********/
function getYoutubeId(url){
	var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = url.match(regExp);
	if (match&&match[2].length==11){
		return match[2];
	}else{
		return false;
	}
};





/****************************************************************************************/
/************************************* TIME & DATE **************************************/
/****************************************************************************************/


/********** CREATE TO ISO STRING - FOR IE8 AND BELOW **********/
if (!Date.prototype.toISOString) {
	if (!Date.prototype.toJSON) {
		Date.prototype.toJSON = function (key) {
			function f(n) {
				return n < 10 ? '0' + n : n;
			};
			return this.getUTCFullYear()   + '-' +
				f(this.getUTCMonth() + 1) + '-' +
				f(this.getUTCDate())      + 'T' +
				f(this.getUTCHours())     + ':' +
				f(this.getUTCMinutes())   + ':' +
				f(this.getUTCSeconds())   + 'Z';
		};
	};
	Date.prototype.toISOString = Date.prototype.toJSON;
};


/********** UNIX TIMESTAMP TO JS DATE - 1393431645 **********/
function timestampToDate(timestamp){
	return new Date(timestamp*1000);
};


/********** TIME STRING TO JS DATE - YYYY-MM-DD HH:MM:SS **********/
function stringToDate(s){
	s = s.split(/[-: ]/);
	return new Date(s[0], s[1]-1, s[2], s[3], s[4], s[5]);
};


/********** JS DATE TO MYSQL STRING - YYYY-MM-DD HH:MM:SS **********/
function dateToString(){
	var d = new Date();
	d = d.getUTCFullYear() + '-' +
		('00' + (d.getUTCMonth()+1)).slice(-2) + '-' +
		('00' + d.getUTCDate()).slice(-2) + ' ' + 
		('00' + d.getUTCHours()).slice(-2) + ':' + 
		('00' + d.getUTCMinutes()).slice(-2) + ':' + 
		('00' + d.getUTCSeconds()).slice(-2);
	return d;
};


/********** DATE - GET EVERYTHING - YYYY-MM-DD HH:MM:SS **********/
function getDateObject(datetime2){
	var datetime = datetime2.split(' ');
	var date = datetime[0].split('-');
	var time = datetime[1].split(':');
	var d = new Date(date[0], date[1], date[2], time[0], time[1], time[2]);
	var object = {
		year: date[0],
		month: date[1],
		month_name: getDateMonth(date[1]),
		month_name_short: getDateMonth(date[1]).substr(0,3),
		day: date[2],
		day_suffix: getNumberSuffix(date[2]),
		day_name: getDateDay(d.getDay()),
		day_name_short: getDateDay(d.getDay()).substr(0,3),
		hour: time[0],
		hour12: getDateHours(time[0]),
		hour_suffix: getDateTimeSuffix(time[0]),
		minute: time[1],
		second: time[2]
	};
	return object;
};


/********** DATE STUFF **********/
function getDateDay(day){
	if(day <= 0) return 'Sunday';
	else if(day == 1) return 'Monday';
	else if(day == 2) return 'Tuesday';
	else if(day == 3) return 'Wednesday';
	else if(day == 4) return 'Thursday';
	else if(day == 5) return 'Friday';
	else if(day == 6) return 'Saturday';
	else
		day = day-7;
		return getDateDay(day);
};
function getDateMonth(month){
	if(month == 0) return 'January';
	if(month == 1) return 'February';
	if(month == 2) return 'March';
	if(month == 3) return 'April';
	if(month == 4) return 'May';
	if(month == 5) return 'June';
	if(month == 6) return 'July';
	if(month == 7) return 'August';
	if(month == 8) return 'September';
	if(month == 9) return 'October';
	if(month == 10) return 'November';
	if(month == 11) return 'December';
};
function getDateHours(hours){
	if(hours > 12){
		return hours-12;
	}else{
		return hours;
	};
};
function getDateMinutes(minutes){
	if(minutes < 10){
		return 0 + minutes.toString();
	}else{
		return minutes;
	};
};
function getDateTimeSuffix(hours){
	if(hours >= 12){
		return 'pm';
	}else{
		return 'am';
	};
};


/********** GET DIFFERENCE BETWEEN 2 DATES - YYYY-MM-DD HH:MM:SS **********/
function getDateDifference(d1,d2){
	var date1 = stringToDate(d1);
	var date2 = stringToDate(d2);
	//convert to utc to avoid DST issues
	var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds());
	var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate(), date2.getHours(), date2.getMinutes(), date2.getSeconds());
	//calculate difference
	var diff = Math.abs(utc2 - utc1);
	diff = diff/1000;
	//split into days/hours/mins/secs
	var days = Math.floor(diff / (24 * 60 * 60));
	diff = diff - (days * 24 * 60 * 60);
	var hours = Math.floor(diff / (60 * 60));
	diff = diff - (hours * 60 * 60);
	var mins = Math.floor(diff / 60);
	diff = diff - (mins * 60);
	var secs = diff;
	//return
	return {
		days: days,
		hours: hours,
		mins: mins,
		secs: secs
	};
};


/********** AGE FROM DATE OF BIRTH **********/
function isLeapYear(year) {
	var d = new Date(year, 1, 28);
	d.setDate(d.getDate() + 1);
	return d.getMonth() == 1;
};
function getAge(date) {
	var d = new Date(date), now = new Date();
	var years = now.getFullYear() - d.getFullYear();
	d.setFullYear(d.getFullYear() + years);
	if(d > now){
		years--;
		d.setFullYear(d.getFullYear() - 1);
	};
	var days = (now.getTime() - d.getTime()) / (3600 * 24 * 1000);
	return Math.floor(years + days / (isLeapYear(now.getFullYear()) ? 366 : 365));
};





