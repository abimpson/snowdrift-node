

var ap = ap || {};
ap.all = {
	
	
	/******************** INIT ********************/
	init: function(){},
	
	
	/******************** FACEBOOK LOGIN ********************/
	/*facebook_login: function(callback){
		if(FACEBOOK.logged_in==true){
			callback();
		}else{
			//ap.shared.showLoader();
			FACEBOOK.login(callback);
		};
	},*/
	
	
	/******************** CALLBACK - FOR TESTING PURPOSES ********************/
	callback: function(response){
		console.log('### CALLBACK ###');
		if(typeof response !== 'undefined'){
			console.log(response);
		};
	},
	
	
	/******************** FACEBOOK - CANVAS SIZE ********************/
	canvas: function(){
		if(typeof(ap.config.canvas_w)!=='undefined'){
			if(typeof(ap.config.canvas_h)!=='undefined' && ap.config.canvas_h!='' && ap.config.canvas_h!=0){
				try{FACEBOOK.canvas_set_size(ap.config.canvas_h,ap.config.canvas_w);}catch(e){};
			}else{
				try{FACEBOOK.canvas_set_size_repeat();}catch(e){};
			};
		};
	},
	
	
	/******************** IMAGE ERROR ********************/
	img_error: function(img){
		img.onerror = "";
		img.src = "assets/images/face.jpg";
		return true;
	},
	
	
	/******************** SCROLL TO PAGE TOP ********************/
	scroll_top: function(){
		window.scrollTo(0,0);
		try{FACEBOOK.canvas_scroll_top();}catch(e){};
	},
	
	
	/******************** LOADING ********************/
	show_loader: function() {
		$('#loader').show();
	},
	hide_loader: function() {
		if(ap.m.device.ie8){
			$('#loader').hide();
		}else{
			$('#loader').fadeOut(250);
		};
	}
	
	
};

