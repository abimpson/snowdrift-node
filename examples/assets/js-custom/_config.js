


/************************************************************************************************************/
/*!************************* SNOWDRIFT JS FRAMEWORK - v3 - by ALEX BIMPSON @ KERVE ************************!*/
/************************************************************************************************************/


/*

// CONFIG & INIT
	_config.js				- app config
	_init.js				- app initialisation

// APP CUSTOM
	a.all.js				- general app functions
	a.auth.js				- authorisation functions and checks
	a.init.js				- initialisation and launch functions and checks
	a.share.js				- social sharing functions

// CONTROLLERS
	c.docsize.js			- handles functions that must run when document resizes
	c.inputs.js				- monitor focus, blur and change events on form inputs
	c.nav.js				- binds all events - clicks etc
	c.scroll.js				- handles scroll events
	c.windowsize.js			- handles functions that must run when window resizes or orientation changes

// HELPERS
	h.ajax.js				- standard functions for AJAX calls
	h.analytics.js			- analytics tracking, such as google or facebook
	h.errors.js				- handles app errors
	h.hijax.js				- hijack url links with an AJAX call
	h.plupload.js			- plupload image uploader
	h.preload.js			- preload images etc
	h.videos.js				- html5 video manager

// MODELS
	m.all.js				- general collection of data models

// VIEWS
	v.all_pages.js			- handles page switching and functions to run when loading each page
	v.all_popups.js			- handles popups and functions to run when loading each popup
	v.all_toggles.js		- handles toggling of actions/data e.g. showing and hiding elements

// WATCHERS
	w.all.js				- watch for object changes

// STANDARD FUNCTIONS
	s.all.js				- standard functions
	s.fastclick				- standard fastclick function
	s.filter.js				- standard functions - language filter
	s.pushstate.js			- check for HTML5 pushstate - include at top of page (doesn't require jquery)
	s.watch.js				- object watching

// EXTERNAL API WRAPPERS
	x.bitly.js				- bit.ly api
	x.facebook.3.js			- facebook graph api
	x.gmaps.js				- google maps api
	x.twitter.js			- twitter api

*/





/************************************************************************************************************/
/************************************************ APP CONFIG ************************************************/
/************************************************************************************************************/


/******************** DEFINE APP ********************/
var ap = ap || {};


/******************** CONFIG ********************/
ap.config = {
	//DEV MODE
	dev:					true,
	//FACEBOOK CANVAS
	canvas_w:				810,
	canvas_h:				0, //set as 0 if height varies
	//GOOGLE ANALYTICS
	analytics:				true, //use google analytics tracking,
	analytics_name:			'',
	analytics_type:			'universal', //classic or universal?
	//FACEBOOK
	fb_link:				'http://snowdrift.prrple.com',
	fb_picture:				'',
	fb_title:				"Title",
	fb_caption:				"Caption",
	fb_description:			"Description",
	//MISC
};


/******************** CONFIG - ENVIRONMENTAL ********************/
ap.env_current = window.location.hostname;
if(ap.env_current=='mydomain.com'){
	/**** ALEX ****/
	ap.env_config = {
		//PATHS
		api_root:				'/api/v1/',
		image_root:				'/assets/images/',
		fb_app_id:				'1431981420377693'
	};
}else{
	/**** DEFAULT ****/
	ap.env_config = {
		//PATHS
		api_root:				'/api/v1/',
		image_root:				'/assets/images/',
		fb_app_id:				'1431981420377693'
	};
};
$.extend(ap.config,ap.env_config);


/******************** ERROR HANDLING ********************/
ap.config_errors = {
	show_generic: true,		//whether to show generic error messages
	show_response: false,	//whether to show response error messages
	show_timeout: false,	//whether to show timeout error messages
	show_ajax: false,		//whether to show AJAX error messages
	log_errors: true,		//whether to log errors in the database
};





/************************************************************************************************************/
/************************************************* APP COPY *************************************************/
/************************************************************************************************************/


/******************** PLACEHOLDER COPY - MUST MATCH RELATED COPY HARDCODED IN HTML ********************/
ap.placeholders = {
	
};





/************************************************************************************************************/
/********************************************* IMAGE PRELOADING *********************************************/
/************************************************************************************************************/


/******************** IMAGES TO PRELOAD ********************/
ap.config_preload = {
	timeout: 6000,
	//ALL
	files: Array(
		
	),
	//ALL - NON RETINA
	files_non_retina: Array(
		
	),
	//ALL - RETINA
	files_retina: Array(
		
	),
	//DESKTOP
	files_d: Array(
		
	),
	//DESKTOP - NON RETINA
	files_d_non_retina: Array(
		
	),
	//DESKTOP - RETINA
	files_d_retina: Array(
		
	),
	//MOBILE
	files_m: Array(
		
	),
	//MOBILE - NON RETINA
	files_m_non_retina: Array(
		
	),
	//MOBILE - RETINA
	files_m_retina: Array(
		
	)
};





/************************************************************************************************************/
/******************************************* EXTERNAL API CONFIG ********************************************/
/************************************************************************************************************/


/******************** FACEBOOK APP CONFIG ********************/
var FACEBOOK_CONFIG = {
	app_id: ap.config.fb_app_id,
	scope: '',
	user_fields: 'id,first_name,last_name,gender,email,picture.height(200).width(200)'
};


/******************** FACEBOOK CALLBACK FUNCTIONS ********************/
var FACEBOOK_FUNCTIONS = {
	//READY
	fnc_ready: function(){
		
	},
	//AUTO - NOT LOGGED IN TO FACEBOOK
	fnc_auto_not_logged_in: function(){
		$('.facebook_loading').hide();
		$('.facebook_login').css('display','inline-block');
	},
	//AUTO - NOT AUTHENTICATED
	fnc_auto_not_auth: function(){
		$('.facebook_loading').hide();
		$('.facebook_login').css('display','inline-block');
	},
	//AUTO - AUTHENTICATED
	fnc_auto_auth: function(){
		$('.facebook_loading').hide();
		$('.facebook_login').css('display','inline-block').addClass('revisit');
	},
	//LOGIN - PRE-AUTHENTICATION
	fnc_login_pre: function(){
		
	},
	//LOGIN - AUTHENTICATED
	fnc_login_auth: function(){
		
	},
	//LOGIN CANCELLED
	fnc_cancelled: function(){
		
	},
	//PERMISSIONS REVOKED
	fnc_revoked: function(){
		location.reload();
	}
};


/******************** TWITTER CALLBACK FUNCTIONS ********************/
var TWITTER_FUNCTIONS = {
	//CLICK - PRE-AUTHENTICATION
	twit_click_pre: function(){
		
	},
	//CLICK - AUTHENTICATED
	twit_login_auth: function(){
		
	},
	//LOGIN CANCELLED
	twit_cancelled: function(){
		
	}
};






