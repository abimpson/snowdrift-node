// JavaScript Document


var ap = ap || {};
ap.m = ap.m || {};


/******************** APP ********************/
ap.m.app = {
	ready: false,
	loaded: false
},


/******************** DEVICE ********************/
ap.m.device = {
	retina: false,
	mustard: false,
	ie8: false,
	ie7: false,
	ie6: false
},
ap.m.device_update = function(){
	//IE
	if(yourBrowser=='IE' || $('#page_ie7').length>0){
		if(yourBrowserVersion<9){
			ap.m.device.ie8 = true;
		};
		if(yourBrowserVersion<8 || $('#page_ie7').length>0){
			ap.m.device.ie7 = true;
		};
		if(yourBrowserVersion<7){
			ap.m.device.ie6 = true;
		};
	};
	//DEVICE
	if(typeof(DEVICE)!=='undefined'){
		$.extend(ap.m.device,DEVICE);
	};
	//CUT THE MUSTARD - TEST FOR MODERN HTML 5 JS BROWSERS
	if('querySelector' in document && 'localStorage' in window && 'addEventListener' in window){
		ap.m.device.mustard = true;
	};
},


/******************** URL DATA ********************/
ap.m.url_data = {
	set: false
},
ap.m.url_data_update = function(){
	if(typeof(URL_DATA)!=='undefined'){
		$.extend(ap.m.url_data,URL_DATA);
	};
},


/******************** FACEBOOK APP DATA ********************/
ap.m.app_data = {
	set: false
},
ap.m.app_data_update = function(){
	if(typeof(APP_DATA)!=='undefined'){
		$.extend(ap.m.app_data,APP_DATA);
	};
},


/******************** FACEBOOK APP REQUESTS ********************/
ap.m.app_requests = {
	set: false
},
ap.m.app_requests_update = function(){
	if(typeof(APP_REQUESTS)!=='undefined'){
		$.extend(ap.m.app_requests,APP_REQUESTS);
		ap.m.app_requests_get();
	};
},
ap.m.app_requests_get = function(){
	/*if(FACEBOOK.got_user==true){
		for(var i in ap.m.app_requests.data){
			FB.api('/'+ap.m.app_requests.data[i], 'get', { //?access_token=
				//access_token: FACEBOOK.auth_response.accessToken
			}, function(response) {
				console.log(response);
			});
		};
	}else{
		setTimeout(function(){
			ap.m.app_requests_get();
		},50);
	};*/
},


/******************** USER ********************/
ap.m.user = {
	identity: null,
	confirmed: 0,
	verified: 0
},
ap.m.user_update = function(){
	
},


/******************** INVITER ********************/
ap.m.inviter = {
	set: false,
	identity: null
},
ap.m.inviter_update = function(data){
	
},


/******************** INVITEES ********************/
ap.m.invitees = {
	set: false
},
ap.m.invitees_update = function(data){
	
}

