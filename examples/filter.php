<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- HTML5 SHIV -->
	<!--[if lt IE 9]>
	<script src="assets/js-plugins/html5shiv.js"></script>
	<![endif]-->
	
	<!-- JQUERY -->
	<script src="assets/js-plugins/jquery.1.11.1.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_init2.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	<script src="../web/v4/assets/js/src/h.filter.js"></script>
	<script>
		$(function(){
			$('#input').on('keyup',function(){
				$('#output').html('<strong>Filtered</strong>: ' + $filter.check($(this).val()));
				$windowsize.run(true);
				return false;
			});
		});
	</script>
	
</head>
<body>
	
	
	
	<!-- HEADER -->
	<?php include('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Example - Swear Filter</h1>
			<p>Begin typing below to see how filtered text will appear.</p>
			<br />
			<input type="text" id="input" class="fullwidth" />
			<br /><br />
			<div id="output"></div>
		</div>
		
	</div>
	
	
	
	<!-- FOOTER -->
	<?php include('_footer.php'); ?>
	
	
	
</body>
</html>