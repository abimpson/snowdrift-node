<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- HTML5 SHIV -->
	<!--[if lt IE 9]>
	<script src="assets/js-plugins/html5shiv.js"></script>
	<![endif]-->
	
	<!-- JQUERY -->
	<script src="assets/js-plugins/jquery.1.11.1.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_init2.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	<script src="../web/v4/assets/js/src/x.youtube.js"></script>
	<script>
		//EMBED
		var v1,v2;
		function embed1(){
			v1 = $youtube.embed({
				video_id: 'LJwn1avKRNo',
				element: 'video1'
			});
			v2 = $youtube.embed({
				video_id: 'EkP3lwveeCI',
				element: 'video2'
			});
		};
		$(function(){
			embed1();
			// embed2();
		});
		//SEARCH
		$(function(){
			$('#btn_search').click(function(){
				var q = $('#input_search').val();
				if($.trim(q)!=''){
					$youtube.search({
						q: q
					},
					function(response){
						var output = '';
						if(response.status=='ok'){
							if(response.data.pageInfo.totalResults>0){
								output = '<strong>Search results:</strong><br />';
								for(var i in response.data.items){
									output += '<a href="http://www.youtube.com/watch?v=' + response.data.items[i].id.videoId + '" target="_blank">' + response.data.items[i].snippet.title + '</a><br />';
								};
							}else{
								output = '<strong>No search results.</strong>';
							};
						}else{
							output = '<strong>Sorry, there was a problem querying the YouTube API.</strong>';
						};
						$('#output').html(output);
					});
				}else{
					$('#output').html('<strong>Please enter a search query.</strong>');
				};
				return false;
			});
		});
	</script>
	
	
	
</head>
<body>
	
	
	
	<!-- HEADER -->
	<?php include('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Example - YouTube API</h1>
			<h2>Search</h2>
			<p>Search the YouTube API below.</p>
			<br />
			<input type="text" id="input_search" />
			<a class="btn" id="btn_search">Search</a>
			<br /><br />
			<div id="output"></div>
		</div>
		
		<div class="container">
			<h2>Video Embed</h2>
			<p>Control the embedded youtube videos below, through the youTube API.</p>
			<br />
			<div class="videowrap">
				<div id="video1"></div>
			</div>
			<div class="controls">
				<a class="btn" onclick="v1.play();">Play</a>
				<a class="btn" onclick="v1.pause();">Pause</a>
				<a class="btn" onclick="v1.stop();">Stop</a>
				<a class="btn" onclick="v1.mute();">Mute</a>
				<a class="btn" onclick="v1.unmute();">Unmute</a>
				<a class="btn" onclick="v1.remove();">Remove</a>
				<a class="btn" onclick="embed1();">Embed</a>
			</div>
			<br />
			<div class="videowrap">
				<div id="video2"></div>
			</div>
			<div class="controls">
				<a class="btn" onclick="v2.play();">Play</a>
				<a class="btn" onclick="v2.pause();">Pause</a>
				<a class="btn" onclick="v2.stop();">Stop</a>
				<a class="btn" onclick="v2.mute();">Mute</a>
				<a class="btn" onclick="v2.unmute();">Unmute</a>
				<a class="btn" onclick="v2.remove();">Remove</a>
				<a class="btn" onclick="embed2();">Embed</a>
			</div>
		</div>
		
		<div class="container">
			<h2>Global Video Controls</h2>
			<p>Control all embedded videos simultaneously.</p>
			<br />
			<a class="btn" onclick="$youtube.playAll();">Play All</a>
			<a class="btn" onclick="$youtube.pauseAll();">Pause All</a>
			<a class="btn" onclick="$youtube.stopAll();">Stop All</a>
		</div>
		
	</div>
	
	
	
	<!-- FOOTER -->
	<?php include('_footer.php'); ?>
	
	
	
</body>
</html>