<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- JQUERY -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js-plugins/jquery.1.11.0.min.js">\x3C/script>')</script>
	
	<!-- PLUPLOAD -->
	<script src="../web/v4/assets/js/vendor/plupload/plupload.full.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_init2.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	<script src="../web/v4/assets/js/src/c.plupload.js"></script>
	
	<!-- JS VARS -->
	<script type="text/javascript">
		$(document).ready(function(){
			var p1 = new $plupload({
				btn: 'btn_plupload1',
				src: '../web/v4/assets/js/vendor/plupload/',
				php: '../web/v4/assets/php/plupload.php',
				callback_before_upload: function(response){
					$('#plupload1_output').html('Uploading image...');
					$windowsize.run(true);
				},
				callback_upload_complete: function(response){
					var n1 = response[0].target_name;
					var n2 = n1.split('.');
					n = {
						orig: n1,
						/*small: n2[0]+'_small.png',
						med: n2[0]+'_med.png',
						large: n2[0]+'_large.png',
						scaled: n2[0]+'_scaled.jpg'*/
					};	
					var output = '';
						output += '<img src="uploads/'+n.orig+'" width="280px" onload="$windowsize.run();" />';
						/*output += '<img src="'+ap.plupload.config.image_root+n.small+'" />';
						output += '<img src="'+ap.plupload.config.image_root+n.med+'" />';
						output += '<img src="'+ap.plupload.config.image_root+n.large+'" />';
						output += '<img src="'+ap.plupload.config.image_root+n.scaled+'" />';*/
					$('#plupload1_output').html(output);
					$windowsize.run(true);
				}
			});
		});
	</script>
	
	
	
</head>
<body>
	<div id="fb-root"></div>
	
	
	
	<!-- HEADER -->
	<?php include('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Example - Plupload</h1>
			<p>Click to upload an image via the AJAX image uploader.</p>
			<p><button class="btn" id="btn_plupload1">Add / Edit Image</button></p>
			<br />
			<div id="plupload1_output"></div>
		</div>
	
	</div>
	
	
	
	<!-- FOOTER -->
	<?php include('_footer.php'); ?>
	
	
	
</body>
</html>