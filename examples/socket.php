<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- HTML5 SHIV -->
	<!--[if lt IE 9]>
	<script src="assets/js-plugins/html5shiv.js"></script>
	<![endif]-->
	
	<!-- JQUERY -->
	<script src="assets/js-plugins/jquery.1.11.1.min.js"></script>
	
	<!-- SOCKET -->
	<script src="../web/v4/assets/js/vendor/socket.io.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_init2.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	<script src="../web/v4/assets/js/src/c.socket.js"></script>
	<script>
		//CONSOLE LOGS
		console.log = function(a){
			$('#console').append('<br />'+JSON.stringify(a));
			$windowsize.run(true);
		};
		//INIT SOCKET
		var s1 = new $socket({
			url: 'http://www.prrple.com:8889',
			status_id: 'socket_status1',
			listeners: [
				{
					method: 'test',
					func: function(response){
						console.log('receive');
						console.log(response);
					}
				}
			]
		});
		//BUTTON
		$(function(){
			$('#btn_socket').click(function(){
				var d = {
					'foo':'bar',
					'ping':'pong'
				};
				console.log('send');
				console.log(d);
				s1.send(
					'test',
					d
				);
				return false;
			});
		});
	</script>
	
	
	
</head>
<body>
	
	
	
	<!-- HEADER -->
	<?php include('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Example - Socket.io</h1>
			<p>Console logs are output below, so that you can see data being sent and received via socket.io.</p>
			<p><a href="" class="btn" id="btn_socket">Send Test Data</a></p>
			<br /><br />
			<div id="console"></div>
		</div>
		
	</div>
	
	
	
	<!-- FOOTER -->
	<?php include('_footer.php'); ?>
	
	
	
</body>
</html>