<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- HTML5 SHIV -->
	<!--[if lt IE 9]>
	<script src="assets/js-plugins/html5shiv.js"></script>
	<![endif]-->
	
	<!-- JQUERY -->
	<script src="assets/js-plugins/jquery-1.12.0.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_init2.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	
	
	
</head>
<body>
	
	
	
	<!-- HEADER -->
	<?php include('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Examples</h1>
			<p><a href="/examples/api.php">External API's</a></p>
			<p><a href="/examples/plupload.php">Plupload</a></p>
			<p><a href="/examples/socket.php">Socket.io</a></p>
			<p><a href="/examples/filter.php">Swear Filter</a></p>
			<p><a href="/examples/youtube.php">YouTube API</a></p>
		</div>
		
	</div>
	
	
	
	<!-- FOOTER -->
	<?php include('_footer.php'); ?>
	
	
	
</body>
</html>