


(function(window) {


	/******************** CONFIG ********************/
	var vars = {
		got: false,
		room: 'blue',
		ip: '192.168.0.200',
		port: 4000
	};
	if(vars.room=='blue'){
		vars.port = 5000;
	}


	/******************** DEV - LOCALSTORAGE OVERRIDE ********************/
	try{
		var localroom = localStorage.getItem('room');
		if(localroom!=null && typeof(localroom)==='string'){
			vars.room = localroom;
		}
	}catch(e){}
	//localStorage.setItem('room','alex');


	/******************** EXPORT ********************/
	if(typeof define == 'function' && define.amd){
        define(function(){ return vars; });
    }else if(typeof module === 'object' && module.exports){
        module.exports = vars;
    }else{
        window.vars = vars;
    }
	
	
})((typeof(window)!=='undefined'?window:''));




