/******************** REQUIRES ********************/
var electron = require('electron');
var path = require('path');
var url = require('url');
var os = require('os');

/******************** APP ********************/
var app = electron.app; // Module to control application life.
var BrowserWindow = electron.BrowserWindow; // Module to create native browser window.
var Menu = electron.Menu;
var MenuItem = electron.MenuItem;
var ipcMain = electron.ipcMain;
// set local storage path
app.setPath('userData', app.getPath('appData') + '/Electron');

/******************** DATA ********************/
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow;

//electron.webFrame.setZoomLevelLimits(1, 1);

//app.commandLine.appendSwitch('--enable-viewport-meta', 'true');

/*var webFrame = electron.webFrame;
webFrame.setVisualZoomLevelLimits(1, 1);
webFrame.setLayoutZoomLevelLimits(0, 0);*/

/******************** CREATE WINDOW ********************/
function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    kiosk: false,
    width: 1280,
    height: 720,
    webPreferences: {
      // nodeIntegration: true
      //webSecurity: false
      preload: path.join(__dirname, 'preload.js')
    },
    titleBarStyle: 'hiddenInset', // default hidden hiddenInset customButtonsOnHover
    frame: process.platform === 'darwin' ? false : true,
    // show: false,
    // transparent: true,
    vibrancy: 'sidebar' // appearance-based light medium-light dark ultra-dark titlebar selection menu popover sidebar
  });

  // and load the index.html of the app.
  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, '../public/index.html'),
      protocol: 'file:',
      slashes: true
    })
  );

  // define menus
  var menuTemplate = [
    {
      label: 'Electron',
      submenu: [
        {
          label: 'Close Window',
          accelerator: 'Cmd+W',
          click: function() {
            app.quit();
          }
        },
        {
          label: 'Quit',
          accelerator: 'Cmd+Q',
          click: function() {
            app.quit();
          }
        }
      ]
    },
    {
      label: 'Edit',
      submenu: [
        //{ label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
        //{ label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
        //{ type: "separator" },
        {label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:'},
        {label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:'},
        {label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:'},
        {label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:'}
      ]
    },
    {
      label: 'Developer',
      submenu: [
        /*{
				label: 'Toggle Inspector',
				accelerator: 'Cmd+Alt+I',
				click: function(){
				mainWindow.webContents.openDevTools();
			}
		},*/
        {
          role: 'reload'
        },
        {
          role: 'forcereload'
        },
        {
          role: 'toggledevtools'
        }
      ]
    }
  ];
  var menu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(menu);

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Open links in new browser window
  mainWindow.webContents.on('new-window', function(event, url) {
    event.preventDefault();
    var shell = electron.shell;
    shell.openExternal(url);
  });

  // Open links in new browser window
  mainWindow.webContents.on('vibrancy', function(value) {
    console.log(value);
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
    app.quit();
  });

  // Send details to window
  setTimeout(function() {
    sendIP();
  }, 1000);
  setTimeout(function() {
    sendIP();
  }, 2000);
  setTimeout(function() {
    sendIP();
  }, 4000);
  setTimeout(function() {
    sendIP();
  }, 8000);
  function sendIP() {
    try {
      mainWindow.webContents.send('message', 'running');
    } catch (e) {}
  }
}

/******************** APP STATES ********************/
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function() {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipcMain.on('vibrancy', (x, message) => {
  mainWindow.setVibrancy(message);
});
