


var webpack = require("webpack");
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');


module.exports = {
  
  watch: true,
  
  entry: {
    app: "./assets/js/src/_init.js",
    vendor: [
      "./assets/js/vendor/jquery-2.2.0.min.js",
      "./assets/js/vendor/socket.io.js",
      "./assets/js/vendor/webfontloader.js"
    ]
  },
  
  devtool: 'source-map',
  
  output: {
    path: "/assets/js/dist",
    publicPath: './assets/js/dist/',
    filename: "[name].min.js",
    chunkFilename: "[id].min.js"
  },
  
  plugins: [
    //new UglifyJSPlugin(),
    //new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.bundle.js' }),
    new CleanWebpackPlugin(['./assets/js/dist'],{watch:false}),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor']
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
  
  resolve: {
    alias: {
      jquery: "../vendor/jquery-2.2.0.min.js"
    }
  },
  
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            // presets: ['env'],
            // plugins: [require('babel-plugin-transform-object-rest-spread')]
            // "presets": [
            // 	["env", {
            // 		"targets": {
            // 			"browsers": ["Firefox > 50"]
            // 		}
            // 	}]
            // ]
          }
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  }
  
};



