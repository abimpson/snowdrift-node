


/******************** REQUIREMENTS ********************/
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var gcmq = require('gulp-group-css-media-queries');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');

var webpack = require('webpack-stream');


/******************** CONFIG ********************/
var options = {
	// general
	watch_interval: 100,
	// paths
	root: './',
	assets_root: 'assets/',
	// browsersync
	bs: true,
	bs_open: false,
	bs_port: 8000,
	//bs_proxy: 'kerve.dev',
	// css
	css_minify: true,
	css_comments: false,
	css_sourcemaps: true,
	css_src: 'css/src',
	css_dist: 'css/dist',
	// js
	js_minify: true,
	js_strip_debug: false,
	js_sourcemaps: true,
	js_src: 'js/src',
	js_dist: 'js/dist',
	js_vendor: 'js/vendor'
};


/******************** ERROR HANDLER ********************/
function error_handler(err) {
	console.log(err.toString());
	this.emit('end');
};


/******************** BROWSER SYNC ********************/
gulp.task('browser-sync', function() {
	if(options.bs){
		var bsopts = {
			open: options.bs_open,
			port: options.bs_port,
			ghostMode: false
		};
		if(!!options.bs_proxy){
			bsopts.proxy = options.bs_proxy;
		}else{
			bsopts.server= {
				baseDir: options.root
			};
		}
		browserSync.init(bsopts);
	}
});


/******************** SASS - CONCATENATE AND MINIFY ********************/
gulp.task('scss', function() {
	return gulp
		// source files
		.src([
			options.assets_root+options.css_src+'/*.scss'
		])
		// plumber - error handler
		.pipe(plumber({
			errorHandler:error_handler
		}))
		// sourcemaps init
		.pipe(gulpif(options.css_sourcemaps,sourcemaps.init()))
		// sass
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: require('node-bourbon').includePaths
		}))
		// auto prefixer
		.pipe(autoprefixer())
		// sourcemaps output
		.pipe(gulpif(options.css_sourcemaps,sourcemaps.write('',{
			includeContent: false,
			sourceRoot: '../../'+options.css_src
		})))
		// save
		.pipe(gulp.dest(options.assets_root+options.css_dist))
		// stream
		.pipe(gulpif(options.bs,browserSync.stream()));
});


/******************** JS - CONCATENATE AND MINIFY ********************/
gulp.task('js', function() {
	return gulp
		// source files
		.src([
			options.assets_root+options.js_src+'/_init.js'
		])
		// plumber - error handler
		.pipe(plumber({
			errorHandler:error_handler
		}))
		// webpack
		.pipe(webpack(require('./webpack.config.js')))
		// save
		.pipe(gulp.dest(options.assets_root+options.js_dist))
		// stream
		.pipe(gulpif(options.bs,browserSync.stream()));
});


/******************** JS - VENDOR - CONCATENATE AND MINIFY ********************/
gulp.task('js-vendor', function() {
	/*return gulp
		// source files
		.src([
			options.assets_root+options.js_vendor+'/jquery.easing.1.3.min.js',
			options.assets_root+options.js_vendor+'/jquery.touchSwipe.1.6.9.js',
			options.assets_root+options.js_vendor+'/masonry.pkgd.min.js',
			options.assets_root+options.js_vendor+'/imagesloaded.pkgd.min.js',
			options.assets_root+options.js_vendor+'/prrple.slider.js',
			options.assets_root+options.js_vendor+'/socket.io.js',
			options.assets_root+options.js_vendor+'/webfontloader.js'
		])
		// plumber - error handler
		.pipe(plumber({
			errorHandler:error_handler
		}))
		// sourcemaps init
		.pipe(gulpif(options.js_sourcemaps,sourcemaps.init()))
		// concatenate
		.pipe(concat('plugins.min.js'))
		// strip debug
		.pipe(gulpif(options.js_strip_debug,stripDebug()))
		// minify
		.pipe(gulpif(options.js_minify,uglify()))
		// sourcemaps output
		.pipe(gulpif(options.js_sourcemaps,sourcemaps.write('',{
			includeContent: false,
			sourceRoot: '../../'+options.js_vendor
		})))
		// save
		.pipe(gulp.dest(options.assets_root+options.js_dist))
		// stream
		.pipe(gulpif(options.bs,browserSync.stream()));*/
});


/******************** WATCH FOR CHANGES ********************/
gulp.task('watch', function() {
	// html / php
	if(options.bs){
		gulp.watch(options.root+'*.html', {interval:options.watch_interval}).on('change', browserSync.reload);
		gulp.watch(options.root+'*.php', {interval:options.watch_interval}).on('change', browserSync.reload);
	}
	// scss
	gulp.watch(options.assets_root+options.css_src+'/**/*.scss', {interval:options.watch_interval}, gulp.parallel(['scss']));
	// js
	//gulp.watch(options.assets_root+options.js_src+'/**/*.js', {interval:options.watch_interval}, ['js']);
	gulp.watch(options.assets_root+options.js_vendor+'/**/*.js', {interval:options.watch_interval}, gulp.parallel(['js-vendor']));
});


/******************** RUN TASKS ********************/
gulp.task('default', gulp.parallel([
	'browser-sync',
	'scss',
	'js',
	'js-vendor',
	'watch'
]));




