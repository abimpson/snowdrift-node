


(function(){
	
	var model = require('./m.all.js');
	var analytics = require('./h.analytics.js');

	module.exports = {


		/******************** INITIALISE ********************/
		init: function() {},


		/******************** CURRENT POPUP ********************/
		current: '',


		/******************** SHOW POPUP ********************/
		show: function(popup,vars){
			console.log('%c--- popup - '+popup+' ---','color:#E1BF97;');
			var self = this;
			// SET CURRENT
			self.current = popup;
			// HIDE EXISTING POPUPS
			$('.popup').hide();
			// SHOW POPUP
			if(model.device.ie8){
				$('#popup_'+popup).show();
				$('#popups').stop().show();
			}else{
				$('#popup_'+popup).show();
				$('#popups').stop().fadeIn(300);
			};
			// SCROLL TO TOP
			window.scroll(0,0);
			// POPUP START
			self.start(self.current);
			// ANALYTICS
			analytics.google.page('/popup/'+popup);
		},


		/******************** HIDE POPUPS ********************/
		hide: function() {
			if(model.device.ie8){
				$('#popups').hide();
			}else{
				$('#popups').fadeOut(300);
			};
		},


		/******************** START POPUP FUNCTIONS ********************/
		start: function(popup){
			var self = this;
			if(!$('#popup_'+popup).hasClass('loading')){
				// RESIZE
				self.resize();
				// POPUP SPECIFIC START
				/*if(hasKey($popup,popup+'.start')){
					$popup[popup].start();
					$popup[popup].started = true;
				};*/
			}else{
				clearTimeout(self.timeout_run);
				self.timeout_run = setTimeout(function(){
					self.start(popup);
				},10);
			};
		},


		/******************** RESIZE FUNCTIONS ********************/
		resize: function(){
			
		}


	};
	
	
}());



