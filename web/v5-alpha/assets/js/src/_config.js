


/**************************************************************************************************/
/*!************************ SNOWDRIFT JS FRAMEWORK - v5 - by ALEX BIMPSON ***********************!*/
/**************************************************************************************************/

(function(){


	/******************** SETUP ********************/
	var config = {};
	

	/******************** GENERAL ********************/
	// analytics
	config.analytics =					true; //use google analytics tracking,
	config.analytics_name =				'';
	// sharing - facebook
	config.fb_link =					'';
	config.fb_picture =					'';
	config.fb_title =					"";
	config.fb_caption =					"";
	config.fb_description =				"";


	/******************** PATHS ********************/
	config.path = {};
	if(window.location.host.indexOf('localhost')>=0){
		config.path.api =				'/api/v1/';
		config.path.images =			'/assets/images/';
	}else{
		config.path.api =				'/api/v1/';
		config.path.images =			'/assets/images/';
	}


	/******************** ERROR HANDLING ********************/
	config.errors = {
		show_generic:					true, // show generic error messages?
		show_response:					false, // show response error messages?
		show_timeout:					false, // show timeout error messages?
		show_ajax:						false, // show AJAX error messages?
		log_errors:						true, // log errors in the database?
	};
	
	
	/******************** PRELOADING ********************/
	config.preload = {
		timeout: 6000,
		fontcss: ['assets/css/dist/styles.css'],
		// fonts
		fonts: ['HelveticaNeueThin'],
		// all
		files: [
			config.path.images + 'face32.jpg',
			config.path.images + 'face33.jpg'
		],
		// all - non retina
		files_non_retina: [],
		// all - retina
		files_retina: [],
		// desktop
		files_d: [],
		// desktop - non retina
		files_d_non_retina: [],
		// desktop - retina
		files_d_retina: [],
		// mobile
		files_m: [],
		// mobile - non retina
		files_m_non_retina: [],
		// mobile - retina
		files_m_retina: []
	};
	
	
	/******************** FACEBOOK ********************/
	config.facebook = {
		app_id: '',
		scope: '',
		user_fields: 'id,first_name,last_name,gender,email,picture.height(200).width(200)',
		// ready
		fnc_ready: function(){},
		// auto - not logged in to facebook
		fnc_auto_not_logged_in: function(){},
		// auto - not authenticated
		fnc_auto_not_auth: function(){},
		// auto - authenticated
		fnc_auto_auth: function(){},
		// login - pre-authentication
		fnc_login_pre: function(){},
		// login - authenticated
		fnc_login_auth: function(){},
		// login cancelled
		fnc_cancelled: function(){},
		// permissions revoked
		fnc_revoked: function(){
			location.reload();
		}
	};


	/******************** TWITTER ********************/
	config.twitter = {
		// click - pre-authentication
		twit_click_pre: function(){},
		// click - authenticated
		twit_login_auth: function(){},
		// login cancelled
		twit_cancelled: function(){}
	};

	
	/******************** EXPORT ********************/
	module.exports = config;


}());



