


(function(){
	
	var config = require('./_config.js');

	module.exports = {


		/******************** GOOGLE ********************/
		google: {
			// PAGE
			page: function(page){
				if(config.analytics==true){
					console.log('%c••• analytics - google - page - '+page+' •••','color:#ccc;');
					// validate
					if(typeof(page)==='undefined') return false;
					if(page[0]=='/') page = page.substr(1);
					// submit
					ga('send', 'pageview', '/'+page);
				};
			},
			// EVENT
			event: function(category,action,label,value){
				if(config.analytics==true){
					console.log('%c••• analytics - google - event - '+category+' - '+action+' •••','color:#ccc;');
					// validate vars
					if(typeof(category)==='undefined') category = '';
					if(typeof(action)==='undefined') action = '';
					if(typeof(label)==='undefined') label = '';
					if(typeof(value)==='undefined') value = '';
					// submit
					ga('send', 'event', category, action, label, value);
				};
			},
			// CONVERSION
			/*conversion: function(){
				if(config.analytics_ga==true){
					console.log('%c••• analytics - google - conversion •••','color:#ccc;');
					// overall transaction
					var transaction = {
						'id': $shop.cart.order_number,
						'affiliation': 'Marmite',
						'revenue': $shop.getSubTotalCost()/100,
						'shipping': $shop.getShippingCost()/100,
						//'tax': '1.29',
						'currency': 'GBP'
					};
					// products
					var products = [];
					for(var i in $shop.cart.items){
						var product = $shop.getCartItemDetails($shop.cart.items[i].attributes);
						products = $analytics.google.conversion_add(products,{
							'id': $shop.cart.order_number,
							'name': product.helper_name,
							'sku': product.helper_sku,
							'category': 'Jar',
							'price': product.helper_price,
							'quantity': product.quantity,
							'currency': 'GBP'
						});
						if(product.giftwrap>0){
							products = $analytics.google.conversion_add(products,{
								'id': $shop.cart.order_number,
								'name': product.helper_giftwrap_name,
								'sku': product.helper_giftwrap_sku,
								'category': 'Giftbox',
								'price': product.helper_giftwrap_price,
								'quantity': product.giftwrap,
								'currency': 'GBP'
							});
						};
					};
					// submit
					ga('ecommerce:addTransaction',transaction);
					for(var j in products){
						ga('ecommerce:addItem',products[j]);
					};
					ga('ecommerce:send');
				};
			},
			conversion_add: function(products,product){
				var found = false;
				for(var i in products){
					if(
						products[i].name == product.name &&
						products[i].sku == product.sku &&
						products[i].category == product.category &&
						products[i].price == product.price
					){
						found = true;
						products[i].quantity = parseInt(products[i].quantity) + parseInt(product.quantity);
					};
				};
				if(!found){
					products.push(product);
				};
				return products;
			}*/
		},


		/******************** FACEBOOK ********************/
		/*fb:{
			// INIT
			inited:false,
			init:function(){
				if(config.analytics_fb==true){
					if($analytics.fb.inited==false){
						$analytics.fb.inited = true;
						!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
						n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
						n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
						t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
						document,'script','//connect.facebook.net/en_US/fbevents.js');
						fbq('init', config.analytics_fb_code);
					};
				};
			},
			// PAGE
			pages: [],
			page: function(page){
				if(config.analytics_fb==true){
					if($.inArray(page,$analytics.fb.pages)==-1){
						console.log('%c••• analytics - facebook - page - '+page+' •••','color:#ccc;');
						$analytics.fb.pages.push(page);
						$analytics.fb.init();
						//fbq('track', 'PageView', {value: [page]});
						fbq('track','ViewContent',{url:page});
						$analytics.google.event('fbpixel','fbpixel-page-'+page);
					};
				};
			},
			// CONVERSION
			conversion: function(value){
				if(config.analytics_fb==true){
					console.log('%c••• analytics - facebook - conversion •••','color:#ccc;');
					$analytics.fb.init();
					if(typeof(value)==='undefined'){
						value = '0.00';
					}else{
						value = round((value/100),2).toString();
					};
					fbq('track', 'Purchase', {value: value, currency: 'GBP'});
					$analytics.google.event('fbpixel','fbpixel-'+value);
				};
			}
		},*/


		/******************** PINTEREST ********************/
		/*pin: {
			// INIT
			inited:false,
			init: function(){
				if(config.analytics_pin==true){
					if($analytics.pin.inited==false){
						$analytics.pin.inited = true;
						!function(e){if(!window.pintrk){window.pintrk=function(){
							window.pintrk.queue.push(Array.prototype.slice.call(arguments))};
							var n=window.pintrk;n.queue=[],n.version="3.0";
							var t=document.createElement("script");t.async=!0,t.src=e;
							var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)
						}}("https://s.pinimg.com/ct/core.js");
						pintrk('load','2615523469883');
					}
				}
			},
			// PAGE
			pages: [],
			page: function(page){
				if(config.analytics_pin==true){
					if($.inArray(page,$analytics.pin.pages)==-1){
						console.log('%c••• analytics - pinterest - page - '+page+' •••','color:#ccc;');
						$analytics.pin.pages.push(page);
						$analytics.pin.init();
						pintrk('page',{
							page_name:page,
							page_category:page
						});
						$analytics.google.event('pinpixel','pinpixel-page-'+page);
					};
				};
			},
			// CONVERSION
			conversion: function(value,quantity){
				if(config.analytics_pin==true){
					console.log('%c••• analytics - pinterest - conversion •••','color:#ccc;');
					$analytics.pin.init();
					if(typeof(value)==='undefined'){
						value = '0.00';
					}else{
						value = round((value/100),2).toString();
					};
					if(typeof(quantity)==='undefined'){
						quantity = 0;
					}else{
						quantity = parseInt(quantity);
					};
					pintrk('track','checkout',{
						value: parseFloat(value),
						order_quantity: parseFloat(quantity),
					});
					$analytics.google.event('pinpixel','pinpixel-'+value+'-'+quantity);
				};
			}
		},*/


		/******************** TWITTER ********************/
		/*twitter:{
			// PIXEL CONVERSION TRACKING
			pixel: function(){
				console.log('%c••• ANALYTICS - TWITTER - TRACKING PIXEL •••','color:#ccc;');
				if(config.twit_pixel==true){
					twttr.conversion.trackPid('l5ewl');
				};
			}
		}*/


		/******************** AVINASH ********************/
		/*avinash: function(data){
			if(config.analytics_avinash){
				//['trackEvent', 'Conversion', 'Click to Purchase', 'Item ID']
				try{
					UDM.evq.push(data);
					console.log('%c••• analytics - avinash •••','color:#ccc;');
				}catch(e){};
			};
		}*/


		/******************** VE INTERACTIVE (CREAM UK) TRACKING ********************/
		/*ve_pixel: function(){
			console.log('%c••• ANALYTICS - VE INTERACTIVE - TRACKING PIXEL •••','color:#ccc;');
			try{
				var s = document.createElement("script");
				s.type = "text/javascript";
				s.src = "//config1.veinteractive.com/tags/190E95BA/7BD8/4C79/8614/1D5DB3DF46E5/tag.js";
				$("head").append(s);
			}catch(e){};
		}*/


	};
	
	
}());



