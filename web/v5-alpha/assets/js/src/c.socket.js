


(function(){

	module.exports = function(opts){
		
		
		/******************** SETUP ********************/
		var self = this;
		var io = require('../vendor/socket.io.js');


		/******************** CONFIG ********************/
		self.options = {
			// setup
			url: '',
			room: null,
			// debug
			debug_show: false,
			status_id: 'socket_status',
			debug_id: 'socket_debug',
			error_id: 'socket_error',
			// callbacks
			callback_connected: function(){},
			callback_disconnected: function(){},
			callback_error: function(){},
			callback_unavailable: function(){},
			// listeners
			listeners: []
		};


		/******************** DATA STORAGE ********************/
		self.s = null;
		self.data = {
			// status
			active: false,
			last_callback: ''
		};


		/******************** INIT ********************/
		self.init = function(){
			console.log('%c--- socket - initialising ---','color:#0097c9;');
			if(typeof(io)!=='undefined'){
				// CONNECT
				self.s = io.connect(self.options.url, {
					timeout: 5000,
					reconnection: true,
					reconnectionDelay: 100,
					reconnectionDelayMax : 5000,
					reconnectionAttempts: 99999
				});
				// CONNECT CALLBACK
				self.s.on('connect', function(){
					self.callbacks.connected();
				});
				// DISCONNECT CALLBACK
				self.s.on('connect_error', function(){
					self.callbacks.disconnected();
				});
				self.s.on('connect_timeout', function(){
					self.callbacks.disconnected();
				});
				self.s.on('disconnect', function(){
					self.callbacks.disconnected();
				});
				// RECONNECT
				self.s.on('reconnect', function(){});
				self.s.on('reconnect_attempt', function(){});
				self.s.on('reconnecting', function(){});
				self.s.on('reconnect_error', function(){});
				self.s.on('reconnect_failed', function(){});
				// ERROR CALLBACK
				self.s.on('error', function(){
					self.callbacks.error();
				});
				// LISTEN
				self.listen();
			}else{
				// UNAVAILABLE
				self.callbacks.unavailable();
			};
		};


		/******************** STATUS CALLBACKS ********************/
		self.callbacks = {
			// CONNECTED
			connected: function(){
				console.log('%c--- socket - connected ---','color:#0097c9;');
				self.data.active = true;
				self.status('connected');
				// join room
				if(self.options.room!=null){
					self.s.emit('join',self.options.room);
				};
				// save status
				self.data.last_callback = 'connected';
				// callback
				self.options.callback_connected();
			},
			// DISCONNECTED
			disconnected: function(){
				if(self.data.last_callback!='disconnected'){
					console.log('%c--- socket - disconnected ---','color:#0097c9;');
					self.data.active = false;
					self.status('disconnected');
					// save status
					self.data.last_callback = 'disconnected';
					// callback
					self.options.callback_disconnected();
				}
			},
			// ERROR
			error: function(){
				console.log('%c--- socket - error ---','color:#0097c9;');
				self.data.active = false;
				self.status('error');
				// save status
				self.data.last_callback = 'error';
				// callback
				self.options.callback_error();
			},
			// UNAVAILABLE
			unavailable: function(){
				// socket.io script has not been included
				console.log('%c--- socket - socket.io is unavailable ---','color:#0097c9;');
				self.data.active = false;
				self.status('unavailable');
				// save status
				self.data.last_callback = 'unavailable';
				// callback
				self.options.callback_unavailable();
			}
		};


		/******************** STATUS INDICATORS ********************/
		self.status = function(status){
			switch (status){
				case 'connected':
					$('#'+self.options.error_id).hide();
					$('#'+self.options.status_id).removeClass('green orange').addClass('green');
					self.debug('Socket Connected');
					break;
				case 'error':
					$('#'+self.options.error_id).show();
					$('#'+self.options.status_id).removeClass('green orange');//.addClass('orange');
					self.debug('Socket Error');
					break;
				case 'disconnected':
					$('#'+self.options.error_id).show();
					$('#'+self.options.status_id).removeClass('green orange');//.addClass('orange');
					self.debug('Socket Disconnected');
					break;
				case 'unavailable':
					$('#'+self.options.error_id).show();
					$('#'+self.options.status_id).removeClass('green orange');//.addClass('orange');
					self.debug('Socket Not Available');
					break;
				default:
					break;
			}
		};


		/******************** DISCONNECT ********************/
		self.disconnect = function(){
			self.disconnected = true;
			self.s.disconnect();
		};


		/******************** DEBUG TO HTML ELEMENT ********************/
		self.debug = function(text){
			if(self.options.debug_show == true){
				$('#'+self.options.debug_id).text(text);
			};
		};


		/******************** PING ********************/
		self.ping = function(){
			if(self.data.active == true){
				self.s.emit('ping', {});
			};
		};


		/******************** CLOSE ********************/
		self.close = function(){
			self.send('close',{});
		};


		/******************** SEND ********************/
		self.send = function(method,d){
			if(self.data.active == true){
				self.s.emit(method,d);
			};
		};


		/******************** ROOM - JOIN ********************/
		self.join_room = function(room){
			if(self.options.room!=null){
				self.s.emit('leave',self.options.room);
			}
			self.options.room = room;
			self.s.emit('join',room);
		};


		/******************** ROOM - LEAVE ********************/
		self.leave_room = function(room){
			self.options.room = null;
			self.s.emit('leave',room);
		};


		/******************** LISTEN ********************/
		self.listen = function(){
			// STANDARD
			self.s.on('init', function(response) {
				console.log('%c--- received init ---','color:#0097c9;');
			});
			self.s.on('ping', function(response) {
				console.log('%c--- received ping ---','color:#0097c9;');
			});
			self.s.on('close', function(response) {
				console.log('%c--- socket closed ---','color:#0097c9;');
				self.data.active = false;
				self.debug('Socket Closed');
			});
			self.s.on('join', function(response) {
				console.log('%c--- joined room '+response.room+' ---','color:#0097c9;');
			});
			// CUSTOM
			if(typeof(self.options.listeners)==='object' || typeof(self.options.listeners)==='object'){
				for(var i in self.options.listeners){
					self.listen_add(self.options.listeners[i]);
				};
			};
		};
		self.listen_add = function(l){
			self.s.on(l.method, function(response){
				l.func(response);
			});
		};


		/******************** INIT ********************/
		if(typeof(opts)==='object'){
			$.extend(self.options,opts);
		};
		self.init();


	};
	
}());



