/*jshint esversion: 6 */


(function(){
	
	
	/******************** DOCUMENT READY ********************/
    $(document).ready(function(){
		
		console.log('%c******************** DOCUMENT READY ********************','color:#c70284;');
		try{console.timeStamp('READY');}catch(e){};
		
		// modernizr
			if(typeof(Modernizr)!=='undefined'){
				console.log('%c*** MODERNIZR ***','color:#098c0c;');
				console.log(Modernizr);
			};
		// config
			var config = require('./_config.js'); console.log(config);
		// data model
			var model = require('./m.all.js'); model.init();
		// bind events
			var nav = require('./b.nav.js');
			var inputs = require('./b.inputs.js');
			var windowsize = require('./b.windowsize.js'); console.log(windowsize.w);
		// views
			var pages = require('./v.pages.js'); pages.init();
			var popups = require('./v.popups.js'); popups.init();
			//var social = require('./v.social.js');
			//var toggles = require('./v.toggles.js');
		// general app init
			var ajax = require('./h.ajax.js');
			//var common = require('./a.common.js');
		
		
		/*
		//bind events
		if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run(true);};
		if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run(true);};
		//facebook canvas sizing
		if(typeof($app)!=='undefined'&&typeof($app.canvas)!=='undefined'){$app.canvas();};
		//general app init
		if(typeof($errors)!=='undefined'&&typeof($errors.init)!=='undefined'){$errors.init();};
		if(typeof($hijax)!=='undefined'&&typeof($hijax.init)!=='undefined'){$hijax.init();};
		if(typeof($checks)!=='undefined'&&typeof($checks.init)!=='undefined'){$checks.init();};
		if(typeof($auth)!=='undefined'&&typeof($auth.init)!=='undefined'){$auth.init();};
		*/
		
		
		// css
		/*require.ensure(['../../css/dist/styles.css'], function(require) {
			require('../../css/dist/styles.css');
		});*/
		// $('head').append('<link rel="stylesheet" href="assets/css/dist/styles.css" type="text/css" />');
		
		// plupload
		//var plupload = require('./c.plupload.js');
		
		// socket
		/*var socket = require('./c.socket.js');
		var s = new socket({
			url: 'http://www.prrple.com:8889',
			listeners: [
				{
					method: 'test',
					func: function(response){
						console.log('test');
						console.log(response);
					}
				}
			]
		});*/
		
		// test
        var z_test = require('./z_test.js');
		z_test.init();
		
		// function
        var z_func = require('./z_func.js');
		z_func();
		
	});
	
	
	/******************** WINDOW LOADED ********************/
	$(window).load(function(){
		console.log('%c******************** WINDOW LOADED ********************','color:#c70284;');
		try{console.timeStamp('LOADED');}catch(e){};
		
		// preload
		var preload = require('./h.preload.js'); preload.init();
		
		// analytics
		var analytics = require('./h.analytics.js');
		setTimeout(function(){
			analytics.google.page('test');
		},1000);
		
		
		/*if(typeof($m)!=='undefined'){$m.app.loaded = true;};
		//preload
		if(typeof($preload)!=='undefined'&&typeof($preload.init)!=='undefined'){$preload.init();};
		//bind events
		if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run(true);};
		if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true);};
		if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run(true);};*/
		//check for facebook server
		/*if(!window.FB){
			$errors.generic_error('Sorry, Facebook\'s app server is currently unavailable.<br />Please try again shortly.');
		};*/
	});



	/******************** WINDOW RESIZE ********************/
	$(window).resize(function(){
		//bind events
		/*if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run();};
		if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run();};
		if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run();};*/
	});



	/******************** WINDOW SCROLL ********************/
	$(window).scroll(function(){
		//bind events
		/*if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run();};*/
	});



	/******************** WINDOW BLUR / FOCUS ********************/
	$(window).blur(function(){

	});
	$(window).focus(function(){

	});
	

}());



