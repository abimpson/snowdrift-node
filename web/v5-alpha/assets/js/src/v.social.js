


(function(){
	
	var config = require('./_config.js');
	var analytics = require('./h.analytics.js');

	module.exports = {


		/******************** FACEBOOK ********************/
		fb: {
			//BASIC SHARE DIALOG
			share: function(url){
				window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(url),'_blank');
				analytics.google.page('/share/facebook/share');
			},
			//TIMELINE
			timeline: function(options){
				var d = {
					method: 		'feed',
					to:				(typeof(to)!=='undefined'?to:''),
					link: 			config.fb_link, //+($m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+$m.user.identity+'"}'):'')
					picture: 		config.fb_picture,
					name: 			config.fb_title,
					caption: 		config.fb_caption,
					description: 	config.fb_description
				};
				$.extend(d,options);
				console.log(options,d);
				FB.ui(d,function(response){
					console.log(response);
					analytics.google.page('/share/facebook/timeline');
				});
			},
			//MESSAGE
			message: function(options){
				var d = {
					method:			'send',
					//to:			userId,
					link:			config.fb_link, //+($m.user.identity!=null?'?app_data='+encodeURIComponent('{"id":"'+$m.user.identity+'"}'):'')
					picture:		config.fb_picture,
					name:			config.fb_title,
					description:	config.fb_description
				};
				$.extend(d,options);
				FB.ui(d,function(response){
					console.log(response);
					analytics.google.page('/share/facebook/message');
				});
			},
			//APP REQUEST
			request: function(options){
				var d = {
					method:			'apprequests',
					message:		config.fb_description,
					data:			encodeURIComponent('{"id":"'+$m.user.identity+'"}')
				};
				$.extend(d,options);
				FB.ui(d,function(response){
					console.log(response);
					analytics.google.page('/share/facebook/app_request');
				});
			},	
			//POST TO WALL (AUTOMATIC)
			post: function(){
				FB.api(
					"/me/feed",
					"POST",
					{
						"message": "This is a test post",
						"link": "http://www.kerve.com"
					},
					function (response) {
						console.log(response);
						analytics.google.page('/share/facebook/post_to_wall');
					}
				);
			},
			//PHOTO
			photo: function(){
				$facebook.upload_photo(
					'http://3.bp.blogspot.com/-6m8Efby6bRw/Tphtmo3zR3I/AAAAAAAAASY/QVcJtUoYUDE/s400/sea-wallpaper+5.jpg',
					'This is a test post',
					$app.callback
				);
				analytics.google.page('/share/facebook/photo');
			},
			//ACTION
			action: function(){
				$facebook.add_perms('publish_actions',this.fb.action2);
			},
			action2: function(){
				FB.api('/me/'+$facebook.namespace+':create', 'post', 
					{
						soundtrack:actionUrl
					},
					function(response) {
						console.log(response);
						analytics.google.page('/share/facebook/action');
					}
				);
			},
			//NOTIFICATION
			notification: function(){
				$facebook.send_notification(
					$facebook.user.id,
					APP_ACCESS_TOKEN,
					'http://www.kerve.com',
					'This is a test notification',
					function(response){
						console.log(response);
						analytics.google.page('/share/facebook/notification');
					}
				);
			}
		},


		/******************** TWITTER ********************/
		twitter: {
			//BASIC SHARE DIALOG
			share: function(options){
				var d = {
					copy: config.tweet_copy,
					url: config.tweet_url
				};
				$.extend(d,options);
				if(typeof(d.copy)=='string' && d.copy!='' && typeof(d.url)=='string' && d.url!=''){
					// copy & url
					window.open('https://twitter.com/share?url='+encodeURIComponent(d.url)+'&text='+encodeURIComponent(d.copy),'_blank');
				}else if(typeof(d.copy)=='string' && d.copy!=''){
					// copy only
					window.open('https://twitter.com/share?text='+encodeURIComponent(d.copy),'_blank');
				}else if(typeof(d.url)=='string' && d.url!=''){
					// url only
					window.open('https://twitter.com/share?url='+encodeURIComponent(d.url),'_blank');
				}else{
					// blank
					window.open('https://twitter.com/share','_blank');
				};
				analytics.google.page('/share/twitter/tweet');
			}
		}


	};
	
	
}());



