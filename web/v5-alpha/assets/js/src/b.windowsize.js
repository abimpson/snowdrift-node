


(function(){

	var pages = require('./v.pages.js');
	var popups = require('./v.popups.js');
	
	module.exports = {


		/******************** CONFIG ********************/
		use: true,
		freq: 100,


		/******************** DATA ********************/
		timeout: null,
		w: 0,
		h: 0,
		r: 0,
		w2: 0,
		h2: 0,
		r2: 0,
		
		
		/******************** INIT ********************/
		init: function(){
			self = this;
			self.run(true);
			$(document).ready(function(){
				self.run(true);
			});
			$(window).load(function(){
				self.run(true);
			});
			$(window).resize(function(){
				self.run();
			});
		},


		/******************** RUN ********************/
		run: function(force){
			self = this;
			if(self.use == true){
				if(force==true){
					self.func(force);
				}else{
					clearTimeout(self.timeout);
					self.timeout = setTimeout(function(){
						self.func(force);
					}, self.freq);
				};
			};
		},
		func: function(force){
			console.log('%c--- windowsize ---','color:#DDD6AA');
			self = this;
			var oldWidth = self.w;
			var oldHeight = self.h;
			var oldWidth2 = self.w2;
			var oldHeight2 = self.h2;
			self.w = $(window).width();
			self.h = $(window).height();
			self.r = self.w/self.h;
			self.w2 = (window.innerWidth ? window.innerWidth : self.w);
			self.h2 = (window.innerHeight ? window.innerHeight : self.h);
			self.r2 = self.w2/self.h2;
			if(force == true){
				self.w_func();
				self.h_func();
				self.x_func();
			}else{
				if(self.w!=oldWidth || self.w2!=oldWidth2){
					self.w_func();
				};
				if(self.h!=oldHeight || self.h2!=oldHeight2){
					self.h_func();
				};
				if(self.w!=oldWidth || self.w2!=oldWidth2 || self.h!=oldHeight || self.h2!=oldHeight2){
					self.x_func();
				};
			};
		},


		/******************** CALLBACK FUNCTIONS ********************/
		w_func: function(){

		},
		h_func: function(){

		},
		x_func: function(){
			//page resizing
			if(typeof(pages)!=='undefined' && typeof(pages.resize)!=='undefined'){
				pages.resize();
			}
			//popup resizing
			if(typeof(popups)!=='undefined' && typeof(popups.resize)!=='undefined'){
				popups.resize();
			}
		}


	};

	module.exports.init();

}());



