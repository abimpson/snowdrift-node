


(function(){

	module.exports = {


		/******************** CONFIG ********************/
		similarLongChars:			[["|<", "k"], ["(.)","boob"]],
		similarChars:				[["@", "a"], ["0", "o"], ["1", "i"], ["2", "r"], ["3", "e"], ["4", "a"], ["5", "s"], ["7", "t"], ["8", "b"], ["9", "g"], ["Ã¤", "a"], ["Ã£", "a"], ["Ã¢", "a"], ["Ã¤", "a"], ["Ã¡", "a"], ["Ã ", "a"], ["Ã¥", "a"], ["Ã©", "e"], ["Ã¨", "e"], ["Ã«", "e"], ["Ãª", "e"], ["Â§", "s"], ["$", "s"], ["Â£", "l"], ["â‚¬", "e"], ["Ã¼", "u"], ["Ã»", "u"], ["Ãº", "u"], ["Ã¹", "u"], ["Ã®", "i"], ["Ã¯", "i"], ["Ã", "i"], ["Ã¬", "i"], ["Ã¿", "y"], ["Ã½", "y"], ["Ã¶", "o"], ["Ã´", "o"], ["Ãµ", "o"], ["Ã³", "o"], ["Ã²", "o"]],
		badCompleteWordList:		['anal','arse','ass','balls','bum','cock','coon','cum','muff','rape','sex','tit','turd','kunt'],
		// original list
		//badList:					['69','anal','analsex','anus','arsebandit','arsewipe','ass','asshole','assmunch','ballbag','ballsack','bareback','bastard','beefcurtain','bellend','biatch','bimbo','bitch','bitches','bloody','blowjob','bollock','bollocks','bollok','bondage','boner','boob','boobs','booty','breasts','bugger','bullshit','bumhole','butt','buttcheeks','buttplug','clit','clitoris','clunge','cocaine','cockface','cockgobbler','crap','cumface','cumming','cumslag','cunnilingus','cunt','damn','deepthroat','dickhead','dildo','doggiestyle','doggystyle','dyke','ecstasy','ejaculation','fag','faggot','fannyflaps','fart','feck','felch','felching','fellate','fellatio','fingerbang','fingering','fisting','flange','flaps','foof','footjob','fuck','fudgepacker','gash','genitals','goddamn','gspot','handjob','hitler','homo','hooker','humping','incest','intercourse','jackoff','japseye','jerk','jerkoff','jizz','ketamine','kinky','knob','knobbing','knobend','labia','lesbo','lmao','lmfao','makemewet','marijuana','masturbate','mdma','meatcurtain','meatsword','milf','minge','minger','muffdiver','nazi','negro','nigga','nigger','nignog','nipple','nobcheese','nobhead','nympho','nymphomania','orgasm','orgy','paedo','paedophile','paki','panties','penis','piss','poof','poofter','poon','poop','porksword','porn','pornography','prick','prostitute','pube','pussy','queaf','queer','raghead','raping','rapist','rectum','rentboy','rimjob','rimming','sadism','schlong','scrotum','shart','shit','slag','slagbag','sleaze','slit','slut','smeg','smegma','smut','spacko','spaff','spastic','spunk','strapon','swastika','teabagging','testicle','threesome','throating','tits','titties','titty','titwank','tosser','tosspot','tranny','twat','upthearse','upthebum','vagina','vibrator','voyeur','vulva','wank','wanker','wankonmytit','weed','whore','woofter','wtf'],
		// new list
		badList:					['69','50 yard cunt punt','5h1t','5hit','a_s_s','a2m','a55','adult','amateur','anal','Anal','anal impaler','anal leakage','Anal sex','analsex','anilingus','anus','ar5e','arrse','arse','arsebandit','arsehole','arsewipe','ass','Ass','ass fuck','ass-fucker','asses','assfucker','assfukka','asshole','Asshole','assholes','assmucus','assmunch','asswhole','autoerotic','b!tch','b00bs','b17ch','b1tch','Ball bag','ballbag','ballsack','bang (ones) box','bangbros','bareback','bastard','Bastard','beastial','beastiality','beef curtain','beefcurtain','bellend','bestial','bestiality','bi+ch','biatch','bimbo','bimbos','birdlock','bitch','Bitch','bitch tit','bitcher','bitchers','bitches','bitchin','bitching','BJ','bloody','blow job','blow me','blow mud','blowjob','blowjobs','blue waffle','blumpkin','boiolas','bollock','bollocks','bollok','bondage','boner','Boner','boob','Boob','boobs','booobs','boooobs','booooobs','booooooobs','booty','Breast','breasts','breximite','buceta','bugger','Bukkake','bullshit','bum','bumhole','bunny fucker','bust a load','busty','butt','butt fuck','buttcheeks','butthole','buttmuch','buttplug','c0ck','c0cksucker','carpet muncher','carpetmuncher','cawk','chink','Chink','choade','chota bags','cipa','cl1t','clit','clit licker','clitoris','clits','clitty litter','clung','clusterfuck','cnut','cocaine','cock','Cock','cock pocket','cock snot','cock-sucker','cockface','cockgobbler','cockhead','cockmunch','cockmuncher','cocks','cocksuck ','cocksucked ','cocksucker','cocksucking','cocksucks ','cocksuka','cocksukka','cok','cokmuncher','coksucka','Condom','coon','cop some wood','cornhole','corp whore','cox','crap','cum','Cum','cum chugger','cum dumpster','cum freak','cum guzzler','cumdump','cumface','cummer','cumming','cums','cumshot','cumslag','cunilingus','cunillingus','cunnilingus','cunt','Cunt','cunt hair','cunt-struck','cuntbag','cuntlick ','cuntlicker ','cuntlicking ','cunts','cuntsicle','cut rope','cyalis','cyberfuc','cyberfuck ','cyberfucked ','cyberfucker','cyberfuckers','cyberfucking ','d1ck','Dago','damn','Deep throat','Deep-throat','deepthroat','Deepthroating','dick','Dick','dick hole','dick shy','dickhead','DickwadDike','dildo','Dildo','dildos','dink','dinks','dirsa','dirty Sanchez','dlck','dog-fucker','doggie style','doggiestyle','doggin','dogging','doggystyle','donkeyribber','doosh','Douche bag','duche','dyke','Dyke','eat a dick','eat hair pie','ecstasy','ejaculate','ejaculated','ejaculates ','ejaculating ','ejaculatings','ejaculation','ejakulate','Erection','erotic','Erotic','Explicit','f u c k','f u c k e r','f_u_c_k','f4nny','facial','fag','Fag','fagging','faggitt','faggot','faggs','fagot','fagots','fags','fanny','fannyflaps','fannyfucker','fanyy','fart','fatass','fcuk','fcuker','fcuking','feck','fecker','felch','felching','fellate','fellatio','Fetish','fingerbang','Fingered','fingerfuck ','fingerfucked ','fingerfucker ','fingerfuckers','fingerfucking ','fingerfucks ','fingering','fist fuck','fistfuck','fistfucked ','fistfucker ','fistfuckers ','fistfucking ','fistfuckings ','fistfucks ','fisting','Fisting','flange','flaps','flog the log','foof','fook','fooker','footjob','Fornicate','Fuc','fuck','fuck hole','fuck puppet','fuck trophy','fuck yo mama','fuck-ass','fuck-bitch','fucka','fucked','fucker','Fucker','fuckers','fuckhead','fuckheads','fuckin','fucking','fuckings','fuckingshitmotherfucker','fuckme ','fuckmeat','fucks','fucktoy','fuckwhit','fuckwit','fudge packer','fudgepacker','fuk','Fuk','fuker','fukker','fukkin','fuks','fukwhit','fukwit','fux','fux0r','Gang bang','gang-bang','gangbang','gangbanged ','gangbangs ','gash','gassy ass','gaylord','gaysex','Genital','Genitalia','genitals','Get laid','Getlaid','Glory hole','goatse','god','god damn','god-dam','god-damned','goddamn','goddamned','gspot','ham flap','handjob','Hard-on','hardcoresex ','Hardon','hell','heshe','hitler','hoar','hoare','hoer','homo','homoerotic','Honkey','hooker','hore','horniest','horny','hotsex','how to kill','how to murdep','humping','Hustler','incest','Inseminate','intercourse','jack-off ','jackoff','jap','japseye','jerk','Jerk off','jerk-off ','jerkoff','jism','jiz ','jizm ','jizz','Jizz face','Jizz MonkeyJizz','kawk','ketamine','kinky','kinky Jesus','knob','knob end','knobbing','knobead','knobed','knobend','knobhead','Knobjockie','knobjocky','knobjokey','kock','kondum','kondums','kum','kummer','kumming','kums','kunilingus','kwif','l3i+ch','l3itch','labia','LEN','lesbo','lmao','lmfao','lust','lusting','m0f0','m0fo','m45terbate','ma5terb8','ma5terbate','mafugly','makemewet','marijuana','masochist','master-bate','masterb8','masterbat*','masterbat3','masterbate','masterbation','masterbations','masturbate','Masturbation','mdma','meatcurtain','meatsword','milf','Milf','minge','minger','mo-fo','mof0','mofo','Molest','mothafuck','mothafucka','mothafuckas','mothafuckaz','mothafucked ','mothafucker','mothafuckers','mothafuckin','mothafucking ','mothafuckings','mothafucks','mother fucker','motherfuck','motherfucked','motherfucker','Motherfucker','motherfuckers','motherfuckin','motherfucking','motherfuckings','motherfuckka','motherfucks','muff','muff puff','muffdiver','mutha','muthafecker','muthafuckker','muther','mutherfucker','n1gga','n1gger','nazi','need the dick','negro','nigg3r','nigg4h','nigga','Nigga','niggah','niggas','niggaz','Niggaz','nigger','Nigger','niggers ','nignog','nipple','Nipple','nob','nob jokey','nobcheese','nobhead','nobjocky','Nude','Nudity','numbnuts','nut butter','nutsack','nympho','nymphomania','omg','Oral sex','orgasim ','orgasims ','orgasm','orgasms ','Orgie','orgy','p0rn','paedo','paedophile','paki','Paki','Panface','panties','Panties','pawn','pecker','Pedophil','Pedophile','Penetrate','penis','Penis','penisfucker','phonesex','phuck','phuk','phuked','phuking','phukked','phukking','phuks','phuq','pigfucker','pimpis','piss','Piss','pissed','pisser','pissers','pisses ','pissflaps','pissin ','pissing','pissoff ','poof','poofter','poon','poop','porksword','porn','Porn','porno','pornography','pornos','prick','pricks ','pron','prostitute','pube','Pubic','pusse','pussi','pussies','pussy','pussy fart','pussy palace','pussys ','queaf','queaf','queer','raghead','Rape','raping','rapist','rectum','rentboy','retard','rimjaw','rimjob','Rimjob','rimming','s hit','s_h_i_t','s.o.b.','sadism','sadist','sandbar','sausage queen','schlong','screwing','scroat','scrote','scrotum','semen','sex','Sexual','Sexually explicit','sh!+','sh!t','sh1t','shag','shagger','shaggin','shagging','shart','shemale','shi+','shit','shit fucker','shitdick','shite','shited','shitey','shitfuck','shitfull','shithead','shiting','shitings','shits','shitted','shitter','shitters ','shitting','shittings','shitty ','Sit on my face','Sixty-nineSixtynine','skank','slag','Slag','slagbag','sleaze','slit','slope','slut','Slut','slut bucket','sluts','smeg','smegma','smut','snatch','son-of-a-bitch','spac','spacko','spaff','spastic','Sperm','Spic','Spooge','spunk','Squirt','strapon','suck my dick','suck my cock','suck my penis','suck my scrotum','scrotum','swastika','t1tt1e5','t1tties','teabagging','Teabagging','teets','teez','tesco','testical','testicle','Testicles','Three-way','threesome','throating','tit','tit wank','titfuck','tits','Tits','titt','tittie5','tittiefucker','titties','titty','tittyfuck','tittywank','titwank','Topless','tosser','tosspot','tranny','turd','tw4t','twat','twathead','twatty','Twink','twunt','twunter','Uncircumcised','upthearse','upthebum','Urination','v14gra','v1gra','vagina','viagra','vibrator','voyeur','vulva','w00se','wang','wank','wanker','Wanking','wankonmytit','wanky','weed','Whack off','Whackoff','Whip it out','Whipitout','White swallows','Whiteswallow','whoar','whore','willies','willy','woofter','wtf','xrated','xxx'],


		/******************** VARS ********************/
		i:							0,
		p:							0,
		word:						'',
		stars:						'',
		chars:						[],
		newString:					'',
		origString:					'',
		origStringNoSpaces:			[],
		origSpaceHyphenPositions:	[],


		/******************** CENSOR STRING ********************/
		censor: function(str){
			var self = this;
			// reset vars
			self.origSpaceHyphenPositions = [];
			// set to lowercase
			self.origString = str.toLowerCase();
			// replace non-single similar characters in Orig string
			self.resetVars();
			for (self.i=0; self.i< self.similarLongChars.length; self.i++){
				self.chars = self.similarLongChars[self.i];
				self.origString = self.stringReplace(self.origString, self.chars[0], self.chars[1]);
			};
			self.origStringNoSpaces = self.origString.split(' ');
			// replace bad complete words
			self.resetVars();
			$(self.badCompleteWordList).each(function(){
				for (self.i=0; self.i<self.origStringNoSpaces.length; self.i++){
					var tempWord = self.origStringNoSpaces[self.i].split('-').join('');
					if(this == self.origStringNoSpaces[self.i] || this == tempWord){
						self.stars = "";
						for(self.p=0; self.p<this.length; self.p++){
							self.stars = self.stars+"*";
						};
						self.origStringNoSpaces[self.i] = self.stars;
					};
				};
			});
			self.newString = self.origStringNoSpaces.join('');
			// space & hyphen positions
			self.resetVars();
			for (self.i=0; self.i<self.origString.length; self.i++) {
				if(self.origString.charAt(self.i) == " " || self.origString.charAt(self.i) == "-") {
					var arr = [];
					arr.push(self.i);
					arr.push(self.origString.charAt(self.i));
					self.origSpaceHyphenPositions.push(arr);
				};
			};
			self.newString = self.newString.split('-').join('');
			// replace single similar characters in New string
			self.resetVars();
			for (self.i=0; self.i<self.similarChars.length; self.i++){
				self.chars = self.similarChars[self.i];
				self.newString = self.stringReplace(self.newString, self.chars[0], self.chars[1]);
			};
			// replace bad words with stars
			self.resetVars();
			$(self.badList).each(function(){
				if (self.newString.indexOf(this) != -1) {
					self.stars = "";
					for(self.i=0; self.i<this.length; self.i++){
						self.stars = self.stars+"*";
					};
					self.newString = self.stringReplace(self.newString, this, self.stars);
				};
			});
			// replace spaces & hyphens
			self.resetVars();
			for (self.i=0; self.i<self.origSpaceHyphenPositions.length; self.i++){
				self.newString = self.newString.slice(0, self.origSpaceHyphenPositions[self.i][0]) + self.origSpaceHyphenPositions[self.i][1] + self.newString.slice(self.origSpaceHyphenPositions[self.i][0]);
			};
			// replace similar characters
			self.resetVars();
			for (self.i=0; self.i<self.origString.length; self.i++) {
				for (self.p=0; self.p<self.similarChars.length; self.p++) {
					if(self.origString.charAt(self.i) == self.similarChars[self.p][0]) {
						if(self.newString.charAt(self.i) != "*"){
							self.newString = self.newString.slice(0, self.i) + self.similarChars[self.p][0] + self.newString.slice(self.i+1);
						};
						self.p=1000;
					};
				};
			};
			// return
			//console.log( self.origString+" - "+self.newString );
			return self.newString;
		},


		/******************** CHECK STRING ********************/
		check: function(str){
			var self = this;
			return (str==self.censor(str)?true:false);
		},


		/******************** RESET VARS ********************/
		resetVars: function(){
			var self = this;
			self.i = 0;
			self.p = 0;
			self.chars = [];
		},


		/******************** STRING REPLACE ********************/
		stringReplace: function(input, find, replace) {
			while (input.indexOf("find") != -1){
				input = input.split(find).join(replace);
			};
			return input.split(find).join(replace);
		}
		
		
	};
	
	
}());



