


(function(){
	
	var ajax = require('./h.ajax.js');
	var pages = require('./v.pages.js');
	var popups = require('./v.popups.js');
	
    $(document).ready(function(){
		

		/******************** GENERAL ********************/
		// pages
		$('body').on('click','[data-page]',function(){
			var page = $(this).attr('data-page');
			if(page && typeof(page)!=='undefined'){
				pages.show(page);
			};
			return false;
		});
		// popups
		$('body').on('click','[data-popup]',function(){
			var popup = $(this).attr('data-popup');
			if(popup && typeof(popup)!=='undefined'){
				popups.show(popup);
			};
			return false;
		});
		// close popups
		$('body').on('click','.btn_close_popup, .btn_close_popup2, .blackout',function(){
			$popups.hide();
			return false;
		});


		/******************** MISC ********************/
		// click - tweet
		$('.btn_tweet').click(function(){
			var social = require('./v.social.js');
			social.twitter.share({
				copy: 'my copy',
				url: 'http://www.kerve.co.uk'
			});
		});
		// click - require
		$('.btn_require').click(function(){
			require.ensure(['./z_require.js'], function() {
				var z_require = require('./z_require.js');
				z_require.init();
			});
		});
		// click - require
		/*$('.btn_require2').click(function(){
			var req = './' + $(this).attr('data-require') + '.js';
			console.log(req);
			require.ensure([req], function() {
				var z_require = require(req);
				z_require.init();
			});
		});*/
		// click - ajax
		$('.btn_ajax').click(function(){
			ajax.request({
				url: 'assets/data/data.json',
				data: {},
				method: 'GET',
				callback: function(response){
					console.log(response);
				}
			});
		});
		// click - ping
		$('.btn_ping').click(function(){
			s.ping();
			//s.send('test',{test:1});
		});
		// filter
		$('#btn_filter').click(function(){
			var filter = require('./h.filter.js');
			var string = $('#input_filter').val();
			var check = filter.check(string);
			var censored = filter.censor(string);
			var output = (!check?'Profanity filtered: '+censored:'No profanity found.');
			$('#output_filter').text(output);
		});
		
		
    });

}());



