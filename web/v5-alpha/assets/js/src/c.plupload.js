


(function(){

	module.exports = function(opts){


		/******************** CONFIG ********************/
		var self = this;
		this.options = {
			//source files
			src: 'assets/js-plugins/plupload/',
			php: 'assets/php/plupload.php',
			//button
			btn: '#btn_plupload',
			//callbacks
			callback_before_upload: function(){},
			callback_upload_complete: function(){}
		};


		/******************** DATA STORAGE ********************/
		this.uploader = null;
		this.inited = false;


		/******************** INIT ********************/
		this.init = function(u){
			console.log('--- Plupload Init ---');
			//if(self.inited == false || (yourBrowser=='IE' && yourBrowserVersion<10)){
				self.inited = true;
				self.init_func(
					self.options.btn.replace('#',''),
					self.uploader
				);
				self.reset();
			//};
		};


		/******************** INIT UPLOADER ********************/
		this.init_func = function(btn,u){
			console.log('--- Plupload Init Run ---');
			//INIT
			u = new plupload.Uploader({
				runtimes : 'html5,flash,silverlight,html4', //runtimes
				browse_button: btn, //browse button - this can be an id of a DOM element or the DOM element itself
				//container: 'plupload_container',
				url: self.options.php, //php upload handler
				multi_selection: false, //allow multiple files?
				//chunk_size: 5242880, //chunking
				filters: { //file filters
					max_file_size : '5mb',
					prevent_duplicates: false,
					mime_types: [
						{title : "Image files", extensions : "jpg,jpeg,png"}
						//{title : "Zip files", extensions : "zip"}
					]
				},
				resize: { //client-side resizing
					width: 600,
					height: 600,
					crop: true
				},
				unique_names: true, //save files with unique file names
				flash_swf_url : self.options.src+'Moxie.swf', //Flash settings //'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
				silverlight_xap_url : self.options.src+'Moxie.xap', //Silverlight settings //'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',
				file_data_name: 'userfile', //field name
				multipart_params: {} //form fields
			});
			u.init();
			//POST INIT
			u.bind('PostInit', function(up,file){
				self.post_init(u,up,file);
			});
			//LIST FILE QUEUE
			u.bind('FilesAdded', function(up,files){
				self.files_added(u,up,files);
			});
			//BEFORE UPLOAD
			u.bind('BeforeUpload', function(up){
				self.before_upload(u,up);
			});
			//UPLOAD - PROGRESS
			u.bind('UploadProgress', function(up,file){
				self.upload_progress(u,up,file);
			});
			//UPLOAD - COMPLETE
			u.bind('UploadComplete', function(up,response){
				self.upload_complete(u,up,response);
			});
			//UPLOAD - ERROR
			u.bind('Error', function(up,err){
				self.upload_error(u,up,err);
			});
		};


		/******************** STATUS FUNCTIONS ********************/
		//reset uploader
		this.reset = function(){
			$(self.options.btn).removeClass('disabled').removeAttr('disabled');
		};
		//disable uploader - e.g. during upload
		this.disable = function(){
			$(self.options.btn).addClass('disabled').attr('disabled','disabled');
		};
		//enable uplaoded - e.g. after upload
		this.enable = function(){
			$(self.options.btn).removeClass('disabled').removeAttr('disabled');
		};


		/******************** SUBMIT FUNCTION ********************/
		this.submit = function(u){
			//pass vars to php
			u.settings.multipart_params = {
				'foo': 'bar'
			};
			//submit
			u.start();
		};


		/******************** CALLBACK - INITIALISED ********************/
		this.post_init = function(u,up,file){
			console.log('--- PostInit ---');
		};


		/******************** CALLBACK - FILES ADDED ********************/
		this.files_added = function(u,up,files){
			console.log('--- FilesAdded ---');
			console.log(up.files);
			while (up.files.length > 1) {
				up.removeFile(up.files[0]);
			};
			if(up.files.length > 0){
				//
				self.disable();
				//SUBMIT
				self.submit(u);
			};
		};


		/******************** CALLBACK - BEFORE UPLOAD ********************/
		this.before_upload = function(u,up){
			console.log('--- BeforeUpload ---');
			console.log(up);
			self.options.callback_before_upload(up);
		};


		/******************** CALLBACK - UPLOAD IN PROGRESS ********************/
		this.upload_progress = function(u,up,file){
			//console.log('--- UploadProgress ---');
			//console.log(file);
		};


		/******************** CALLBACK - UPLOAD COMPLETE ********************/
		this.upload_complete = function(u,up,response){
			console.log('--- UploadComplete ---');
			console.log(response);
			console.log(response[0]);
			if(response && response[0] && response[0].target_name && response[0].target_name!='undefined' && response[0].target_name!=''){
				//SUCCESS
				//var filename = response[0].target_name;
				self.enable();
				self.options.callback_upload_complete(response);
			}else{
				//ERROR
				console.log('--- Error ---');
				self.reset();
			};
			up.splice();
			up.refresh();
		};


		/******************** CALLBACK - UPLOAD ERROR ********************/
		this.upload_error = function(u,up,err){
			console.log('--- Error ---');
			console.log(err);
			//self.reset();
		};


		/******************** INIT ********************/
		if(typeof(opts)==='object'){
			$.extend(this.options,opts);
		};
		this.init();


	};
	
}());



