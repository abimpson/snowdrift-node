


(function(){

	module.exports = {
		
		
		/******************** START ********************/
		started: false,
		start: function(){
			console.log('MODULE START');
		},

		
		/******************** STOP ********************/
		stop: function(){
			console.log('MODULE STOP');
		},

		
		/******************** RESIZE ********************/
		resize: function(){
			console.log('MODULE RESIZE');
		}
		

	};
	
}());



