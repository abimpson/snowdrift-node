


(function(){

	module.exports = {


		/******************** INIT ********************/
		init: function() {
			var self = this;
			// INPUTS
			self.inputs();
			// CHECKBOXES
			self.checkboxes();
		},


		/******************** INPUTS ********************/
		inputs: function(){
			if(!Modernizr.input.placeholder){
				// apply placeholders
				$('[placeholder]').each(function(){
					var placeholder = $(this).attr('placeholder');
					// set placeholder
					if($(this).val()==''){
						$(this).val(placeholder).addClass('has_placeholder');
					}
					// focus
					$(this).focus(function(){
						if($(this).val()==placeholder){
							$(this).val('').removeClass('has_placeholder');
						}
					});
					// blur
					$(this).blur(function(){
						if($(this).val()==''){
							$(this).val(placeholder).addClass('has_placeholder');
						}
					});
				});
				// remove on form submission
				/*$('form').submit(function(){
					$('[placeholder]').each(function(){
						if($(this).val()==placeholder){
							$(this).val('');
						}
					});
				});*/
			}
		},


		/******************** CHECKBOXES ********************/
		checkboxes: function(){
			$('.js_checkbox').each(function(){
				$(this).data({
					checked: 'no'
				});
				$(this).click(function(){
					if($(this).data('checked')=='no'){
						$(this).data({
							checked: 'yes'
						});
						$(this).addClass('checked');
					}else{
						$(this).data({
							checked: 'no'
						});
						$(this).removeClass('checked');
					};
				});
			});
		},


		/******************** SELECTS ********************/
		selects: function(){
			/*$('#form_bra_size').change(function(){
				if($(this).val()=='SELECT'){
					$('#confirm_brasize .selectric').addClass('xselect');
				}else{
					$('#confirm_brasize .selectric').removeClass('xselect');
				};
			});*/
		}


	};
					  
}());



