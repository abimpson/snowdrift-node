


(function(){
	
	var model = require('./m.all.js');
	var analytics = require('./h.analytics.js');

	module.exports = {


		/******************** CONFIG ********************/
		default: 'home',
		pushstate: false,


		/******************** DATA ********************/
		page: {
			prev: '',
			current: ''
		},
		module: {
			prev: null,
			current: null
		},
		
		
		
		
		prev: {
			page: '',
			module: null
		},
		current: {
			page: '',
			module: null
		},


		/******************** INIT ********************/
		init: function(){
			var self = this;
			self.pop();
			self.show(self.default);
		},


		/******************** LAUNCH FIRST PAGE ********************/
		launch: function(){
			var self = this;
			var page = getURLParameter('page');
			if(page!=null && page!='null' && $('#page_'+page).length>0){
				self.show(page,pushit);
			}else{
				self.show(self.default,pushit);
			}
		},


		/******************** SHOW PAGE ********************/
		show: function(page,pushit) {
			console.log('%c--- page - '+page+' ---','color:#E1BF97;');
			var self = this;
			if(self.page.current != page){
				// UPDATE PAGE
				self.page.prev = self.page.current;
				self.page.current = page;
				// UPDATE MODULE
				var module;
				var newmodule = $('#page_'+self.page.current).attr('data-module');
				switch(newmodule){
					case 'home':
						//require.ensure(['./v.pages_home.js'], function() {
							module = require('./v.pages_home.js');
						//});
						break;
					default:
						module = null;
						break;
				}
				self.module.prev = self.module.current;
				self.module.current = module;
				// HISTORY
				if(pushit!=false){
					self.push(page);
				}
				// LOAD PAGE
				self.load();
				// ANALYTICS
				//analytics.google.page('page/'+page);
			};
		},


		/******************** HISTORY ********************/
		push: function(page){
			var self = this;
			if(self.pushstate && model.device.pushstate){
				console.log('push');
				history.pushState(
					{}, //data
					document.title, //title
					'?page='+page //url
				);
			}
		},
		pop: function(){
			var self = this;
			if(self.pushstate && model.device.pushstate){
				window.onpopstate = function(e){
					console.log('pop');
					self.launch(false);
				};
			}
		},


		/******************** LOAD PAGE ********************/
		load: function(){
			var self = this;
			// NAV - CURRENT CLASS
			if(self.page.current!='nav'){
				$('.nav').removeClass('current prev');
				$('.nav_'+self.page.current).addClass('current prev');
			};
			// SHOW PAGE
			$('.page').removeClass('visible');
			$('#page_'+self.page.current).addClass('visible');
			// SCROLL TOP
			window.scroll(0,0);
			// PAGE START & STOP
			self.start();
			self.stop();
		},


		/******************** START PAGE FUNCTIONS ********************/
		start: function(){
			var self = this;
			var page = self.page.current;
			if(!$('#page_'+page).hasClass('loading')){
				// RESIZE
				self.resize(page);
				// PAGE SPECIFIC START
				console.log(self.module.current);
				if(self.module.current!=null){
					self.module.current.start();
				}
				
				
				/*if(hasKey($page,page+'.start')){
					$page[page].start();
					$page[page].started = true;
				};*/
			}else{
				clearTimeout(self.timeout_run);
				self.timeout_run = setTimeout(function(){
					self.start();
				},10);
			};
		},


		/******************** STOP PAGE FUNCTIONS ********************/
		stop: function(){
			var self = this;
			var page = self.page.prev;
			/*if(hasKey($page,page+'.stop')){
				$page[page].stop();
			};*/
		},


		/******************** RESIZE FUNCTIONS ********************/
		resize: function(){
			var self = this;
			/*if(hasKey($page,page+'.resize')){
				$page[page].resize();
			};*/
		}


	};
	
	
}());



