


(function(){

	module.exports = {
		
		
		request: function(opts){


			console.log('%c--- AJAX ---','color:#007257');


			/******************** CONFIG ********************/
			var options = {
				headers: {},				// headers // {'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
				url: '',					// url to fetch
				data: {},					// parameters to pass
				dataType: 'json',			// format of data beign fetched (json,xml etc)
				method: 'POST',				// ajax method (POST or GET)
				callback: function(){},		// callback function after AJAX
				callback_vars: false,		// additional vars to pass to the callback function
				retry: 0,					// stores how many times the query has been retried (don't edit this)
				timeout: 20000,				// ajax function timeout limit
				limit: 1,					// how many times to retry after a timeout
				encrypt: false,				// encrypt ajax call?
				error: false,				// is this an error log?
				crossdomain: true			// cross domain?
			};
			$.extend(options, opts);


			/******************** ENCRYPTION ********************/
			if(options.encrypt == true){
				options.data = {
					data:(apiEncrypt(encodeObject(options.data),$config.encryptkey)),
					s:apiEncrypt(session_id,'1234')
				};
			};


			/******************** PREVENT CACHING ********************/
			$.ajaxSetup({cache:false});


			/******************** RETRIES ********************/
			options.retry++;


			/******************** AJAX CALL ********************/
			$.ajax({
				headers:		options.headers,
				type:			options.method,
				url:			options.url,
				data:			options.data,
				dataType:		options.dataType,
				async:			true,
				timeout:		options.timeout,
				crossDomain:	options.crossdomain,

				// SUCCESS
				success: function(response) {
					// callback
					options.callback(response,options.callback_vars);
					//handle API errors
					/*if(options.error==false && response.error && response.error.length>0){
						if(typeof($errors)!=='undefined' && typeof($errors.response_error)!=='undefined'){
							$errors.response_error(response);
						};
						if(typeof($errors)!=='undefined' && typeof($errors.log_error)!=='undefined'){
							$errors.log_error({
								title: 'AJAX Response Error',
								message: '',
								endpoint: options.url
							});
						};
					};*/
				},

				// ERROR
				error: function(xhr,status,error){
					console.log(xhr,status,error);
					if(options.error==false){
						if(xhr.status != 200 && options.retry < options.limit){
							// retry AJAX call
							module.exports.request(options);
						}else{
							// timeout error
							if(status == 'timeout'){
								//show popup
								if(typeof($errors)!=='undefined' && typeof($errors.timeout_error)!=='undefined'){
									$errors.timeout_error(options.url,xhr,status,error);
								}
								//log to database
								if(typeof($errors)!=='undefined' && typeof($errors.log_error)!=='undefined'){
									$errors.log_error({
										title: 'AJAX Timeout Error',
										message: '',
										endpoint: options.url
									});
								}
							}
							// unknown error
							else{
								//show popup
								if(typeof($errors)!=='undefined' && typeof($errors.ajax_error)!=='undefined'){
									$errors.ajax_error(options.url,xhr,status,error);
								}
								//log to database
								if(typeof($errors)!=='undefined' && typeof($errors.log_error)!=='undefined'){
									$errors.log_error({
										title: 'AJAX Unknown Error',
										message: '',
										endpoint: options.url
									});
								}
							}
							// callback
							options.callback({},options.callback_vars);
						}
					}
				}
			});
			
			
		}
	
	
	};
	
}());



