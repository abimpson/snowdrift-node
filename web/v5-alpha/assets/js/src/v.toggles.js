


(function(){

	module.exports = {


		/******************** NAV ********************/
		nav: {
			visible: false,
			show: function(){
				if(!this.visible){
					this.visible = true;
					console.log('show');
				};
			},
			hide: function(){
				if(this.visible){
					this.visible = false;
					console.log('hide');
				};
			},
			toggle: function(){
				if(this.visible){
					this.hide();
				}else{
					this.show();
				}
			}
		}


	};
	
	
}());



