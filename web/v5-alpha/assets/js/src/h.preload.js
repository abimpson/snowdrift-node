


(function(){
	
	var config = require('./_config.js');
	var model = require('./m.all.js');

	module.exports = {


		/******************** CONFIG ********************/
		method: 1,
		fontcss: ['assets/css/styles.css'],


		/******************** VARS ********************/
		total: 0,
		done: 0,
		fonts_loaded: false,
		timeout: 5000,


		/******************** LIST FILES TO PRELOAD ********************/
		// FONTS
		fonts: [],
		// ALL
		files: [],
		// ALL - NON RETINA
		files_non_retina: [],
		// ALL - RETINA
		files_retina: [],
		// DESKTOP
		files_d: [],
		// DESKTOP - NON RETINA
		files_d_non_retina: [],
		// DESKTOP - RETINA
		files_d_retina: [],
		// MOBILE
		files_m: [],
		// MOBILE - NON RETINA
		files_m_non_retina: [],
		// MOBILE - RETINA
		files_m_retina: [],
		// DEMAND FILES
		files_demand: [],


		/******************** INIT PRELOAD ********************/
		init: function(){
			var self = this;
			// EXTEND
			if(typeof(config.preload)!=='undefined'){
				$.extend(self,config.preload);
			}
			// FONTS
			self.loadfonts();
			// ALL
			for(var i in self.files){
				self.preload(self.files[i]);
			};
			if($('#retina').is(':visible')){
				for(var j in self.files_retina){
					self.preload(self.files_retina[j]);
				};
			}else{
				for(var k in self.files_non_retina){
					self.preload(self.files_non_retina[k]);
				};
			};
			// DESKTOP
			if(model.device.type!='Mobile'){
				for(var m in self.files_d){
					self.preload(self.files_d[m]);
				};
			};
			if(model.device.type!='Mobile' && $('#retina').is(':visible')){
				for(var n in self.files_d_retina){
					self.preload(self.files_d_retina[n]);
				};
			}else if(model.device.type!='Mobile'){
				for(var o in self.files_d_non_retina){
					self.preload(self.files_d_non_retina[o]);
				};
			};
			// MOBILE
			if(model.device.type=='Mobile'){
				for(var p in self.files_m){
					self.preload(self.files_m[p]);
				};
			};
			if(model.device.type=='Mobile' && $('#retina').is(':visible')){
				for(var q in self.files_m_retina){
					self.preload(self.files_m_retina[q]);
				};
			}else if(model.device.type=='Mobile'){
				for(var r in self.files_m_non_retina){
					self.preload(self.files_m_non_retina[r]);
				};
			};
			// TIMEOUT FALLBACK
			setTimeout(function(){
				self.done = self.total;
				self.loaded_fonts = true;
			},self.timeout);
		},


		/******************** PRELOAD FILES ********************/
		preload: function(i){
			var self = this;
			var img;
			self.total++;
			if(self.method==1){
				img = new Image();
				img.src = i;
				img.onload = function(){
					self.done++;
					console.log('%c--- preload - '+self.done+'/'+self.total+' - '+i+' ---','color:#aaa');
				};
			}else{
				img = $("<img />").attr("src",i); 
				$(img).load(function(){
					self.done++;
					console.log('%c--- preload - '+self.done+'/'+self.total+' - '+i+' ---','color:#aaa');
				});
			};
		},


		/******************** PRELOAD FILES ON DEMAND ********************/
		demand: function(file_array){
			var self = this;
			for(var i in file_array){
				self.files_demand.push(file_array[i]);
				self.preload(file_array[i]);
			};
		},


		/******************** PRELOAD FONTS ********************/
		loadfonts: function(){
			var self = this;
			var webfontloader = require('../vendor/webfontloader.js');
			if(self.fonts.length>0 && model.device.type!='mobile'){
				webfontloader.load({
					custom: {
						families: self.fonts,
						urls: self.fontcss
					},
					active: function(){
						console.log('%c--- preload - fonts loaded ---','color:#8FC685');
						self.fonts_loaded = true;
					},
					inactive: function(){
						console.log('%c--- preload - fonts failed to load ---','color:##8FC685');
					}
				});
			};
		}


	};
	
	
}());



