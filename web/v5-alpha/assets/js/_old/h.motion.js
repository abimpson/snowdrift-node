


var $motion = {
	
	
	/******************** CONFIG ********************/
	options: {
		cursor: true,			//monitor cursor?
		ori: true,				//monitor orientation?
		acc: false,				//monitor accelleration?
		acc_g: false,			//monitor accelleration with gravity?
		rot: false,				//monitor rotation?
		//output
		output: false,			//log motion
		output_el: '#motion_data',	//log output element
		output_dp: 4,			//log decimal places
	},
	
	
	/******************** DATA STORAGE ********************/
	functions: Array(),
	p: {
		xreset: false,
		//refresh interval
		interval: null,
		//cursor - x/y
		c_x: 0,
		c_y: 0,
		//orientation - alpha/beta/gamma
		o_a: 0,
		o_b: 0,
		o_g: 0,
		o2_a: 0,
		o2_b: 0,
		o2_g: 0,
		//acceleration - x/y/z
		a_x: null,
		a_x_max: 0,
		a_x_min: 0,
		a_y: null,
		a_y_max: 0,
		a_y_min: 0,
		a_z: null,
		a_z_max: 0,
		a_z_min: 0,
		//acceleration with gravity - x/y/z
		ag_x: null,
		ag_y: null,
		ag_z: null,
		//rotation - alpha/beta/gamma
		rotation: null,
		r_a: 0,
		r_a_max: 0,
		r_a_min: 0,
		r_b: 0,
		r_b_max: 0,
		r_b_min: 0,
		r_g: 0,
		r_g_max: 0,
		r_g_min: 0
	},
	
	
	/******************** INIT ********************/
	init: function(){
		//EXTEND FUNCTIONS
		if(typeof($config.motion) !== 'undefined' && $config.motion != null){
			$.extend($motion, $config.motion);
		};
		//MONITOR
		$motion.monitor();
		//ORIENTATION CHANGE
		if(!!window.DeviceOrientationEvent && $motion.options.ori==true){
			window.onorientationchange = function(){
				$motion.reset_orientation();
			};
		};
	},
	
	
	/******************** RESET ORIENTATION ********************/
	reset_orientation: function(){
		$motion.p.o2_a = $motion.p.o_a;
		$motion.p.o2_b = $motion.p.o_b;
		$motion.p.o2_g = $motion.p.o_g;
	},
	
	
	/******************** MONITOR ********************/
	monitor: function(){
		//cursor
		if($motion.options.cursor==true){
			$motion.monitor_cursor();
		};
		//orientation
		if($motion.options.ori==true && $m.device.type!='desktop'){
			$motion.monitor_orientation();
		};
		//accelleration / rotation
		if(($motion.options.acc==true || $motion.options.acc_g==true || $motion.options.acc_rot==true) && $m.device.type!='desktop'){
			$motion.monitor_motion();
		};
	},
	
	
	/******************** MONITOR CURSOR ********************/
	monitor_cursor: function(){
		$(document).mousemove(function(event){
			//save
			$motion.p.c_x = event.clientX;
			$motion.p.c_y = event.clientY;
			//output
			$motion.output();
			$motion.checks();
		});
	},
	
	
	/******************** MONITOR ORIENTATION ********************/
	monitor_orientation: function(){
		if(!!window.DeviceOrientationEvent){
			window.ondeviceorientation = function(event) {
				//rotating clockwise/anticlockwise
				$motion.p.o_a = (event.alpha);
				//tilting forwards / backwards
				$motion.p.o_b = (event.beta);
				//tilting left/right
				if($motion.p.o_b>90){
					$motion.p.o_g = -(event.gamma);
				}else{
					$motion.p.o_g = (event.gamma);
				};
				//output
				$motion.output();
				$motion.checks();
				//reset at launch
				if($motion.p.xreset==false){
					$motion.p.xreset = true;
					$motion.reset_orientation();
				};
			};
		};
	},
	
	
	/******************** MONITOR MOTION ********************/
	monitor_motion: function(){
		if(!!window.DeviceMotionEvent){
			window.ondevicemotion = function(event) {
				//interval
				$motion.p.interval = event.interval;
				//acceleration
				if($motion.options.acc==true){
					$motion.p.a_x = event.acceleration.x;
					$motion.p.a_y = event.acceleration.y;
					$motion.p.a_z = event.acceleration.z;
					$motion.p.a_x_max = (event.acceleration.x>$motion.p.a_x_max?event.acceleration.x:$motion.p.a_x_max);
					$motion.p.a_y_max = (event.acceleration.y>$motion.p.a_y_max?event.acceleration.y:$motion.p.a_y_max);
					$motion.p.a_z_max = (event.acceleration.z>$motion.p.a_z_max?event.acceleration.z:$motion.p.a_z_max);
					$motion.p.a_x_min = (event.acceleration.x<$motion.p.a_x_min?event.acceleration.x:$motion.p.a_x_min);
					$motion.p.a_y_min = (event.acceleration.y<$motion.p.a_y_min?event.acceleration.y:$motion.p.a_y_min);
					$motion.p.a_z_min = (event.acceleration.z<$motion.p.a_z_min?event.acceleration.z:$motion.p.a_z_min);
				};
				//acceleration with gravity
				if($motion.options.acc_g==true){
					$motion.p.ag_x = event.accelerationIncludingGravity.x;
					$motion.p.ag_y = event.accelerationIncludingGravity.y;
					$motion.p.ag_z = event.accelerationIncludingGravity.z;
				};
				//rotation rate
				if($motion.options.rot==true){
					$motion.p.rotation = event.rotationRate;
					if($motion.p.rotation != null){
						$motion.p.r_a = Math.round($motion.p.rotation.alpha);
						$motion.p.r_b = Math.round($motion.p.rotation.beta);
						$motion.p.r_g = Math.round($motion.p.rotation.gamma);
						$motion.p.r_a_max = (Math.round($motion.p.rotation.alpha)>$motion.p.r_a_max?Math.round($motion.p.rotation.alpha):$motion.p.r_a_max);
						$motion.p.r_b_max = (Math.round($motion.p.rotation.beta)>$motion.p.r_b_max?Math.round($motion.p.rotation.beta):$motion.p.r_b_max);
						$motion.p.r_g_max = (Math.round($motion.p.rotation.gamma)>$motion.p.r_g_max?Math.round($motion.p.rotation.gamma):$motion.p.r_g_max);
						$motion.p.r_a_min = (Math.round($motion.p.rotation.alpha)<$motion.p.r_a_min?Math.round($motion.p.rotation.alpha):$motion.p.r_a_min);
						$motion.p.r_b_min = (Math.round($motion.p.rotation.beta)<$motion.p.r_b_min?Math.round($motion.p.rotation.beta):$motion.p.r_b_min);
						$motion.p.r_g_min = (Math.round($motion.p.rotation.gamma)<$motion.p.r_g_min?Math.round($motion.p.rotation.gamma):$motion.p.r_g_min);
						//output
						$motion.output();
						$motion.checks();
					};
				};
			};
		};
	},
	
	
	/******************** OUTPUT MONITOR RESULTS ********************/
	output: function(){
		if($motion.options.output==true && $motion.options.output_el!=''){
			var output = '<h1>Device Motion</h1>';
			output += '<pre>';
			if($motion.options.ori==true){
				output += '<strong>ORIENTATION</strong><br />';
				output += 'rotation: ' + $motion.p.o_a + '<br />';
				output += 'forwards tilt: ' + $motion.p.o_b + '<br />';
				output += 'side tilt: ' + $motion.p.o_g + '<br />';
				output += 'rotation: ' + ($motion.p.o2_a - $motion.p.o_a) + '<br />';
				output += 'forwards tilt: ' + ($motion.p.o2_b - $motion.p.o_b) + '<br />';
				output += 'side tilt: ' + ($motion.p.o2_g - $motion.p.o_g) + '<br />';
				output += '<br />';
			};
			if($motion.options.acc==true){
				output += '<strong>ACCELERATION</strong><br />(current / max / min)<br />';
				output += 'x (left/right): ' + round($motion.p.a_x,$motion.options.output_dp) + ' / ' + round($motion.p.a_x_max,$motion.options.output_dp) + ' / ' + round($motion.p.a_x_min,$motion.options.output_dp) + '<br />';
				output += 'y (forward/back): ' + round($motion.p.a_y,$motion.options.output_dp) + ' / ' + round($motion.p.a_y_max,$motion.options.output_dp) + ' / ' + round($motion.p.a_y_min,$motion.options.output_dp) + '<br />';
				output += 'z (up/down): ' + round($motion.p.a_z,$motion.options.output_dp) + ' / ' + round($motion.p.a_z_max,$motion.options.output_dp) + ' / ' + round($motion.p.a_z_min,$motion.options.output_dp) + '<br />';
				output += '<br />';
			};
			if($motion.options.rot==true){
				output += '<strong>ROTATION</strong><br />(current / max / min)<br />';
				output += 'alpha (forwards tilt): ' + $motion.p.r_a + ' / ' + $motion.p.r_a_max + ' / ' + $motion.p.r_a_min + '<br />';
				output += 'beta (side tilt): ' + $motion.p.r_b + ' / ' + $motion.p.r_b_max + ' / ' + $motion.p.r_b_min + '<br />';
				output += 'gamma (rotation): ' + $motion.p.r_g + ' / ' + $motion.p.r_g_max + ' / ' + $motion.p.r_g_min + '<br />';
			};
			output += '</pre>';
			$($motion.options.output_el).html(output);
		};
	}
	
	
	/******************** CHECKS ********************/
	/*checks: function(){
		if(typeof($page[$pages.current])!=='undefined' && typeof($page[$pages.current].motion)!=='undefined'){
			//GET X & Y
			if($m.device.type!='desktop' && !!window.DeviceOrientationEvent && $motion.options.ori==true){
				//DEVICE MOTION
				var x = -($motion.p.o2_g - $motion.p.o_g)/5;
				var y = -($motion.p.o2_b - $motion.p.o_b)/5;
				if(window.orientation==90){
					var x2 = x;
					x = y;
					y = -x2;
				}else if(window.orientation==-90){
					var x2 = x;
					x = -y;
					y = x2;
				}else if(window.orientation==180 || window.orientation==-180){
					x = -x;
					y = -y;
				};
			}else{
				//CURSOR
				var x = -(($motion.p.c_x-($windowsize.w/2)) / ($windowsize.w/2));
				var y = -(($motion.p.c_y-($windowsize.h/2)) / ($windowsize.h/2));
			};
			//RUN FUNCTIONS
			$page[$pages.current].motion(x,y);
		};
	},
	limits: function(val,limit){
		if($m.device.type!='Desktop' && !!window.DeviceOrientationEvent && $motion.options.ori==true){}else{
			val = val * limit;
		};
		if(val>limit){
			val = limit;
		}else if(val<-limit){
			val = -limit;
		};
		return val;
	}*/
	
	
};








