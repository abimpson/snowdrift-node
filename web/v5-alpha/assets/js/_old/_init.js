


/******************** DOCUMENT READY ********************/
$(document).ready(function() {
	console.log('%c******************** DOCUMENT READY ********************','color:#c70284;');
	try{console.timeStamp('READY');}catch(e){};
	if(typeof($m)!=='undefined'){$m.app.ready = true;};
	//modernizr
	if(typeof(Modernizr)!=='undefined'){
		console.log('%c*** MODERNIZR ***','color:#098c0c;');
		console.log(Modernizr);
	};
	//update data
	if(typeof($m)!=='undefined'&&typeof($m.device_update)!=='undefined'){$m.device_update();};
	if(typeof($m)!=='undefined'&&typeof($m.url_data_update)!=='undefined'){$m.url_data_update();};
	if(typeof($m)!=='undefined'&&typeof($m.app_data_update)!=='undefined'){$m.app_data_update();};
	if(typeof($m)!=='undefined'&&typeof($m.app_requests_update)!=='undefined'){$m.app_requests_update();};
	//object watching
	if(typeof($watchers)!=='undefined'&&typeof($watchers.init)!=='undefined'){$watchers.init();};
	//bind events
	if(typeof($nav)!=='undefined'&&typeof($nav.init)!=='undefined'){$nav.init();};
	if(typeof($inputs)!=='undefined'&&typeof($inputs.init)!=='undefined'){$inputs.init();};
	if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run(true);};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true);};
	if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run(true);};
	//views
	if(typeof($pages)!=='undefined'&&typeof($pages.init)!=='undefined'){$pages.init();};
	if(typeof($popups)!=='undefined'&&typeof($popups.init)!=='undefined'){$popups.init();};
	//facebook canvas sizing
	if(typeof($app)!=='undefined'&&typeof($app.canvas)!=='undefined'){$app.canvas();};
	//general app init
	if(typeof($errors)!=='undefined'&&typeof($errors.init)!=='undefined'){$errors.init();};
	if(typeof($hijax)!=='undefined'&&typeof($hijax.init)!=='undefined'){$hijax.init();};
	if(typeof($app)!=='undefined'&&typeof($app.init)!=='undefined'){$app.init();};
	if(typeof($checks)!=='undefined'&&typeof($checks.init)!=='undefined'){$checks.init();};
	if(typeof($auth)!=='undefined'&&typeof($auth.init)!=='undefined'){$auth.init();};
});



/******************** WINDOW LOADED ********************/
$(window).load(function(){
	console.log('%c******************** WINDOW LOADED ********************','color:#c70284;');
	try{console.timeStamp('LOADED');}catch(e){};
	if(typeof($m)!=='undefined'){$m.app.loaded = true;};
	//preload
	if(typeof($preload)!=='undefined'&&typeof($preload.init)!=='undefined'){$preload.init();};
	//bind events
	if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run(true);};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run(true);};
	if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run(true);};
	//check for facebook server
	/*if(!window.FB){
		$errors.generic_error('Sorry, Facebook\'s app server is currently unavailable.<br />Please try again shortly.');
	};*/
});



/******************** WINDOW RESIZE ********************/
$(window).resize(function(){
	//bind events
	if(typeof($docsize)!=='undefined'&&typeof($docsize.run)!=='undefined'){$docsize.run();};
	if(typeof($windowsize)!=='undefined'&&typeof($windowsize.run)!=='undefined'){$windowsize.run();};
	if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run();};
});



/******************** WINDOW SCROLL ********************/
$(window).scroll(function(){
	//bind events
	if(typeof($scroll)!=='undefined'&&typeof($scroll.run)!=='undefined'){$scroll.run();};
});



/******************** WINDOW BLUR / FOCUS ********************/
$(window).blur(function(){
	
});
$(window).focus(function(){
	
});






