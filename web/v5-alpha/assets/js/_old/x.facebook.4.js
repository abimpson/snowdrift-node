

/*
	
	AUTHOR:		Alex Bimpson @ Prrple
	NAME:		Facebook JS SDK Wrapper
	WEBSITE:	http://www.prrple.com
	LICENSE:	Distributed under the MIT License
	VERSION:	4.3
	UPDATED:	2017-06-07
	
*/


/********************************************************************************************************************************/
/****************************************************** FACEBOOK - FRAMEWORK ****************************************************/
/********************************************************************************************************************************/

var $facebook = {
	
	
	/******************** CONFIG ********************/
	debug:					true,
	version:				'v2.9',
	app_id:					'',
	namespace:				'',
	user_fields:			'',
	scope:					'',
	get_stream_privacy:		false,
	channel_file:			'',
	
	
	/******************** DATA STORAGE ********************/
	//GENERAL
	init:					false,
	got_status:				false,
	auth_response:			'',
	//USER DATA
	logged_in:				false,
	got_user:				false,
	user:					'',
	status:					'',
	permissions:			'',
	friends:				'',
	default_stream_privacy:	'',
	//CANVAS DATA
	canvas_info:			'',
	//FRAMEWORK VARS
	clicked:				false,
	
	
	/******************** RESET DATA STORAGE (AFTER LOGOUT) ********************/
	reset: function(){
		$facebook.auth_response = '';
		$facebook.logged_in = false;
		$facebook.got_user = false;
		$facebook.user = '';
		$facebook.status = '';
		$facebook.permissions = '';
		$facebook.friends = '';
		$facebook.default_stream_privacy = '';
		$facebook.canvas_info = '';
		$facebook.clicked = false;
	},
	
	
	/******************** INITIALISE ********************/
	initialise: function(){
		if($facebook.debug) console.log('%c--- facebook - init ---','color:#3b5998');
		$facebook.init = true;
		$facebook.fb_ready();
	},
	
	
	/******************** STATUS CHANGES ********************/
	status_change: function(response){
		if(response.status == 'connected'){
			//authenticated
			if($facebook.debug) console.log('%c--- facebook - status - authenticated ---','color:#3b5998');
			$facebook.status = 'connected';
			$facebook.status_authenticated();
		}else if(response.status == 'not_authorized'){
			//logged in but not authenticated
			if($facebook.debug) console.log('%c--- facebook - status - not authenticated ---','color:#3b5998');
			$facebook.status = 'not_authorized';
			$facebook.status_not_authenticated();
		}else{
			//not logged in to facebook
			if($facebook.debug) console.log('%c--- facebook - status - not logged in ---','color:#3b5998');
			$facebook.status = 'not_logged_in';
			$facebook.status_not_logged_in();
		};
	},
	status_authenticated: function(){
		$facebook.logged_in = true;
		$facebook.get_user();
		$facebook.fb_auto_auth();
	},
	status_not_authenticated: function(){
		$facebook.reset();
		$facebook.fb_auto_not_auth();
	},
	status_not_logged_in: function(){
		$facebook.reset();
		$facebook.fb_auto_not_logged_in();
	},
	
	
	/******************** FACEBOOK - LOGIN ********************/
	login: function(callback){
		if($facebook.debug) console.log('%c--- facebook - login ---','color:#3b5998');
		$facebook.fb_login_pre();
		if($facebook.init == true){
			$facebook.authenticate(callback);
		}else{
			setTimeout(function(){
				$facebook.login(callback);
			},50);
		};
	},
	//LOGIN FUNCTION
	authenticate: function(callback){
		if($facebook.debug) console.log('%c--- facebook - authenticate ---','color:#3b5998');
		if($facebook.logged_in == true){
			$facebook.fb_login_auth(callback);
		}else{
			//IF IE THEN DIRECTLY LOGIN - AVOIDS POPUP BLOCKER
			if(typeof(yourBrowser)!=='undefined' && yourBrowser=='IE'){
				FB.login(function(response) {
					if (response.authResponse) {
						//successfully logged in
						$facebook.fb_login_auth(callback);
					}else{
						//User cancelled login or did not fully authorize
						$facebook.fb_cancelled(callback);
					};
				}, {scope: $facebook.scope});
			}else if(navigator.userAgent.match('CriOS')){
				window.open('https://www.facebook.com/dialog/oauth?client_id='+$facebook.app_id+'&redirect_uri='+document.location.href+'&scope='+$facebook.scope, '', null);
			}else{
				FB.getLoginStatus(function(response) {
					if(response.status == 'connected'){
						//already authenticated
						$facebook.fb_login_auth(callback);
					}else if(response.status == 'not_authorized'){
						//logged in to facebook but not authenticated
						$facebook.authenticate_func(callback);
					}else{
						//not logged in to facebook
						$facebook.authenticate_func(callback);
					};
				});
			};
		};
	},
	//LOGIN FUNCTION FOR NON IE BROWSERS
	authenticate_func: function(callback){
		FB.login(function(response) {
			if (response.authResponse) {
				//successfully logged in
				$facebook.fb_login_auth(callback);
			}else{
				//User cancelled login or did not fully authorize
				$facebook.fb_cancelled(callback);
			};
		}, {scope: $facebook.scope});
	},
	
	
	/******************** GET USER DETAILS ********************/
	get_user: function(){
		if($facebook.debug) console.log('%c--- facebook - get_user ---','color:#3b5998');
		if($facebook.got_user == false){
			//AUTH RESPONSE
			$facebook.auth_response = FB.getAuthResponse();
			//CHECK PERMISSIONS
			var fields = $facebook.user_fields.split(',');
			if(jQuery.inArray('permissions', fields) == -1){
				$facebook.user_fields = $facebook.user_fields+',permissions';
			}
			//GET AND STORE FACEBOOK USER DATA
			FB.api('/me?fields='+$facebook.user_fields, function(response) {
				if(response.permissions){
					$facebook.permissions = response.permissions;
				}
				$facebook.user = response;
				$facebook.got_user = true;
			});
			//GET STREAM PRIVACY
			if($facebook.get_stream_privacy == true){
				FB.api({
					method: 'fql.query',
					query: "SELECT name, value, description FROM privacy_setting WHERE name = 'default_stream_privacy'"
				}, function(response) {
					$facebook.default_stream_privacy = response.description;
				});
			};
		};
	},
	
	
	/******************** AUTH CALLBACKS ********************/
	//INITIALISED
	fb_ready: function(){
		$facebook.fnc_ready();
	},
	//AUTO - NOT LOGGED IN TO FACEBOOK
	fb_auto_not_logged_in: function(){
		$facebook.fnc_auto_not_logged_in();
	},
	//AUTO - NOT AUTHENTICATED
	fb_auto_not_auth: function(){
		$facebook.fnc_auto_not_auth();
	},
	//AUTO - AUTHENTICATED
	fb_auto_auth: function(){
		//ensure user hasn't already clicked manual login
		if($facebook.clicked == false){
			if($facebook.got_user == true){
				$facebook.fnc_auto_auth();
			}else{
				setTimeout(function(){
					$facebook.fb_auto_auth();
				},50);
			};
		};
	},
	//LOGIN - PRE-AUTHENTICATION
	fb_login_pre: function(){
		$facebook.clicked = true;
		$facebook.fnc_login_pre();
	},
	//LOGIN - AUTHENTICATED
	fb_login_auth: function(callback){
		if($facebook.got_user == true){
			if(typeof(callback) == "function"){
				callback();
			}else{
				$facebook.fnc_login_auth();
			};
		}else{
			setTimeout(function(){
				$facebook.fb_login_auth(callback);
			},50);
		};
	},
	//LOGIN CANCELLED
	fb_cancelled: function(){
		$facebook.fnc_cancelled();
	},
	//PERMISSIONS REVOKED
	fb_revoked: function(callback){
		$facebook.reset();
		if(typeof(callback) == "function"){
			callback();
		}else{
			$facebook.fnc_revoked();
		};
	},
	//LOGGED OUT
	fb_loggedout: function(callback){
		$facebook.reset();
		if(typeof(callback) == "function"){
			callback();
		}else{
			$facebook.fnc_loggedout();
		};
	},
	
	
	/******************** CALLBACK FUNCTIONS ********************/
	fnc_ready: function(){},
	fnc_auto_not_logged_in: function(){},
	fnc_auto_not_auth: function(){},
	fnc_auto_auth: function(){},
	fnc_login_pre: function(){},
	fnc_login_auth: function(){},
	fnc_cancelled: function(){},
	fnc_revoked: function(){},
	fnc_loggedout: function(){},
	
	
	/******************** REQUEST ADDITIONAL PERMISSIONS ********************/
	add_perms: function(permissions,callback){
		if($facebook.debug) console.log('%c--- facebook - add_perms ---','color:#3b5998');
		if($facebook.got_user == true){
			//SEE IF ALREADY GOT
			var permsExisting = [];
			for(var i in $facebook.permissions.data){
				if($facebook.permissions.data[i].status == 'granted'){
					permsExisting.push($facebook.permissions.data[i].permission);
				};
			};
			var permsRequested = permissions.split(',');
			var permsNeeded = [];
			for(var j in permsRequested){
				if(jQuery.inArray(permsRequested[j], permsExisting) <= -1){
					permsNeeded.push(permsRequested[j]);
				};
			};
			//GET NEW PERMS
			if(permsNeeded.length >= 1){
				permsNeeded = permsNeeded.join(',');
				FB.login(function(response2) {
					//CHECK PERMISSIONS WERE ACCEPTED
					FB.api('/me/permissions', function(response){
						$facebook.permissions = response;
						var permsExisting = [];
						for(var i in $facebook.permissions.data){
							if($facebook.permissions.data[i].status == 'granted'){
								permsExisting.push($facebook.permissions.data[i].permission);
							};
						};
						var permsRequested = permissions.split(',');
						var permsNeeded = [];
						for(var k in permsRequested){
							if(jQuery.inArray(permsRequested[k], permsExisting) <= -1){
								permsNeeded.push(permsRequested[k]);
							};
						};
						if(permsNeeded.length >= 1){
							//not accepted
							if(typeof(callback)=="function"){
								callback(false);
							};
						}else{
							//accepted
							if(typeof(callback)=="function"){
								callback(true);
							};
						};
					});
				},{
					scope: permsNeeded,
					auth_type: 'rerequest'
				});
			}else{
				//CONTINUE
				if(typeof(callback)=="function"){
					callback(true);
				};
			};
		}else{
			setTimeout(function(){
				$facebook.add_perms(permissions,callback);
			},300);
		};
	},
	
	
	/******************** GET FRIENDS WHO USE THE APP ********************/
	get_app_friends: function(callback){
		if($facebook.debug) console.log('%c--- facebook - get_app_friends ---','color:#3b5998');
		FB.api('/me/friends',function(response){
			$facebook.friends = response.data;
			if(typeof(callback) == "function"){
				callback(response);
			};
		});
	},
	
	
	/******************** UPLOAD A PHOTO ********************/
	upload_photo: function(url,description,callback){
		if($facebook.debug) console.log('%c--- facebook - upload_photo ---','color:#3b5998');
		FB.api('/me/photos', 'post', {
			message: (typeof(description)!=='undefined'?description:''),
			url: url
		}, function(response){
			if(typeof(callback) == "function"){
				callback(response);
			};
		});
	},
	
	
	/******************** FRIEND REQUEST ********************/
	friend_request: function(fbid,callback){
		if($facebook.debug) console.log('%c--- facebook - friend_request ---','color:#3b5998');
		FB.ui({
			method:			'friends',
			id:				fbid
		}, function(response){
			if(typeof(callback) == "function"){
				callback(response);
			};
		});
	},
	
	
	/******************** SEND NOTIFICATION ********************/
	//note - this shouldn't be done client side as it requires an app access token
	send_notification: function(fbid,app_token,href,template,callback){
		if($facebook.debug) console.log('%c--- facebook - send_notification ---','color:#3b5998');
		FB.api('/'+fbid+'/notifications', 'post', {
			access_token: app_token,
			href: href,
			template: template
		}, function(response) {
			if(typeof(callback) == "function"){
				callback(response);
			};
		});
	},
	
	
	/******************** REVOKE PERMISSIONS ********************/
	revoke: function(callback){
		if($facebook.debug) console.log('%c--- facebook - revoke ---','color:#3b5998');
		FB.api('/me/permissions', 'delete', function(response) {
			$facebook.fb_revoked(callback);
		});
	},
	
	
	/******************** LOGOUT OF FACEBOOK ********************/
	logout: function(callback){
		if($facebook.debug) console.log('%c--- facebook - logout ---','color:#3b5998');
		FB.logout(function(response) {
			$facebook.fb_loggedout(callback);
		});
	},
	
	
	/******************** CANVAS STUFF ********************/
	//GET CANVAS INFO
	canvas_get_info: function(callback){
		if($facebook.init == true){
			if($facebook.debug) console.log('%c--- facebook - canvas_get_info ---','color:#3b5998');
			FB.Canvas.getPageInfo(function(response){
				$facebook.canvas_info = response;
				if(typeof(callback) == "function"){
					callback(response);
				};
			});
		}else{
			setTimeout(function(){
				$facebook.canvas_get_info();
			},200);
		}
	},
	//SCROLL CANVAS TO TOP
	canvas_scroll_top: function(){
		if($facebook.init == true){
			if($facebook.debug) console.log('%c--- facebook - canvas_scroll_top ---','color:#3b5998');
			FB.Canvas.scrollTo(0,0);
		}else{
			setTimeout(function(){
				$facebook.canvas_scroll_top();
			},200);
		}
	},
	//SCROLL CANVAS
	canvas_scroll_to: function(y){
		if($facebook.init == true){
			if($facebook.debug) console.log('%c--- facebook - canvas_scroll_to ---','color:#3b5998');
			FB.Canvas.scrollTo(0,y);
		}else{
			setTimeout(function(){
				$facebook.canvas_scroll_to(y);
			},200);
		}
	},
	//RESIZE CANVAS
	canvas_set_size: function(xheight,xwidth){
		if($facebook.init == true){
			if($facebook.debug) console.log('%c--- facebook - canvas_set_size ---','color:#3b5998');
			if(typeof(xwidth) !== 'undefined' && xwidth != null && typeof(xheight) !== 'undefined' && xheight != null){
				//height & width
				FB.Canvas.setSize({
					width:xwidth,
					height:xheight
				});
			}else if(typeof(xheight) !== 'undefined' && xheight != null){
				//height only
				FB.Canvas.setSize({
					width: 810,
					height:xheight
				});
			}else{
				//no dimensions passed
				xheight = $('html').outerHeight();
				FB.Canvas.setSize({
					width: 810,
					height:xheight
				});
			};
		}else{
			setTimeout(function(){
				$facebook.canvas_set_size(xheight,xwidth);
			},200);
		};
	},
	//RESIZE CANVAS REPEATEDLY (for apps of variable height)
	canvas_set_size_repeat_int: '',
	canvas_set_size_repeat: function(){
		if($facebook.debug) console.log('%c--- facebook - canvas_set_size_repeat ---','color:#3b5998');
		if(window.self != window.top){
			$facebook.canvas_set_size_repeat_int = window.setInterval(function(){
				var xheight = $('html').outerHeight();
				$facebook.canvas_set_size(xheight,810);
			},500);
		};
	},
	//AUTO RESIZE CANVAS (only grows, won't shrink again)
	canvas_set_autosize: function(){
		if($facebook.debug) console.log('%c--- facebook - canvas_set_autosize ---','color:#3b5998');
		if($facebook.init == true){
			FB.Canvas.setAutoGrow();
		}else{
			setTimeout(function(){
				$facebook.canvas_set_autosize();
			},200);
		}
	}
	
	
};





/********************************************************************************************************************************/
/**************************************************** FACEBOOK - INITIALISE SDK *************************************************/
/********************************************************************************************************************************/


/******************** LOAD FACEBOOK SDK ********************/
$(document).ready(function(){
	//EXTEND CONFIG
	if(typeof $facebook_config !== 'undefined' && $facebook_config != null){
		$.extend($facebook, $facebook_config);
	};
	if(typeof $facebook_functions !== 'undefined' && $facebook_functions != null){
		$.extend($facebook, $facebook_functions);
	};
	//LOAD THE FACEBOOK SDK ASYNCHRONOUSLY
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;};
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.com/en_US/sdk.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
});


/******************** FACEBOOK INIT ********************/
window.fbAsyncInit = function() {
	if($facebook.app_id != ''){
		//INIT THE SDK
		FB.init({
			version:	$facebook.version,
			appId:		$facebook.app_id,		// App ID
			channelUrl:	$facebook.channel_file,	// Path to your Channel File
			status:		true,					// check login status
			cookie:		true,					// enable cookies to allow the server to access the session
			xfbml:		true,					// parse XFBML
			oauth:		true
		});
		//GET LOGIN STATUS ON INITIALISE
		FB.getLoginStatus(function(response) {
			$facebook.status_change(response);
			$facebook.got_status = true;
		},true);
		//LISTEN FOR AND HANDLE LOGIN STATUS CHANGES
		FB.Event.subscribe('auth.statusChange', function(response) {
			$facebook.status_change(response);
		});
		//FACEBOOK CONNECTION INITIALISED
		$facebook.initialise();
	};
};




