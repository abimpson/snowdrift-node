


var $scroll = {
	
	
	/******************** CONFIG ********************/
	use: true,
	freq: 100,
	
	
	/******************** DATA ********************/
	scrolled_y: 0,
	visible_y: 0,
	
	prev_direction: 'down',
	scrolling: false,
	animating: false,
	timeout: null,
	timeout2: null,
	timeout3: null,
	
	wheel_timeout: null,
	
	
	/******************** SCROLL TO SECTION ********************/
	to: function(section){
		var y = parseInt($('#page_'+$pages.current+' #trigger'+section).position().top) - parseInt($('#header').height());
		$(window).scrollTop(y);
	},
	
	
	/******************** RUN ********************/
	run: function(force){
		//console.log('%c--- scroll ---','color:#ddc000');
		if(window.self === window.top){
			$scroll.scrolled_y = $(window).scrollTop();
			if($scroll.scrolled_y < 0){
				$scroll.scrolled_y = 0;
			};
			$scroll.visible_y = $scroll.scrolled_y + $windowsize.h;
			$scroll.y_func(force);
		};
		//start & finish
		if(force != true){
			if($scroll.scrolling == false){
				$scroll.scrolling = true;
				$scroll.start();
			};
			clearTimeout($scroll.timeout);
			$scroll.timeout = setTimeout(function(){
				$scroll.scrolling = false;
				$scroll.end();
			},200);
		}else{
			if(hasKey($page,$pages.current+'.current') && hasKey($page,$pages.current+'.current_split')){
				$scroll.go_to(true,$pages[$pages.current].current,$pages[$pages.current].current_split);
				//$scroll.start(true);
				//$scroll.end(true);
			};
		};
	},
	
	
	/******************** CALLBACK FUNCTIONS - START / END ********************/
	start: function(force){
		//console.log('%c--- scroll - start ---','color:#ddc000');
		/*if($('body').hasClass('bg_fixed')){
			//direction
			var direction = null;
			if($scroll.scrolled_y>$scroll.start_prev){
				direction = 'down';
			}else if($scroll.scrolled_y<$scroll.start_prev){
				direction = 'up';
			};
			$scroll.start_prev = $scroll.scrolled_y;
			//action
			if(direction!=null){
				var total = $('#page_'+$pages.current+' .sectionx').length;
				var current = $pages.home.current;
				console.log(direction);
				console.log(current);
				console.log(total);
				if(direction=='down' && current<total){
					current++;
					$scroll.go_to(force,current);
				}else if(direction=='up' && current>1){
					current--;
					$scroll.go_to(force,current);
				};
			};
			//force
			if(force==true){
				$scroll.go_to(force,$pages.home.current);
			};
		};*/
	},
	end: function(force){
		//console.log('%c--- scroll - end ---','color:#ddc000');
		if(($pages.current == 'valentines' && phase>1) || $pages.current == 'valentines_shared'){
			var current = 1;
			for(var i=1;i<=5;i++){
				if(($scroll.scrolled_y+$windowsize.h) >= $('#page_'+$pages.current+' .trigger'+i).offset().top){
					current = i;
				};
			};
			$m.game['trigger_'+$pages.current] = current;
			$('#page_'+$pages.current+' .nav_side a').removeClass('current');
			$('#page_'+$pages.current+' .nav_side a.nav'+current).addClass('current');
		};
	},
	
	
	/******************** CALLBACK FUNCTIONS - TRIGGERS ********************/
	y_func: function(force){
		//console.log('%c--- scroll - y ---','color:#ddc000');
	},
	
	
	/******************** JQUERY MOUSEWHEEL PLUGIN ********************/
	wheel: function(deltaX, deltaY, deltaFactor){
		if(!$('body').hasClass('size_s')){
			//DIRECTION
			var direction = null;
			if(deltaY<0){
				direction = 'down';
			}else if(deltaY > 0){
				direction = 'up';
			};
			//ACTION
			if(direction!=null){
				//console.log('wheel - '+direction);
				if(direction!=$scroll.prev_direction || ($scroll.animating==false && $scroll.scrolling==false)){
					$scroll.scrolling = true;
					$scroll.animating = true;
					$scroll.prev_direction = direction;
					$scroll.wheel_start_y(direction);
					clearTimeout($scroll.timeout2);
					$scroll.timeout2 = setTimeout(function(){
						$scroll.animating=false;
					},1000);
				};
				clearTimeout($scroll.wheel_timeout);
				$scroll.wheel_timeout = setTimeout(function(){
					$scroll.scrolling = false;
					$scroll.wheel_end_y();
				},10);
			};
		};
	},
	wheel_start_y: function(direction){
		//console.log('%c--- wheel - start ---','color:#ddc000');
		if(direction != null){
			
		};
		if(direction=='down'){
			$scroll.scroll_down();
		}else if(direction=='up'){
			$scroll.scroll_up();
		};
	},
	wheel_end_y: function(){
		//console.log('%c--- wheel - end ---','color:#ddc000');
		/*clearTimeout($scroll.timeout3);
		$scroll.timeout3 = setTimeout(function(){
			$scroll.animating=false;
		},200);*/
	},
	
	
	/******************** APP FUNCTIONS ********************/
	scroll_up: function(){
		var current = $pages[$pages.current].current;
		var current_split = $pages[$pages.current].current_split;
		var scrolling = false;
		if($windowsize.r > $config.portrait_r){
			//normal
			if(current>1){
				scrolling = true;
				current--;
				current_split = 1;
			};
		}else{
			//split (portrait) view
			if(current_split>1){
				scrolling = true;
				current_split--;
			}else if(current>1){
				scrolling = true;
				current--;
				if($('#page_'+$pages.current+' .section'+current+'_4').length > 0){
					current_split = 4;
				}else if($('#page_'+$pages.current+' .section'+current+'_3').length > 0){
					current_split = 3;
				}else if($('#page_'+$pages.current+' .section'+current+'_2').length > 0){
					current_split = 2;
				}else{
					current_split = 1;
				};
			};
		};
		if(scrolling==true){
			$scroll.go_to(false,current,current_split);
			if(typeof(loop)==='undefined' || loop!=true){
				$m.loop[$pages.current]=false;
			};
		};
	},
	scroll_down: function(loop){
		var current = parseInt($pages[$pages.current].current);
		var current_split = parseInt($pages[$pages.current].current_split);
		var total = $('#page_'+$pages.current+' .sectionx').length;
		var scrolling = false;
		if($windowsize.r > $config.portrait_r){
			//normal
			if(current<total){
				scrolling = true;
				current++;
				current_split = 1;
			}else if(loop==true){
				scrolling = true;
				current = 1;
				current_split = 1;
			};
		}else{
			//split (portrait) view
			var total_split = parseInt($('#page_'+$pages.current+' .child_'+current).length);
			if(current_split<total_split){
				scrolling = true;
				current_split++;
			}else if(current<total){
				scrolling = true;
				current++;
				current_split = 1;
			}else if(loop==true){
				scrolling = true;
				current = 1;
				current_split = 1;
			};
		};
		if(scrolling==true){
			$scroll.go_to(false,current,current_split);
			if(loop!=true){
				$m.loop[$pages.current]=false;
			};
		};
	},
	go_to: function(force,id,current_split){
		//console.log('%c--- scroll to ---','color:#ddc000');
		if(!$('body').hasClass('popups_visible')){
			if(typeof(current_split)==='undefined' || $windowsize.r>$config.portrait_r){
				current_split = 1;
			};
			if(current_split>1 || ($windowsize.r<=$config.portrait_r && $('#page_'+$pages.current+' .child_'+id).length>1)){
				id = id+'_'+current_split;
			};
			$pages[$pages.current].current = parseInt(id);
			$pages[$pages.current].current_split = parseInt(current_split);
			//side nav
			$('#page_'+$pages.current+' .nav_side a').removeClass('current');
			$('#page_'+$pages.current+' .nav_side a.nav'+id).addClass('current');
			//content
			var current = $('#page_'+$pages.current+' .section.current');
			var next = $('#page_'+$pages.current+' .section'+id);
			current.hide().removeClass('current');
			next.show().addClass('current');
			//background
			$('#page_'+$pages.current+' .backgrounds .current').removeClass('current');
			$('#page_'+$pages.current+' .backgrounds .bg'+id).addClass('current');
			if(force!=true){
				//flash 1
				$('#page_'+$pages.current+' .backgrounds .bg'+id+' .whiteout').css({
					opacity: 1
				}).animate({
					opacity: 0
				},700);
				//flash 2
				/*setTimeout(function(){
					$('#page_'+$pages.current+' .backgrounds .bg'+id+' .whiteout2').css({
						opacity: 0.9
					}).animate({
						opacity: 0
					},800);
				},125);*/
				//exposed
				$('#page_'+$pages.current+' .backgrounds .bg'+id+' .exposed').css({
					display: 'block',
					opacity: 1
				}).animate({
					opacity: 0
				},700);
				//blackout
				/*$('.backgrounds .bg'+id+' .fadeout').css({
					opacity: 0
				}).delay(400).animate({
					opacity: 1
				},300);*/
			};
		};
	},
	go_to_scroll: function(section,force){
		var current = null;
		if($pages.current=='valentines'){
			current = $m.game.current_step;
		}else if($pages.current=='valentines_shared'){
			current = $m.guess.current_step;
		};
		if(current!=null && current+1 >= section){
			var t = parseInt($('#page_'+$pages.current+' .section'+section).position().top) + parseInt($('#page_'+$pages.current+' .section'+section).css('margin-top'));
			t = t - 50; //header height
			if(section==2 || section==3 || section==4){
				t = t - 38;
			};
			$('html,body').stop().animate({
				scrollTop: t
			},(force==true?0:700),'easeInOutCubic');
		};
		//tracking
		$m.user_update();
		if(force!=true){
			
		};
	}
	
	
};


