


var $gmaps = {
	
	
	/********** CONFIG **********/
	options: {
		//api_key: 'AIzaSyDhfhyVgkKHuPSwdQFrYzAX5LUjkFM2mA4',
		container: 'gmap',
		center: {
			lat: 50.2164339,
			long: -5.4785555
		},
		icons: {
			'marker': {
				image:$config.image_root+'common/map_marker.png',
				width:47,
				height:62
			},
			'parking': {
				image:$config.image_root+'common/map_marker_parking.png',
				width:47,
				height:62
			}
		},
		markers: [
			{
				icon: 'marker',
				lat: 50.2164339,
				long: -5.4785555
			},
			{
				icon: 'parking',
				lat: 50.2147121,
				long: -5.4881672
			}
		]
	},
	
	
	/********** DATA STORAGE **********/
	map: null,
	icons: {},
	markers: [],
	geo: null,
	
	
	/********** MAP - INITIALISE **********/
	init: function(container){
		//OPTIONS
		google.maps.visualRefresh = true;
		//GET CONTAINER
		if(typeof(container)==='undefined'){
			container = $gmaps.options.container;
		};
		//CREATE CUSTOM ICONS
		for(var i in $gmaps.options.icons){
			$gmaps.icons[i] = new google.maps.MarkerImage($gmaps.options.icons[i].image, null, null, null, new google.maps.Size($gmaps.options.icons[i].width,$gmaps.options.icons[i].height));
		};
		//GENERATE MAP;
		$gmaps.map = new google.maps.Map(
			document.getElementById(container),
			{
				//position
				center: new google.maps.LatLng($gmaps.options.center.lat,$gmaps.options.center.long),
				zoom: 15,
				maxZoom:20,
				minZoom:1,
				//style
				mapTypeId: 'roadmap',
				styles: [
					//water
					{
						"featureType": "water",
						"elementType": "all",
						"stylers": [
							{
								"color": "#a0d8f4" //9ed4ef
							}
						]
					},
					//coast
					{
						"featureType": "landscape.natural",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"visibility": "on"
							},
							{
								"color": "#efebe5"
							}
						]
					},
					/*{ 
						featureType: "landscape", 
						elementType: "geometry", 
						stylers: [ 
							{ hue: "#ff2200" }, 
							{ saturation: 85 }, 
							{ lightness: -42 } 
						] 
					},*/
					/*{
						"elementType": "geometry",
						"stylers": [
							{
								"hue": "#ff4400"
							}
						]
					},*/
					//points of itnerest
					{
						featureType: "poi",
						stylers: [{
							visibility: "off"
						}]
					},
					/*{
						"featureType": "poi",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"visibility": "off"
							},
							{
								"hue": "#1900ff"
							},
							{
								"color": "#c0e8e8"
							}
						]
					},*/
					//roads
					/*{
						"featureType": "road",
						"elementType": "geometry",
						"stylers": [
							{
								"lightness": 100
							},
							{
								"visibility": "simplified"
							}
						]
					},
					{
						"featureType": "road",
						"elementType": "labels",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "transit.line",
						"elementType": "geometry",
						"stylers": [
							{
								"visibility": "on"
							},
							{
								"lightness": 700
							}
						]
					}*/
				],
				//ui controls
				draggable: false,
				disableDefaultUI: false,
				panControl: false,
				rotateControl: false,
				zoomControl: true,
				scaleControl: true,
				streetViewControl: false,
				mapTypeControl: true,
				scrollwheel: false
			}
		);
		//HIDE POINTS OF INTEREST
		//$gmaps.hide_poi();
		//ADD CUSTOM MARKERS
		$gmaps.add_markers();
		//FIT BOUNDS
		$gmaps.fit_bounds();
	},
	
	
	/********** MAP - HIDE POINTS OF INTEREST **********/
	/*hide_poi: function(){
		var noPoi = [{
			featureType: "poi",
			stylers: [{
				visibility: "off"
			}]
		}];
		$gmaps.map.setOptions({styles: noPoi});
	},*/
	
	
	/********** MAP - ADD MARKERS **********/
	add_markers: function(){
		if(typeof($gmaps.options.markers)!=='undefined'){
			for(var i in $gmaps.options.markers){
				//MARKER
				$gmaps.markers[i] = new google.maps.Marker({
					position: new google.maps.LatLng($gmaps.options.markers[i].lat,$gmaps.options.markers[i].long),
					map: $gmaps.map,
					icon: $gmaps.icons[$gmaps.options.markers[i].icon]
				});
				//MARKER DATA
				$gmaps.markers[i].setValues({
					'id': 'marker_'+i,
					'class': 'testclass'
				});
				var marker = $gmaps.markers[i];
				//INFO WINDOW
				/*var infowindow = new google.maps.InfoWindow({
					disableAutoPan:true
				});
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
					return function() {
						infowindow.setContent('<div id="map_infowindow_'+i+'">'+$gmaps.options.markers[i].title+'</div>');
						infowindow.open($gmaps.map, marker);
					}
				})(marker, i));
				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
					return function() {
						//infowindow.close($gmaps.map, marker);
					}
				})(marker, i));*/
				//CUSTOM INFO WINDOW
				/*var ib = new InfoBox({
					content: '&nbsp;',
					disableAutoPan: true,
					maxWidth: 0,
					pixelOffset: new google.maps.Size(-107, -80),
					zIndex: null,
					infoBoxClearance: new google.maps.Size(1, 1),
					isHidden: false,
					pane: "floatPane",
					enableEventPropagation: false,
					alignBottom: true,
					//box css style
					boxClass: 'gmaps_infobox',
					boxStyle: {
						background: "none",
						width: "215px"
					},
					//close button
					closeBoxURL: "",
					closeBoxMargin: "10px 2px 2px 2px"
				});
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
					return function() {
						ib.setContent('<div class="gmaps_infobox_inner"><h3>'+$gmaps.options.markers[i].title+'</h3>'+$gmaps.options.markers[i].content+'<div class="gmaps_infobox_bottom"></div></div>')
						ib.open($gmaps.map, marker);
					};
				})(marker, i));
				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
					return function() {
						ib.close($gmaps.map, marker);
					};
				})(marker, i));
				//POPUP
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						$('#map_popup'+i).show();
					};
				})(marker, i));*/
			};
		};
	},
	
	
	/********** MAP - FIT BOUNDS **********/
	fit_bounds: function(){
		if(typeof($gmaps.options.markers)!=='undefined'){
			if(objectLength($gmaps.options.markers) > 0){
				//MAP POINTS ARRAY
				var latlng = [];
				for(var i in $gmaps.options.markers){
					latlng.push(new google.maps.LatLng($gmaps.options.markers[i].lat,$gmaps.options.markers[i].long));
				};
				//GET BOUNDS
				var bounds = new google.maps.LatLngBounds();
				for (var i = 0; i < latlng.length; i++) {
					bounds.extend(latlng[i]);
				};
				//
				if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
					var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.003, bounds.getNorthEast().lng() + 0.003);
					var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.003, bounds.getNorthEast().lng() - 0.003);
					bounds.extend(extendPoint1);
					bounds.extend(extendPoint2);
				};
				//FIT BOUNDS
				$gmaps.map.fitBounds(bounds);
				//RESET ZOOM IF TOO HIGH
				google.maps.event.addListenerOnce($gmaps.map, 'idle', function() {
					if($gmaps.map.getZoom()>17) {
						$gmaps.map.setZoom(17);
					};
				});
			};
		};
	},
	
	
	/********** MAP - CHANGE TAGGED MARKER ICONS **********/
	/*show_tagged_markers: function(tag){
		$gmaps.reset_marker_icons();
		if(typeof($gmaps.options.markers)!=='undefined'){
			for(var i in $gmaps.options.markers){
				var found = false;
				for(var j in $gmaps.options.markers[i].tags){
					if($gmaps.options.markers[i].tags[j] == tag){
						found = true;
					};
				};
				if(found == true){
					$gmaps.markers[i].setIcon($gmaps.icons.marker);
					$gmaps.fit_bounds();
				};
			};
		};
	},*/
	
	
	/********** MAP - RESET MARKER ICONS **********/
	/*reset_marker_icons: function(){
		if(typeof($gmaps.options.markers)!=='undefined'){
			for(var i in $gmaps.options.markers){
				$gmaps.markers[i].setIcon($gmaps.icons.marker);
			};
		};
	},*/
	
	
	/********** GEOCODER - LOOKUP - LONG & LAT **********/
	/*geo_init: function(){
		$gmaps.geo = new google.maps.Geocoder;
	},*/
	
	
	/********** GEOCODER - LOOKUP - LONG & LAT **********/
	/*geo_lookup: function(){
		$gmaps.geo_init();
		var i = 0;
		//$gmaps.lookup_longlat($gmaps.options.markers[i].title,$gmaps.options.markers[i].content,i);
		$gmaps.lookup_longlat('The Eagle','250 Ladbroke Grove, London, W10 6HQ');
	},*/
	
	
	/********** GEOCODER - LOOKUP - LONG & LAT **********/
	/*lookup_longlat: function(title,address,i){
		$gmaps.geo.geocode({'address':title+', '+address},function(results, status){
			if (status == google.maps.GeocoderStatus.OK) {
				console.log(results);
				function toTitleCase(str){*/
					//return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
				/*};
				$('body').append('{\
					"title":"'+toTitleCase(title.toLowerCase())+'",\
					"content":"'+toTitleCase(address.toLowerCase())+'",\
					"lat":"'+results[0].geometry.location.d+'",\
					"long":"'+results[0].geometry.location.e+'"\
				},<br />');
			}else{
				console.log('no');
			};
			setTimeout(function(){
				//$gmaps.lookup_longlat($gmaps.options.markers[i+1].title,$gmaps.options.markers[i+1].content,i+1);
			},1000);
	   });
	}*/
	
	
};





/********************************************************************************************************************************/
/************************************************** GOOGLE GMAPS - INITIALISE SDK ************************************************/
/********************************************************************************************************************************/


/********** INITIALISE **********/
/*$(document).ready(function(){
	if($('.map').length>0){
		//EXTEND CONFIG
		if(typeof $gmaps_config !== 'undefined' && $gmaps_CONFIG != null){
			$.extend($gmaps, $gmaps_config);
		};
		//INITIALISE
		//if(google){ google.maps.event.addDomListener(window, 'load', $gmaps.init('map1')); };
		//GEO LOOKUP
		//$gmaps.geo_lookup();
	};
});*/






