/*
	by ALEX BIMPSON @ KERVE
*/
define(
	['jquery','core/ajax','kerve/helpers/windowscroll','kerve/helpers/windowsize','kerve/helpers/helper'],
	function($,ajax,windowscroll,windowsize,helper) {
	
	
	/****************************** DEFINE ******************************/
	var w = function($el,app){
		this.el = $el;
	};
	
	
	/****************************** START ******************************/
	w.prototype.start = function() {
		//CONSOLE LOGS
		m.logs = (helper.config.logs==true || m.dev==true?true:false);
		if(m.logs) console.log('%c*** WIDGET - 3.5 ALL TIME SCROBBLES - START ***','color:#098c0c;');
		//CACHE ELEMENTS
		m.el = this.el;
		m.locale = m.el.attr('data-locale') || '';
		m.locale_plays = m.el.attr('data-locale-plays') || '';
		m.locale_play = m.el.attr('data-locale-play') || '';
		m.has_transform = helper.has_transform();
		cache();
		//INIT
		init();
		//SCROLL
		var h2 = m.alltimewrap.offset().top;
		var h1 = h2 + (m.alltimewrap.height()/2);
		var h3 = h2 + m.alltimewrap.height();
		windowscroll.add(
			'alltime',
			//scroll init
			h1,
			function(){},
			//pause & resume
			h2,
			h3,
			function(){
				m.paused = true;
			},
			function(){
				m.paused = false;
			}
		);
		//WINDOWSIZE
		windowsize.add('alltime',{
			x_func: function(){
				//update scroll trigger
				var h2 = m.alltimewrap.offset().top;
				var h1 = h2 + (m.alltimewrap.height()/2);
				var h3 = h2 + m.alltimewrap.height();
				windowscroll.update_pos(
					'alltime',
					h1,
					h2,
					h3
				);
				//labels
				helper.label_scroll(m.label_scroll,m.label_span,false);
			}
		});
	};
	
	
	/****************************** STOP ******************************/
	w.prototype.stop = function() {
		if(m.logs) console.log('%c*** WIDGET - 3.5 ALL TIME SCROBBLES - STOP ***','color:#098c0c;');
		//CLEAR INTERVALS & TIMEOUTS
		clearTimeout(m.timeout_init);
		clearInterval(m.int_get_data);
		clearInterval(m.int_animate);
		clearInterval(m.int_animate_tracks);
		//CLEAR DATA
		data = [];
		//SCROLL
		windowscroll.reset();
		//WINDOWSIZE
		windowsize.reset();
	};
	
	
	/****************************** MODELS ******************************/
	var m = {
		//CONFIG
		dev: false, //dev mode?
		dev_data: false, //static dev data?
		query_url: 'http://64.30.224.201/kerve/scrobblecount',
		data_interval: 10000, //get data interval
		track_interval: 5000, //how often to update track
		animation_interval: 2000, //how often to update count
		animation_time: 800, //time to scroll numbers
		initial_step: 768, //fake data for first counter
		//DIMS
		col_h: 100, //height of each column
		chars: 12, //no of digits
		//DATA
		got_data: false, //has first data been fetched
		paused: false, //is the slider paused?
		has_transform: false, //css transforms available?
		intervals: 0, //animation intervals per query
		dif: 0, //store difference between values
		step: 0, //store step to increase by each time
		track: null, //store latest track
		total: 96654507258, //store latest total
		total2: 0, //currently displayed total
		class: 0, //store class
		//INTERVALS
		timeout_init: null, //init timeout
		int_get_data: null, //get data interval
		int_animate: null, // animation interval
		int_animate_tracks: null // tracks animation interval
	};
	var data = []; //store rolling number data
	
	
	/****************************** CACHE ******************************/
	function cache(){
		m.alltimewrap = m.el.find('.alltimewrap');
		m.alltime_cols = m.el.find('.alltime_count.rolling');
		m.label_scroll = m.el.find('.label_scroll');
		m.label_span = m.el.find('.label_span');
		m.result = m.el.find('.alltime_result');
		m.scrobbles = m.el.find('.alltime_scrobbles');
		m.genre = m.el.find('.alltime_genre');
	};
	
	
	/****************************** INIT ******************************/
	function init(){
		//animation intervals per data query
		m.intervals = m.data_interval / m.animation_interval;
		//output html
		init_html();
		//ajax first data
		get_data_ajax();
		//ajax interval data
		m.int_get_data = setInterval(function(){
			get_data_ajax();
		},m.data_interval);
	};
	function init_html(){
		//GENERATE HTML
		var output = '';
		var count = 2;
		for(i=0;i<12;i++){
			count++;
			if(count==3){
				count = 0;
				if(i!=0){
					output += '<div class="alltime_comma">,</div>';
				};
			};
			output += '<div class="rolling_col" id="rolling_col'+i+'">';
				for(j=0;j<10;j++){
					output += '<div class="rolling_no">'+j+'</div>';
				};
				for(j=0;j<10;j++){
					output += '<div class="rolling_no">'+j+'</div>';
				};
			output += '</div>';
		};
		//OUTPUT
		m.alltime_cols.html(output);
		//CACHE
		for(i=0;i<=11;i++){
			m['col'+i] = m.el.find('#rolling_col'+i);
		};
	};
	
	
	/****************************** DATA ******************************/
	//AJAX - GET DATA
	function get_data_ajax(callback){
		//GET DATA
		if(m.dev_data==true || helper.config.dev==true){
			var b = helper.dev['3-5-alltime'](m.total);
			//latest track
			m.track = b;
			xtrack();
			//latest total
			m.total = b.global_scrobbles;
			//maths
			get_data_maths();
			//start animating numbers
			if(m.got_data==false){
				m.got_data = true;
				m.timeout_init = setTimeout(function(){
					animate();
				},1000);
			};
		}else{
			//AJAX
			ajax.get(m.query_url, null, { //url, options, data, callback
				format:'json'
			}, function(a,b,c,d){
				if(m.logs) console.log('--- ajax response - 3.5 all time scrobbles ---');
				if(m.logs) console.log(b);
				if(typeof(b.global_scrobbles)!=='undefined'){
					//latest track
					m.track = b;
					xtrack();
					//latest total
					m.total = b.global_scrobbles;
					//maths
					get_data_maths();
					//start animating numbers
					if(m.got_data==false){
						m.got_data = true;
						m.timeout_init = setTimeout(function(){
							animate();
						},1000);
					};
				};
			});
		};
	};
	//MATHS
	function get_data_maths(){
		//maths
		if(m.total2==0){
			m.dif = m.initial_step * m.intervals;
			m.step = m.initial_step;
			m.total2 = m.total - m.dif;
		}else{
			m.dif = Math.abs(m.total - m.total2);
			m.step = m.dif / m.intervals;
			m.total2 = m.total - m.dif;
		};
	};
	
	
	/****************************** ANIMATE NUMBERS ******************************/
	//ANIMATE NUMBERS INTERVAL
	function animate(){
		m.int_animate = setInterval(function(){
			m.total2 = Math.ceil(m.total2 + m.step);
			roll_to(m.total2);
		},m.animation_interval);
	};
	//ROLL TO NUMBER
	function roll_to(val){
		if(m.paused!=true){
			//split digits
			var s = val.toString();
			var a = s.split('');
			var l = s.length
			var dif = m.chars - l;
			//class
			m.class = (m.class==0?1:0);
			//change each number
			var change = false;
			for(var i in a){
				if(change==true || a[i]!=data[i]){
					//get top, check direction and reposition
					if(typeof(data[i])==='undefined'){
						var t_old = 0;
						var t = (a[i] * m.col_h);
						var dir = '=';
					}else if(data[i] >= a[i]){
						var t_old = (data[i] * m.col_h);
						var t = ((parseInt(a[i]) + 10) * m.col_h);
						var dir = '>';
					}else{
						var t_old = (data[i] * m.col_h);
						var t = (a[i] * m.col_h);
						var dir = '<';
					};
					//animate
					roll_to_animate((parseInt(i)+dif),t_old,t,m.class);
					//save new value
					data[i] = a[i];
					change = true;
				};
			};
		};
	};
	function roll_to_animate(id,t_old,t,xclass){
		if(m.has_transform){
			m['col'+id].removeClass('red0 red1').css({
				'-webkit-transform': 'translate(0,-'+t_old+'%)',
				'-moz-transform': 'translate(0,-'+t_old+'%)',
				'-ms-transform': 'translate(0,-'+t_old+'%)',
				'transform': 'translate(0,-'+t_old+'%)',
			});
			setTimeout(function(){
				m['col'+id].css({
					'-webkit-transform': 'translate(0,-'+t+'%)',
					'-moz-transform': 'translate(0,-'+t+'%)',
					'-ms-transform': 'translate(0,-'+t+'%)',
					'transform': 'translate(0,-'+t+'%)',
				}).addClass('done red'+xclass);
			},100);
		}else{
			m['col'+id].removeClass('red0 red1').css({
				top: '-'+t_old+'%'
			}).animate({
				top: '-'+t+'%'
			},m.animation_time).addClass('done red'+xclass);
		};
	};
	
	
	/****************************** TRACKS ******************************/
	//OUTPUT TRACK
	function xtrack(){
		//data
		var artist = m.track.artist;
		var artist_url = m.track.artist_url;
		var track = m.track.track;
		var track_url = m.track.track_url;
		var tag = m.track.tag;
		var tag_url = m.track.tag_url;
		var scrobbles = m.track.scrobbles;
		var playlink = m.track.playlink;
		//output track
		m.result.html(helper.track(artist,artist_url,track,track_url,m.locale)+helper.play(m.locale_play,playlink)).show();
		m.scrobbles.text(m.locale_plays + ': ' + helper.commas(scrobbles)).show();
		//output tag
		if(typeof(tag)!=='undefined' && tag!=''){
			if(typeof(tag_url)!=='undefined' && tag_url!=''){
				m.genre.html('<a href="'+helper.url(tag_url,m.locale)+'">'+tag+'</a>');
			}else{
				m.genre.html(tag);
			}
		}else{
			m.genre.html('...');
		};
		helper.label_scroll(m.label_scroll,m.label_span,false);
	};
	
	
	/****************************** RETURN ******************************/
	return w;
	
	
});
