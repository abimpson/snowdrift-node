/*
	BY ALEX BIMPSON @ KERVE
*/
define(['jquery'], function($){
	
	
	/****************************** DEFINE ******************************/
	var ap = function($el, app) {};
	
	
	/****************************** START ******************************/
	ap.prototype.start = function() {
		console.log('%c************************* DASHBOARD - START *************************','color:#0097c9;');
		//CLASS
		$('body').addClass('dashbody');
		//CSS
		$('head').append(
			'<link rel="stylesheet" href="/static/styles/kerve/main.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-1-venn.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-2-loved.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-3-wave.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-4-spikeartist.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-5-alltime.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-6-spikingtracks.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-7-world.css" type="text/css" />' +
			'<link rel="stylesheet" href="/static/styles/kerve/widgets/3-8-listeningnow.css" type="text/css" />'
		);
	};
	
	
	/****************************** STOP ******************************/
	ap.prototype.stop = function() {
		console.log('%c************************* DASHBOARD - STOP *************************','color:#0097c9;');
		//CLASS
		$('body').removeClass('dashbody');
	};
	
	
	/****************************** RETURN ******************************/
	return ap;
	
	
});