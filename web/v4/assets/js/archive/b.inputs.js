var $inputs = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    // MONITOR INPUTS
    // $inputs.monitor($('#claim_firstname'),$placeholders.first_name);
    // CHECKBOXES
    $inputs.checkbox($('.register_checkbox'));
  },

  // ======================================================================
  // MONITOR INPUT CHANGES
  // ======================================================================

  monitor: function(input, value) {
    // POPULATE ON LOAD
    input.val(value);
    // MONITOR CHANGES
    input.focus(function() {
      input.removeClass('error');
      if (input.val().toLowerCase() == value.toLowerCase()) {
        input.val('');
        input.addClass('done');
      }
    });
    input.blur(function() {
      if (input.val() == '' || input.val().toLowerCase() == value.toLowerCase()) {
        input.val(value);
        input.removeClass('done');
      } else {
        input.addClass('done');
      }
    });
    input.change(function() {
      if (input.val() == '' || input.val().toLowerCase() == value.toLowerCase()) {
        input.val(value);
        input.removeClass('done');
      } else {
        input.addClass('done');
      }
    });
  },

  // ======================================================================
  // CHECKBOXES
  // ======================================================================

  checkbox: function(input) {
    input.data({
      checked: 'no'
    });
    input.click(function() {
      if (input.data('checked') == 'no') {
        input.data({
          checked: 'yes'
        });
        input.addClass('checked');
      } else {
        input.data({
          checked: 'no'
        });
        input.removeClass('checked');
      }
      return false;
    });
  },

  // ======================================================================
  // SELECTS
  // ======================================================================

  selects: function() {
    // $('#form_bra_size').change(function(){
    //   if($(this).val()=='SELECT'){
    //     $('#confirm_brasize .selectric').addClass('xselect');
    //   }else{
    //     $('#confirm_brasize .selectric').removeClass('xselect');
    //   };
    // });
  }
};
