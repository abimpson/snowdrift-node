var $app = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {},

  // ======================================================================
  // BLOCKER
  // ======================================================================

  blocker: {
    show: function() {
      $('#blocker').addClass('visible');
    },
    hide: function() {
      $('#blocker').removeClass('visible');
    }
  },

  // ======================================================================
  // LOADER
  // ======================================================================

  loader: {
    show: function() {
      $('#loader').addClass('visible');
    },
    hide: function() {
      $('#loader').removeClass('visible');
    }
  }

  // ======================================================================
  // LOCAL STORAGE
  // ======================================================================

  // localstorage: {
  //   set: function(xvar, data) {
  //     if ($m.device.localstorage) {
  //       try {
  //         localStorage.setItem($config.storage_name + xvar, JSON.stringify(data));
  //       } catch (e) {}
  //     }
  //   },
  //   get: function(xvar) {
  //     if ($m.device.localstorage) {
  //       try {
  //         return JSON.parse(localStorage.getItem($config.storage_name + xvar));
  //       } catch (e) {
  //         return null;
  //       }
  //     } else {
  //       return null;
  //     }
  //   },
  //   unset: function(xvar) {
  //     if ($m.device.localstorage) {
  //       try {
  //         localStorage.removeItem($config.storage_name + xvar);
  //       } catch (e) {
  //         return null;
  //       }
  //     }
  //   },
  //   clear: function() {
  //     if ($m.device.localstorage) {
  //       try {
  //         localStorage.clear();
  //       } catch (e) {
  //         return null;
  //       }
  //     }
  //   }
  // },

  // ======================================================================
  // STORE.JS
  // ======================================================================

  // storage: {
  //   clear_expired: function() {
  //     store.removeExpiredKeys();
  //   },
  //   set: function(key, value, expires) {
  //     try {
  //       if (typeof expires !== 'undefined') {
  //         expires = new Date().getTime() + expires; //new Date().getTime() + (30*24*60*60*1000) // new Date(1507975967586).toString();
  //       } else {
  //         expires = undefined;
  //       }
  //       store.set($config.storage_name + key, value, expires);
  //     } catch (e) {}
  //   },
  //   get: function(key) {
  //     try {
  //       return store.get($config.storage_name + key);
  //     } catch (e) {
  //       return undefined;
  //     }
  //   },
  //   get_expires: function(key) {
  //     var expires = store.getExpiration($config.storage_name + key);
  //     if (typeof expires !== 'undefined') {
  //       console.log(new Date(expires).toString());
  //     } else {
  //       console.log(expires);
  //     }
  //   }
  // },

  // ======================================================================
  // SCROLL
  // ======================================================================

  // scroll: {
  //   // scroll to top of page
  //   top: function(time) {
  //     window.scrollTo(0, 0);
  //     //if(typeof($facebook)!=='undefined'&&typeof($facebook.canvas_scroll_top)!=='undefined'){$facebook.canvas_scroll_top();};
  //   },
  //   // scroll to position
  //   pos: function(top, time) {
  //     $('html, body').animate(
  //       {
  //         scrollTop: top
  //       },
  //       time
  //     );
  //   },
  //   // scroll to element
  //   element: function(element, time) {
  //     // get vars
  //     var top = element.offset().top - 30;
  //     if (Modernizr.mq('(max-width:1024px)')) {
  //       top = top - $('#nav').height();
  //     }
  //     if (top < 0) top = 0;
  //     if (typeof time === 'undefined') time = 500;
  //     // animate
  //     $('html, body').animate(
  //       {
  //         scrollTop: top
  //       },
  //       time
  //     );
  //     // window.scrollTo(0,top);
  //   }
  // },

  // ======================================================================
  // DEV CALLBACK
  // ======================================================================

  // callback: function(response) {
  //   console.log('### CALLBACK ###');
  //   if (typeof response !== 'undefined') {
  //     console.log(response);
  //   }
  // },

  // ======================================================================
  // FACEBOOK LOGIN
  // ======================================================================

  // facebook_login: function(callback) {
  //   if ($facebook.logged_in == true) {
  //     callback();
  //   } else {
  //     //$app.show_loader();
  //     $facebook.login(callback);
  //   }
  // },

  // ======================================================================
  // IMAGE ERROR
  // ======================================================================

  // img_error: function(img) {
  //   img.onerror = '';
  //   img.src = 'assets/images/face.jpg';
  //   return true;
  // },

  // ======================================================================
  // REQUIRE FILE
  // ======================================================================

  // require: {
  //   // load specified files
  //   load: function(arr) {
  //     // string to array
  //     if (typeof arr == 'string') {
  //       arr = arr.split(',');
  //     }
  //     //
  //     for (var i in arr) {
  //       var src = arr[i];
  //       var parts = src.split('.');
  //       var ext = parts[parts.length - 1];
  //       var el;
  //       if (ext == 'css') {
  //         // check for existing
  //         el = $('head').find('link[href="' + src + '"]');
  //         // append
  //         if (el.length == 0) {
  //           $('head').append('<link rel="stylesheet" type="text/css" href="' + src + '">');
  //         }
  //       } else if (ext == 'js') {
  //         // check for existing
  //         el = $('head').find('script[src="' + src + '"]');
  //         // append
  //         if (el.length == 0) {
  //           $('head').append('<script src="' + src + '"></script>');
  //         }
  //       }
  //     }
  //   },
  //   // check files are loaded
  //   check: function(arr) {
  //     // string to array
  //     if (typeof arr == 'string') {
  //       arr = arr.split(',');
  //     }
  //     //
  //     for (var i in arr) {
  //       var src = arr[i];
  //       var parts = src.split('.');
  //       var ext = parts[parts.length - 1];
  //       var el;
  //       if (ext == 'css') {
  //         el = $('head').find('link[href="' + src + '"]');
  //         if (el.length == 0) {
  //           return false;
  //         }
  //       } else if (ext == 'js') {
  //         el = $('head').find('script[src="' + src + '"]');
  //         if (el.length == 0) {
  //           return false;
  //         }
  //       }
  //     }
  //     return true;
  //   }
  // }
};
