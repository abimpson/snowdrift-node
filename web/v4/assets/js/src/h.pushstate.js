var $pushstate = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  root: 'http://test.kerve.co.uk',

  // ======================================================================
  // CHECK PUSHSTATE AVAILABILITY
  // ======================================================================

  available: false,
  check: function() {
    if (!!(window.history && history.pushState)) {
      $pushstate.available = true;
      $pushstate.yes();
    } else {
      $pushstate.available = false;
      $pushstate.no();
    }
  },

  // ======================================================================
  // IF SUPPORTED, CHECK FOR VARS
  // ======================================================================

  yes: function() {
    // see if hashbangs in url
    if (window.location.hash) {
      var u = window.location.hash;
      var location = '/' + u.replace('#!/', '');
      window.location.replace(location);
    }
  },

  // ======================================================================
  // IF NOT SUPPORTED, CHECK FOR VARS
  // ======================================================================

  no: function() {
    // see if url vars
    var u = window.location.pathname;
    if (u != '' && !window.location.hash) {
      var location = '/#!' + u;
      var location2 = $pushstate.root + location;
      window.location.replace(location);
    }
  }
};
$pushstate.check();
