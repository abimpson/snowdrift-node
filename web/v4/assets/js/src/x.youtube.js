var $youtube = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  options: {
    api_key: '',
    load_api: true
  },

  // ======================================================================
  // DATA
  // ======================================================================

  ready: false, // has the api been loaded?
  v: [], // stores video objects

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    if ($youtube.ready == false) {
      // EXTEND CONFIG
      if (typeof $youtube_config !== 'undefined' && $youtube_config != null) {
        $.extend($youtube, $youtube_config);
      }
      // LOAD YOUTUBE API
      if ($youtube.options.load_api) {
        var tag = document.createElement('script');
        tag.src = 'https:// www.youtube.com/iframe_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      }
    }
  },

  // ======================================================================
  // SEARCH YOUTUBE
  // ======================================================================

  search: function(query, callback) {
    console.log('youtube search');
    // QUERY PARAMETERS
    var options = {
      key: $youtube.options.api_key,
      q: '',
      part: 'snippet',
      maxResults: 5,
      order: 'relevance',
      safeSearch: 'moderate',
      type: 'video',
      videoEmbeddable: true
    };
    if (typeof query === 'object') {
      options = $.extend(options, query);
    } else {
      options.q = query;
    }
    // AJAX
    $.ajax({
      type: 'get',
      url: 'https:// www.googleapis.com/youtube/v3/search',
      data: options,
      dataType: 'jsonp',
      async: true,
      timeout: 20000,
      // SUCCESS
      success: function(response) {
        // console.log('youtube search results');
        // console.log(response);
        if (typeof callback === 'function') {
          var r = {
            status: 'ok',
            data: response
          };
          callback(r);
        }
      },
      // ERROR
      error: function(xhr, status, error) {
        // console.log('youtube search error');
        // console.log(xhr,status,error);
        if (typeof callback === 'function') {
          var r = {
            status: 'error',
            data: {
              error: error,
              status: status,
              xhr: xhr
            }
          };
          callback(r);
        }
      }
    });
  },

  // ======================================================================
  // EMBED VIDEO
  // ======================================================================

  embed: function(opts) {
    return new $youtube.video(opts);
  },

  // ======================================================================
  // VIDEO CLASS
  // ======================================================================

  video: function(opts) {
    // CONFIG
    var $this = this;
    this.options = {};

    // DATA STORAGE
    this.v = '';

    // INIT
    this.init = function() {
      if ($youtube.ready) {
        $this.v = $youtube.v.length;
        $youtube.v[$this.v] = new YT.Player($this.options.element, {
          videoId: $this.options.video_id,
          width: '100%',
          height: '100%',
          iv_load_policy: 3,
          playerVars: {
            wmode: 'transparent',
            autohide: 1,
            egm: 0,
            hd: 1,
            iv_load_policy: 3,
            modestbranding: 1,
            rel: 0,
            showinfo: 0,
            showsearch: 0,
            fs: 1,
            controls: 1
            // autoplay: 1
          },
          events: {
            onReady: $this.ready,
            onStateChange: $this.state
          }
        });
      } else {
        setTimeout(function() {
          $this.init();
        }, 50);
      }
    };

    // VIDEO READY
    this.ready = function() {
      // $this.play();
    };

    // VIDEO STATE CHANGE
    this.state = function(state) {
      // unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5)
      // console.log(state);
    };

    // GENERAL VIDEO CONTROLS
    this.play = function() {
      $youtube.v[$this.v].playVideo();
    };
    this.pause = function() {
      $youtube.v[$this.v].pauseVideo();
    };
    this.stop = function() {
      $youtube.v[$this.v].stopVideo();
    };
    this.volume = function(v) {
      $youtube.v[$this.v].setVolume(v);
    };
    this.mute = function() {
      $youtube.v[$this.v].mute();
    };
    this.unmute = function() {
      $youtube.v[$this.v].unMute();
    };

    // VIDEO REMOVE
    this.remove = function() {
      delete $youtube.v[$this.v];
      $('#' + $this.options.element).replaceWith('<div id="' + $this.options.element + '"></div>');
    };

    // INIT
    if (typeof opts === 'object') {
      $.extend(this.options, opts);
    }
    this.init();
  },

  // ======================================================================
  // GLOBAL VIDEO CONTROLS
  // ======================================================================

  playAll: function() {
    for (var i in $youtube.v) {
      $youtube.v[i].playVideo();
    }
  },
  pauseAll: function() {
    for (var i in $youtube.v) {
      $youtube.v[i].pauseVideo();
    }
  },
  stopAll: function() {
    for (var i in $youtube.v) {
      $youtube.v[i].stopVideo();
    }
  }

  // ======================================================================
  // YOUTUBE - CUE VIDEO
  // ======================================================================

  // function previewCueVideo(youtubeId){
  //   if(PREVIEW_YOUTUBE_READY == true){
  //     PREVIEW_YOUTUBE.cueVideoById(youtubeId, 0, 'hd720');
  //     $('.playlist_row_title').removeClass('playing');
  //     $('#playlist_row_'+youtubeId+' .playlist_row_title').addClass('playing');
  //   }else{
  //     setTimeout(function(){
  //       previewCueVideo(youtubeId);
  //     },200);
  //   }
  // }

  // ======================================================================
  // YOUTUBE - LOAD VIDEO
  // ======================================================================

  // function previewLoadVideo(youtubeId){
  //   PREVIEW_YOUTUBE.loadVideoById(youtubeId, 0, 'hd720');
  //   $('.playlist_row_title').removeClass('playing');
  //   $('#playlist_row_'+youtubeId+' .playlist_row_title').addClass('playing');
  // }

  // ======================================================================
  // YOUTUBE - SKIP
  // ======================================================================

  // function previewVideoLeft(){
  //   if(PREVIEW_YOUTUBE_CURRENT <= 0){
  //     PREVIEW_YOUTUBE_CURRENT = PREVIEW_YOUTUBE_COUNT-1;
  //   }else{
  //     PREVIEW_YOUTUBE_CURRENT--;
  //   }
  //   previewCueVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
  // }
  // function previewVideoRight(play){
  //   if(PREVIEW_YOUTUBE_CURRENT >= PREVIEW_YOUTUBE_COUNT-1){
  //     PREVIEW_YOUTUBE_CURRENT = 0;
  //   }else{
  //     PREVIEW_YOUTUBE_CURRENT++;
  //   }
  //   if(play && play==true){
  //     previewLoadVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
  //   }else{
  //     previewCueVideo(PREVIEW_YOUTUBE_PLAYLIST[PREVIEW_YOUTUBE_CURRENT]);
  //   }
  // }
};
function onYouTubeIframeAPIReady() {
  $youtube.ready = true;
}
$(document).ready(function() {
  $youtube.init();
});
