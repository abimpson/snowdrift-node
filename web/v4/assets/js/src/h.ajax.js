var $ajax = function(opts) {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  console.log('%c--- AJAX ---', 'color:#007257');
  var options = {
    // ajax options
    headers: {}, // headers // {'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
    method: 'POST', // ajax method (POST or GET)
    url: '', // url to fetch
    data: {}, // parameters to pass
    dataType: 'json', // format of data beign fetched (json,xml etc)
    timeout: 20000, // ajax function timeout limit
    crossdomain: true, // cross domain?
    async: true, // async?
    callback: function() {}, // callback function after AJAX
    callback_vars: false, // additional vars to pass to the callback function
    // other options
    log_errors: false, // log errors
    show_errors: true, // show errors
    encrypt: false, // encrypt ajax call?
    error: false, // is this an error log?
    // data stores
    retry: 0, // stores how many times the query has been retried (don't edit this)
    limit: 1 // stores how many times to retry after a timeout
  };
  $.extend(options, opts);

  // ======================================================================
  // ENCRYPTION
  // ======================================================================

  if (options.encrypt) {
    options.data = {
      data: apiEncrypt(encodeObject(options.data), $config.encryptkey),
      s: apiEncrypt(session_id, '1234')
    };
  }

  // ======================================================================
  // PREVENT CACHING
  // ======================================================================

  $.ajaxSetup({cache: false});

  // ======================================================================
  // RETRIES
  // ======================================================================

  options.retry++;

  // ======================================================================
  // AJAX CALL
  // ======================================================================

  $.ajax({
    headers: options.headers,
    type: options.method,
    url: options.url,
    data: options.data,
    dataType: options.dataType,
    timeout: options.timeout,
    crossDomain: options.crossdomain,
    async: options.async,

    // SUCCESS
    success: function(response) {
      // callback
      options.callback(response, options.callback_vars);
      // handle API errors
      if (!options.error && response.error && response.error.length > 0) {
        // show error popup
        if (options.show_errors) {
          var output = '';
          for (var i in response.error) {
            output += '<br />';
            output += response.error[i];
            if ($.trim(response.error[i]).slice(-1) != '.') {
              output += '.';
            }
          }
          $popups.error('Oops!', 'Sorry, the following errors occurred: ' + output);
        }
        // log error
        log_error({
          title: 'AJAX Response Error',
          message: '',
          endpoint: options.url
        });
      }
    },

    // ERROR
    error: function(xhr, status, error) {
      if (options.error == false) {
        if (xhr.status != 200 && options.retry < options.limit) {
          // retry AJAX call
          $ajax(options);
        } else {
          // timeout error
          if (status == 'timeout') {
            // show error popup
            if (options.show_errors) {
              $popups.error('Oops!', 'A timeout error has occurred.<br />Please refresh the page to try again.');
            }
            // log to database
            log_error({
              title: 'AJAX Timeout Error',
              message: '',
              endpoint: options.url
            });
          }
          // unknown error
          else {
            // show popup
            if (options.show_errors) {
              $popups.error('Oops!', 'An AJAX error has occurred.<br />Please refresh the page to try again.');
            }
            // log to database
            log_error({
              title: 'AJAX Unknown Error',
              message: '',
              endpoint: options.url
            });
          }
          // callback
          options.callback({}, options.callback_vars);
        }
      }
    }
  });

  // ======================================================================
  // LOG ERROR
  // ======================================================================

  function log_error() {
    if (options.log_errors) {
      console.log('%c### ERROR LOG ###', 'color:#F00;');
      var d = {
        // identity: (typeof($m.user)!=='undefined' && typeof($m.user.identity)!=='undefined' && $m.user.identity!=null?$m.user.identity:''),
        endpoint: 'Unknown',
        title: 'Unknown Error',
        message: ''
      };
      $.extend(d, details);
      $ajax({
        url: $config.api_root + 'app.logError',
        method: 'POST',
        error: true,
        data: d,
        callback: function(response) {
          console.log(response);
        }
      });
    }
  }
};
