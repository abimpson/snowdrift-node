var $toggles = {
  // ======================================================================
  // NAV
  // ======================================================================

  nav: {
    visible: false,
    show: function() {
      if ($toggles.nav.visible == false) {
        if (DEVICE.type == 'mobile') {
          $('#btn_nav').fadeIn();
        } else {
          $toggles.nav.visible = true;
          $('#nav').animate({
            bottom: 0,
            opacity: 1
          });
        }
      }
    }
  }
};
