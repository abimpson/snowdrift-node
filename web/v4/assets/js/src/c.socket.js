var $socket = function(opts) {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  var self = this;
  this.options = {
    // setup
    url: '',
    room: null,
    // debug
    debug_show: false,
    status_id: 'socket_status',
    debug_id: 'socket_debug',
    error_id: 'socket_error',
    // callbacks
    callback_connected: function() {},
    callback_disconnected: function() {},
    callback_error: function() {},
    callback_unavailable: function() {},
    // listeners
    listeners: []
  };

  // ======================================================================
  // DATA
  // ======================================================================

  this.s = null;
  this.data = {
    // status
    active: false,
    last_callback: ''
  };

  // ======================================================================
  // INIT
  // ======================================================================

  this.init = function() {
    console.log('%c--- socket - init ---', 'color:#0097c9;');
    if (typeof io !== 'undefined') {
      // CONNECT
      this.s = io.connect(this.options.url, {
        timeout: 5000,
        reconnection: true,
        reconnectionDelay: 100,
        reconnectionDelayMax: 5000,
        reconnectionAttempts: 99999
      });
      // CONNECT CALLBACK
      this.s.on('connect', function() {
        self.callbacks.connected();
      });
      // DISCONNECT CALLBACK
      this.s.on('connect_error', function() {
        self.callbacks.disconnected();
      });
      this.s.on('connect_timeout', function() {
        self.callbacks.disconnected();
      });
      this.s.on('disconnect', function() {
        self.callbacks.disconnected();
      });
      // RECONNECT
      this.s.on('reconnect', function() {});
      this.s.on('reconnect_attempt', function() {});
      this.s.on('reconnecting', function() {});
      this.s.on('reconnect_error', function() {});
      this.s.on('reconnect_failed', function() {});
      // ERROR CALLBACK
      this.s.on('error', function() {
        self.callbacks.error();
      });
      // LISTEN
      this.listen();
    } else {
      // UNAVAILABLE
      self.callbacks.unavailable();
    }
  };

  // ======================================================================
  // UPDATE URL
  // ======================================================================

  this.update_url = function(url) {};

  // ======================================================================
  // STATUS CALLBACKS
  // ======================================================================

  this.callbacks = {
    // CONNECTED
    connected: function() {
      console.log('%c--- socket - connected ---', 'color:#0097c9;');
      self.data.active = true;
      self.status('connected');
      // join room
      if (self.options.room != null) {
        self.s.emit('join', self.options.room);
      }
      // save status
      self.data.last_callback = 'connected';
      // callback
      self.options.callback_connected();
    },
    // DISCONNECTED
    disconnected: function() {
      if (self.data.last_callback != 'disconnected') {
        console.log('%c--- socket - disconnected ---', 'color:#0097c9;');
        self.data.active = false;
        self.status('disconnected');
        // save status
        self.data.last_callback = 'disconnected';
        // callback
        self.options.callback_disconnected();
      }
    },
    // ERROR
    error: function() {
      console.log('%c--- socket - error ---', 'color:#0097c9;');
      self.data.active = false;
      self.status('error');
      // save status
      self.data.last_callback = 'error';
      // callback
      self.options.callback_error();
    },
    // UNAVAILABLE
    unavailable: function() {
      // SOCKET.IO SCRIPT HAS NOT BEEN INCLUDED
      console.log('%c--- socket - unavailable ---', 'color:#0097c9;');
      self.data.active = false;
      self.status('unavailable');
      // save status
      self.data.last_callback = 'unavailable';
      // callback
      self.options.callback_unavailable();
    }
  };

  // ======================================================================
  // STATUS INDICATORS
  // ======================================================================

  this.status = function(status) {
    // console.log(status);
    switch (status) {
      case 'connected':
        $('#' + self.options.error_id).hide();
        $('#' + self.options.status_id).addClass('green');
        self.debug('Socket Connected');
        break;
      case 'error':
        $('#' + self.options.error_id).show();
        $('#' + self.options.status_id).removeClass('green');
        self.debug('Socket Error');
        break;
      case 'disconnected':
        $('#' + self.options.error_id).show();
        $('#' + self.options.status_id).removeClass('green');
        self.debug('Socket Disconnected');
        break;
      case 'unavailable':
        $('#' + self.options.error_id).show();
        $('#' + self.options.status_id).removeClass('green');
        this.debug('Socket Not Available');
        break;
      default:
        break;
    }
  };

  // ======================================================================
  // DISCONNECT
  // ======================================================================

  this.disconnect = function() {
    this.disconnected = true;
    this.s.disconnect();
  };

  // ======================================================================
  // DEBUG TO HTML ELEMENT
  // ======================================================================

  this.debug = function(text) {
    if (this.options.debug_show == true) {
      $('#' + this.options.debug_id).text(text);
    }
  };

  // ======================================================================
  // CLOSE
  // ======================================================================

  // this.close = function() {
  // 	console.log('%c--- socket - close ---', 'color:#0097c9;');
  // 	this.send('close', {});
  // };

  // ======================================================================
  // PING
  // ======================================================================

  this.ping = function() {
    console.log('%c--- socket - ping ---', 'color:#0097c9;');
    if (this.data.active == true) {
      this.s.emit('ping', {});
    }
  };

  // ======================================================================
  // ROOMS
  // ======================================================================

  // JOIN ROOM
  this.join_room = function(room) {
    console.log('%c--- socket - join_room ---', 'color:#0097c9;');
    if (self.options.room != null) {
      self.s.emit('leave', self.options.room);
    }
    self.options.room = room;
    self.s.emit('join', room);
  };

  // LEAVE
  this.leave_room = function(room) {
    console.log('%c--- socket - leave_room ---', 'color:#0097c9;');
    self.options.room = null;
    self.s.emit('leave', room);
  };

  // ======================================================================
  // SEND DATA
  // ======================================================================

  this.send = function(method, d) {
    console.log('%c--- socket - send ---', 'color:#0097c9;');
    console.log(method, d);
    if (this.data.active == true) {
      this.s.emit(method, d);
    }
  };

  this.send_to_self = function(d) {
    this.send('send', d);
  };

  this.send_to_room = function(d) {
    if (self.options.room != null) {
      this.send('send_to_room', {
        room: self.options.room,
        data: d
      });
    } else {
      console.log('you are not connected to a room');
    }
  };

  this.send_to_all = function(d) {
    this.send('send_to_all', d);
  };

  // ======================================================================
  // LISTENERS
  // ======================================================================

  this.listen = function() {
    // STANDARD
    this.s.on('init', function(response) {
      console.log('%c--- socket - confirmed ---', 'color:#0097c9;');
    });
    this.s.on('ping', function(response) {
      // console.log('%c--- socket - received ping ---', 'color:#0097c9;');
    });
    this.s.on('close', function(response) {
      console.log('%c--- socket - closed ---', 'color:#0097c9;');
      this.data.active = false;
      this.debug('Socket Closed');
    });
    this.s.on('join', function(response) {
      console.log('%c--- socket - joined room ' + response.room + ' ---', 'color:#0097c9;');
    });
    // CUSTOM
    if (typeof this.options.listeners === 'object' || typeof this.options.listeners === 'object') {
      for (var i in this.options.listeners) {
        this.listen_add(this.options.listeners[i]);
      }
    }
  };
  this.listen_add = function(l) {
    this.s.on(l.method, function(response) {
      l.func(response);
    });
  };

  // ======================================================================
  // INIT
  // ======================================================================

  if (typeof opts === 'object') {
    $.extend(this.options, opts);
  }
  this.init();
};
