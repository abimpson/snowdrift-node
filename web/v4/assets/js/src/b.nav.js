var $nav = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    // BLOCKMOVE
    // document.ontouchmove = function(event) {
    //   event.preventDefault();
    // };

    // GENERAL
    $('body').on('click', '[data-page]', function() {
      var page = $(this).attr('data-page');
      if (page && typeof page !== 'undefined') {
        $pages.show(page);
      }
      return false;
    });
    $('body').on('click', '[data-popup]', function() {
      var popup = $(this).attr('data-popup');
      if (popup && typeof popup !== 'undefined') {
        $popups.show(popup);
      }
      return false;
    });
    $('body').on('click', '.btn_close_popup, .btn_close_popup2, .blackout', function() {
      $popups.hide();
      return false;
    });

    // TOUCHSWIPE
    // if (!$m.device.ie8) {
    //   $('#page_game').swipe({
    //     swipeRight: function() {
    //       $('#game_slider').prrpleSliderLeft();
    //     },
    //     swipeLeft: function() {
    //       $('#game_slider').prrpleSliderRight();
    //     },
    //     threshold: 100,
    //     excludedElements: ''
    //   });
    // }
  }
};
