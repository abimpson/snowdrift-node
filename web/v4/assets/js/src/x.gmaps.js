var $gmaps = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  // <script src="https:// maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=$gmaps.init" async defer></script>
  options: {
    container: 'gmap',
    center: {
      lat: 51.489477,
      lng: 0.007536
    },
    icons: {
      mudano: {
        image: '/wp-content/themes/mudano/assets/images/common/marker.svg',
        width: 45,
        height: 68
      }
    }
  },

  // ======================================================================
  // DATA
  // ======================================================================

  map: null,
  icons: {},
  markers: [],
  geo: null,
  infowindow: null,

  // ======================================================================
  // INIT
  // ======================================================================

  init: function(container) {
    // OPTIONS
    google.maps.visualRefresh = true;
    // GET CONTAINER
    if (typeof container === 'undefined') {
      container = $gmaps.options.container;
    }
    // CREATE CUSTOM ICONS
    for (var i in $gmaps.options.icons) {
      $gmaps.icons[i] = new google.maps.MarkerImage(
        $gmaps.options.icons[i].image,
        null,
        null,
        null,
        new google.maps.Size($gmaps.options.icons[i].width, $gmaps.options.icons[i].height)
      );
    }
    // GENERATE MAP;
    $gmaps.map = new google.maps.Map(document.getElementById(container), {
      // position
      center: new google.maps.LatLng($gmaps.options.center.lat, $gmaps.options.center.lng),
      zoom: 12,
      maxZoom: 20,
      minZoom: 1,
      // ui controls
      draggable: true,
      disableDefaultUI: false,
      panControl: false,
      rotateControl: false,
      zoomControl: true,
      scaleControl: true,
      streetViewControl: false,
      mapTypeControl: true,
      scrollwheel: false,
      // style
      mapTypeId: 'roadmap',
      // styles: $gmaps.options.styles,
      backgroundColor: 'none',
      // zoomControl: false,
      mapTypeControl: false,
      fullscreenControl: false,
      scaleControl: false
    });
    // HIDE POINTS OF INTEREST
    // $gmaps.hide_poi();
    // ADD CUSTOM MARKERS
    $gmaps.add_markers();
    // FIT BOUNDS
    $gmaps.fit_bounds();
  },

  // ======================================================================
  // REDRAW MAP
  // ======================================================================

  redraw: function() {
    // google.maps.event.trigger($gmaps.map,'resize');
    // $gmaps.map.setCenter($gmaps.map.getCenter());
    $gmaps.map.setZoom($gmaps.map.getZoom());
  },

  // ======================================================================
  // MARKERS - ADD
  // ======================================================================

  add_markers: function() {
    if (typeof $gmaps.options.markers !== 'undefined') {
      for (var i in $gmaps.options.markers) {
        // MARKER
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng($gmaps.options.markers[i].lat, $gmaps.options.markers[i].lng),
          map: $gmaps.map,
          icon: $gmaps.icons[$gmaps.options.markers[i].icon]
        });
        // MARKER DATA
        marker.setValues({
          id: i
        });
        // SAVE MARKER
        $gmaps.markers[i] = marker;
        // INFO WINDOW
        // google.maps.event.addListener(
        //   marker,
        //   'click',
        //   (function(marker, i) {
        //     return function() {
        //       $gmaps.show_window(i);
        //     };
        //   })(marker, i)
        // );
        // google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {})(marker, i));
        // google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {})(marker, i));
      }
      // var markerCluster = new MarkerClusterer($gmaps.map, $gmaps.markers, {imagePath: 'http:// localhost:3000/web/assets/images/m'});
    }
  },

  // ======================================================================
  // MARKERS - CLEAR
  // ======================================================================

  clear_markers: function() {
    for (var i in $gmaps.markers) {
      $gmaps.markers[i].setIcon($gmaps.icons.icon);
    }
  },

  // ======================================================================
  // FIT BOUNDS
  // ======================================================================

  fit_bounds: function() {
    try {
      if (typeof $gmaps.options.markers !== 'undefined') {
        if (objectLength($gmaps.options.markers) > 0) {
          // BUILD MARKERS ARRAY
          var latlng = [];
          for (var i in $gmaps.options.markers) {
            latlng.push(new google.maps.LatLng($gmaps.options.markers[i].lat, $gmaps.options.markers[i].lng));
          }
          // CALCULATE BOUNDS
          var bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < latlng.length; i++) {
            bounds.extend(latlng[i]);
          }
          // IF NO BOUNDS, ADD MARGIN
          if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
            var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.002, bounds.getNorthEast().lng() + 0.002);
            var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.002, bounds.getNorthEast().lng() - 0.002);
            bounds.extend(extendPoint1);
            bounds.extend(extendPoint2);
          }
          // APPLY BOUNDS
          $gmaps.map.fitBounds(bounds);
          // ZOOM OUT IF TOO HIGH
          google.maps.event.addListenerOnce($gmaps.map, 'idle', function() {
            var zoom = $gmaps.map.getZoom();
            var zoom_max = 17;
            // zoom = zoom - 1;
            if (zoom > zoom_max) {
              zoom = zoom_max;
              $gmaps.map.setZoom(zoom);
            }
          });
        }
      }
    } catch (e) {}
  },

  // ======================================================================
  // CENTER MAP ON LOCATION
  // ======================================================================

  center: function(latLng, zoom) {
    $gmaps.map.setCenter(latLng);
    $gmaps.map.setZoom(12);
  },

  // ======================================================================
  // LOOKUP - ADDRESS
  // ======================================================================

  // lookup: function(address) {
  //   var geocoder = new google.maps.Geocoder();
  //   geocoder.geocode(
  //     {
  //       address: address
  //     },
  //     function(results, status) {
  //       if (status == 'OK' && results.length > 0) {
  //         return results[0];
  //       }
  //     }
  //   );
  // },

  // ======================================================================
  // GO TO LOCATION
  // ======================================================================

  go_to_location: function(lat, lng) {
    $gmaps.clear_markers();
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(lat, lng);
    $gmaps.center(latLng);
    // add marker
    $gmaps.options.markers = [
      {
        lat: lat,
        lng: lng,
        icon: 'mudano'
      }
    ];
    $gmaps.add_markers();
    // redraw
    setTimeout(function() {
      $gmaps.redraw();
    }, 150);
    setTimeout(function() {
      $gmaps.redraw();
    }, 250);
    setTimeout(function() {
      $gmaps.redraw();
    }, 500);
    setTimeout(function() {
      $gmaps.redraw();
    }, 750);
    setTimeout(function() {
      $gmaps.redraw();
    }, 1000);
    // geocoder.geocode(
    //   {
    //     address: address
    //   },
    //   function(results, status) {
    //     // console.log(location.geometry.location);
    //     // console.log(new google.maps.LatLng(location.geometry.location.lat(),location.geometry.location.lng()))
    //     if (status == 'OK' && results.length > 0) {
    //       var location = results[0];
    //
    //       $gmaps.center(location.geometry.location);
    //
    //       setTimeout(function() {
    //         $gmaps.redraw();
    //       }, 500);
    //
    //       // $gmaps.options.markers = [{
    //       //   lat: location.geometry.location.lat(),
    //       //   lng: location.geometry.location.lng()
    //       // }];
    //       // $gmaps.add_markers();
    //       // $gmaps.fit_bounds();
    //     }
    //   }
    // );
  }

  // ======================================================================
  // MAP - HIDE POINTS OF INTEREST
  // ======================================================================

  // hide_poi: function() {
  //   var noPoi = [
  //     {
  //       featureType: 'poi',
  //       stylers: [
  //         {
  //           visibility: 'off'
  //         }
  //       ]
  //     }
  //   ];
  //   $gmaps.map.setOptions({styles: noPoi});
  // },

  // ======================================================================
  // INFO WINDOW
  // ======================================================================

  // show_window: function(i) {
  //   $gmaps.hide_window();
  //   $gmaps.infowindow = new google.maps.InfoWindow();
  //   var output = '<div class="gmaps_marker">';
  //   output += '<div class="gmaps_title">' + $gmaps.options.markers[i].name + '</div>';
  //   output += '<a class="gmaps_more" data-pub="' + $gmaps.options.markers[i].id + '">More info</a>';
  //   output += '</div>';
  //   $gmaps.infowindow.setContent(output);
  //   $gmaps.infowindow.open($gmaps.map, $gmaps.markers[i]);
  //   $gmaps.clear_markers();
  //   $gmaps.markers[i].setIcon($gmaps.icons.icon_highlight);
  // },
  // hide_window: function() {
  //   if ($gmaps.infowindow) {
  //     $gmaps.infowindow.close();
  //   }
  // }

  // ======================================================================
  // FIND CLOSEST MARKER
  // ======================================================================

  // find_closest: function(postcode) {
  //   // TN131AA, CM16EB
  //   var geocoder = new google.maps.Geocoder();
  //   geocoder.geocode(
  //     {
  //       address: postcode
  //     },
  //     function(results, status) {
  //       if (status == 'OK' && results.length > 0) {
  //         var pt = results[0].geometry.location; // new google.maps.LatLng(51.489477,0.007536);
  //         var num = 1;
  //         var closest = $gmaps.get_closest(pt, num);
  //         $gmaps.clear_markers();
  //         if (closest.length > 0) {
  //           closest[0].setIcon($gmaps.icons.icon_highlight);
  //           $gmaps.show_window(closest[0].id);
  //         } else {
  //           console.log('No Results 1');
  //           $gmaps.search_error();
  //         }
  //       } else {
  //         console.log('No Results 2');
  //         $gmaps.search_error();
  //       }
  //     }
  //   );
  // },
  //
  // search_error: function() {
  //   $gmaps.clear_markers();
  //   $popups.error('Oops!', 'Sorry, there were no results for your postcode search');
  // },
  //
  // get_closest: function(pt, num) {
  //   var closest = [];
  //   var markers = $gmaps.markers;
  //   for (var i = 0; i < markers.length; i++) {
  //     markers[i].distance = google.maps.geometry.spherical.computeDistanceBetween(pt, markers[i].getPosition());
  //     // markers[i].setMap(null);
  //     closest.push(markers[i]);
  //   }
  //   closest.sort($gmaps.sort_closest_by_dist);
  //   return closest.splice(0, num);
  // },
  //
  // sort_closest_by_dist: function(a, b) {
  //   return a.distance - b.distance;
  // }
};
