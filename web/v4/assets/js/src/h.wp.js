var $wp = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  el_posts: '.js-wp-lazyload',
  el_more: '.js-wp-more',
  el_loading: '.js-wp-loading',
  el_trigger: '.js-wp-trigger',

  // ======================================================================
  // DATA
  // ======================================================================

  inited: false,
  masonry: false,
  path: null, // store current AJAX path
  path_query: '', // store current path query
  page: 1, // store current page
  last: false, // is this the last page?
  count: 0, // count to ensure correct data is shown
  loading: false, // is a page loading?
  el: {}, // cache elements

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    // cache elements
    $wp.el = {
      posts: $($wp.el_posts),
      more: $($wp.el_more),
      loading: $($wp.el_loading),
      trigger: $($wp.el_trigger)
    };
    // check if required
    if ($wp.el.posts.length > 0) {
      // get current page path
      $wp.get_path();
      // check for masonry
      $wp.masonry = $wp.el.posts.hasClass('columns--masonry');
      // load more on click
      // $wp.el.more.click(function() {
      //   $wp.more();
      //   return false;
      // });
      // inited
      $wp.inited = true;
    }
  },

  // ======================================================================
  // GET PATH
  // ======================================================================

  get_path: function() {
    // get path
    var path = window.location.href;
    // get search vars
    path = path.split('?');
    var query = path.length > 1 ? path[1] : '';
    path = path[0];
    // remove pagination
    path = path.split('/page');
    path = path[0];
    // remove trailing slash
    if (path.substr(path.length - 1) == '/') {
      path = path.substring(0, path.length - 1);
    }
    // save
    $wp.path = path;
    $wp.path_query = query;
  },

  // ======================================================================
  // SCROLL
  // ======================================================================

  scroll: function(currentScrollPos) {
    if ($wp.inited) {
      var trigger = $wp.el.trigger.offset().top;
      var pos = currentScrollPos + $windowsize.h;
      if (pos >= trigger) {
        $wp.more();
      }
    }
  },

  // ======================================================================
  // LOAD MORE
  // ======================================================================

  more: function() {
    if (!$wp.last && !$wp.loading) {
      console.log('%c--- wp - more ---', 'color:#00A1A1');
      $wp.loading = true;
      // elements
      $wp.el.more.hide();
      $wp.el.loading.show();
      // get path
      var path = $wp.path;
      // append page
      $wp.page++;
      path = path + '/page/' + $wp.page;
      if ($wp.path_query == '') {
        path = path + '/?ajax=true';
      } else {
        path = path + '/?' + $wp.path_query + '&ajax=true';
      }
      // ajax
      $wp.ajax(path);
    }
  },

  // ======================================================================
  // AJAX
  // ======================================================================

  ajax: function(path, replace) {
    console.log('%c--- wp - ajax ---', 'color:#00A1A1');
    console.log(path);
    $wp.count++;
    $ajax({
      url: path,
      data: {},
      method: 'GET',
      dataType: 'html',
      callback: $wp.ajax_callback,
      callback_vars: {
        count: $wp.count,
        replace: replace
      }
    });
  },

  // ======================================================================
  // AJAX CALLBACK
  // ======================================================================

  ajax_callback: function(response, vars) {
    console.log('%c--- wp - callback ---', 'color:#00A1A1');
    // ensure this isn't an old callback
    if ($wp.count == vars.count) {
      // elements
      $wp.el.loading.hide();
      // get new content
      var items = $(response)
        .find($wp.el_posts)
        .children()
        .remove('.col--grid-sizer');
      // check for last page
      if (items.length < 1) {
        $wp.last = true;
      } else {
        $wp.last = false;
        // add new items
        if (vars.replace === true) {
          $wp.page = 1;
          $wp.el.posts.html(items);
          if ($wp.masonry) {
            $wp.el.posts.masonry('reloadItems');
          }
        } else {
          $wp.el.posts.append(items);
          if ($wp.masonry) {
            $wp.el.posts.masonry('appended', items);
            $app.masonry.imagesLoaded(items);
          }
        }
        $scroll.images.append();
        // show more button
        $wp.el.more.show();
      }
      // re-enable
      clearTimeout($wp.timeout);
      $wp.timeout = setTimeout(function() {
        $wp.loading = false;
      }, 500);
    }
  }
};
