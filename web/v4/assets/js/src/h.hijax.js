var $hijax = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  config: {
    use: false, // use hijax?
    title: 'Kerve',
    wrap: 'content', // id to load into
    append: true // append or replace content?
  },

  // ======================================================================
  // CALLBACKS
  // ======================================================================

  callback_before: function(id) {
    $('.loader').show();
  },
  callback_after: function(id) {
    $('.loader').hide();
  },
  callback_failed: function(response, status, xhr, id) {
    $('.loader').hide();
    if (xhr.status == '404') {
      $el('#page_' + id + ' .pagewrap').html('<h1>404 page does not exist</h1>');
    } else {
      $el('#page_' + id + ' .pagewrap').html('<h1>' + xhr.status + 'Error loading page</h1>');
    }
  },

  // ======================================================================
  // PAGE ID
  // ======================================================================

  id: localStorage && localStorage.getItem('page_id') ? localStorage.getItem('page_id') : 0,

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    if ($hijax.config.use == true) {
      // WATCH FOR URL CHANGES
      window.onpopstate = function(e) {
        // detect browser button direction
        var backwards = false;
        if (typeof e.state !== 'undefined' && e.state != null && typeof e.state.id !== 'undefined') {
          var changestate = true;
          if (e.state.id < $hijax.id) {
            var backwards = true;
          }
        }
        // load page
        $hijax.ajaxpage({
          backwards: backwards
        });
        // update id
        if (changestate == true) {
          $hijax.id = e.state.id;
          if (localStorage) {
            localStorage.setItem('page_id', $hijax.id);
          }
        }
      };
      window.onhashchange = function(e) {
        $hijax.ajaxpage();
      };
      // HIJAX CLICK EVENTS
      $hijax.clicks();
    }
  },

  // ======================================================================
  // CLICKS
  // ======================================================================

  clicks: function() {
    $('body').on('click', '.hijax', function() {
      // GET FILE PATH
      var path = $(this).attr('href');
      // DIRECTION
      var backwards = false;
      if ($(this).hasClass('hijax_backwards')) {
        backwards = true;
      }
      // LOAD
      $hijax.loadpage({
        url: path,
        backwards: backwards
      });
      // PREVENT DEFAULT CLICK BEHAVIOUR
      return false;
    });
  },

  // ======================================================================
  // LOAD PAGE
  // ======================================================================

  loadpage: function(params) {
    var options = $.extend(
      {
        url: '',
        backwards: false
      },
      params
    );
    // GET FILENAME
    var filename = $hijax.path_to_filename(options.url);
    // GET URL
    var url = $hijax.filename_to_url(filename);
    // CHANGE URL
    if ($pushstate.available == true) {
      console.log('pushstate');
      $hijax.id++;
      if (localStorage) {
        localStorage.setItem('page_id', $hijax.id);
      }
      history.pushState(
        {id: $hijax.id}, // data
        $hijax.config.title, // title
        url // url
      );
    } else {
      window.location.hash = '!' + url;
    }
    // LOAD PAGE
    $hijax.ajaxpage(options);
  },

  // ======================================================================
  // AJAX PAGE CONTENT
  // ======================================================================

  ajaxpage: function(options) {
    console.log('--- HIJAX ---');
    var classes = '';
    // GET PATH + FILENAME
    var path = $hijax.get_path();
    var filename = $hijax.path_to_filename(path);
    var filepath = $hijax.filename_to_path(filename);
    var id = $hijax.filename_to_id(filename);
    // CHECK FOR NEWS
    if (filename.substr(0, 11) == 'everything/') {
      classes += 'page_article';
      filepath = '/html-pages/everything/article.php?id=' + id.replace('everything_', '');
    }
    // LOAD PAGE
    if ($('#page_' + id).length <= 0) {
      // CALLBACK - BEFORE
      $hijax.callback_before(id);
      // CREATE PAGE
      if ($hijax.config.append == true) {
        $el('#' + $hijax.config.wrap).append(
          '<div class="page loading ' +
            classes +
            '" id="page_' +
            id +
            '" style="display:none;"><div class="pagewrap"><div class="loading">Loading...</div></div></div>'
        );
        var div = '#page_' + id + ' .pagewrap';
      } else {
        var div = '#' + $hijax.config.wrap;
      }
      // SHOW PAGE
      $pages.show(id, options.backwards);
      // AJAX CONTENT
      $(div).load(filepath, {}, function(response, status, xhr) {
        if (status != 'error') {
          // CALLBACK - AFTER
          $hijax.callback_after(id);
        } else {
          // HANDLE ERROR
          $hijax.callback_failed(response, status, xhr, id);
        }
        $el('#page_' + id).removeClass('loading');
      });
    } else {
      // SHOW PAGE
      $pages.show(id, options.backwards);
    }
  },

  // ======================================================================
  // HELPER - GET PATH
  // ======================================================================

  get_path: function() {
    if (!!(window.history && history.pushState)) {
      var path = window.location.pathname.substring(1);
    } else {
      var path = window.location.hash.replace('#!/', '');
    }
    return path;
  },

  // ======================================================================
  // HELPER - PATH TO FILENAME
  // ======================================================================

  path_to_filename: function(path) {
    // REPLACE FIRST SLASH
    if (path.charAt(0) == '/') {
      path = path.substr(1, path.length);
    }
    // REMOVE ROOT IF APPLICABLE
    if (typeof $config.root !== 'undefined' && $config.root != '') {
      if ($config.root.charAt(0) == '/') {
        var root = $config.root.substr(1, $config.root.length);
      } else {
        var root = $config.root;
      }
      path = path.replace(root, '');
    }
    // REMOVE EXTENSION IF APPLICABLE
    if (path.substr(path.length - 4) == '.php') {
      path = path.substring(0, path.length - 4);
    }
    // REMOVE TRAILING SLASH IF APPLICABLE
    if (path.substr(path.length - 1) == '/') {
      path = path.substring(0, path.length - 1);
    }
    // REPLACE SLASHES WITH UNDERSCORES
    // path = path.replace(/\// g,'_');
    // IF PATH IS EMPTY, SET TO HOME
    if (path == '') {
      path = 'home';
    }
    // RETURN
    return path;
  },

  // ======================================================================
  // HELPER - FILENAME TO PATH
  // ======================================================================

  filename_to_path: function(filename) {
    if (typeof $config.root !== 'undefined' && $config.root != '') {
      return $config.root + 'html-pages/' + filename + '.php';
    } else {
      return '/html-pages/' + filename + '.php';
    }
  },

  // ======================================================================
  // HELPER - FILENAME TO ID
  // ======================================================================

  filename_to_id: function(filename) {
    return filename.replace(/\// g, '_');
  },

  // ======================================================================
  // HELPER - FILENAME TO URL
  // ======================================================================

  filename_to_url: function(filename) {
    // CHECK FOR HOME
    if (filename == 'home') {
      filename = '';
    }
    // CHECK FOR SITE ROOT
    if (typeof $config.root !== 'undefined' && $config.root != '') {
      url = $config.root + filename;
    } else {
      url = '/' + filename;
    }
    return url;
  }
};
