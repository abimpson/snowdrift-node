var $page = $page || {};
var $pages = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  historyUse: true,
  historyHome: 'home',
  historyPrefix: '/',

  // ======================================================================
  // VARS
  // ======================================================================

  prev: '',
  current: '',

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    $pages.historyInit();
  },

  // ======================================================================
  // LAUNCH FIRST PAGE
  // ======================================================================

  // launch: function() {
  //   var page = getURLParameter('page');
  //   if (page != null && page != 'null' && $('#page_' + page).length > 0) {
  //     $pages.show(page, pushit);
  //   } else {
  //     $pages.show($pages.default);
  //   }
  // },

  // ======================================================================
  // SHOW PAGE
  // ======================================================================

  show: function(page, pushit) {
    console.log('%c--- page - ' + page + ' ---', 'color:#E1BF97;');
    if ($pages.current != page) {
      // STORE PAGE
      $pages.prev = $pages.current;
      $pages.current = page;
      // PUSHSTATE
      if (pushit != false) {
        $pages.historyPush(page);
      }
      // LOAD PAGE
      $pages.load();
      // CANVAS SIZE
      // $facebook.canvas_set_size();
      // ANALYTICS
      if (page != 'loading') {
        $analytics.google.page('page/' + page);
      }
    }
  },

  // ======================================================================
  // LOAD PAGE
  // ======================================================================

  load: function() {
    // NAV - CURRENT CLASS
    if ($pages.current != 'nav') {
      $el('.nav').removeClass('current prev');
      $el('.nav_' + $pages.current).addClass('current prev');
    }
    // SHOW PAGE
    $('.page')
      .removeClass('current')
      .hide();
    $el('#page_' + $pages.current)
      .addClass('current')
      .show();
    // SCROLL TOP
    window.scroll(0, 0);
    // PAGE START & STOP
    $pages.start($pages.current);
    $pages.stop($pages.prev);
  },

  // ======================================================================
  // HISTORY / PUSHSTATE
  // ======================================================================

  historyInit: function() {
    if ($pages.historyUse && $m.device.pushstate) {
      window.onpopstate = function(e) {
        $pages.historyPop();
      };
    }
  },
  historyPush: function(page) {
    if ($pages.historyUse && $m.device.pushstate) {
      if (page === $pages.historyHome) {
        page = '';
      }
      history.pushState(
        {}, // data
        document.title, // title
        $pages.historyPrefix + page // url
      );
    }
  },
  historyPop: function() {
    var page = window.location.pathname.replace($pages.historyPrefix, '');
    if (page === '' || page === '/') {
      page = $pages.historyHome;
    }
    if (page != null && page != 'null' && $('#page-' + page).length > 0) {
      $pages.show(page, false);
    }
  },

  // ======================================================================
  // START PAGE FUNCTIONS
  // ======================================================================

  start: function(page) {
    if (!$el('#page_' + page).hasClass('loading')) {
      // RESIZE
      $pages.resize(page);
      // PAGE SPECIFIC START
      if (hasKey($page, page + '.start')) {
        $page[page].start();
        $page[page].started = true;
      }
    } else {
      clearTimeout($pages.timeout_run);
      $pages.timeout_run = setTimeout(function() {
        $pages.start(page);
      }, 10);
    }
  },

  // ======================================================================
  // STOP PAGE FUNCTIONS
  // ======================================================================

  stop: function(page) {
    if (hasKey($page, page + '.stop')) {
      $page[page].stop();
    }
  },

  // ======================================================================
  // RESIZE PAGE FUNCTIONS
  // ======================================================================

  resize: function(page) {
    if (hasKey($page, page + '.resize')) {
      $page[page].resize();
    }
  }
};
