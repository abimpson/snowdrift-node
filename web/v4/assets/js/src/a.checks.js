var $checks = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    $checks.start();
  },

  // ======================================================================
  // CHECK FUNCTIONS
  // ======================================================================

  current: 0,
  status: null, // null, started, finished
  // START CHECKS
  start: function() {
    if (!$m.device.ie8 && $checks.status == null) {
      $checks.status = 'started';
      $checks.next();
      if (typeof $checks.recurring !== 'undefined') {
        $checks.recurring();
      }
    }
  },
  // CONTINUE CHECKS
  next: function() {
    $checks.current++;
    if (typeof $checks['check' + $checks.current] !== 'undefined') {
      $checks.goto($checks.current);
    } else {
      $checks.finish();
    }
  },
  // GO TO CHECK
  goto: function(check) {
    $checks['check' + check]();
  },
  // RECHECK
  recheck: function(check) {
    $checks.current = check;
    $checks.goto_check($checks.current);
  },

  // ======================================================================
  // CHECK 1 - GET DATA
  // ======================================================================

  check1: function() {
    console.log('--- CHECK 1 - GET DATA ---');
    // $ajax({
    //   url: $config.api_root + 'user.getDetails',
    //   data: {
    //     identity: $m.app_data.data.id
    //   },
    //   callback: $checks.check1_callback,
    //   method: 'GET'
    // });
    $checks.next();
  },
  check1_callback: function(response) {
    console.log(response);
    $checks.next();
  },

  // ======================================================================
  // RECURRING CHECKS
  // ======================================================================

  // recurring_int: null,
  // recurring_delay: 30000,
  // recurring: function() {
  //   clearInterval($checks.recurring_int);
  //   $checks.recurring_int = setInterval(function() {}, $checks.recurring_delay);
  // },

  // ======================================================================
  // FINISH
  // ======================================================================

  finish: function() {
    console.log('--- CHECKS - FINISHED ---');
    $checks.status = 'finished';
    $checks.launch();
  },

  // ======================================================================
  // LAUNCH
  // ======================================================================

  launch_timeout: null,
  launch: function() {
    if ($m.app.loaded == true && $preload.done >= $preload.total && (typeof $auth === 'undefined' || $auth.status == 'no_user' || $auth.status == 'finished')) {
      $checks.launch2();
    } else {
      clearTimeout($checks.launch_timeout);
      $checks.launch_timeout = setTimeout(function() {
        $checks.launch();
      }, 50);
    }
  },
  launch2: function() {
    console.log('%c*** LAUNCH ***', 'color:#0097c9;');
    $app.loader.hide();
    $pages.launch();
  }
};
