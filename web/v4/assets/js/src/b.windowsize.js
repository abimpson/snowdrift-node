var $windowsize = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  use: true,
  freq: 100,

  // ======================================================================
  // DATA
  // ======================================================================

  timeout: null,
  w: 0,
  h: 0,
  r: 0,
  w2: 0,
  h2: 0,
  r2: 0,

  // ======================================================================
  // RUN
  // ======================================================================

  run: function(force) {
    if ($windowsize.use == true) {
      if (force == true) {
        $windowsize.func(force);
      } else {
        clearTimeout($windowsize.timeout);
        $windowsize.timeout = setTimeout(function() {
          $windowsize.func(force);
        }, $windowsize.freq);
      }
    }
  },
  func: function(force) {
    console.log('%c--- windowsize ---', 'color:#DDD6AA');
    var oldWidth = $windowsize.w;
    var oldHeight = $windowsize.h;
    var oldWidth2 = $windowsize.w2;
    var oldHeight2 = $windowsize.h2;
    $windowsize.w = $(window).width();
    $windowsize.h = $(window).height();
    $windowsize.r = $windowsize.w / $windowsize.h;
    $windowsize.w2 = window.innerWidth ? window.innerWidth : $windowsize.w;
    $windowsize.h2 = window.innerHeight ? window.innerHeight : $windowsize.h;
    $windowsize.r2 = $windowsize.w2 / $windowsize.h2;
    if (force == true) {
      $windowsize.w_func();
      $windowsize.h_func();
      $windowsize.x_func();
    } else {
      if ($windowsize.w != oldWidth || $windowsize.w2 != oldWidth2) {
        $windowsize.w_func();
      }
      if ($windowsize.h != oldHeight || $windowsize.h2 != oldHeight2) {
        $windowsize.h_func();
      }
      if ($windowsize.w != oldWidth || $windowsize.w2 != oldWidth2 || $windowsize.h != oldHeight || $windowsize.h2 != oldHeight2) {
        $windowsize.x_func();
      }
    }
  },

  // ======================================================================
  // CALLBACK FUNCTIONS
  // ======================================================================

  w_func: function() {},
  h_func: function() {},
  x_func: function() {
    // page resizing
    if (typeof $pages !== 'undefined' && hasKey($pages, 'resize')) {
      $pages.resize();
    }
    if (typeof $pages !== 'undefined' && hasKey($page, $pages.current + '.resize')) {
      $page[$pages.current].resize();
    }
    // popup resizing
    if (typeof $popups !== 'undefined' && hasKey($popups, 'resize')) {
      $popups.resize();
    }
    if (typeof $popups !== 'undefined' && hasKey($popup, $popups.current + '.resize')) {
      $popup[$popups.current].resize();
    }
  }
};
