var $auth = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    if ($m.device.ie8 == false) {
      $auth.start();
    }
  },

  // ======================================================================
  // CHECK FUNCTIONS
  // ======================================================================

  current: 0,
  status: null, // null, started, finished, no_user
  // START CHECKS
  start: function() {
    if ($auth.status == null || $auth.status == 'no_user') {
      if (typeof $facebook === 'undefined' || ($facebook.got_status == true && $facebook.logged_in == false)) {
        $auth.status = 'no_user';
        $checks.launch();
      } else if ($facebook.got_status == true && $facebook.logged_in == true && $facebook.got_user == true) {
        $auth.status = 'started';
        $auth.next();
      } else {
        setTimeout(function() {
          $auth.start();
        }, 50);
      }
    }
  },
  // CONTINUE CHECKS
  next: function() {
    $auth.current++;
    if (typeof $auth['check' + $auth.current] !== 'undefined') {
      $auth.goto($auth.current);
    } else {
      $auth.finish();
    }
  },
  // GO TO CHECK
  goto: function(check) {
    $auth['check' + check]();
  },
  // RECHECK
  recheck: function(check) {
    $auth.current = check;
    $auth.goto_check($auth.current);
  },

  // ======================================================================
  // AUTH 1 - GET DATA
  // ======================================================================

  check1: function() {
    console.log('--- AUTH 1 - GET DATA ---');
    $auth.next();
  },
  check1_callback: function(response) {
    console.log(response);
    if (response.success == true) {
    }
    // CONTINUE CHECKS
    $auth.next();
  },

  // ======================================================================
  // AUTH 2 - DEV OVERRIDES
  // ======================================================================

  check2: function() {
    console.log('--- AUTH 2 - DEV OVERRIDES ---');
    if ($config.dev == true) {
      // $m.user.confirmed = 1;
      // $m.user.verified = 1;
    }
    // CONTINUE CHECKS
    $auth.next();
  },

  // ======================================================================
  // AUTH - FINISHED
  // ======================================================================

  finish: function() {
    console.log('--- AUTH - FINISHED ---');
    $auth.status = 'finished';
    $auth.launch();
  },

  // ======================================================================
  // LAUNCH
  // ======================================================================

  launch_timeout: null,
  launch: function() {
    if ($auth.status == null || $auth.status == 'no_user') {
      $auth.start();
    } else if ($auth.status == 'finished') {
      $checks.launch();
    }
  }
};
