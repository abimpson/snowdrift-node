var $filter = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  similarLongChars: [
    // ['|<', 'k'], ['(.)','tit']
  ],
  similarChars: [
    ['@', 'a'],
    ['0', 'o'],
    ['1', 'i'],
    ['2', 'r'],
    ['3', 'e'],
    ['4', 'a'],
    ['5', 's'],
    ['7', 't'],
    ['8', 'b'],
    ['9', 'g'],
    ['Ã¤', 'a'],
    ['Ã£', 'a'],
    ['Ã¢', 'a'],
    ['Ã¤', 'a'],
    ['Ã¡', 'a'],
    ['Ã ', 'a'],
    ['Ã¥', 'a'],
    ['Ã©', 'e'],
    ['Ã¨', 'e'],
    ['Ã«', 'e'],
    ['Ãª', 'e'],
    ['Â§', 's'],
    ['$', 's'],
    ['Â£', 'l'],
    ['â‚¬', 'e'],
    ['Ã¼', 'u'],
    ['Ã»', 'u'],
    ['Ãº', 'u'],
    ['Ã¹', 'u'],
    ['Ã®', 'i'],
    ['Ã¯', 'i'],
    ['Ã', 'i'],
    ['Ã¬', 'i'],
    ['Ã¿', 'y'],
    ['Ã½', 'y'],
    ['Ã¶', 'o'],
    ['Ã´', 'o'],
    ['Ãµ', 'o'],
    ['Ã³', 'o'],
    ['Ã²', 'o']
  ],
  badCompleteWordList: ['anal', 'arse', 'ass', 'balls', 'bum', 'cock', 'coon', 'cum', 'muff', 'rape', 'sex', 'tit', 'turd', 'kunt'],
  badList: [
    '69',
    'analsex',
    'anus',
    'arsebandit',
    'arsewipe',
    'ass',
    'asshole',
    'assmunch',
    'ballbag',
    'ballsack',
    'bareback',
    'bastard',
    'beefcurtain',
    'bellend',
    'biatch',
    'bimbo',
    'bitch',
    'bitches',
    'bloody',
    'blowjob',
    'bollock',
    'bollocks',
    'bollok',
    'bondage',
    'boner',
    'boob',
    'boobs',
    'booty',
    'breasts',
    'bugger',
    'bullshit',
    'bumhole',
    'butt',
    'buttcheeks',
    'buttplug',
    'clit',
    'clitoris',
    'clunge',
    'cocaine',
    'cockface',
    'cockgobbler',
    'crap',
    'cumface',
    'cumming',
    'cumslag',
    'cunnilingus',
    'cunt',
    'damn',
    'deepthroat',
    'dickhead',
    'dildo',
    'doggiestyle',
    'doggystyle',
    'dyke',
    'ecstasy',
    'ejaculation',
    'fag',
    'faggot',
    'fannyflaps',
    'fart',
    'feck',
    'felch',
    'felching',
    'fellate',
    'fellatio',
    'fingerbang',
    'fingering',
    'fisting',
    'flange',
    'flaps',
    'foof',
    'footjob',
    'fuck',
    'fudgepacker',
    'gash',
    'genitals',
    'goddamn',
    'gspot',
    'handjob',
    'hitler',
    'homo',
    'hooker',
    'humping',
    'incest',
    'intercourse',
    'jackoff',
    'japseye',
    'jerk',
    'jerkoff',
    'jizz',
    'ketamine',
    'kinky',
    'knob',
    'knobbing',
    'knobend',
    'labia',
    'lesbo',
    'lmao',
    'lmfao',
    'makemewet',
    'marijuana',
    'masturbate',
    'mdma',
    'meatcurtain',
    'meatsword',
    'milf',
    'minge',
    'minger',
    'muffdiver',
    'nazi',
    'negro',
    'nigga',
    'nigger',
    'nignog',
    'nipple',
    'nobcheese',
    'nobhead',
    'nympho',
    'nymphomania',
    'orgasm',
    'orgy',
    'paedo',
    'paedophile',
    'paki',
    'panties',
    'penis',
    'piss',
    'poof',
    'poofter',
    'poon',
    'poop',
    'porksword',
    'porn',
    'pornography',
    'prick',
    'prostitute',
    'pube',
    'pussy',
    'queaf',
    'queer',
    'raghead',
    'raping',
    'rapist',
    'rectum',
    'rentboy',
    'rimjob',
    'rimming',
    'sadism',
    'schlong',
    'scrotum',
    'shart',
    'shit',
    'slag',
    'slagbag',
    'sleaze',
    'slit',
    'slut',
    'smeg',
    'smegma',
    'smut',
    'spacko',
    'spaff',
    'spastic',
    'spunk',
    'strapon',
    'swastika',
    'teabagging',
    'testicle',
    'threesome',
    'throating',
    'tits',
    'titties',
    'titty',
    'titwank',
    'tosser',
    'tosspot',
    'tranny',
    'twat',
    'upthearse',
    'upthebum',
    'vagina',
    'vibrator',
    'voyeur',
    'vulva',
    'wank',
    'wanker',
    'wankonmytit',
    'weed',
    'whore',
    'woofter',
    'wtf'
  ],

  // ======================================================================
  // CHECK WORD
  // ======================================================================

  check: function(string) {
    var censored = $filter.censor(string);
    return string == censored;
  },

  // ======================================================================
  // CENSOR WORD
  // ======================================================================

  censor: function(string) {
    // vars
    var i;
    var p;
    var chars = '';
    var stars = '';
    var origString = '';
    var newString = '';
    var origStringNoSpaces = [];
    var origSpaceHyphenPositions = [];

    // set string to lowercase
    origString = string.toLowerCase();

    // replace long similar characters in orig string
    for (i = 0; i < $filter.similarLongChars.length; i++) {
      chars = $filter.similarLongChars[i];
      origString = $filter.stringReplace(origString, chars[0], chars[1]);
    }

    // replace single similar characters in new string
    for (i = 0; i < $filter.similarChars.length; i++) {
      chars = $filter.similarChars[i];
      origString = $filter.stringReplace(origString, chars[0], chars[1]);
    }

    // replace bad complete words with stars
    origStringNoSpaces = origString.split(' ');
    $($filter.badCompleteWordList).each(function() {
      for (i = 0; i < origStringNoSpaces.length; i++) {
        var tempWord = origStringNoSpaces[i].split('-').join('');
        if (this == origStringNoSpaces[i] || this == tempWord) {
          stars = '';
          for (p = 0; p < this.length; p++) {
            stars = stars + '*';
          }
          origStringNoSpaces[i] = stars;
        }
      }
    });
    newString = origStringNoSpaces.join('');

    // space & hyphen positions
    for (i = 0; i < origString.length; i++) {
      if (origString.charAt(i) == ' ' || origString.charAt(i) == '-') {
        var arr = [];
        arr.push(i);
        arr.push(origString.charAt(i));
        origSpaceHyphenPositions.push(arr);
      }
    }
    newString = newString.split('-').join('');

    // replace bad partial words with stars
    $($filter.badList).each(function() {
      if (newString.indexOf(this) != -1) {
        stars = '';
        for (i = 0; i < this.length; i++) {
          stars = stars + '*';
        }
        newString = $filter.stringReplace(newString, this, stars);
      }
    });

    // replace spaces & hyphens
    for (i = 0; i < origSpaceHyphenPositions.length; i++) {
      newString = newString.slice(0, origSpaceHyphenPositions[i][0]) + origSpaceHyphenPositions[i][1] + newString.slice(origSpaceHyphenPositions[i][0]);
    }

    // replace similar characters
    for (i = 0; i < origString.length; i++) {
      for (p = 0; p < $filter.similarChars.length; p++) {
        if (origString.charAt(i) == $filter.similarChars[p][0]) {
          if (newString.charAt(i) != '*') {
            newString = newString.slice(0, i) + $filter.similarChars[p][0] + newString.slice(i + 1);
          }
          p = 1000;
        }
      }
    }

    // restore caps (note - doesn't work with long similar chars)
    if (newString != string) {
      string = string.split('');
      for (var i in newString) {
        if (newString[i] == '*') {
          string[i] = '*';
        }
      }
      newString = string.join('');
    }

    // return
    // console.log(string + ' - ' + newString);
    return newString;
  },

  // ======================================================================
  // STRING REPLACE
  // ======================================================================

  stringReplace: function(input, find, replace) {
    while (input.indexOf('find') != -1) {
      input = input.split(find).join(replace);
    }
    return input.split(find).join(replace);
  }
};
