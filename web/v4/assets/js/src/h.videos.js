var $videos = {
  /*
  // PLAY
  video.play();
  // PAUSE
  video.pause();
  // SEEK
  video.currentTime = 1;
  // VOLUME
  video.volume = 0.0;
  */

  // ======================================================================
  // DATA
  // ======================================================================

  videos: {},
  data: {},

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {},

  // ======================================================================
  // CREATE VIDEO
  // ======================================================================

  create: function(path, div, id, callback_ready, callback_failed) {
    console.log('%c--- video - ' + id + ' ---', 'color:#f78600;');
    // OUTPUT HTML
    if (yourBrowser == 'IE') {
      div.html('<video id="' + id + '" src="' + path + '.mp4" style="width:100%;height:100%;"></video>');
    } else if (yourBrowser == 'Chrome') {
      div.html('<video id="' + id + '" style="width:100%;height:100%;"></video>');
      $('#' + id).attr('src', '' + path + '.mp4');
    } else {
      div.html(
        '<video id="' +
          id +
          '" style="width:100%;height:100%;">\
        <source src=\'' +
          path +
          ".mp4' type='video/mp4; codecs=\"avc1.4D401E, mp4a.40.2\"'>\
        <source src='" +
          path +
          ".webm' type='video/webm; codecs=\"vp8.0, vorbis\"'>\
        <source src='" +
          path +
          ".ogv' type='video/ogg; codecs=\"theora, vorbis\"'>\
      </video>"
      );
    }
    // CACHE VIDEO
    $videos.videos[id] = document.getElementById(id);
    // STORE VIDEO STATUS DATA
    $videos.data[id] = {
      inited: true,
      loaded: false,
      // started: false,
      // finished: false,
      failed: false,
      timer: 0,
      callback_ready: callback_ready,
      callback_failed: callback_failed
    };
    // TIMER
    $videos.timer(id);
  },

  // ======================================================================
  // TIMER
  // ======================================================================

  timer: function(id) {
    // console.log($videos.videos[id].readyState+' - '+id);
    var t = 50;
    var t2 = 5000;
    $videos.data[id].timer = $videos.data[id].timer + t;
    if ($videos.videos[id].readyState == 4) {
      // READY
      console.log('%c--- video ready ---', 'color:#f78600;');
      $videos.data[id].loaded = true;
      if (typeof $videos.data[id].callback_ready == 'function') {
        $videos.data[id].callback_ready();
      }
    } else if ($videos.data[id].timer >= t2) {
      // FAILED
      console.log('%c--- video failed ---', 'color:#f78600;');
      $videos.data[id].failed = true;
      if (typeof $videos.data[id].callback_failed == 'function') {
        $videos.data[id].callback_failed();
      }
    } else {
      // TIMEOUT
      setTimeout(function() {
        $videos.timer(id);
      }, t);
    }
  },

  // ======================================================================
  // STOP ALL
  // ======================================================================

  pause: function() {
    if (!$('body').hasClass('mobile') && Modernizr.video != false) {
      for (var i in $videos.videos) {
        $videos.videos[i].pause();
      }
    }
  }
};
