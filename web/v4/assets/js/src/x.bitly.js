var $bitly = new Object({
  // ======================================================================
  // OPTIONS
  // ======================================================================

  api_token: '0727ee6c61dd030eb3673d0c27d91eba54bd0c57',
  api_url: 'https://api-ssl.bitly.com',

  // ======================================================================
  // GET SHORT URL
  // ======================================================================

  shorten: function(url, callback) {
    $.ajax({
      type: 'get',
      url: $bitly.api_url + '/v3/shorten?access_token=' + $bitly.api_token + '&longUrl=' + encodeURIComponent(url),
      dataType: 'json',
      async: true,
      timeout: 20000,
      // SUCCESS
      success: function(response) {
        if (typeof callback == 'function') {
          callback(response);
        }
      },
      // ERROR
      error: function(xhr, status, error) {
        if (typeof callback == 'function') {
          var response = {
            response: 'error',
            xhr: xhr,
            status: status,
            error: error
          };
          callback(response);
        }
      }
    });
  },

  // ======================================================================
  // GET LONG URL
  // ======================================================================

  expand: function(url, callback) {
    $.ajax({
      type: 'get',
      url: $bitly.api_url + '/v3/expand?access_token=' + $bitly.api_token + '&shortUrl=' + encodeURIComponent(url),
      dataType: 'json',
      async: true,
      timeout: 20000,
      // SUCCESS
      success: function(response) {
        if (typeof callback == 'function') {
          callback(response);
        }
      },
      // ERROR
      error: function(xhr, status, error) {
        if (typeof callback == 'function') {
          var response = {
            response: 'error',
            xhr: xhr,
            status: status,
            error: error
          };
          callback(response);
        }
      }
    });
  }
});
