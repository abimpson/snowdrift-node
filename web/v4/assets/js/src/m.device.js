var $device = {
  // ======================================================================
  // VARS
  // ======================================================================

  type: '',
  ie: false,
  ie9: false,
  ie8: false,
  ie7: false,
  ie6: false,
  isTouchScreen: false,

  // css feature detection
  css: {
    positionSticky: false
  },

  // js feature detection
  js: {
    localStorage: window.localStorage ? true : false,
    sessionStorage: window.sessionStorage ? true : false,
    pushState: !!(window.history && history.pushState),
    replaceState: !!(window.history && history.replaceState)
  },

  // generic check for "modern browsers"
  isModernBrowser: 'querySelector' in document && 'localStorage' in window && 'addEventListener' in window,

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    $device.detectIE();
    $device.detectTouchScreen();
    $device.detectCssFeatures();
    // $device.extend();
  },

  // ======================================================================
  // DETECT IE
  // ======================================================================

  detectIE: function() {
    if (yourBrowser == 'IE' || yourBrowser == 'Mozilla') {
      $device.ie = true;
    }
    if (yourBrowser == 'IE') {
      if (yourBrowserVersion < 10) {
        $device.ie9 = true;
      }
      if (yourBrowserVersion < 9) {
        $device.ie8 = true;
      }
      if (yourBrowserVersion < 8) {
        $device.ie7 = true;
      }
      if (yourBrowserVersion < 7) {
        $device.ie6 = true;
      }
    }
  },

  // ======================================================================
  // DETECT TOUCH SCREEN
  // ======================================================================

  detectTouchScreen: function() {
    try {
      document.createEvent('TouchEvent');
      $device.isTouchScreen = true;
    } catch (e) {}
  },

  // ======================================================================
  // DETECT CSS FEATURES
  // ======================================================================

  detectCssFeatures: function() {
    // position sticky
    try {
      $device.css.positionSticky = (function() {
        var el = document.createElement('a'),
          mStyle = el.style;
        mStyle.cssText = 'position:sticky;position:-webkit-sticky;position:-ms-sticky;';
        var supportsPositionSticky = mStyle.position.indexOf('sticky') !== -1;
        if (supportsPositionSticky) {
          $el('body').addClass('css-position-sticky');
        }
        return supportsPositionSticky;
      })();
    } catch (e) {}
  }

  // ======================================================================
  // EXTEND - USING PHP GENERATED DATA
  // ======================================================================

  // extend: function(){
  //   if (typeof DEVICE !== 'undefined') {
  //     $.extend($device, DEVICE);
  //   }
  // },

  // ======================================================================
  // URL DATA
  // ======================================================================

  // url_data: {
  //   set: false
  // },
  // url_data_update: function() {
  //   if (typeof URL_DATA !== 'undefined') {
  //     $.extend($device.url_data, URL_DATA);
  //   }
  // },
};
