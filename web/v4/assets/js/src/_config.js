// ######################################################################
// SNOWDRIFT JS FRAMEWORK - v4 - by ALEX BIMPSON
// ######################################################################

// ======================================================================
// CONFIG
// ======================================================================

var $config = {
  // DEV
  dev: true,
  storage_name: 'a1_',
  // FACEBOOK CANVAS
  canvas_w: 810,
  canvas_h: 0, //set as 0 if height varies
  // GOOGLE ANALYTICS
  analytics: false, //use google analytics tracking,
  analytics_type: 'gtag',
  analytics_id: '',
  // FACEBOOK
  fb_link: '',
  fb_picture: '',
  fb_title: '',
  fb_caption: '',
  fb_description: ''
};

// ======================================================================
// CONFIG - ENVIRONMENTAL
// ======================================================================

var $config_env;
var $environment = window.location.hostname;
if ($environment == 'mydomain.dev') {
  // ALEX LOCAL
  $config_env = {
    // PATHS
    api_root: '/api/v1/',
    image_root: '/assets/images/',
    fb_app_id: ''
  };
} else {
  // DEFAULT
  $config_env = {
    // PATHS
    api_root: '/api/v1/',
    image_root: '/assets/images/',
    fb_app_id: ''
  };
}
$.extend($config, $config_env);

// ======================================================================
// PLACEHOLDER COPY
// ======================================================================

// var $placeholders = {};

// ======================================================================
// PRELOADING
// ======================================================================

// $config.preload = {
//   timeout: 6000,
//   fontcss: ['assets/css/styles.css'],
//   // FONTS
//   fonts: Array(),
//   // ALL
//   files: Array(),
//   // ALL - NON RETINA
//   files_non_retina: Array(),
//   // ALL - RETINA
//   files_retina: Array(),
//   // DESKTOP
//   files_d: Array(),
//   // DESKTOP - NON RETINA
//   files_d_non_retina: Array(),
//   // DESKTOP - RETINA
//   files_d_retina: Array(),
//   // MOBILE
//   files_m: Array(),
//   // MOBILE - NON RETINA
//   files_m_non_retina: Array(),
//   // MOBILE - RETINA
//   files_m_retina: Array()
// };

// ======================================================================
// FACEBOOK - CONFIG
// ======================================================================

// var $facebook_config = {
//   app_id: $config.fb_app_id,
//   scope: '',
//   user_fields: 'id,first_name,last_name,gender,email,picture.height(200).width(200)'
// };

// ======================================================================
// FACEBOOK - CALLBACK FUNCTIONS
// ======================================================================

// var $facebook_functions = {
//   // READY
//   fnc_ready: function() {},
//   // AUTO - NOT LOGGED IN TO FACEBOOK
//   fnc_auto_not_logged_in: function() {
//     $('.facebook_loading').hide();
//     $('.facebook_login').css('display', 'inline-block');
//   },
//   // AUTO - NOT AUTHENTICATED
//   fnc_auto_not_auth: function() {
//     $('.facebook_loading').hide();
//     $('.facebook_login').css('display', 'inline-block');
//   },
//   // AUTO - AUTHENTICATED
//   fnc_auto_auth: function() {
//     $('.facebook_loading').hide();
//     $('.facebook_login')
//       .css('display', 'inline-block')
//       .addClass('revisit');
//   },
//   // LOGIN - PRE-AUTHENTICATION
//   fnc_login_pre: function() {},
//   // LOGIN - AUTHENTICATED
//   fnc_login_auth: function() {},
//   // LOGIN CANCELLED
//   fnc_cancelled: function() {},
//   // PERMISSIONS REVOKED
//   fnc_revoked: function() {
//     location.reload();
//   }
// };

// ======================================================================
// TWITTER - CALLBACK FUNCTIONS
// ======================================================================

// var $twitter_functions = {
//   // CLICK - PRE-AUTHENTICATION
//   twit_click_pre: function() {},
//   // CLICK - AUTHENTICATED
//   twit_login_auth: function() {},
//   // LOGIN CANCELLED
//   twit_cancelled: function() {}
// };
