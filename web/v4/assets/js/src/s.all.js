/*
  
AUTHOR:    Alex Bimpson @ Kerve
NAME:      Standard Functions
WEBSITE:   http://www.kerve.com
LICENSE:   Distributed under the MIT License
VERSION:   4.14
UPDATED:   2019-09-13
  
*/

// ======================================================================
// CONFIG
// ======================================================================

$.support.cors = true; //force enable cross-site scripting
jQuery.fx.interval = 20;
var CONFIG_LOG = true; //enable console logs

// ======================================================================
// DEVICE DETECTION
// ======================================================================

/******************** BROWSER DETECTION ********************/

var browserDetect = {
  init: function() {
    this.browser = this.searchString(this.dataBrowser) || 'Other';
    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || 'Unknown';
    this.OS = this.searchString(this.dataOS) || 'Unknown';
  },
  searchString: function(data) {
    for (var i = 0; i < data.length; i++) {
      var dataString = data[i].string;
      this.versionSearchString = data[i].subString;
      if (dataString.indexOf(data[i].subString) != -1) {
        return data[i].identity;
      }
    }
  },
  searchVersion: function(dataString) {
    var index = dataString.indexOf(this.versionSearchString);
    if (index == -1) return;
    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
  },
  dataBrowser: [
    {string: navigator.userAgent, subString: 'Chrome', identity: 'Chrome'},
    {string: navigator.userAgent, subString: 'MSIE', identity: 'IE'},
    {string: navigator.userAgent, subString: 'Firefox', identity: 'Firefox'},
    {string: navigator.userAgent, subString: 'Safari', identity: 'Safari'},
    {string: navigator.userAgent, subString: 'Opera', identity: 'Opera'},
    {string: navigator.userAgent, subString: 'OmniWeb', versionSearch: 'OmniWeb/', identity: 'OmniWeb'},
    {string: navigator.vendor, subString: 'iCab', identity: 'iCab'},
    {string: navigator.vendor, subString: 'KDE', identity: 'Konqueror'},
    {string: navigator.vendor, subString: 'Camino', identity: 'Camino'},
    // for newer Netscapes (6+)
    {string: navigator.userAgent, subString: 'Netscape', identity: 'Netscape'},
    {string: navigator.userAgent, subString: 'MSIE', identity: 'Explorer', versionSearch: 'MSIE'},
    {string: navigator.userAgent, subString: 'Gecko', identity: 'Mozilla', versionSearch: 'rv'},
    // for older Netscapes (4-)
    {string: navigator.userAgent, subString: 'Mozilla', identity: 'Netscape', versionSearch: 'Mozilla'}
  ],
  dataOS: [
    {string: navigator.platform, subString: 'Win', identity: 'Windows'},
    {string: navigator.platform, subString: 'Mac', identity: 'Mac'},
    {string: navigator.userAgent, subString: 'iPhone', identity: 'iPhone/iPod'},
    {string: navigator.platform, subString: 'Linux', identity: 'Linux'}
  ]
};
browserDetect.init();
var yourBrowser = browserDetect.browser;
var yourBrowserVersion = browserDetect.version;
var yourOS = browserDetect.OS;

/******************** MOBILE DETECTION ********************/

/*var mobileDetect = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};*/

/******************** TABLET DETECTION ********************/

/*var tabletDetect = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};*/

// ======================================================================
// CONSOLE LOGGING
// ======================================================================

/******************** DISABLE CONSOLE LOGS - FOR BROWSERS THAT DON'T SUPPORT IT ********************/

if (
  !window.console ||
  (typeof CONFIG_LOG !== 'undefined' && CONFIG_LOG == false) ||
  (typeof $config !== 'undefined' && typeof $config.console_log !== 'undefined' && $config.console_log == false)
) {
  // || (yourBrowser == 'IE' && yourBrowserVersion < 9)) {
  (function() {
    var names = [
      'log',
      'debug',
      'info',
      'warn',
      'error',
      'assert',
      'dir',
      'dirxml',
      'group',
      'groupEnd',
      'time',
      'timeEnd',
      'count',
      'trace',
      'profile',
      'profileEnd'
    ];
    window.console = {};
    for (var i = 0; i < names.length; ++i) {
      window.console[names[i]] = function() {};
    }
  })();
}

/******************** WINDOW LOG FUNCTION ********************/

// window.log = function(string) {
//   parseConsoleLog(string, 'log');
// };
// window.error = function(string) {
//   parseConsoleLog(string, 'error');
// };
// parseConsoleLog = function(string, name) {
//   if (CONFIG_LOG == true) {
//     if (name == null || name == '' || name == 'undefined') {
//       var name = 'log';
//     }
//     if (yourBrowser == 'IE') {
//       //IF IE - OUTPUT OBJECT AS STRING
//       if ($.isPlainObject(string) == true || typeof string === 'object') {
//         console[name](JSON.stringify(string));
//       } else {
//         console[name](string);
//       }
//     } else {
//       //NORMAL CONSOLE LOG
//       console[name](string);
//     }
//   }
// };

// ======================================================================
// CACHE ELEMENTS
// ======================================================================

var $cache = {};
function $el(el, force) {
  if (force == true || typeof $cache[el] === 'undefined') {
    var el2 = $(el);
    if (el2.length < 1) {
      return el2;
    } else {
      $cache[el] = el2;
      return $cache[el];
    }
  } else {
    return $cache[el];
  }
}

// ======================================================================
// POLYFILLS
// ======================================================================

/******************** INDEXOF - FOR < IE8 ********************/

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(val) {
    return jQuery.inArray(val, this);
  };
}

/******************** OBJECT WATCH ********************/

/*
  2012-04-03
  By Eli Grey, http://eligrey.com
  Public Domain.
  NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
*/
// if(!Object.prototype.watch){
//   Object.defineProperty(Object.prototype, "watch", {
//     enumerable: false,
//     configurable: true,
//     writable: false,
//     value: function(prop, handler){
//       var oldval = this[prop],
//       newval = oldval,
//       getter = function(){
//         return newval;
//       },
//       setter = function(val){
//         oldval = newval;
//         return newval = handler.call(this,prop,oldval,val);
//       };
//       if(delete this[prop]){ // can't watch constants
//         Object.defineProperty(this, prop, {
//           get: getter,
//           set: setter,
//           enumerable: true,
//           configurable: true
//         });
//       };
//     }
//   });
// };
// if(!Object.prototype.unwatch){
//   Object.defineProperty(Object.prototype, "unwatch", {
//     enumerable: false,
//     configurable: true,
//     writable: false,
//     value: function(prop){
//       var val = this[prop];
//       delete this[prop]; // remove accessors
//       this[prop] = val;
//     }
//   });
// };

// ======================================================================
// VALIDATION
// ======================================================================

/******************** VALIDATE STRING LENGTH ********************/

function isValidLength(val, length) {
  if (val.length < length || checkWhitespace(val) == true) {
    return false;
  } else {
    return true;
  }
}
function isValidLengthRange(val, minl, maxl) {
  if (val.length < minl || val.length > maxl || checkWhitespace(val) == true) {
    return false;
  } else {
    return true;
  }
}

/******************** VALIDATE NUMBER, NO OTHER CHARS ********************/

function isValidNumber(number) {
  if ($.trim(number) != '') {
    var regExp = /^\d+$/;
    return regExp.test(number);
  } else {
    return false;
  }
}

/******************** VALIDATE NAME ********************/

function isValidName(name) {
  if (name.length < 2 || checkWhitespace(name) == true) {
    return false;
  } else {
    return true;
  }
}

/******************** VALIDATE POSTCODE ********************/

function isValidPostcode(postcode) {
  if ($.trim(postcode) != '') {
    var regExp = /^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$/i;
    return regExp.test(postcode);
  } else {
    return false;
  }
}

/******************** VALIDATE ZIP CODE ********************/

function isValidZip(zip) {
  if ($.trim(zip) != '') {
    var regExp = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
    return regExp.test(zip);
  } else {
    return false;
  }
}

/******************** VALIDATE PHONE ********************/

function isValidPhone(phone) {
  if ($.trim(phone) != '') {
    var cleaned = phone
      .replace(/\+/g, '')
      .replace(/\-/g, '')
      .replace(/\(/g, '')
      .replace(/\)/g, '')
      .replace(/ /g, '');
    if (isValidNumber(cleaned)) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/******************** VALIDATE EMAIL ADDRESS ********************/

function isValidEmail(e) {
  if ($.trim(e) != '') {
    var regExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(e);
  } else {
    return false;
  }
}

/******************** VALIDATE URL ********************/

function isValidUrl(url) {
  if ($.trim(url) != '') {
    var regExp = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
    return regExp.test(url);
  } else {
    return false;
  }
}

/******************** GET YOUTUBE ID FROM URL ********************/

function getYoutubeId(url) {
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);
  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return false;
  }
}

// ======================================================================
// ENCRYPTION
// ======================================================================

/******************** BASIC ENCRYPTION ********************/

// Jonas Raoni Soares Silva
// http://jsfromhell.com/string/rot13 [v1.0]
// String.prototype.rot13 = function() {
//   return this.replace(/[a-zA-Z]/g, function(c) {
//     return String.fromCharCode((c <= 'Z' ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
//   });
// };

// ======================================================================
// API ENCRYPTION
// ======================================================================

/******************** ENCODE STRING ********************/

// function encodeString(s) {
//   if (typeof s === 'string') {
//     // backslash
//     s = s.replace(/\\/g, '%5C%5C');
//     // quotes
//     s = s.replace(/'/g, '%27');
//     s = s.replace(/"/g, '%22');
//   }
//   return s;
// }

/******************** ENCODE OBJECT ********************/

// function encodeObject(obj, prefix) {
//   var newString = '%7B';
//   var stringLength = 0;
//   var count = 0;
//   for (var j in obj) {
//     stringLength++;
//   }
//   for (var i in obj) {
//     count++;
//     var string = encodeURIComponent(encodeString(obj[i]));
//     // string = string.replace(/%/g,'').replace(/\\/g,''); // remove chars - % \
//     console.log(string);
//     newString += '%22' + i + '%22:%22' + string + '%22' + (count < stringLength ? ',' : '');
//   }
//   newString += '%7D';
//   return newString;
// }

/******************** ENCRYPT ********************/

// function apiEncrypt(string, key) {
//   // validate string
//   string = string.toString();
//   if (typeof string === 'undefined' || string == null || $.trim(string) == '') {
//     return false;
//   }
//   // validate key
//   if (typeof key === 'undefined' || key == null || $.trim(key) == '') {
//     return false;
//   }
//   // encrypt
//   var returnString = '';
//   var chars = 'hdF6icSAV1gK5QPNuqLwIko9ftXlGzDjsa2EYvn0W4bZry7RT3pO8eHxmJUBCM';
//   var localString = string.split('');
//   var localChars = chars.split('');
//   var localKey = key.split('');
//   var keyorder = [];
//   for (a = 0; a < localKey.length; a++) {
//     keyorder[a] = a;
//   }
//   keyorder = keyorder.sort(function() {
//     return 0.5 - Math.random();
//   });
//   for (a = 0; a < localString.length; a++) {
//     var numeric = localString[a].charCodeAt() + localKey[keyorder[a % localKey.length]].charCodeAt();
//     returnString += localChars[Math.floor(numeric / localChars.length)];
//     returnString += localChars[numeric % localChars.length];
//   }
//   var keyOrderString = '';
//   for (var k in keyorder) {
//     keyOrderString += localChars[keyorder[k]];
//   }
//   // return
//   return keyOrderString + '' + returnString;
// }

// ======================================================================
// URL FUNCTIONS
// ======================================================================

/******************** CHECK DOMAIN PROTOCOL ********************/

// function isSecure() {
//   return window.location.protocol == 'https:';
// }

/******************** GET URL DIRECTORY ********************/

// function getURLDirectory() {
//   var u = window.location.pathname.substring(1);
//   return u;
// }

/******************** GET PARAMETER VALUE FROM URL ********************/

// function getURLParameter(param) {
//   var u = window.location.search.substring(1);
//   var uVars = u.split('&');
//   for (var i = 0; i < uVars.length; i++) {
//     var parameterName = uVars[i].split('=');
//     if (parameterName[0] == param) {
//       return parameterName[1];
//     }
//     if (i == uVars.length - 1) {
//       return null;
//     }
//   }
// }

/******************** SLUGIFY STRING ********************/

// function slugify(Text) {
//   return Text.toLowerCase()
//     .replace(/[^\w ]+/g, '')
//     .replace(/ +/g, '+');
// }
// function deslugify(Text) {
//   return Text.replace(/\++/g, ' ');
// }

// ======================================================================
// STRING FUNCTIONS
// ======================================================================

/******************** CAPITALISE ALL WORDS IN STRING ********************/

// function ucwords(str) {
//   return str.replace(/(?:^|\s)\S/g, function(a) {
//     return a.toUpperCase();
//   });
// }
// String.prototype.capitalise = function() {
//   return ucwords(this);
// };

/******************** CAPITALISE WORD ********************/

// function capitaliseWord(string) {
//   return string.charAt(0).toUpperCase() + string.slice(1);
// }

/******************** WHITESPACE ********************/

// function removeWhitespace(text) {
//   return $.trim(text);
// }
// function checkWhitespace(text) {
//   if (removeWhitespace(text) == '') {
//     return true;
//   } else {
//     return false;
//   }
// }

/******************** TRIM A STRING TO A SPECIFIC NUMBER OF WORDS ********************/

// function trimWords(string, numWords, dots) {
//   var countString = string.split(/\s+/).length;
//   var expString = string.split(/\s+/, numWords);
//   var newString = expString.join(' ');
//   if (dots && dots == true) {
//     if (countString > numWords) {
//       newString += '...';
//     }
//   }
//   return newString;
// }

/******************** REPLACE LABELS ********************/

// function replaceLabels(html, vars) {
//   for (var i in vars) {
//     var pattern = '{{' + i + '}}';
//     var regex = new RegExp(pattern, 'g');
//     html = html.replace(regex, vars[i]);
//   }
//   return html;
// }

/******************** GENERATE RANDOM STRING ********************/

// function getRandomString(length) {
//   if (typeof length === 'undefined') {
//     length = 5;
//   }
//   var text = '';
//   var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//   for (var i = 0; i < length; i++) {
//     text += possible.charAt(Math.floor(Math.random() * possible.length));
//   }
//   return text;
// }

// ======================================================================
// NUMBER FUNCTIONS
// ======================================================================

/******************** GET NUMBER SUFFIX ********************/

// function getNumberSuffix(n) {
//   var j = n % 10,
//     k = n % 100;
//   if (j == 1 && k != 11) {
//     return 'st';
//   }
//   if (j == 2 && k != 12) {
//     return 'nd';
//   }
//   if (j == 3 && k != 13) {
//     return 'rd';
//   }
//   return 'th';
// }

/******************** CHECK IF VAR IS A NUMBER OR NOT ********************/

// function isNumber(n) {
//   return !isNaN(parseFloat(n)) && isFinite(n);
// }

/******************** ODD AND EVEN CHECKS ********************/

// function isEven(n) {
//   return isNumber(n) && n % 2 == 0;
// }
// function isOdd(n) {
//   return isNumber(n) && n % 2 == 1;
// }

/******************** GET RANDOM NUMBER ********************/

// function rand(minNo, maxNo) {
//   var difference = maxNo - minNo + 1;
//   var randomNo = Math.floor(Math.random() * difference);
//   return randomNo + minNo;
// }

/******************** ROUNDING - TO X DECIMAL PLACES ********************/

// function round(number, dp, fix) {
//   if (dp && dp != '' && dp != 'undefined') {
//     var multiplier = '1';
//     for (i = 0; i < dp; i++) {
//       multiplier = multiplier + '0';
//     }
//     var response = Math.round(number * parseInt(multiplier)) / parseInt(multiplier);
//     if (fix) {
//       response = addDecimals(response, dp);
//     }
//     return response;
//   } else {
//     return Math.round(number);
//   }
// }
// function roundUp(number, dp, fix) {
//   if (dp && dp != '' && dp != 'undefined') {
//     var multiplier = '1';
//     for (i = 0; i < dp; i++) {
//       multiplier = multiplier + '0';
//     }
//     var response = Math.ceil(number * parseInt(multiplier)) / parseInt(multiplier);
//     if (fix) {
//       response = addDecimals(response, dp);
//     }
//   } else {
//     return Math.ceil(number);
//   }
// }
// function roundDown(number, dp, fix) {
//   if (dp && dp != '' && dp != 'undefined') {
//     var multiplier = '1';
//     for (i = 0; i < dp; i++) {
//       multiplier = multiplier + '0';
//     }
//     var response = Math.floor(number * parseInt(multiplier)) / parseInt(multiplier);
//     if (fix) {
//       response = addDecimals(response, dp);
//     }
//   } else {
//     return Math.floor(number);
//   }
// }
// function addDecimals(number, dp) {
//   var s = number.toString().split('.');
//   if (s.length == 1) {
//     number = number + '.00';
//   } else {
//     var t = s[1];
//     var l = t.length;
//     if (l < dp) {
//       var d = dp - l;
//       for (i = 0; i < d; i++) {
//         number = number + '0';
//       }
//     }
//   }
//   return number;
// }

/******************** NUMBER TO WORDS ********************/

// function numberToWords(s) {
//   // Convert numbers to words
//   // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
//   // permission to use this Javascript on your web page is granted
//   // provided that all of the code (including this copyright notice) is
//   // used exactly as shown (you can change the numbering system if you wish)
//
//   // VARS
//   var th = ['', 'thousand', 'million', 'billion', 'trillion']; // American Numbering System
//   // var th = ['','thousand','million', 'milliard','billion'];// uncomment this line for English Number System
//   var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
//   var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
//   var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
//   // CALCULATE
//   s = s.toString();
//   s = s.replace(/[\, ]/g, '');
//   if (s != parseFloat(s)) return 'not a number';
//   var x = s.indexOf('.');
//   if (x == -1) x = s.length;
//   if (x > 15) return 'too big';
//   var n = s.split('');
//   var str = '';
//   var sk = 0;
//   for (var i = 0; i < x; i++) {
//     if ((x - i) % 3 == 2) {
//       if (n[i] == '1') {
//         str += tn[Number(n[i + 1])] + ' ';
//         i++;
//         sk = 1;
//       } else if (n[i] != 0) {
//         str += tw[n[i] - 2] + ' ';
//         sk = 1;
//       }
//     } else if (n[i] != 0) {
//       str += dg[n[i]] + ' ';
//       if ((x - i) % 3 == 0) str += 'hundred ';
//       sk = 1;
//     }
//     if ((x - i) % 3 == 1) {
//       if (sk) str += th[(x - i - 1) / 3] + ' ';
//       sk = 0;
//     }
//   }
//   if (x != s.length) {
//     var y = s.length;
//     str += 'point ';
//     for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
//   }
//   return str.replace(/\s+/g, ' ');
// }

/******************** DEGREES / RADIANS CONVERSION ********************/

// function toDegrees(n) {
//   return n * (180 / Math.PI);
// }
// function toRadians(n) {
//   return n * (Math.PI / 180);
// }

// ======================================================================
// ARRAY FUNCTIONS
// ======================================================================

/******************** ARRAY - CHECK IF OBJECT IS ARRAY ********************/

// function isArray(a) {
//   if (Object.prototype.toString.call(a) === '[object Array]') {
//     return true;
//   } else {
//     return false;
//   }
// }

/******************** ARRAY - MAKE UNIQUE ********************/

// function uniqueArray(array) {
//   return $.grep(array, function(el, index) {
//     return index == $.inArray(el, array);
//   });
// }

/******************** ARRAY - DELETE ITEM ********************/

// function removeFromArray(array, x) {
//   var index = $.inArray(x, array);
//   if (index > -1) {
//     array.splice(index, 1);
//   }
// }

/******************** ARRAY - REMOVE KEY ********************/

// function removeKey(arrayName, key) {
//   var x;
//   var tmpArray = [];
//   for (x in arrayName) {
//     if (x != key) {
//       tmpArray[x] = arrayName[x];
//     }
//   }
//   return tmpArray;
// }

/******************** ARRAY - SHUFFLE ********************/

// function shuffleArray(array) {
//   // for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
//   // return o;
//   var currentIndex = array.length,
//     temporaryValue,
//     randomIndex;
//   // While there remain elements to shuffle...
//   while (0 !== currentIndex) {
//     // Pick a remaining element...
//     randomIndex = Math.floor(Math.random() * currentIndex);
//     currentIndex -= 1;
//     // And swap it with the current element.
//     temporaryValue = array[currentIndex];
//     array[currentIndex] = array[randomIndex];
//     array[randomIndex] = temporaryValue;
//   }
//   return array;
// }

// ======================================================================
// OBJECT FUNCTIONS
// ======================================================================

/******************** CHECK IF OBJECT HAS KEY ********************/

function hasKey(obj, key) {
  return key.split('.').every(function(x) {
    if (typeof obj === 'undefined' || obj === null || typeof x === 'undefined' || x == null || x == '' || typeof obj[x] === 'undefined') {
      // || !x in obj
      return false;
    }
    obj = obj[x];
    return true;
  });
}

/******************** RETURN THE LENGTH OF AN OBJECT ********************/

function objectLength(object) {
  var length = 0;
  for (var i in object) {
    length = parseInt(length) + 1;
  }
  return length;
}

/******************** SORT OBJECT - BY KEY - e.g. object: { keyToSortBy: val } ********************/

// function sortObject(obj, order) {
//   var key,
//     tempArray = [],
//     i,
//     tempObj = {};
//   // build the tempArray
//   for (key in obj) {
//     tempArray.push(key);
//   }
//   // sort the tempArray
//   tempArray.sort(function(a, b) {
//     return a.toLowerCase().localeCompare(b.toLowerCase());
//   });
//   // create new object
//   if (order === 'desc') {
//     for (i = tempArray.length - 1; i >= 0; i--) {
//       tempObj[tempArray[i]] = obj[tempArray[i]];
//     }
//   } else {
//     for (i = 0; i < tempArray.length; i++) {
//       tempObj[tempArray[i]] = obj[tempArray[i]];
//     }
//   }
//   return tempObj;
// }

/******************** SORT OBJECT - BY VAL - e.g. object:{ key: valToSortBy } ********************/

// function sortObjectByVal(obj, val, order) {}

/******************** SORT OBJECT - BY VAL 2 - e.g. object:{ key: { key: valToSortBy } } ********************/

// function sortObjectByVal2(obj, val, order) {
//   var tempArray = [];
//   var tempObj = {};
//   // build the tempArray
//   for (var x in obj) {
//     tempArray.push({key: x, val: obj[x][val]});
//   }
//   // sort the tempArray
//   tempArray.sort(function(a, b) {
//     var as = a['val'],
//       bs = b['val'];
//     return as == bs ? 0 : as > bs ? 1 : -1;
//   });
//   // create new object
//   if (order === 'desc') {
//     for (i = tempArray.length - 1; i >= 0; i--) {
//       tempObj[tempArray[i]['key']] = obj[tempArray[i]['key']];
//     }
//   } else {
//     for (i = 0; i < tempArray.length; i++) {
//       tempObj[tempArray[i]['key']] = obj[tempArray[i]['key']];
//       console.log(tempArray[i]['key'] + ' - ' + tempArray[i]['val']);
//     }
//   }
//   return tempObj;
// }

// ======================================================================
// TIME AND DATE FUNCTIONS
// ======================================================================

/******************** PROTOTYPE - TO ISO STRING - FOR IE8 AND BELOW ********************/

// if (!Date.prototype.toISOString) {
//   if (!Date.prototype.toJSON) {
//     Date.prototype.toJSON = function(key) {
//       function f(n) {
//         return n < 10 ? '0' + n : n;
//       }
//       return (
//         this.getUTCFullYear() +
//         '-' +
//         f(this.getUTCMonth() + 1) +
//         '-' +
//         f(this.getUTCDate()) +
//         'T' +
//         f(this.getUTCHours()) +
//         ':' +
//         f(this.getUTCMinutes()) +
//         ':' +
//         f(this.getUTCSeconds()) +
//         'Z'
//       );
//     };
//   }
//   Date.prototype.toISOString = Date.prototype.toJSON;
// }

/******************** GET CURRENT UNIX TIMESTAMP ********************/

// function getTimestamp() {
//   var d = new Date();
//   return (d.getTime() / 1000) | 0;
// }

/******************** UNIX TIMESTAMP TO JS DATE - 1393431645 ********************/

// function timestampToDate(timestamp) {
//   return new Date(timestamp * 1000);
// }

/******************** TIME STRING TO JS DATE - YYYY-MM-DD HH:MM:SS ********************/

// function stringToDate(s) {
//   s = s.split(/[-: ]/);
//   return new Date(s[0], s[1] - 1, s[2], s[3], s[4], s[5]);
// }

/******************** JS DATE TO MYSQL STRING - YYYY-MM-DD HH:MM:SS ********************/

// function dateToString() {
//   var d = new Date();
//   d =
//     d.getUTCFullYear() +
//     '-' +
//     ('00' + (d.getUTCMonth() + 1)).slice(-2) +
//     '-' +
//     ('00' + d.getUTCDate()).slice(-2) +
//     ' ' +
//     ('00' + d.getUTCHours()).slice(-2) +
//     ':' +
//     ('00' + d.getUTCMinutes()).slice(-2) +
//     ':' +
//     ('00' + d.getUTCSeconds()).slice(-2);
//   return d;
// }

/******************** GET USER FRIENDLY DATE ELEMENTS ********************/

// function getDateDay(day) {
//   if (day <= 0) return 'Sunday';
//   else if (day == 1) return 'Monday';
//   else if (day == 2) return 'Tuesday';
//   else if (day == 3) return 'Wednesday';
//   else if (day == 4) return 'Thursday';
//   else if (day == 5) return 'Friday';
//   else if (day == 6) return 'Saturday';
//   else day = day - 7;
//   return getDateDay(day);
// }
// function getDateMonth(month) {
//   if (month == 0) return 'January';
//   if (month == 1) return 'February';
//   if (month == 2) return 'March';
//   if (month == 3) return 'April';
//   if (month == 4) return 'May';
//   if (month == 5) return 'June';
//   if (month == 6) return 'July';
//   if (month == 7) return 'August';
//   if (month == 8) return 'September';
//   if (month == 9) return 'October';
//   if (month == 10) return 'November';
//   if (month == 11) return 'December';
// }
// function getDateHours(hours) {
//   if (hours > 12) {
//     return hours - 12;
//   } else {
//     return hours;
//   }
// }
// function getDateMinutes(minutes) {
//   if (minutes < 10) {
//     return 0 + minutes.toString();
//   } else {
//     return minutes;
//   }
// }
// function getDateTimeSuffix(hours) {
//   if (hours >= 12) {
//     return 'pm';
//   } else {
//     return 'am';
//   }
// }

/******************** DATE - GET EVERYTHING - YYYY-MM-DD HH:MM:SS ********************/

// function getDateObject(datetime2) {
//   console.log(datetime2);
//   var datetime = datetime2.split(' ');
//   var date = datetime[0].split('-');
//   var time = datetime[1].split(':');
//   // console.log(date[0], date[1]-1, date[2], time[0], time[1], time[2]);
//   var d = new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
//   var object = {
//     _date: d,
//     day_d: parseInt(date[2]),
//     day_dd: date[2],
//     month_m: parseInt(date[1]),
//     month_mm: date[1],
//     year_yy: date[0].substr(2, 2),
//     year_yyyy: date[0],
//     day: d.getDay(),
//     day_suffix: getNumberSuffix(date[2]),
//     day_name: getDateDay(d.getDay()),
//     day_name_short: getDateDay(d.getDay()).substr(0, 3),
//     month_name: getDateMonth(date[1] - 1),
//     month_name_short: getDateMonth(date[1] - 1).substr(0, 3),
//     hour12: getDateHours(time[0]),
//     hour24: time[0],
//     hour_suffix: getDateTimeSuffix(time[0]),
//     minute: time[1],
//     second: time[2]
//   };
//   return object;
// }

/******************** GET DIFFERENCE BETWEEN 2 DATES - YYYY-MM-DD HH:MM:SS ********************/

// function getDateDifference(d1, d2) {
//   var date1 = stringToDate(d1);
//   var date2 = stringToDate(d2);
//   // convert to utc to avoid DST issues
//   var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds());
//   var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate(), date2.getHours(), date2.getMinutes(), date2.getSeconds());
//   // calculate difference
//   var diff = Math.abs(utc2 - utc1);
//   diff = diff / 1000;
//   // split into days/hours/mins/secs
//   var days = Math.floor(diff / (24 * 60 * 60));
//   diff = diff - days * 24 * 60 * 60;
//   var hours = Math.floor(diff / (60 * 60));
//   diff = diff - hours * 60 * 60;
//   var mins = Math.floor(diff / 60);
//   diff = diff - mins * 60;
//   var secs = diff;
//   // return
//   return {
//     days: days,
//     hours: hours,
//     mins: mins,
//     secs: secs
//   };
// }

/******************** AGE FROM DATE OF BIRTH ********************/

// function isLeapYear(year) {
//   var d = new Date(year, 1, 28);
//   d.setDate(d.getDate() + 1);
//   return d.getMonth() == 1;
// }
// function getAge(date) {
//   var d = new Date(date),
//     now = new Date();
//   var years = now.getFullYear() - d.getFullYear();
//   d.setFullYear(d.getFullYear() + years);
//   if (d > now) {
//     years--;
//     d.setFullYear(d.getFullYear() - 1);
//   }
//   var days = (now.getTime() - d.getTime()) / (3600 * 24 * 1000);
//   return Math.floor(years + days / (isLeapYear(now.getFullYear()) ? 366 : 365));
// }

// ======================================================================
// ANIMATION FUNCTIONS
// ======================================================================

/******************** ANIMATE BG ********************/

// $.fn.animateBG = function(x, y, speed) {
//   var pos = this.css('background-position').split(' ');
//   this.x = pos[0] || 0;
//   this.y = pos[1] || 0;
//   $.Animation(
//     this,
//     {
//       x: x,
//       y: y
//     },
//     {
//       duration: speed,
//       easing: 'linear'
//     }
//   ).progress(function(e) {
//     this.css('background-position', e.tweens[0].now + 'px ' + e.tweens[1].now + 'px');
//   });
//   return this;
// };

// ======================================================================
// MISC FUNCTIONS
// ======================================================================

/******************** COPY TO CLIPBOARD ********************/

// function copyToClipboard(string) {
//   // IE
//   if (window.clipboardData && clipboardData.setData) {
//     clipboardData.setData('Text', string);
//   }
// }

/******************** TEXT AREA MAX LENGTH ********************/

// $(document).ready(function() {
//   var txts = document.getElementsByTagName('textarea');
//   for (var i = 0, l = txts.length; i < l; i++) {
//     if (/^[0-9]+$/.test(txts[i].getAttribute('maxlength'))) {
//       var func = function() {
//         var len = parseInt(this.getAttribute('maxlength'), 10);
//         if (this.value.length > len) {
//           alert('Maximum length exceeded: ' + len);
//           this.value = this.value.substr(0, len);
//           return false;
//         }
//       };
//       txts[i].onkeyup = func;
//       txts[i].onblur = func;
//     }
//   }
// });

/******************** CHECK IF ELEMENT IS ON SCREEN ********************/

// $.fn.isOnScreen = function() {
//   var win = $(window);
//   var viewport = {
//     top: win.scrollTop(),
//     left: win.scrollLeft()
//   };
//   viewport.right = viewport.left + win.width();
//   viewport.bottom = viewport.top + win.height();
//   var bounds = this.offset();
//   bounds.right = bounds.left + this.outerWidth();
//   bounds.bottom = bounds.top + this.outerHeight();
//   return !(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom);
// };

/******************** FIT IMAGE TO PARENT ********************/

// function fitImageToParent(img) {
//   var parent = img.parent();
//   if (parent && typeof parent != 'undefined') {
//     img.removeAttr('style').css({
//       height: '100%',
//       width: 'auto'
//     });
//     var img_w = img.width();
//     var img_h = img.height();
//     var parent_w = parent.width();
//     var parent_h = parent.height();
//     if (img_w < parent_w) {
//       img.css({
//         width: '100%',
//         height: 'auto'
//       });
//       var img_t = (parent_h - img_h) / 2;
//       img.css({
//         top: '-' + img_t + 'px'
//       });
//     } else {
//       var img_l = (parent_w - img_w) / 2;
//       img.css({
//         left: img_l + 'px'
//       });
//     }
//   }
// }
