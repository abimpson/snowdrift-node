<?php
	define('TURN_OFF_REDIRECT', 'yes');
	//require_once "../../bootstrap.php";
	
	//CONFIG
	$fileElementName = 'userfile';//'profile_upload';
	$max_file_size = 3072000; //locale_setting('file_uploaded_maxsize');  //3072000
	$uploadPath = preg_replace("/\/\//","/",$_SERVER['DOCUMENT_ROOT'].'/examples/uploads/'); //.locale_setting('app_relative_folder').'/uploaded_images/');
	$file_allowed_extensions = explode(",",'png,jpg,jpeg'); //locale_setting('allowed_file_uploaded_extensions'));
	//FILE NAME
	if(isset($_REQUEST["name"])){
		$t1 = explode('.',$_REQUEST["name"]);
		$temp_file_name = $t1[0];
	}else{
		$temp_file_name = get_rand_id(10);
	}
	$file_extension = substr($_FILES[$fileElementName]['name'], strrpos($_FILES[$fileElementName]['name'], '.') + 1);
	$file_name = $temp_file_name.'.'.$file_extension;
	$file_name_small = $temp_file_name.'_small';
	$file_name_med = $temp_file_name.'_med';
	$file_name_large = $temp_file_name.'_large';
	$file_name_scaled = $temp_file_name.'_scaled';
	//
	$file_data['FILES'] = $_FILES;
	
	if(!empty($_FILES[$fileElementName]['error'])) {
		switch($_FILES[$fileElementName]['error']) {

			case '1':
				$error[] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error[] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error[] = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error[] = 'No file was uploaded.';
				break;
			case '6':
				$error[] = 'Missing a temporary folder';
				break;
			case '7':
				$error[] = 'Failed to write file to disk';
				break;
			case '8':
				$error[] = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error[] = 'No error code avaiable';
		}
	} elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {
		$error[] = 'No file was uploaded..';
	} else {
		
		if(in_array(strtolower($file_extension),$file_allowed_extensions) == false) {
			$error[] = 'File extension not allowed';
		} else {
			if($_FILES[$fileElementName]['size'] >= $max_file_size){
				$error[] = 'File size is too big';
			} else {
				if(!move_uploaded_file($_FILES[$fileElementName]['tmp_name'], $uploadPath.$file_name)) {
					$error[] = 'Error moving the file';
				} else {
					
					$file_data['a1'] = $_FILES[$fileElementName]['tmp_name'];
					$file_data['a2'] = $file_med;
					$file_data['original_file_name'] = $_FILES[$fileElementName]['name'];
					$file_data['actual_file_name'] = $file_name;
					$file_data['relative_file_url'] = preg_replace("/\/\//","/",$uploadPath.$file_name); //locale_setting('app_relative_folder').'/'.locale_setting('file_uploaded_relativefolder').$file_name);
					$cdn = '';//locale_setting('app_cdn_url');
					if(!empty($cdn)) {
						$file_data['full_file_url'] = preg_replace("/\/\//","/",$uploadPath.$file_name); //$cdn.locale_setting('app_relative_folder').'/'.locale_setting('file_uploaded_relativefolder').$file_name);
					} else {
						$file_data['full_file_url'] = ''; //locale_setting('facebook_full_www_path').locale_setting('file_uploaded_relativefolder').$file_name;
					}
					
					$file_data['file_size'] = @filesize($uploadPath.$file_name);
					
					//GENERATE DIFFERENT VERSIONS
					/*$filePath = $uploadPath.$file_name_small.'.png';
					require_once('../../lib/profile_small.php');
					$file_data['icon_small'] = $file_name_small.'.png';
					
					$filePath = $uploadPath.$file_name_med.'.png';
					require_once('../../lib/profile_med.php');
					$file_data['icon_med'] = $file_name_med.'.png';
					
					$filePath = $uploadPath.$file_name_large.'.png';
					require_once('../../lib/profile_large.php');
					$file_data['icon_large'] = $file_name_large.'.png';
					
					$filePath = $uploadPath.$file_name_scaled.'.jpg';
					require_once('../../lib/profile_scale.php');
					$file_data['icon_scaled'] = $file_name_scaled.'.jpg';*/
					
					//for security reason, we force to remove all uploaded file
					//@unlink($_FILES[$fileElementName]);		
				}
				
				if(!file_exists($uploadPath.$file_name)) {
					$error[] = 'Error finding the file after moving it to public';
				}
			}
		}
	}
	
	//Check for any errors
	if(count($error) > 0) {
		$success = false;
	} else {
		$success = true;
	}
	
	
	//Create array to store the files
	$returnData = array(
		'request' => $_REQUEST,
		'success' => $success,
		'error' => $error,
		'response' => $file_data
	);
	
	//Echo the response array in JSON format
	echo json_encode($returnData);
	
?>