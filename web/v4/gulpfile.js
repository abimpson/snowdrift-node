// ======================================================================
// REQUIRES
// ======================================================================

var argv = require('yargs').argv;
var autoprefixer = require('gulp-autoprefixer');
var bourbon = require('node-bourbon');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var stripDebug = require('gulp-strip-debug');
var terser = require('gulp-terser');

// ======================================================================
// CONFIG
// ======================================================================

var options = {
  // general
  watch_interval: 100,
  // browsersync
  bs: {
    run: argv.bs ? true : false,
    proxy: 'snowdrift.test',
    root: './',
    opts: {
      open: argv.bs ? true : false,
      https: false,
      port: 3000,
      reloadDelay: 200,
      ghostMode: false
    }
  },
  // html
  html: {
    src: [''],
    exts: ['.html', '.php']
  },
  // css
  css: {
    minify: true,
    sourcemaps: argv.dev ? true : false,
    src: ['assets/css/src/'],
    dist: ['assets/css/dist/']
  },
  // js
  js: {
    minify: true,
    strip_debug: argv.dev ? false : true,
    sourcemaps: argv.dev ? true : false,
    src: ['assets/js/src/'],
    dist: ['assets/js/dist/']
  },
  // js - vendor
  vendor: {
    minify: true,
    strip_debug: true,
    sourcemaps: false,
    src: ['assets/js/vendor/'],
    dist: ['assets/js/dist/']
  }
};

// ======================================================================
// ERROR HANDLING
// ======================================================================

function error_handler(err) {
  console.log(err.toString());
  this.emit('end');
}

// ======================================================================
// BROWSER SYNC
// ======================================================================

gulp.task('browser-sync', function() {
  if (options.bs.run) {
    var bsopts = options.bs.opts;
    if (!!options.bs.proxy) {
      bsopts.proxy = options.bs.proxy;
    } else {
      bsopts.server = {
        baseDir: options.bs.root
      };
    }
    browserSync.init(bsopts);
  }
});

// ======================================================================
// SCSS/SASS - CONCATENATE AND MINIFY
// ======================================================================

for (var i in options.css.src) {
  (function(i) {
    gulp.task('scss_' + i, function() {
      return (
        gulp
          // source files
          .src([options.css.src[i] + '/*.scss'])
          // plumber - error handler
          .pipe(
            plumber({
              errorHandler: error_handler
            })
          )
          // sourcemaps init
          .pipe(gulpif(options.css.sourcemaps, sourcemaps.init()))
          // sass
          .pipe(
            sass({
              outputStyle: options.css.minify ? 'compressed' : 'expanded',
              includePaths: bourbon.includePaths
            })
          )
          // auto prefixer
          .pipe(autoprefixer())
          // sourcemaps output
          .pipe(
            gulpif(
              options.css.sourcemaps,
              sourcemaps.write('', {
                includeContent: false,
                sourceRoot: '../../' + options.css.src[i]
              })
            )
          )
          // save
          .pipe(gulp.dest(options.css.dist[i]))
          // stream
          .pipe(gulpif(options.bs.run, browserSync.stream()))
      );
    });
  })(i);
}

// ======================================================================
// JS - CONCATENATE AND MINIFY
// ======================================================================

for (var i in options.js.src) {
  (function(i) {
    gulp.task('js_' + i, function() {
      return (
        gulp
          // source files
          .src([options.js.src[i] + '/*.js'])
          // plumber - error handler
          .pipe(
            plumber({
              errorHandler: error_handler
            })
          )
          // sourcemaps init
          .pipe(gulpif(options.js.sourcemaps, sourcemaps.init()))
          // strip debug
          .pipe(gulpif(options.js.strip_debug, stripDebug()))
          // minify
          .pipe(gulpif(options.js.minify, terser()))
          // concatenate
          .pipe(concat('scripts.min.js'))
          // sourcemaps output
          .pipe(
            gulpif(
              options.js.sourcemaps,
              sourcemaps.write('', {
                includeContent: false,
                sourceRoot: '../../' + options.js.src[i]
              })
            )
          )
          // save
          .pipe(gulp.dest(options.js.dist[i]))
          // stream
          .pipe(gulpif(options.bs.run, browserSync.stream()))
      );
    });
  })(i);
}

// ======================================================================
// JS VENDOR - CONCATENATE AND MINIFY
// ======================================================================

for (var i in options.vendor.src) {
  (function(i) {
    gulp.task('vendor_' + i, function() {
      // return (
      //   gulp
      //     // source files
      //     .src([
      //       // options.vendor.src + '/imagesloaded.pkgd.min.js',
      //       // options.vendor.src + '/jquery.calendario.min.js',
      //       // options.vendor.src + '/jquery.easing.1.3.min.js',
      //       // options.vendor.src + '/jquery.selectric.min.js',
      //       // options.vendor.src + '/jquery.touchSwipe.1.6.min.js',
      //       // options.vendor.src + '/masonry.pkgd.min.js',
      //       // options.vendor.src + '/prrple.slider.js',
      //       // options.vendor.src + '/socket.io.js',
      //       // options.vendor.src + '/webfontloader.js'
      //     ])
      //     // plumber - error handler
      //     .pipe(
      //       plumber({
      //         errorHandler: error_handler
      //       })
      //     )
      //     // sourcemaps init
      //     .pipe(gulpif(options.vendor.sourcemaps, sourcemaps.init()))
      //     // strip debug
      //     .pipe(gulpif(options.vendor.strip_debug, stripDebug()))
      //     // minify
      //     .pipe(gulpif(options.vendor.minify, terser()))
      //     // concatenate
      //     .pipe(concat('plugins.min.js'))
      //     // sourcemaps output
      //     .pipe(
      //       gulpif(
      //         options.vendor.sourcemaps,
      //         sourcemaps.write('', {
      //           includeContent: false,
      //           sourceRoot: '../../' + options.vendor.src[i]
      //         })
      //       )
      //     )
      //     // save
      //     .pipe(gulp.dest(options.vendor.dist[i]))
      //     // stream
      //     .pipe(gulpif(options.bs.run, browserSync.stream()))
      // );
    });
  })(i);
}

// ======================================================================
// WATCH FOR CHANGES
// ======================================================================

gulp.task('watch', function() {
  // html
  for (var i in options.html.src) {
    for (var j in options.html.exts) {
      gulp.watch(options.html.src[i] + '**/*' + options.html.exts[j], {interval: options.watch_interval}).on('change', browserSync.reload);
    }
  }
  // scss
  for (var i in options.css.src) {
    gulp.watch(options.css.src[i] + '**/*.scss', {interval: options.watch_interval}, gulp.parallel(['scss_' + i]));
  }
  // js
  for (var i in options.js.src) {
    gulp.watch(options.js.src[i] + '**/*.js', {interval: options.watch_interval}, gulp.parallel(['js_' + i]));
  }
  // js - vendor
  for (var i in options.vendor.src) {
    gulp.watch(options.vendor.src[i] + '**/*.js', {interval: options.watch_interval}, gulp.parallel(['vendor_' + i]));
  }
});

// ======================================================================
// RUN TASKS
// ======================================================================

var tasks = ['browser-sync', 'watch'];
for (var i in options.css.src) {
  tasks.push('scss_' + i);
}
for (var i in options.js.src) {
  tasks.push('js_' + i);
}
for (var i in options.vendor.src) {
  tasks.push('vendor_' + i);
}
gulp.task('default', gulp.parallel(tasks));
