<?php
	
	
	
	/***** PARSEDOWN *****/
	require_once('web/v4/assets/php/parsedown.php');
	
	
	
?><!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS Framework v4</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="/examples/assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- JQUERY -->
	<script src="examples/assets/js-plugins/jquery-1.12.0.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="examples/assets/js-custom/_init2.js"></script>
	<script src="examples/assets/js-custom/b.windowsize.js"></script>
	
	
	
</head>
<body>
	<div id="fb-root"></div>
	
	
	
	<!-- HEADER -->
	<div id="header">
		<h1 class="title">SnowDrift JS Framework v4</h1>
		<ul id="header_nav">
			<li><a href="/">Home</a></li>
			<li><a href="/examples/">Examples</a></li>
		</ul>
	</div>
	<a class="github" href="https://github.com/Prrple/SnowDrift" target="_blank">Fork Me On Github</a>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<?php
				$md = file_get_contents('README.md');
				$md = explode('---',$md);
				echo Parsedown::instance()
					->setBreaksEnabled(true)
					->text($md[1]);
			?>
		</div>
	
	</div>
	
	
	
	<!-- FOOTER -->
	<div id="footer" class="absolute">
		<div id="footer_inner">
			<ul id="footer_nav">
				
			</ul>
			<div class="clearme"></div>
			<div id="footer_keyline"></div>
			<div id="footer_notes">
				<div id="footer_notes_left">
					&copy; 2011 - <?php echo date('Y'); ?> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="http://www.prrple.com" target="_blank">Prrple</a>
				</div>
				<div class="clearme"></div>
			</div>
		</div>
	</div>
	
	
	
</body>
</html>