# SnowDrift

[By Prrple](http://www.prrple.com)

[Visit online guide and examples](http://snowdrift.prrple.com)

---



## Getting Started

The assets folder contains the base structure for css, javascript and other static assets, such as images, fonts, audio and videos.

There is a *package.json* and a *gulpfile* in the project root, which when installed and run will automatically watch and compile JS and CSS. Browsersync is also included for local development. Gulpfile.js includes various config options which can be configured as you want them.

There are then several *start* folders containing skeleton HTML / PHP to get started with a website, web app or wordpress theme. Just copy the desired set of files into the source of your project in order to get started.



## JS Source Overview

### Config & Init

_config.js - _app config_
- $config
    - errors
    - preload
- $placeholders

_init.js - _app initialisation_


### App General Code

a.app.js - _general app functions_
- $app

a.auth.js - _authorisation checks, run after a user logs in_
- $auth

a.checks.js - _initialisation checks, run on start_
- $checks


### Binders

b.docsize.js - _handles functions that must run when document resizes_
- $docsize

b.inputs.js - _monitor focus, blur and change events on form inputs_
- $inputs

b.nav.js - _binds all events - clicks etc_
- $nav

b.scroll.js - _handles scroll events_
- $scroll

b.windowsize.js - _handles functions that must run when window resizes or orientation changes_
- $windowsize


### Classes

c.plupload.js - _integrates plupload plugin for creation of rich ajax image uploading_
- $plupload

c.socket.js - _allows creation of socket.io connections_
- $socket


### Helpers

h.ajax.js - _standard functions for AJAX calls_
- $ajax

h.ajax_errors.js - _handles ajax errors_
- $errors

h.analytics.js - _analytics tracking, such as google or facebook_
- $analytics

h.filter.js - _swear filter_
- $filter

h.hijax.js - _hijack url links with an AJAX call_
- $hijax

h.motion.js - _detect device motion - e.g. using gyroscope and accellerometer_
- $motion

h.preload.js - _preload images etc_
- $preload

h.pushstate.js - _check for HTML5 pushstate - include at top of page (doesn't require jquery)_
- $pushstate

h.videos.js - _HTML5 video manager_
- $videos


### Models

m.all.js - _general collection of data models_
- $m


### Standard Functions

s.all.js - _standard functions_
s.fastclick - _standard fastclick function_


### Views

v.pages.js - _handles page switching and functions to run when loading each page_
- $pages
- $page

v.popups.js - _handles popups and functions to run when loading each popup_
- $popups
- $popup

v.social.js - _social sharing functions_
- $social

v.toggles.js - _handles toggling of actions/data e.g. showing and hiding elements_
- $toggles


### Watchers

w.all.js - _watch for object changes_
- $watchers


### External API Wrappers

x.bitly.js - _bit.ly api_
- $bitly

x.facebook.4.js - _facebook open graph api_
- $facebook

x.gmaps.js - _google maps api_
- $gmaps

x.youtube.js - _youtube api_
- $youtube





